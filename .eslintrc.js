module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2019,
    allowImportExportEverywhere: true,
    ecmaFeatures: {
      'jsx': true
    }
  },
  env: {
    es6: true,
    node: true,
    jest: true
  },
  extends: [
    'standard',
    'standard-react',
    'plugin:promise/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:jsx-a11y/recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'prettier',
    'prettier/react',
    'prettier/standard',
  ],
  plugins: [
    'jsx-a11y',
    "react-hooks"
  ],
  rules: {
    'prettier/prettier': [
      'error',
      {
        'trailingComma': 'es5',
        'singleQuote': true,
      }
    ],
    'linebreak-style': 'off',
    'import/no-extraneous-dependencies': 'off',
    'react/forbid-prop-types': [
      'off'
    ],
    // it's OK to use setState in componentDidUpdate as long as it's inside a condition
    "react/no-did-update-set-state": 'off',
    // this rule can be disabled when using Next.js
    'react/react-in-jsx-scope': 'off',
    // prevent ESLint complaining about <a> not having href attribute
    "jsx-a11y/anchor-is-valid": ["error", {
      "components": ["Link"],
      "specialLink": ["hrefLeft", "hrefRight"],
      "aspects": ["invalidHref", "preferButton"]
    }],
    "jsx-a11y/img-redundant-alt": "off",
    // Hooks: recommended settings
    "react-hooks/rules-of-hooks": "error", // Checks rules of Hooks
    "react-hooks/exhaustive-deps": "warn" // Warns when dependencies are specified incorrectly and suggests a fix
  }
}
