const path = require('path');
const { argv } = require('yargs');
const { spawn } = require('child_process');
const changeCase = require('change-case');

const Maker = require('../src');

const spawner = (name, args) => {
  return new Promise((resolve, reject) => {
    const proc = spawn(name, args);
    proc.stdout.on('data', data => console.log(data.toString()));
    proc.stderr.on('data', data => console.error(data.toString()));
    proc.on('exit', code => {
      resolve(`child process exited with code ${code.toString()}`);
    });
    proc.on('error', e => reject(e));
  });
};

const init = async () => {
  const defaultAction = 'configure';
  const defaultEnv = 'dev';
  const defaultModules = ['envoy', 'http', 'mongo', 'services'].join(',');

  const supportedActions = [
    'build',
    'stop',
    'run',
    'configure',
    'clean',
    'push',
  ];
  const supportedEnvs = ['dev', 'stag', 'prod'];
  const supportedModules = ['envoy', 'http', 'mongo', 'services'];

  const ACTION = argv.ACTION || argv._[0] || defaultAction;
  const ENV = argv.ENV || argv._[1] || defaultEnv;
  const MODULES = (argv.MODULES || argv._[2] || defaultModules).split(',');

  if (!supportedEnvs.includes(ENV))
    throw new Error(`Environment ${ENV} not supported.`);

  if (!supportedActions.includes(ACTION))
    throw new Error(`Action ${ACTION} not supported.`);

  if (MODULES.some(m => supportedModules.indexOf(m) === -1)) {
    throw new Error(`Some modules ${MODULES.join(',')} are not supported.`);
  }

  const ROOT_DIR = path.join(__dirname, '../');

  const config = require(path.join(ROOT_DIR, 'config', `${ENV}.js`));
  const { version } = require(path.join(ROOT_DIR, 'lerna.json'));

  const loaders = MODULES.map(changeCase.paramCase).map(
    moduleName => outputDir =>
      require(`@isa-music/builders-${moduleName}`)[ENV](
        outputDir,
        ROOT_DIR,
        config,
        version
      )
  );

  const BUILD_DIR = path.join(ROOT_DIR, 'build');

  const maker = new Maker(path.join(BUILD_DIR, ENV), spawner);

  await maker[ACTION](loaders);
};

init().catch(console.error);
