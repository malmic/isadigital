const path = require('path');
const fs = require('fs-extra');
const yaml = require('js-yaml');

const { transformListener, transformCluster } = require('./transform');
const { translateCluster, translateListeners } = require('./translate');

const generateSocketAddress = (config, env, platform) => {
  return (m, key) => {
    const port = config[m].ports[key];

    if (env === 'dev') {
      // development on darwin relies on this workaround to access
      // other running services on the host that are not through docker
      // since in development something else (pm2) could run the service
      if (platform === 'darwin') return `host.docker.internal:${port}`;

      // development on unix should not need the network mode host
      // optimization, so we can live with NAT in dev environment
      if (platform === 'linux') return `0.0.0.0:${port}`;
    }

    if (env === 'stag' || env === 'prod') {
      // used to test the production environment on macos
      if (platform === 'darwin') return `isa-${m}-${key}:${port}`;

      // this is the real production environment which
      // corresponds to the network mode 'host' setting
      if (platform === 'linux') return `0.0.0.0:${port}`;
    }

    throw new Error(
      `Your {env, platform} combination of '{${env}, ${platform}}' is not supported!`
    );
  };
};

const moduleToProtocol = {
  resource: 'grpc',
  http: 'http',
};

const getClusterName = (m, key) => `${m}_${key}_service`;

const dev = async (buildDir, coreDir, config, version) => {
  const platform = process.platform;

  const socketAddress = generateSocketAddress(config, 'dev', platform);
  const createCluster = translateCluster(
    socketAddress,
    getClusterName,
    moduleToProtocol
  );

  const listeners = translateListeners(
    config.envoy.hosts,
    getClusterName,
    moduleToProtocol,
    config.envoy.certificates
  );

  const ports = listeners.reduce(
    (acc, { port }) => [...acc, `${port}:${port}`],
    ['9901:9901']
  );

  const dataDir = 'volume';

  const transformedListeners = listeners.map(listener =>
    transformListener(listener, dataDir)
  );

  const transformedClusters = config.envoy.hosts
    .map(createCluster)
    .filter((cluster, idx, self) => {
      return (
        self.findIndex(s => {
          return s.clusterName === cluster.clusterName;
        }) === idx
      );
    })
    .map(transformCluster);

  const envoyDefinition = {
    static_resources: {
      listeners: transformedListeners,
      clusters: transformedClusters,
    },
  };

  const folderName = 'isa-envoy';
  const outputFile = 'isa.yaml';

  const volumeDir = path.join(config.builders.coreDir || coreDir, 'volumes');

  await fs.outputFile(
    path.join(volumeDir, 'envoy', outputFile),
    yaml.dump(envoyDefinition, { lineWidth: 5000 })
  );

  const outputPath = path.join(buildDir, folderName);

  await fs.outputFile(
    path.join(outputPath, 'Dockerfile'),
    `FROM envoyproxy/envoy:v1.14.1
CMD /usr/local/bin/envoy -c /${dataDir}/${outputFile}
`
  );

  const docker = {
    [folderName]: {
      image: `isamusic/envoy:v${version}`,
      container_name: folderName,
      build: { context: outputPath },
      ports,
      volumes: [`${volumeDir}/envoy:/${dataDir}`],
    },
  };

  return { docker, pm2: [] };
};

const stag = async (buildDir, coreDir, config, version) => {
  const platform = process.platform;

  const socketAddress = generateSocketAddress(config, 'stag', platform);
  const createCluster = translateCluster(
    socketAddress,
    getClusterName,
    moduleToProtocol
  );

  const listeners = translateListeners(
    config.envoy.hosts,
    getClusterName,
    moduleToProtocol,
    config.envoy.certificates
  );

  const ports = listeners.reduce(
    (acc, { port }) => [...acc, `${port}:${port}`],
    ['9901:9901']
  );

  const dataDir = 'volume';

  const transformedListeners = listeners.map(listener =>
    transformListener(listener, dataDir)
  );

  const transformedClusters = config.envoy.hosts
    .map(createCluster)
    .filter((cluster, idx, self) => {
      return (
        self.findIndex(s => {
          return s.clusterName === cluster.clusterName;
        }) === idx
      );
    })
    .map(transformCluster);

  const envoyDefinition = {
    admin: {
      access_log_path: `/tmp/admin_access.log`,
      address: {
        socket_address: { address: '0.0.0.0', port_value: 9901 },
      },
    },
    static_resources: {
      listeners: transformedListeners,
      clusters: transformedClusters,
    },
  };

  const folderName = 'isa-envoy';
  const outputFile = 'isa.yaml';

  const volumeDir = path.join(config.builders.coreDir || coreDir, 'volumes');

  await fs.outputFile(
    path.join(volumeDir, 'envoy', outputFile),
    yaml.dump(envoyDefinition, { lineWidth: 5000 })
  );

  const outputPath = path.join(buildDir, folderName);

  await fs.outputFile(
    path.join(outputPath, 'Dockerfile'),
    `FROM envoyproxy/envoy:v1.14.1
CMD /usr/local/bin/envoy -c /${dataDir}/${outputFile}
`
  );

  const docker = {
    [folderName]: {
      image: `isamusic/envoy:v${version}`,
      container_name: folderName,
      build: { context: outputPath },
      ports,
      volumes: [`${volumeDir}/envoy:/${dataDir}`],
    },
  };

  return { docker, pm2: [] };
};

const prod = async (buildDir, coreDir, config, version) => {
  const socketAddress = generateSocketAddress(config, 'prod', 'linux');
  const createCluster = translateCluster(
    socketAddress,
    getClusterName,
    moduleToProtocol
  );

  const listeners = translateListeners(
    config.envoy.hosts,
    getClusterName,
    moduleToProtocol,
    config.envoy.certificates
  );

  const dataDir = 'volume';

  const transformedListeners = listeners.map(listener =>
    transformListener(listener, dataDir)
  );

  const transformedClusters = config.envoy.hosts
    .map(createCluster)
    .filter((cluster, idx, self) => {
      return self.findIndex(s => s.clusterName === cluster.clusterName) === idx;
    })
    .map(transformCluster);

  const envoyDefinition = {
    static_resources: {
      listeners: transformedListeners,
      clusters: transformedClusters,
    },
  };

  const folderName = 'isa-envoy';
  const outputFile = 'isa.yaml';

  const volumeDir = path.join(config.builders.coreDir || coreDir, 'volumes');

  await fs.outputFile(
    path.join(volumeDir, 'envoy', outputFile),
    yaml.dump(envoyDefinition, { lineWidth: 5000 })
  );

  const docker = {
    [folderName]: {
      image: `isamusic/envoy:v${version}`,
      container_name: folderName,
      network_mode: 'host',
      volumes: [`${volumeDir}/envoy:/${dataDir}`],
    },
  };

  return { docker, pm2: [] };
};

module.exports = { dev, stag, prod };
