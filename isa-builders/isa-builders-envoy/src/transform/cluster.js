module.exports = ({ clusterName, protocol, socketAddress }) => {
  const url = new URL(`http://${socketAddress}`);
  const { port, hostname } = url;

  const type = protocol === 'grpc' ? 'logical_dns' : 'strict_dns';

  const cluster = {
    name: clusterName,
    connect_timeout: '0.25s',
    type,
    lb_policy: 'round_robin',
    load_assignment: {
      cluster_name: clusterName,
      endpoints: [
        {
          lb_endpoints: [
            {
              endpoint: {
                address: {
                  socket_address: {
                    address: hostname,
                    port_value: parseInt(port, 10),
                  },
                },
              },
            },
          ],
        },
      ],
    },
  };

  return cluster;
};
