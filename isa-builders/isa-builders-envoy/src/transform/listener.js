const virtualHost = require('./virtualHost');

module.exports = (listener, dataDir) => {
  const result = {
    name: `listener_${listener.index}`,
    address: {
      socket_address: { address: '0.0.0.0', port_value: listener.port },
    },
    filter_chains: [
      {
        filters: [
          {
            name: 'envoy.http_connection_manager',
            typed_config: {
              '@type':
                'type.googleapis.com/envoy.config.filter.network.http_connection_manager.v2.HttpConnectionManager',
              codec_type: 'auto',
              stat_prefix: 'ingress_http',
              upgrade_configs: [{ upgrade_type: 'websocket' }],
              use_remote_address: true,
              server_name: 'isa',
              route_config: {
                name: 'local_route',
                virtual_hosts: listener.virtualHosts.map(virtualHost),
              },
              http_filters: [
                { name: 'envoy.filters.http.cors' },
                /* {
                  name: 'envoy.filters.http.ext_authz',
                  typed_config: {
                    '@type':
                      'type.googleapis.com/envoy.config.filter.http.ext_authz.v2.ExtAuthz',
                    http_service: {
                      server_uri: {
                        uri: listener.authorization.socketAddress,
                        cluster: listener.authorization.clusterName,
                        timeout: '0.5s',
                      },
                      path_prefix: '/token/verify',
                      authorization_request: {
                        allowed_headers: {
                          patterns: [{ exact: 'cookie' }],
                        },
                      },
                      authorization_response: {
                        allowed_upstream_headers: {
                          patterns: [{ exact: 'x-auth' }],
                        },
                      },
                    },
                    failure_mode_allow: false,
                  },
                }, 
                { name: 'envoy.filters.http.grpc_web' }, */
                { name: 'envoy.filters.http.router' },
              ],
            },
          },
        ],
      },
    ],
  };

  if (listener.certificates.length > 0) {
    result.filter_chains[0].tls_context = {
      common_tls_context: {
        tls_certificates: listener.certificates.map(c => {
          return {
            certificate_chain: { filename: `/${dataDir}/${c.chain}` },
            private_key: { filename: `/${dataDir}/${c.privateKey}` },
          };
        }),
      },
    };
  }

  return result;
};
