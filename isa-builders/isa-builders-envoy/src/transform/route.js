module.exports = ({ path, cluster, httpsRedirect }) => {
  const route = { match: { prefix: path } };

  if (httpsRedirect) {
    route['redirect'] = {
      path_redirect: '/',
      https_redirect: true,
    };
  } else {
    route['route'] = { cluster };
  }

  return route;
};
