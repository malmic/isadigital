const route = require('./route');

module.exports = (virtualHost, idx) => {
  return {
    name: `local_service_${idx}`,
    domains: virtualHost.domains ? virtualHost.domains : ['*'],
    cors: {
      allow_origin_string_match: { suffix: virtualHost.allowSuffix },
      allow_credentials: true,
      allow_headers: virtualHost.allowHeaders.join(','),
      max_age: '1728000',
      expose_headers: virtualHost.exposeHeaders.join(','),
    },
    routes: virtualHost.routes.map(route),
  };
};
