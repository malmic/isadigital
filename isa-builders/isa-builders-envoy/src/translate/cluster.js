module.exports = (socketAddress, getClusterName, moduleToProtocol) => {
  return host => ({
    socketAddress: socketAddress(host.module, host.key),
    clusterName: getClusterName(host.module, host.key),
    protocol: moduleToProtocol[host.module],
  });
};
