const toVirtualHost = require('./virtualHost');
const toRoute = require('./route');

module.exports = (hosts, getCluster, moduleToProtocol) => {
  return hosts.reduce((acc, cur) => {
    const protocol = moduleToProtocol[cur.module];
    const path = protocol === 'grpc' ? `/${cur.key}` : '/';

    const route = toRoute(
      getCluster(cur.module, cur.key),
      path,
      cur.httpsRedirect
    );

    const virtualHost = toVirtualHost(cur.allowSuffix, cur.domains, route);

    const listenerIndex = acc.findIndex(o => o.port === cur.port);
    if (listenerIndex === -1) {
      acc.push({
        port: cur.port,
        index: acc.length,
        virtualHosts: [virtualHost],
        certificates: cur.certificates || [],
      });
    } else {
      const vHostIndex = acc[listenerIndex].virtualHosts.findIndex(v => {
        return v.domains.every(v => cur.domains.includes(v));
      });

      if (vHostIndex === -1) {
        acc[listenerIndex].virtualHosts.push(virtualHost);
      } else {
        acc[listenerIndex].virtualHosts[vHostIndex].routes.push(route);
      }

      acc[listenerIndex].certificates = acc[listenerIndex].certificates.concat(
        cur.certificates || []
      );
    }

    return acc;
  }, []);
};
