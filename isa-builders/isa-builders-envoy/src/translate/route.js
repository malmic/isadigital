module.exports = (cluster, path, httpsRedirect) => {
  return {
    path,
    cluster,
    httpsRedirect,
  };
};
