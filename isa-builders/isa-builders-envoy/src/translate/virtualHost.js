const allowHeaders = [
  'keep-alive',
  'user-agent',
  'cache-control',
  'content-type',
  'content-transfer-encoding',
  'x-accept-content-transfer-encoding',
  'x-accept-response-streaming',
  'x-user-agent',
  'x-grpc-web',
  'grpc-timeout',
];

const exposeHeaders = ['grpc-status', 'grpc-message'];

module.exports = (allowSuffix, domains, route) => {
  return {
    allowSuffix,
    domains,
    exposeHeaders,
    allowHeaders,
    routes: [route],
  };
};
