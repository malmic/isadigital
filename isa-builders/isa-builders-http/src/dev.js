const fs = require('fs-extra');
const changeCase = require('change-case');
const path = require('path');

const stringify = require('@isa-music/libs-stringify');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(config.services.ports, () => '0.0.0.0');

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const zeusPath = path.join(coreDir, 'isa-zeus', 'isa-zeus-http');

  const pm2 = Object.keys(config.http.ports).map(key => {
    const folderName = `isa-http-${changeCase.paramCase(key)}`;
    const packagePath = path.join(coreDir, 'isa-http', folderName);

    const env = { ...config.http };
    delete env.ports;

    if (config.http.CLIENTS[key]) env.CLIENT_ID = config.http.CLIENTS[key];
    if (config.http.APPS.includes(key)) env.APP_HANDLE = key.toUpperCase();

    const outputPath = path.join(buildDir, folderName);

    fs.outputFileSync(
      path.join(outputPath, 'index.js'),
      `const http = require('${zeusPath}');

      http(${JSON.stringify({
        path: packagePath,
        folderName,
      })}, process.env, () => { process.send('ready'); }).catch(console.error);
`
    );

    return {
      name: folderName,
      script: 'index.js',
      cwd: outputPath,
      wait_ready: true,
      watch: [path.join(coreDir, 'isa-http', folderName, 'src', 'routers')],
      ignore_watch: ['node_modules'],
      node_args: '--trace-warnings',
      env: stringify({
        ...env,
        NODE_ENV: 'development',
        APPS: config.http.APPS.map(handle => ({
          handle: handle.toUpperCase(),
        })),
        DIR: packagePath,
        SOCKET_ADDRESS: `0.0.0.0:${config.http.ports[key]}`,
        SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
      }),
    };
  });

  return { docker: {}, pm2 };
};
