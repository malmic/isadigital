const changeCase = require('change-case');

const stringify = require('@isa-music/libs-stringify');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(
    config.services.ports,
    i => `isa-services-cluster-${i}`
  );

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const environment = { ...config.http };
  delete environment.ports;

  const packagePath = '/package/package';

  const docker = Object.keys(config.http.ports).reduce((acc, key) => {
    const protoName = changeCase.paramCase(key);
    const suffix = `http-${protoName}`;
    const folderName = `isa-${suffix}`;

    if (config.http.CLIENTS[key])
      environment.CLIENT_ID = config.http.CLIENTS[key];

    if (config.http.APPS.includes(key))
      environment.APP_HANDLE = key.toUpperCase();

    return {
      ...acc,
      [folderName]: {
        image: `isamusic/${suffix}:v${version}`,
        container_name: folderName,
        restart: 'on-failure',
        environment: stringify({
          ...environment,
          NODE_ENV: 'production',
          SOCKET_ADDRESS: `0.0.0.0:${config.http.ports[key]}`,
          SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
          DIR: packagePath,
          APPS: config.http.APPS.map(handle => ({
            handle: handle.toUpperCase(),
          })),
        }),
      },
    };
  }, {});

  return { docker, pm2: [] };
};
