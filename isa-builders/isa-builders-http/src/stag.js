const fs = require('fs-extra');
const path = require('path');
const changeCase = require('change-case');

const stringify = require('@isa-music/libs-stringify');
const { generateDockerfile } = require('@isa-music/libs-docker');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(
    config.services.ports,
    i => `isa-services-cluster-${i}`
  );

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const library = ['@isa-music/zeus-http'];

  const docker = Object.keys(config.http.ports).reduce((acc, key) => {
    const paramName = changeCase.paramCase(key);
    const folderName = `isa-http-${paramName}`;
    const packagePath = '/package/package';

    const appBuildPath = path.join(buildDir, folderName);
    fs.ensureDirSync(appBuildPath);

    const packageBuildPath = path.join(appBuildPath, 'package');
    fs.ensureDirSync(packageBuildPath);

    const dependencies = [...library].reduce(
      (acc, cur) => ({ ...acc, [cur]: version }),
      {}
    );

    fs.outputFileSync(
      path.join(appBuildPath, 'Dockerfile'),
      generateDockerfile(
        [
          'ARG NPM_TOKEN',
          'COPY /package /package',
          'WORKDIR /package',
          `RUN npm pack @isa-music/http-${paramName}@${version}`,
          `RUN tar -xvf isa-music-http-${paramName}-${version}.tgz`,
        ],
        [
          'RUN rm -f /package/.npmrc',
          'WORKDIR /package/package',
          'RUN npm run-script build',
          'WORKDIR /package',
          'CMD [ "node", "bin/run.js" ]',
        ]
      )
    );

    fs.outputFileSync(
      path.join(packageBuildPath, '.npmrc'),
      `//registry.npmjs.org/:_authToken=\${NPM_TOKEN}`
    );

    fs.outputFileSync(
      path.join(packageBuildPath, 'package.json'),
      JSON.stringify(
        {
          name: folderName,
          version: '1.0.0',
          private: true,
          license: 'UNLICENSED',
          dependencies,
          workspaces: ['package'],
        },
        null,
        2
      )
    );

    fs.outputFileSync(
      path.join(packageBuildPath, 'bin', 'run.js'),
      `const http = require('@isa-music/zeus-http');

http(${JSON.stringify({
        path: `@isa-music/http-${paramName}`,
        folderName,
      })}, process.env).catch(console.error);
`
    );

    const environment = { ...config.http };
    delete environment.ports;

    if (config.http.CLIENTS[key])
      environment.CLIENT_ID = config.http.CLIENTS[key];

    if (config.http.APPS.includes(key))
      environment.APP_HANDLE = key.toUpperCase();

    return {
      ...acc,
      [folderName]: {
        image: `isamusic/http-${paramName}:v${version}`,
        container_name: folderName,
        restart: 'on-failure',
        environment: stringify({
          ...environment,
          NODE_ENV: 'production',
          SOCKET_ADDRESS: `0.0.0.0:${config.http.ports[key]}`,
          SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
          DIR: packagePath,
          APPS: Object.keys(config.http.ports).map(handle => ({
            handle: handle.toUpperCase(),
          })),
        }),
        build: {
          context: appBuildPath,
          args: { NPM_TOKEN: config.builders.NPM_TOKEN },
        },
      },
    };
  }, {});

  return { docker, pm2: [] };
};
