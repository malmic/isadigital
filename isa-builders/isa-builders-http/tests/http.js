const test = require('ava');
const path = require('path');
const fs = require('fs-extra');
const os = require('os');

const { sep } = path;

const { dev, stag, prod } = require('../src');

test.before('setup config', t => {
  const config = {
    builders: { NPM_TOKEN: 'token' },
    services: { ports: { a: 900 } },
    http: {
      ports: { b: 800 },
      ACCESS_TOKEN_LIFETIME: 700,
      REFRESH_TOKEN_LIFETIME: 700,
      SECURE_COOKIE: '',
      SAME_SITE: 'Strict',
      DOMAIN: '.test.local',
      SESSION_KEY: 'test',
      APPS: ['b'],
      CLIENTS: { b: '123' },
    },
  };

  t.context.config = config;
  t.context.version = '5.5.0';
});

test.beforeEach('setup dir', async t => {
  const buildDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`);

  t.context.buildDir = buildDir;
  t.context.coreDir = path.join(buildDir, 'core');
});

test('dev', async t => {
  const { buildDir, coreDir, config, version } = t.context;

  const { pm2: result } = await dev(buildDir, coreDir, config, version);

  t.is(result.length, 1);
  const { env, name, script, cwd } = result[0];

  t.is(script, 'index.js');
  t.is(name, 'isa-http-b');
  t.is(cwd, path.join(buildDir, 'isa-http-b'));
  t.is(env.SECURE_COOKIE, '');
  t.is(env.SAME_SITE, 'Strict');
  t.is(env.ACCESS_TOKEN_LIFETIME, 700);
  t.is(env.REFRESH_TOKEN_LIFETIME, 700);
  // t.is(env.APP_HANDLE, 'B');
  t.is(env.SOCKET_ADDRESS_CLUSTERS, '{"services":{"0.0.0.0:900":["a"]}}');
  // t.is(env.CLIENT_ID, '123');
  // t.is(env.APPS, '[{"handle":"B"}]');
  t.is(env.DOMAIN, '.test.local');
  t.is(env.SESSION_KEY, 'test');
});

test('stag', async t => {
  const { buildDir, coreDir, config, version } = t.context;

  const { docker: result } = await stag(buildDir, coreDir, config, version);

  for (const key of Object.keys(config.http.ports)) {
    const containerName = `isa-http-${key}`;
    t.is(result[containerName].image, `isamusic/http-${key}:v${version}`);
    t.is(result[containerName].container_name, containerName);
    t.is(result[containerName].build.args.NPM_TOKEN, 'token');

    const { environment } = result[containerName];
    t.is(environment.SECURE_COOKIE, '');
    t.is(environment.SAME_SITE, 'Strict');
    t.is(environment.ACCESS_TOKEN_LIFETIME, 700);
    t.is(environment.REFRESH_TOKEN_LIFETIME, 700);
    t.is(environment.SOCKET_ADDRESS, '0.0.0.0:800');
    // t.is(environment.CLIENT_ID, '123');
    t.is(
      environment.SOCKET_ADDRESS_CLUSTERS,
      '{"services":{"isa-services-cluster-0:900":["a"]}}'
    );
    t.is(environment.APP_HANDLE, 'B');
    t.is(environment.APPS, '[{"handle":"B"}]');
    t.is(environment.DOMAIN, '.test.local');
    t.is(environment.SESSION_KEY, 'test');

    const packageJson = path.join(
      buildDir,
      containerName,
      'package',
      'package.json'
    );
    t.true(fs.existsSync(packageJson));

    const npmRC = path.join(buildDir, containerName, 'package', '.npmrc');
    t.true(fs.existsSync(npmRC));

    const dockerFilePath = path.join(buildDir, containerName, 'Dockerfile');
    t.true(fs.existsSync(dockerFilePath));

    const runner = path.join(
      buildDir,
      containerName,
      'package',
      'bin',
      'run.js'
    );
    t.true(fs.existsSync(runner));
  }
});

test('prod', async t => {
  const { config, version } = t.context;

  const { docker: result } = await prod(null, null, config, version);

  for (const key of Object.keys(config.http.ports)) {
    const containerName = `isa-http-${key}`;
    t.is(result[containerName].image, `isamusic/http-${key}:v${version}`);
    t.is(result[containerName].container_name, containerName);
    t.is(result[containerName].build, undefined);

    const { environment } = result[containerName];
    t.is(environment.SECURE_COOKIE, '');
    t.is(environment.SAME_SITE, 'Strict');
    t.is(environment.ACCESS_TOKEN_LIFETIME, 700);
    t.is(environment.REFRESH_TOKEN_LIFETIME, 700);
    t.is(environment.SOCKET_ADDRESS, '0.0.0.0:800');
    // t.is(environment.CLIENT_ID, '123');
    t.is(
      environment.SOCKET_ADDRESS_CLUSTERS,
      '{"services":{"isa-services-cluster-0:900":["a"]}}'
    );
    t.is(environment.APP_HANDLE, 'B');
    t.is(environment.APPS, '[{"handle":"B"}]');
    t.is(environment.DIR, '/package/package');
    t.is(environment.DOMAIN, '.test.local');
    t.is(environment.SESSION_KEY, 'test');
  }
});
