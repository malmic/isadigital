const path = require('path');

module.exports = (buildDir, coreDir, config, version) => {
  const { USERNAME, PASSWORD, PORT } = config.mongo;
  const { builders } = config;

  const volumeDir = path.join(builders.coreDir || coreDir, 'volumes');

  const docker = {
    'isa-mongo': {
      image: 'mongo',
      container_name: 'isa-mongo',
      restart: 'on-failure',
      environment: {
        MONGO_INITDB_ROOT_USERNAME: USERNAME,
        MONGO_INITDB_ROOT_PASSWORD: PASSWORD,
      },
      command: `--port 27018 --bind_ip_all`,
      volumes: [`${volumeDir}/mongo:/data/db`],
      ports: [`27018:${PORT}`],
    },
  };

  return { docker, pm2: [] };
};
