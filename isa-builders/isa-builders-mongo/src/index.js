const dev = require('./dev');
const prod = require('./prod');
const stag = require('./stag');

module.exports = { dev, prod, stag };
