const fs = require('fs-extra');
const path = require('path');
const changeCase = require('change-case');

const stringify = require('@isa-music/libs-stringify');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(config.services.ports, () => '0.0.0.0');

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const MONGO_URL = `mongodb://${config.mongo.USERNAME}:${
    config.mongo.PASSWORD
  }@0.0.0.0:${config.mongo.PORT}/isamusic`;

  const MONGO_AUTH_SOURCE = config.mongo.AUTH_SOURCE;

  const env = {
    ...config.services,
    APPS: config.http.APPS.map(handle => ({
      handle: handle.toUpperCase(),
    })),
  };

  delete env.ports;

  const pm2 = serviceClusters.map((serviceCluster, clusterIndex) => {
    const clusterExecutables = serviceCluster.protoNames.map(protoName => ({
      protoName,
      path: `@isa-music/services-${changeCase.paramCase(protoName)}`,
    }));

    const clusterName = `isa-services-cluster-${clusterIndex}`;
    const outputPath = path.join(buildDir, clusterName);

    fs.outputFileSync(
      path.join(outputPath, 'index.js'),
      `const services = require('@isa-music/zeus-services');

services('${clusterName}', process.env, ${JSON.stringify(
        clusterExecutables
      )}, () => { process.send('ready'); }).catch(console.error);
`
    );

    return {
      script: 'index.js',
      name: clusterName,
      cwd: outputPath,
      wait_ready: true,
      ignore_watch: ['node_modules'],
      node_args: '--trace-warnings',
      env: stringify({
        NODE_ENV: 'development',
        MONGO_URL,
        MONGO_AUTH_SOURCE,
        SOCKET_ADDRESS: `0.0.0.0:${serviceCluster.port}`,
        SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
        ...env,
      }),
    };
  });

  return { docker: {}, pm2 };
};
