const dev = require('./dev');
const stag = require('./stag');
const prod = require('./prod');

module.exports = { dev, stag, prod };
