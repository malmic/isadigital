const stringify = require('@isa-music/libs-stringify');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(
    config.services.ports,
    i => `isa-services-cluster-${i}`
  );

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const MONGO_URL = config.mongo.URL;

  const MONGO_AUTH_SOURCE = config.mongo.AUTH_SOURCE;

  const environment = {
    ...config.services,
    APPS: config.http.APPS.map(handle => ({
      handle: handle.toUpperCase(),
    })),
  };

  delete environment.ports;

  const docker = serviceClusters.reduce((acc, serviceCluster, clusterIndex) => {
    const clusterName = `services-cluster-${clusterIndex}`;
    const folderName = `isa-${clusterName}`;

    return {
      ...acc,
      [folderName]: {
        image: `isamusic/services-${clusterName}:v${version}`,
        container_name: serviceCluster.hostname,
        restart: 'on-failure',
        environment: stringify({
          NODE_ENV: 'production',
          MONGO_URL,
          MONGO_AUTH_SOURCE,
          SOCKET_ADDRESS: `0.0.0.0:${serviceCluster.port}`,
          SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
          ...environment,
        }),
      },
    };
  }, {});

  return { docker, pm2: [] };
};
