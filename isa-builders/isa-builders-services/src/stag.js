const fs = require('fs-extra');
const path = require('path');
const changeCase = require('change-case');

const stringify = require('@isa-music/libs-stringify');
const { generateDockerfile } = require('@isa-music/libs-docker');
const {
  clusterize,
  clusterToSocketAddresses,
} = require('@isa-music/libs-clusters');

module.exports = (buildDir, coreDir, config, version) => {
  const serviceClusters = clusterize(
    config.services.ports,
    i => `isa-services-cluster-${i}`
  );

  const socketAddressClusters = {
    services: clusterToSocketAddresses(serviceClusters),
  };

  const libraryExecutables = ['@isa-music/zeus-services'].map(path => ({
    path,
  }));

  const MONGO_URL = `mongodb://${config.mongo.USERNAME}:${
    config.mongo.PASSWORD
  }@isa-mongo:${config.mongo.PORT}/isamusic`;

  const MONGO_AUTH_SOURCE = config.mongo.AUTH_SOURCE;

  const environment = {
    ...config.services,
    APPS: config.http.APPS.map(handle => ({
      handle: handle.toUpperCase(),
    })),
  };

  delete environment.ports;

  const docker = serviceClusters.reduce((acc, serviceCluster, clusterIndex) => {
    const clusterExecutables = serviceCluster.protoNames.map(protoName => ({
      protoName,
      path: `@isa-music/services-${changeCase.paramCase(protoName)}`,
    }));

    const clusterName = `services-cluster-${clusterIndex}`;
    const folderName = `isa-${clusterName}`;

    const appBuildPath = path.join(buildDir, folderName);
    fs.ensureDirSync(appBuildPath);

    const packageBuildPath = path.join(appBuildPath, 'package');
    fs.ensureDirSync(packageBuildPath);

    const dependencies = [...libraryExecutables, ...clusterExecutables].reduce(
      (acc, cur) => ({
        ...acc,
        [cur.path]: version,
      }),
      {}
    );

    fs.outputFileSync(
      path.join(appBuildPath, 'Dockerfile'),
      generateDockerfile(
        ['ARG NPM_TOKEN', 'COPY /package /package', 'WORKDIR /package'],
        ['RUN rm -f /package/.npmrc', 'CMD [ "node", "bin/run.js" ]']
      )
    );

    fs.outputFileSync(
      path.join(packageBuildPath, '.npmrc'),
      `//registry.npmjs.org/:_authToken=\${NPM_TOKEN}`
    );

    fs.outputFileSync(
      path.join(packageBuildPath, 'package.json'),
      JSON.stringify(
        {
          name: folderName,
          version: '1.0.0',
          private: true,
          license: 'UNLICENSED',
          dependencies,
        },
        null,
        2
      )
    );

    fs.outputFileSync(
      path.join(packageBuildPath, 'bin', 'run.js'),
      `const services = require('@isa-music/zeus-services');

services('${folderName}', process.env, ${JSON.stringify(
        clusterExecutables,
        null,
        2
      )}).catch(console.error);
`
    );

    return {
      ...acc,
      [folderName]: {
        image: `isamusic/${clusterName}:v${version}`,
        container_name: serviceCluster.hostname,
        restart: 'on-failure',
        environment: stringify({
          NODE_ENV: 'production',
          MONGO_URL,
          MONGO_AUTH_SOURCE,
          SOCKET_ADDRESS: `0.0.0.0:${serviceCluster.port}`,
          SOCKET_ADDRESS_CLUSTERS: socketAddressClusters,
          ...environment,
        }),
        build: {
          context: appBuildPath,
          args: { NPM_TOKEN: config.builders.NPM_TOKEN },
        },
      },
    };
  }, {});

  return { docker, pm2: [] };
};
