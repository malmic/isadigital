const test = require('ava');
const path = require('path');
const fs = require('fs-extra');
const os = require('os');

const { sep } = path;

const { dev, stag, prod } = require('../src');

test.before('setup config', t => {
  const config = {
    mongo: {
      PORT: '12345',
      USERNAME: 'TEST',
      PASSWORD: 'TEST',
      AUTH_SOURCE: 'admin',
    },
    services: { ports: { a: 900 } },
    builders: { NPM_TOKEN: 'token' },
    http: { APPS: ['b'] },
  };

  t.context.config = config;
  t.context.version = '5.5.0';
});

test.beforeEach('setup dir', async t => {
  const buildDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`);

  t.context.buildDir = buildDir;
  t.context.coreDir = path.join(buildDir, 'core');
});

test('dev', async t => {
  const { buildDir, coreDir, config, version } = t.context;

  const { pm2: result } = await dev(buildDir, coreDir, config, version);

  t.is(result.length, 1);
  const { env, script, name, cwd } = result[0];

  t.is(script, 'index.js');
  t.is(name, 'isa-services-cluster-0');
  t.is(cwd, path.join(buildDir, result[0].name));
  t.is(env.MONGO_URL, 'mongodb://TEST:TEST@0.0.0.0:12345/isamusic');
  t.is(env.MONGO_AUTH_SOURCE, 'admin');
  t.is(env.APPS, '[{"handle":"B"}]');
});

test('stag', async t => {
  const { buildDir, coreDir, config, version } = t.context;

  const entry = 'isa-services-cluster-0';

  const { docker: result } = await stag(buildDir, coreDir, config, version);
  const { environment, container_name: containerName, image, build } = result[
    'isa-services-cluster-0'
  ];

  const { SOCKET_ADDRESS_CLUSTERS, MONGO_URL } = environment;
  const { services } = JSON.parse(SOCKET_ADDRESS_CLUSTERS);

  t.is(MONGO_URL, 'mongodb://TEST:TEST@isa-mongo:12345/isamusic');
  t.is(services['isa-services-cluster-0:900'][0], 'a');
  t.is(environment.MONGO_AUTH_SOURCE, 'admin');
  t.is(environment.SOCKET_ADDRESS, '0.0.0.0:900');
  t.is(environment.APPS, '[{"handle":"B"}]');

  t.is(containerName, 'isa-services-cluster-0');
  t.is(image, 'isamusic/services-cluster-0:v5.5.0');
  t.is(build.args.NPM_TOKEN, 'token');

  const dockerFile = path.join(buildDir, entry, 'Dockerfile');
  t.true(fs.existsSync(dockerFile));

  const packageJson = path.join(buildDir, entry, 'package', 'package.json');
  t.true(fs.existsSync(packageJson));

  const npmrc = path.join(buildDir, entry, 'package', '.npmrc');
  t.true(fs.existsSync(npmrc));

  const runner = path.join(buildDir, entry, 'package', 'bin', 'run.js');
  t.true(fs.existsSync(runner));
});

test('prod', async t => {
  const { config, version } = t.context;

  const { docker: result } = await prod(null, null, config, version);
  const { environment, build } = result['isa-services-cluster-0'];

  t.is(build, undefined);

  t.is(environment.APPS, '[{"handle":"B"}]');
});
