import React from 'react';
import { node, string } from 'prop-types';
import { Container, Nav, NavItem } from 'reactstrap';
import styled from 'styled-components';

import { Head, HeaderLink, Header } from '../widgets';

const PrivateLayout = ({ children, className, backgroundImage }) => {
  return (
    <div className={className}>
      <Head />
      <Header>
        <Nav className="ml-auto text-right" navbar>
          <NavItem>
            <HeaderLink to="/view-award">Digital Creative Award</HeaderLink>
          </NavItem>
          <NavItem>
            <HeaderLink to="/view-courses">Teaching Room</HeaderLink>
          </NavItem>
          <NavItem>
            <HeaderLink to="/logout">Logout</HeaderLink>
          </NavItem>
        </Nav>
      </Header>
      <Container
        fluid
        style={{
          height: '100%',
          backgroundImage: `url('/backgrounds/${backgroundImage}')`,
          backgroundAttachment: 'fixed',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          paddingTop: '50px',
          paddingBottom: '50px',
        }}
      >
        {children}
      </Container>
    </div>
  );
};

PrivateLayout.propTypes = {
  children: node.isRequired,
  className: string.isRequired,
  backgroundImage: string.isRequired,
};

export default styled(PrivateLayout)`
  display: grid;
  grid-template-rows: auto 1fr;
  min-height: 100%;
  grid-template-columns: 100%;
`;
