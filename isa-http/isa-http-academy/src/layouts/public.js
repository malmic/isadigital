import React from 'react';
import { string, node } from 'prop-types';
import styled from 'styled-components';
import { Row, Col, Container } from 'reactstrap';

import { Head, PageHeading } from '../widgets';

const PublicLayout = ({ title, children, className }) => {
  return (
    <Container fluid className={className}>
      <Head />
      <Row>
        <Col className="align-self-center">
          <PageHeading color="rgba(0,0,0, 0.5)">{title}</PageHeading>
        </Col>
      </Row>
      <Row>
        <Col md={{ size: 4, offset: 4 }}>{children}</Col>
      </Row>
    </Container>
  );
};

PublicLayout.propTypes = {
  children: node.isRequired,
  className: string.isRequired,
  title: string.isRequired,
};

export default styled(PublicLayout)`
  height: 100%;
  min-height: 100%;
  background-image: url('/backgrounds/login.png');
  background-attachment: fixed;
  background-size: cover;
  background-repeat: no-repeat;
  display: grid;
  grid-template-rows: 300px 1fr;
`;
