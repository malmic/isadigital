const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const next = require('next');
const middleware = require('@isa-music/libs-middleware');
const http = require('http');

const { errorHandler, sessionHandler } = middleware;

const routers = require('./routers');

module.exports = async (serviceLoaders, env) => {
  const app = next({ dev: env.NODE_ENV !== 'production', dir: env.DIR });
  const handle = app.getRequestHandler();

  await app.prepare();

  const services = serviceLoaders['services'];

  const router = new Router();
  router.use('/addresses', routers.addresses(env, services).routes());
  router.use('/announcements', routers.announcements(env, services).routes());
  router.use('/authMethods', routers.authMethods(env, services).routes());
  router.use('/awards', routers.awards(env, services).routes());
  router.use('/courses', routers.courses(env, services).routes());
  router.use('/events', routers.events(env, services).routes());
  router.use('/jurors', routers.jurors(env, services).routes());
  router.use('/manifest', routers.manifest().routes());
  router.use('/payments', routers.payments(env, services).routes());
  router.use('/profiles', routers.profiles(env, services).routes());
  router.use('/register', routers.register(env, services).routes());
  router.use('/rooms', routers.rooms(env, services).routes());
  router.use('/session', routers.session(env, services).routes());
  router.use('/sources', routers.sources(env, services).routes());
  router.use('/votes', routers.votes(env, services).routes());
  router.get('*', async ctx => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
  });

  const koa = new Koa();
  koa.keys = [env.SESSION_KEY];

  koa.use(bodyParser());
  koa.use(errorHandler);
  koa.use(sessionHandler(koa, services, env));
  koa.use(router.routes());

  return http.createServer(koa.callback());
};
