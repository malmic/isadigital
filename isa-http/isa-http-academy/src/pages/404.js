import React from 'react';
import { Alert } from 'reactstrap';

import { PublicLayout } from '../layouts';

const NotFound = () => {
  return (
    <PublicLayout title="Not Found">
      <Alert color="danger" className="font-weight-bold text-uppercase">
        The requested page was not found.
      </Alert>
    </PublicLayout>
  );
};

export default NotFound;
