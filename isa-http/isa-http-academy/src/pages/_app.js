import withRedux from 'next-redux-wrapper';
import App from 'next/app';
import { Provider } from 'react-redux';
import cookie from 'cookie';
import { IntlProvider } from 'react-intl';
import Intercom from 'react-intercom';
import axios from 'axios';

import {
  emailAddresses,
  notifications,
  profiles,
  rooms,
  session,
  sources,
} from '../store/actions';
import { createClientStore } from '../store';

import '../styles/shared.scss';

class Entry extends App {
  static async getInitialProps({ Component, ctx }) {
    const isServer = typeof window === 'undefined';

    if (isServer) {
      const { dispatch } = ctx.store;

      try {
        const { SOCKET_ADDRESS } = process.env;

        const { sid } = cookie.parse(ctx.req.headers.cookie || '');

        const app = axios.create({
          baseURL: `http://${SOCKET_ADDRESS}`,
          withCredentials: true,
        });

        if (sid) {
          try {
            const {
              data: { result },
            } = await app.get('/session/me', {
              headers: { Cookie: ctx.req.headers.cookie },
            });

            dispatch(profiles.profileFetched(result.profile));
            dispatch(sources.sourceTypesFetched(result.sourceTypes));
            dispatch(rooms.roomsFetched(result.rooms));

            if (ctx.query.id) {
              dispatch(
                rooms.roomFetched(
                  result.rooms.find(r => r.referenceId === ctx.query.id)
                )
              );
            } else {
              dispatch(rooms.roomFetched(result.rooms.find(r => r.default)));
            }

            dispatch(
              emailAddresses.emailAddressesFetched(result.emailAddresses)
            );

            dispatch(session.sessionCreated(result.user));

            const pageProps = Component.getInitialProps
              ? await Component.getInitialProps(ctx)
              : {};

            const intercomUser = {
              user_id: result.user.referenceId,
              email: result.emailAddresses.find(e => e.active === true).address,
              name: result.emailAddresses.find(e => e.active === true).address,
            };

            return {
              pageProps,
              locale: result.user.languageHandle,
              intercomUser,
            };
          } catch (err) {
            if (err.response && err.response.status === 403) {
              ctx.session = null;

              dispatch(
                notifications.notificationCreated(
                  'ERROR',
                  'Your session has expired.'
                )
              );
            } else {
              dispatch(notifications.notificationCreated('ERROR', err));
            }
          }
        }

        const pageProps = Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {};

        return { pageProps, locale: 'EN', intercomUser: {} };
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    }

    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};

    const { locale } = window.__NEXT_DATA__.props.initialProps;

    return { pageProps, locale, intercomUser: {} };
  }

  constructor(props) {
    super(props);

    this.state = {};
  }

  updateLocale = locale => {
    this.setState({ locale });
  };

  render() {
    const { Component, pageProps, store, locale, intercomUser } = this.props;

    const currentLocale = this.state.locale || locale;

    return (
      <IntlProvider locale={currentLocale.toLowerCase()}>
        <Provider store={store}>
          <>
            <Intercom appID="w06cbfxj" user={intercomUser} />
            <Component {...pageProps} updateLocale={this.updateLocale} />
          </>
        </Provider>
      </IntlProvider>
    );
  }
}

export default withRedux(createClientStore)(Entry);
