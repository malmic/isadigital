import React from 'react';
import { Alert } from 'reactstrap';

import { PublicLayout } from '../layouts';

const Error = () => {
  return (
    <PublicLayout title="Error">
      <Alert color="danger" className="font-weight-bold text-uppercase">
        An error has occured.
      </Alert>
    </PublicLayout>
  );
};

export default Error;
