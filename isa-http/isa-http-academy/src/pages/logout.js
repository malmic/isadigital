import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { Alert } from 'reactstrap';

import { PublicLayout } from '../layouts';

const Logout = ({ dispatch }) => {
  useEffect(() => {
    setTimeout(() => {
      window.location.href = '/session/delete';
    }, 1000);
  }, [dispatch]);

  return (
    <PublicLayout title="Logout">
      <Alert
        className="font-weight-bold text-center text-uppercase"
        color="secondary"
      >
        Logging out...
      </Alert>
    </PublicLayout>
  );
};

Logout.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect(state => ({
  session: state.session,
  notifications: state.notifications,
}))(privateCheck(Logout));
