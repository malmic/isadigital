import React from 'react';
import { connect } from 'react-redux';
import { publicCheck } from '@isa-music/libs-hocs';
import { object } from 'prop-types';

import { PublicLayout } from '../layouts';
import { RegisterForm } from '../widgets';

const Register = ({ router }) => {
  return (
    <PublicLayout title="Register">
      <RegisterForm next={router.query.next} />
    </PublicLayout>
  );
};

Register.propTypes = {
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
}))(publicCheck(Register));
