import React, { useState, useMemo } from 'react';
import { func } from 'prop-types';
import { Button, Row, Col, Form, FormGroup, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../layouts';
import { thunks } from '../store';
import { PageHeading } from '../widgets';

const UpdatePassword = ({ dispatch }) => {
  const updatePassword = () => {
    dispatch(
      thunks.authMethods.updatePassword(
        { currentPassword, newPassword },
        () => {
          window.location.href = '/session/delete';
        }
      )
    );
  };

  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const passwordValid = useMemo(
    () => newPassword && confirmPassword === newPassword,
    [confirmPassword, newPassword]
  );

  return (
    <PrivateLayout>
      <Row>
        <Col>
          <PageHeading color="#000000">Update Password</PageHeading>
        </Col>
      </Row>
      <div className="mt-3" />
      <Row>
        <Col md="4" />
        <Col md="4">
          <Row>
            <Col>
              <Form>
                <FormGroup>
                  <Input
                    className="font-weight-bold"
                    type="password"
                    value={currentPassword}
                    placeholder="Current Password"
                    onChange={e => setCurrentPassword(e.target.value)}
                    name="currentPassword"
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    className="font-weight-bold"
                    type="password"
                    value={newPassword}
                    placeholder="New Password"
                    onChange={e => setNewPassword(e.target.value)}
                    name="newPassword"
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    className="font-weight-bold"
                    type="password"
                    value={confirmPassword}
                    placeholder="Confirm New Password"
                    onChange={e => setConfirmPassword(e.target.value)}
                    name="confirmPassword"
                  />
                </FormGroup>
                <Button
                  className="text-uppercase font-weight-bold"
                  block
                  color="primary"
                  disabled={!passwordValid}
                  onClick={updatePassword}
                >
                  Update Password
                </Button>
                <div className="mt-3" />
                <span className="text-primary text-uppercase font-weight-bold">
                  Upon successful password reset, you will be logged out of the
                  app.
                </span>
              </Form>
            </Col>
          </Row>
        </Col>
        <Col md="4" />
      </Row>
    </PrivateLayout>
  );
};

UpdatePassword.propTypes = {
  dispatch: func.isRequired,
};

export default connect(state => ({
  session: state.session,
}))(privateCheck(UpdatePassword));
