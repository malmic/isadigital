import React, { useEffect } from 'react';
import { array, object, func } from 'prop-types';
import { Button, Row, Col, Card } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { useRouter } from 'next/router';

import { PrivateLayout } from '../layouts';
import { thunks } from '../store';
import { PageHeading } from '../widgets';

const ViewAccount = ({
  dispatch,
  emailAddresses,
  session,
  profile,
  address,
  payment,
}) => {
  useEffect(() => {
    dispatch(thunks.addresses.getAddress());
    dispatch(thunks.payments.getPayment());
  }, [dispatch]);

  const router = useRouter();

  return (
    <PrivateLayout>
      <Row>
        <Col>
          <PageHeading color="#000000">My Profile</PageHeading>
        </Col>
      </Row>
      <div className="mt-3" />
      <Row>
        <Col md="2" />
        <Col md="8">
          <Row>
            <Col md="6">
              {payment.name && (
                <Card className="text-uppercase font-weight-bold">
                  Plan
                  <br />
                  {payment.name}
                  <br />
                  Fee
                  <br />
                  {payment.quantity.handle} {payment.quantity.value}
                </Card>
              )}
              <div className="mt-3" />
              {emailAddresses.length > 0 && (
                <Card className="text-uppercase font-weight-bold">
                  Email Address
                  <br />
                  {emailAddresses[0].address}
                </Card>
              )}
              <div className="mt-3" />
              <Button
                onClick={() => {
                  router.push('/update-password');
                }}
                className="text-uppercase font-weight-bold"
              >
                Update Password
              </Button>
            </Col>
            <Col md="6">
              {profile.typeHandle === 'STUDENT' && (
                <Card className="text-uppercase font-weight-bold">
                  First Name
                  <br />
                  {profile.firstName}
                  <br />
                  Last Name
                  <br />
                  {profile.lastName}
                  <br />
                  Instrument
                  <br />
                  {profile.instrument}
                  <br />
                  University
                  <br />
                  {profile.university}
                </Card>
              )}
              {profile.typeHandle === 'PROFESSOR' && (
                <Card className="text-uppercase font-weight-bold">
                  First Name
                  <br />
                  {profile.firstName}
                  <br />
                  Last Name
                  <br />
                  {profile.lastName}
                  <br />
                  Instrument
                  <br />
                  {profile.instrument}
                  <br />
                  University
                  <br />
                  {profile.university}
                </Card>
              )}
              {profile.typeHandle === 'SPONSOR' && (
                <Card className="text-uppercase font-weight-bold">
                  First Name
                  <br />
                  {profile.firstName}
                  <br />
                  Last Name
                  <br />
                  {profile.lastName}
                </Card>
              )}
              <div className="mt-3" />
              {Object.keys(address).length > 0 && (
                <Card className="text-uppercase font-weight-bold">
                  Street
                  <br />
                  {address.street}
                  <br />
                  Postal Code
                  <br />
                  {address.postalCode}
                  <br />
                  City
                  <br />
                  {address.city}
                  <br />
                  Country
                  <br />
                  {address.country}
                </Card>
              )}
            </Col>
          </Row>
        </Col>
        <Col md="2" />
      </Row>
    </PrivateLayout>
  );
};

ViewAccount.propTypes = {
  emailAddresses: array.isRequired,
  session: object.isRequired,
  dispatch: func.isRequired,
  profile: object.isRequired,
  address: object.isRequired,
  payment: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  profile: state.profiles.profile,
  emailAddresses: state.emailAddresses.data,
  address: state.addresses.address,
  payment: state.payments.payment,
}))(privateCheck(ViewAccount));
