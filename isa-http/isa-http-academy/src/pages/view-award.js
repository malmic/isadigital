import React, { useEffect, useCallback, useMemo } from 'react';
import { func, array, object } from 'prop-types';
import {
  Row,
  Col,
  CardColumns,
  Card,
  CardBody,
  CardTitle,
  Button,
  ButtonGroup,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import {
  CreateSource,
  ViewRooms,
  PageHeading,
  ViewSession,
  ViewSource,
} from '../widgets';
import { PrivateLayout } from '../layouts';
import { thunks } from '../store';
import moment from 'moment';

const ViewAward = ({
  dispatch,
  sources,
  vote,
  votes,
  jurors,
  session,
  awards,
  rooms,
  emailAddresses,
  source,
  router,
}) => {
  useEffect(() => {
    dispatch(thunks.awards.getAwards());
    dispatch(thunks.votes.getVote());
    dispatch(thunks.votes.getVotes());
    dispatch(thunks.jurors.getJurors());
  }, [dispatch]);

  useEffect(() => {
    if (awards.length > 0) {
      dispatch(
        thunks.sources.getSources({
          foreign: [{ id: awards[0].referenceId, typeHandle: 'AWARD' }],
        })
      );
    }
  }, [awards, dispatch]);

  const getVotes = useCallback(
    referenceId => {
      return votes.reduce((acc, cur) => {
        const found = cur.foreign.find(
          f => f.id === referenceId && f.typeHandle === 'SOURCE'
        );

        if (found) return acc + 1;
        return acc;
      }, 0);
    },
    [votes]
  );

  const awardSources = useMemo(() => {
    const result = sources.map(source => ({
      ...source,
      votes: getVotes(source.referenceId),
    }));

    result.sort((a, b) => b.votes - a.votes);

    return result;
  }, [getVotes, sources]);

  useEffect(() => {
    if (awardSources.length > 0) {
      dispatch(
        thunks.sources.getSource({
          referenceId: awardSources[0].referenceId,
        })
      );
    }
  }, [awardSources, dispatch, sources]);

  const hasVoted = useCallback(
    referenceId => !!vote.foreign.find(f => f.id === referenceId),
    [vote.foreign]
  );

  const canVote = useCallback(
    referenceId =>
      vote.foreign.findIndex(f => f.id === referenceId) === -1 &&
      moment
        .utc()
        .isSameOrBefore(
          moment.utc('2020-08-24 03:00 PM', 'YYYY-MM-DD hh:mm A')
        ),
    [vote.foreign]
  );

  const isOwner = useCallback(
    s => {
      const userForeign = s.foreign.find(f => f.typeHandle === 'USER');
      if (!userForeign) return false;

      return session.referenceId === userForeign.id;
    },
    [session.referenceId]
  );

  const isJuror = useMemo(() => {
    return jurors.find(j => j.foreign.id === session.referenceId);
  }, [jurors, session.referenceId]);

  const updateSource = ({ referenceId, statusHandle }) => {
    dispatch(
      thunks.sources.updateSource({
        referenceId,
        statusHandle,
      })
    );
  };

  const createVote = referenceId => {
    dispatch(
      thunks.votes.createVote({
        foreign: [
          { id: referenceId, typeHandle: 'SOURCE' },
          { id: session.referenceId, typeHandle: 'USER' },
        ],
      })
    );
  };

  const createSource = (source, foreign) => {
    dispatch(
      thunks.sources.createSource({
        ...source,
        foreign: [foreign],
        statusHandle: 'PENDING',
      })
    );
  };

  const deleteSource = referenceId => {
    dispatch(thunks.sources.deleteSource({ referenceId }));
  };

  return (
    <PrivateLayout backgroundImage="award.png">
      <Row>
        <Col md="4" className="align-self-center">
          <ViewSession emailAddresses={emailAddresses} />
        </Col>
        <Col md="4" className="align-self-center">
          <PageHeading>Digital Creative Award</PageHeading>
        </Col>
        <Col md="4" />
      </Row>
      <div style={{ paddingTop: '50px' }} />
      <Row>
        <Col md="6">
          <ViewSource backgroundColor="#d8d8d8" source={source} />
          <div className="mt-3" />
        </Col>
        <Col md="6">
          <h6 className="text-uppercase">rooms</h6>
          <hr />
          <div className="mt-3" />
          <ViewRooms
            rooms={rooms}
            roomSelected={id => {
              router.push({ pathname: '/', query: { id } });
            }}
          />
          <h6 className="text-uppercase">Create Nomination</h6>
          <hr />
          <div className="mt-3" />
          <CreateSource
            foreigns={awards.map(award => ({
              id: award.referenceId,
              typeHandle: 'AWARD',
              name: `${award.name} ${award.editionHandle}`,
            }))}
            createSource={createSource}
          />
          <div className="mt-3" />
          <h6 className="text-uppercase">nominations</h6>
          <hr />
          <div className="mt-3" />
          <CardColumns>
            {awardSources.map(source => (
              <Card
                key={source.referenceId}
                style={{ backgroundColor: '#f4b13e' }}
              >
                <CardBody>
                  <CardTitle
                    style={{ cursor: 'pointer', fontWeight: '600' }}
                    className="text-uppercase text-white"
                    onClick={() => {
                      dispatch(
                        thunks.sources.getSource({
                          referenceId: source.referenceId,
                        })
                      );
                    }}
                  >
                    {source.title}
                  </CardTitle>
                  <span
                    style={{ fontWeight: '800' }}
                    className="text-uppercase"
                  >
                    {source.statusHandle === 'PENDING'
                      ? 'AWAITING NOMINATION'
                      : 'NOMINATED'}
                  </span>
                  <br />
                  {isOwner(source) && (
                    <Button
                      className="text-uppercase"
                      onClick={() => {
                        deleteSource(source.referenceId);
                      }}
                    >
                      Delete
                    </Button>
                  )}
                  {source.statusHandle === 'VALID' && (
                    <span
                      className="text-uppercase float-left"
                      color="secondary"
                    >
                      {source.votes} Votes
                    </span>
                  )}
                  <ButtonGroup className="float-right">
                    {isJuror && (
                      <Button
                        className="text-uppercase font-weight-bold"
                        onClick={() => {
                          updateSource({
                            referenceId: source.referenceId,
                            statusHandle:
                              source.statusHandle === 'PENDING'
                                ? 'VALID'
                                : 'PENDING',
                          });
                        }}
                      >
                        {source.statusHandle === 'PENDING'
                          ? 'NOMINATE'
                          : 'WITHDRAW'}
                      </Button>
                    )}
                    {source.statusHandle === 'VALID' && (
                      <Button
                        className="text-uppercase"
                        onClick={() => {
                          createVote(source.referenceId);
                        }}
                        disabled={!canVote(source.referenceId)}
                      >
                        {hasVoted(source.referenceId) ? 'Voted' : 'Vote'}
                      </Button>
                    )}
                  </ButtonGroup>
                </CardBody>
              </Card>
            ))}
          </CardColumns>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

ViewAward.propTypes = {
  sources: array.isRequired,
  dispatch: func.isRequired,
  vote: object.isRequired,
  votes: array.isRequired,
  jurors: array.isRequired,
  session: object.isRequired,
  awards: array.isRequired,
  rooms: array.isRequired,
  emailAddresses: array.isRequired,
  source: object.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  sources: state.sources.data,
  vote: state.votes.vote,
  votes: state.votes.data,
  jurors: state.jurors,
  awards: state.awards.data,
  rooms: state.rooms.data,
  emailAddresses: state.emailAddresses.data,
  source: state.sources.source,
}))(privateCheck(ViewAward));
