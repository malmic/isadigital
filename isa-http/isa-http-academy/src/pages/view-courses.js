import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  Button,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardColumns,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import {
  CreateSource,
  ViewRooms,
  PageHeading,
  ViewProfile,
  ViewSession,
  ViewSource,
} from '../widgets';
import { PrivateLayout } from '../layouts';
import { thunks } from '../store';

const ViewCourses = ({
  dispatch,
  session,
  courses,
  rooms,
  sources,
  emailAddresses,
  source,
  router,
}) => {
  useEffect(() => {
    dispatch(thunks.courses.getCourses());
    dispatch(
      thunks.sources.getSources({
        foreign: [{ id: session.referenceId, typeHandle: 'USER' }],
      })
    );
  }, [dispatch, session.referenceId]);

  useEffect(() => {
    if (sources.length === 0) return;
    dispatch(thunks.sources.getSource({ referenceId: sources[0].referenceId }));
  }, [dispatch, sources]);

  const createSource = (source, foreign) => {
    dispatch(
      thunks.sources.createSource({
        ...source,
        foreign: [foreign],
        statusHandle: 'VALID',
      })
    );
  };

  const getSource = referenceId => {
    dispatch(thunks.sources.getSource({ referenceId }));
  };

  const deleteSource = referenceId => {
    dispatch(thunks.sources.deleteSource({ referenceId }));
  };

  return (
    <PrivateLayout backgroundImage="course.png">
      <Row>
        <Col md="4" className="align-self-center">
          <ViewSession emailAddresses={emailAddresses} />
        </Col>
        <Col md="4" className="align-self-center">
          <PageHeading>Teaching Room</PageHeading>
        </Col>
        <Col md="4" />
      </Row>
      <div style={{ paddingTop: '50px' }} />
      <Row>
        <Col md="6">
          <Row>
            <Col>
              <ViewSource
                backgroundColor="#d8d8d8"
                source={source}
                createSource={createSource}
              />
              <h6 className="text-uppercase">booked courses</h6>
              <hr />
              <div className="mt-3" />
              {courses.map(course => (
                <ViewProfile
                  backgroundColor="#f4b13e"
                  key={course.profile.referenceId}
                  profile={course.profile}
                />
              ))}
            </Col>
          </Row>
        </Col>
        <Col md="6">
          <Row>
            <Col>
              <h6 className="text-uppercase">rooms</h6>
              <hr />
              <div className="mt-3" />
              <ViewRooms
                rooms={rooms}
                roomSelected={id => {
                  router.push({ pathname: '/', query: { id } });
                }}
              />
              <h6 className="text-uppercase">Create Course File</h6>
              <hr />
              <div className="mt-3" />
              <CreateSource
                createSource={createSource}
                foreigns={courses.map(course => ({
                  id: course.referenceId,
                  typeHandle: 'COURSE',
                  name: course.profile.fullName,
                }))}
              />
              <div className="mt-3" />
              <h6 className="text-uppercase">Course Files</h6>
              <hr />
              <div className="mt-3" />
              {courses.map(course => (
                <React.Fragment key={course.referenceId}>
                  <h6>{course.profile.fullName}</h6>
                  <CardColumns>
                    {sources
                      .filter(s =>
                        s.foreign.find(
                          f =>
                            f.typeHandle === 'COURSE' &&
                            f.id === course.referenceId
                        )
                      )
                      .map(source => (
                        <Card
                          key={source.referenceId}
                          style={{ backgroundColor: '#f4b13e' }}
                        >
                          <CardBody>
                            <CardTitle
                              onClick={() => {
                                getSource(source.referenceId);
                              }}
                              style={{
                                cursor: 'pointer',
                                fontWeight: '600',
                              }}
                              className="text-uppercase text-white"
                            >
                              {source.title}
                            </CardTitle>
                          </CardBody>
                          <Button
                            className="text-uppercase"
                            onClick={() => {
                              deleteSource(source.referenceId);
                            }}
                          >
                            Delete
                          </Button>
                        </Card>
                      ))}
                  </CardColumns>
                </React.Fragment>
              ))}
              <div className="mt-3" />
            </Col>
          </Row>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

ViewCourses.propTypes = {
  dispatch: func.isRequired,
  courses: array.isRequired,
  session: object.isRequired,
  rooms: array.isRequired,
  sources: array.isRequired,
  emailAddresses: array.isRequired,
  source: object.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  courses: state.courses.data,
  rooms: state.rooms.data,
  sources: state.sources.data,
  emailAddresses: state.emailAddresses.data,
  source: state.sources.source,
}))(privateCheck(ViewCourses));
