const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAddress', async ctx => {
    const { foreign } = ctx.session;

    const response = await services['addresses'].getAddressAsync({ foreign });

    ctx.body = response;
  });

  return router;
};
