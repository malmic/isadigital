const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAnnouncements', async ctx => {
    const response = await services['announcements'].getAnnouncementsAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
