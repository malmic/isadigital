const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/updatePassword', async ctx => {
    const { foreign } = ctx.session;
    const { currentPassword, newPassword } = ctx.request.body;

    const { result: passwordsEqual } = await services[
      'password'
    ].passwordMatchesAsync({ foreign, plaintextPassword: currentPassword });

    if (!passwordsEqual)
      throw new Error('You have entered an incorrect password');

    const { result: emailAddresses } = await services[
      'emailAddresses'
    ].getEmailAddressesAsync({
      foreign,
    });

    const allEmailAddressesNotSameAsPassword = emailAddresses
      .map(emailAddress => newPassword !== emailAddress.address)
      .every(result => result === true);

    if (!allEmailAddressesNotSameAsPassword) {
      throw new Error(
        'None of your email addresses can match your new password.'
      );
    }

    const response = await services['password'].updatePasswordAsync({
      foreign,
      plaintextPassword: newPassword,
    });

    ctx.body = response;
  });

  return router;
};
