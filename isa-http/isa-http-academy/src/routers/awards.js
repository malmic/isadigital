const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAwards', async ctx => {
    const response = await services['awards'].getAwardsAsync({});

    ctx.body = response;
  });

  return router;
};
