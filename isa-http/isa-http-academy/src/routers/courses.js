const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getCourses', async ctx => {
    const { foreign } = ctx.session;

    const { result } = await services['courses'].getCoursesAsync({
      foreign: [foreign],
    });

    const resultWithProfiles = await Promise.all(
      result.map(async course => {
        const oppositeForeign = course.foreign.find(
          f => f.typeHandle === 'USER' && f.id !== foreign.id
        );

        const { result: profile } = await services['profiles'].getProfileAsync({
          foreign: [oppositeForeign],
        });

        return { ...course, profile };
      })
    );

    ctx.body = { result: resultWithProfiles };
  });

  router.post('/getCourse', async ctx => {
    const { foreign } = ctx.session;

    const response = await services['courses'].getCourseAsync(ctx.request.body);

    const oppositeForeign = response.result.foreign.find(
      f => f.typeHandle === 'USER' && f.id !== foreign.id
    );

    const { result: profile } = await services['profiles'].getProfileAsync({
      foreign: [oppositeForeign],
    });

    response.result.profile = profile;

    ctx.body = response;
  });

  return router;
};
