const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getEvents', async ctx => {
    const response = await services['events'].getEventsAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/getEvent', async ctx => {
    const response = await services['events'].getEventAsync(ctx.request.body);

    ctx.body = response;
  });

  return router;
};
