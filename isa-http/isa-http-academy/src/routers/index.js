const addresses = require('./addresses');
const announcements = require('./announcements');
const authMethods = require('./authMethods');
const awards = require('./awards');
const courses = require('./courses');
const events = require('./events');
const jurors = require('./jurors');
const manifest = require('./manifest');
const payments = require('./payments');
const profiles = require('./profiles');
const register = require('./register');
const rooms = require('./rooms');
const session = require('./session');
const sources = require('./sources');
const votes = require('./votes');

module.exports = {
  addresses,
  announcements,
  authMethods,
  awards,
  courses,
  events,
  jurors,
  manifest,
  payments,
  profiles,
  register,
  rooms,
  session,
  sources,
  votes,
};
