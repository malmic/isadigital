const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getJurors', async ctx => {
    const { result } = await services['jurors'].getJurorsAsync({});

    ctx.body = { result };
  });

  return router;
};
