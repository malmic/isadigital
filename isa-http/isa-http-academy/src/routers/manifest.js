const Router = require('koa-router');

module.exports = () => {
  const router = new Router();
  router.get('/', async ctx => {
    ctx.body = { name: 'isa', short_name: 'isa' };
  });

  return router;
};
