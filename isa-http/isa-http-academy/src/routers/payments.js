const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getPayment', async ctx => {
    const { foreign } = ctx.session;

    const { result } = await services['payments'].getPaymentAsync({
      foreign: [foreign],
    });

    const { result: quantity } = await services['transforms'].toQuantityAsync(
      result.quantity
    );

    ctx.body = { result: { ...result, quantity } };
  });

  return router;
};
