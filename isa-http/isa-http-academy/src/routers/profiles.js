const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getProfiles', async ctx => {
    const response = await services['profiles'].getProfilesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
