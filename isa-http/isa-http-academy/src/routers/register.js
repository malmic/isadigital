const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/', async ctx => {
    try {
      const { emailAddress, password } = ctx.request.body;

      const { result: emailAddressExists } = await services[
        'emailAddresses'
      ].emailAddressExistsAsync({
        address: emailAddress,
      });

      if (emailAddressExists)
        ctx.throw(403, 'This email address already exists!');

      const { result: referenceId } = await services['users'].createUserAsync({
        languageHandle: 'EN',
        defaults: {},
        flags: {},
        validScopes: ['user.*'],
      });

      await services['emailAddresses'].createEmailAddressAsync({
        foreign: { id: referenceId, typeHandle: 'USER' },
        address: emailAddress,
      });

      const { result: passwordCreated } = await services[
        'password'
      ].createPasswordAsync({
        foreign: { id: referenceId, typeHandle: 'USER' },
        plaintextPassword: password,
        statusHandle: 'ACTIVATED',
      });

      if (!passwordCreated) {
        ctx.throw(500, 'The password could not be created.');
      }

      ctx.session.foreign = { id: referenceId, typeHandle: 'USER' };
    } catch (e) {
      console.log(e);
      ctx.redirect('/register');
    } finally {
      ctx.redirect('/');
    }
  });

  return router;
};
