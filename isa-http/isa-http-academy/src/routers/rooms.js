const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getRooms', async ctx => {
    const response = await services['rooms'].getRoomsAsync({});

    ctx.body = response;
  });

  router.post('/getRoom', async ctx => {
    const response = await services['rooms'].getRoomAsync(ctx.request.body);

    ctx.body = response;
  });

  return router;
};
