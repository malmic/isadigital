const Router = require('koa-router');

const getSanitizedRedirect = next => {
  return /^\/[a-zA-Z\-]+$/.test(next) ? next : '/';
};

module.exports = (env, services) => {
  const router = new Router();

  router.post('/login', async ctx => {
    try {
      const { username, password } = ctx.request.body;

      const { result: emailAddress } = await services[
        'emailAddresses'
      ].getEmailAddressAsync({ address: username });

      if (!emailAddress) ctx.throw(404, 'User not found!');

      const { foreign } = emailAddress;

      const { result: passwordsEqual } = await services[
        'password'
      ].passwordMatchesAsync({
        foreign,
        plaintextPassword: password,
      });

      if (!passwordsEqual) ctx.throw(403, 'Passwords are not equal!');

      ctx.session.foreign = foreign;
    } catch (e) {
      console.log(e);
      ctx.redirect('/login');
      return;
    }

    ctx.redirect(getSanitizedRedirect(ctx.request.body.next));
  });

  router.get('/delete', async ctx => {
    try {
      ctx.session = null;
    } catch (e) {
      console.log(e);
    } finally {
      ctx.redirect('/');
    }
  });

  router.get('/me', async ctx => {
    const { foreign } = ctx.session;

    const { result: user } = await services['users'].getUserAsync({
      referenceId: foreign.id,
    });
    const { result: emailAddresses } = await services[
      'emailAddresses'
    ].getEmailAddressesAsync({ foreign });
    const { result: profile } = await services['profiles'].getProfileAsync({
      foreign: [foreign],
    });

    const { result: sourceTypes } = await services[
      'sourceTypes'
    ].getSourceTypesAsync({});
    const { result: rooms } = await services['rooms'].getRoomsAsync({});

    ctx.body = {
      result: {
        emailAddresses,
        profile,
        rooms,
        sourceTypes,
        user,
      },
    };
  });

  return router;
};
