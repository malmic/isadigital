const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  const getOriginalSource = async (typeHandle, referenceId) => {
    if (typeHandle === 'YOUTUBE') {
      const { result: video } = await services['videos'].getVideoAsync({
        referenceId: referenceId,
      });

      return {
        title: video ? video.title : 'Title',
        description: video ? video.description : 'Description',
      };
    }

    return { title: 'Title', description: 'Description' };
  };

  router.post('/getSources', async ctx => {
    const response = await services['sources'].getSourcesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getSource', async ctx => {
    const response = await services['sources'].getSourceAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/createSource', async ctx => {
    const { body } = ctx.request;
    const { foreign } = ctx.session;

    const foreignFound = body.foreign.find(f => f.id === foreign.id);
    if (foreignFound) throw new Error('Cannot declare the user here.');

    const originalSource = await getOriginalSource(
      body.typeHandle,
      body.referenceId
    );

    const source = {
      ...ctx.request.body,
      ...originalSource,
      foreign: [...ctx.request.body.foreign, foreign],
    };

    await services['sources'].createSourceAsync(source);

    ctx.body = { result: source };
  });

  router.post('/deleteSource', async ctx => {
    const { referenceId } = ctx.request.body;

    const { result } = await services['sources'].deleteSourceAsync(
      ctx.request.body
    );

    await services['votes'].deleteVotesAsync({
      foreign: [{ id: referenceId, typeHandle: 'AWARD' }],
    });

    ctx.body = { result };
  });

  router.post('/updateSource', async ctx => {
    const { referenceId, statusHandle } = ctx.request.body;

    if (statusHandle === 'PENDING') {
      await services['votes'].deleteVotesAsync({
        foreign: [{ id: referenceId, typeHandle: 'SOURCE' }],
      });
    }

    const response = await services['sources'].updateSourceAsync({
      referenceId,
      statusHandle,
    });

    ctx.body = response;
  });

  return router;
};
