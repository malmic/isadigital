const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/createVote', async ctx => {
    const { foreign } = ctx.session;

    await services['votes'].deleteVotesAsync({
      foreign: [foreign],
    });

    const response = await services['votes'].createVoteAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/getVotes', async ctx => {
    const response = await services['votes'].getVotesAsync({});

    ctx.body = response;
  });

  router.post('/getVote', async ctx => {
    const { foreign } = ctx.session;

    const response = await services['votes'].getVoteAsync({
      foreign: [foreign],
    });

    ctx.body = response;
  });

  return router;
};
