const ADDRESS_FETCHED = 'ADDRESS_FETCHED';

const addressFetched = data => ({
  type: ADDRESS_FETCHED,
  data,
});

export { ADDRESS_FETCHED, addressFetched };
