const ANNOUNCEMENTS_FETCHED = 'ANNOUCEMENTS_FETCHED';

const announcementsFetched = data => ({
  type: ANNOUNCEMENTS_FETCHED,
  data,
});

export { ANNOUNCEMENTS_FETCHED, announcementsFetched };
