const AWARDS_FETCHED = 'AWARDS_FETCHED';

const awardsFetched = data => ({
  type: AWARDS_FETCHED,
  data,
});

export { AWARDS_FETCHED, awardsFetched };
