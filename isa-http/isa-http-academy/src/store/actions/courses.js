const COURSES_FETCHED = 'COURSES_FETCHED';
const COURSE_FETCHED = 'COURSE_FETCHED';

const coursesFetched = data => ({
  type: COURSES_FETCHED,
  data,
});

const courseFetched = data => ({
  type: COURSE_FETCHED,
  data,
});

export { COURSES_FETCHED, COURSE_FETCHED, coursesFetched, courseFetched };
