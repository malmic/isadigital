const EMAIL_ADDRESSES_FETCHED = 'EMAIL_ADDRESSES_FETCHED';

const emailAddressesFetched = data => ({
  type: EMAIL_ADDRESSES_FETCHED,
  data,
});

export { EMAIL_ADDRESSES_FETCHED, emailAddressesFetched };
