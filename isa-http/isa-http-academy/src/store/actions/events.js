const EVENTS_FETCHED = 'EVENTS_FETCHED';
const EVENT_FETCHED = 'EVENT_FETCHED';

const eventsFetched = data => ({
  type: EVENTS_FETCHED,
  data,
});

const eventFetched = data => ({
  type: EVENT_FETCHED,
  data,
});

export { EVENTS_FETCHED, EVENT_FETCHED, eventsFetched, eventFetched };
