import * as addresses from './addresses';
import * as announcements from './announcements';
import * as awards from './awards';
import * as courses from './courses';
import * as emailAddresses from './emailAddresses';
import * as events from './events';
import * as jurors from './jurors';
import * as maps from './maps';
import * as notifications from './notifications';
import * as payments from './payments';
import * as profiles from './profiles';
import * as rooms from './rooms';
import * as session from './session';
import * as sources from './sources';
import * as votes from './votes';

export {
  addresses,
  announcements,
  awards,
  courses,
  emailAddresses,
  events,
  jurors,
  maps,
  notifications,
  payments,
  profiles,
  rooms,
  session,
  sources,
  votes,
};
