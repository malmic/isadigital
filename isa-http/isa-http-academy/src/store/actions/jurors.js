const JURORS_FETCHED = 'JURORS_FETCHED';

const jurorsFetched = data => ({
  type: JURORS_FETCHED,
  data,
});

export { JURORS_FETCHED, jurorsFetched };
