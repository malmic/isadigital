const FEATURE_FETCHED = 'FEATURE_FETCHED';

const featureFetched = data => ({
  type: FEATURE_FETCHED,
  data,
});

export { FEATURE_FETCHED, featureFetched };
