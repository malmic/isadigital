const PAYMENT_FETCHED = 'PAYMENT_FETCHED';

const paymentFetched = data => ({
  type: PAYMENT_FETCHED,
  data,
});

export { PAYMENT_FETCHED, paymentFetched };
