const PROFILE_FETCHED = 'PROFILE_FETCHED';
const PROFILES_FETCHED = 'PROFILES_FETCHED';

const profileFetched = data => ({
  type: PROFILE_FETCHED,
  data,
});

const profilesFetched = data => ({
  type: PROFILES_FETCHED,
  data,
});

export { profileFetched, profilesFetched, PROFILE_FETCHED, PROFILES_FETCHED };
