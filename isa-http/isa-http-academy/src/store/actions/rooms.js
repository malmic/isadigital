const ROOMS_FETCHED = 'ROOMS_FETCHED';
const ROOM_FETCHED = 'ROOM_FETCHED';

const roomsFetched = data => ({
  type: ROOMS_FETCHED,
  data,
});

const roomFetched = data => ({
  type: ROOM_FETCHED,
  data,
});

export { ROOMS_FETCHED, ROOM_FETCHED, roomsFetched, roomFetched };
