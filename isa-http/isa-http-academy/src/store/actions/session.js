const SESSION_CREATED = 'SESSION_CREATED';

const sessionCreated = session => ({
  type: SESSION_CREATED,
  session,
});

export { sessionCreated, SESSION_CREATED };
