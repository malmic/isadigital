const SOURCE_TYPES_FETCHED = 'SOURCE_TYPES_FETCHED';
const SOURCES_FETCHED = 'SOURCES_FETCHED';
const SOURCE_CREATED = 'SOURCE_CREATED';
const SOURCE_DELETED = 'SOURCE_DELETED';
const SOURCE_UPDATED = 'SOURCE_UPDATED';
const SOURCE_FETCHED = 'SOURCE_FETCHED';

const sourceTypesFetched = data => ({
  type: SOURCE_TYPES_FETCHED,
  data,
});

const sourcesFetched = data => ({
  type: SOURCES_FETCHED,
  data,
});

const sourceCreated = data => ({
  type: SOURCE_CREATED,
  data,
});

const sourceDeleted = data => ({
  type: SOURCE_DELETED,
  data,
});

const sourceUpdated = data => ({
  type: SOURCE_UPDATED,
  data,
});

const sourceFetched = data => ({
  type: SOURCE_FETCHED,
  data,
});

export {
  SOURCE_TYPES_FETCHED,
  SOURCES_FETCHED,
  SOURCE_CREATED,
  SOURCE_DELETED,
  SOURCE_UPDATED,
  SOURCE_FETCHED,
  sourceTypesFetched,
  sourcesFetched,
  sourceCreated,
  sourceDeleted,
  sourceUpdated,
  sourceFetched,
};
