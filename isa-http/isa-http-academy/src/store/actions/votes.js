const VOTE_CREATED = 'VOTE_CREATED';
const VOTE_FETCHED = 'VOTE_FETCHED';
const VOTES_FETCHED = 'VOTES_FETCHED';

const voteFetched = data => ({
  type: VOTE_FETCHED,
  data,
});

const voteCreated = data => ({
  type: VOTE_CREATED,
  data,
});

const votesFetched = data => ({
  type: VOTES_FETCHED,
  data,
});

export {
  voteCreated,
  voteFetched,
  votesFetched,
  VOTE_FETCHED,
  VOTE_CREATED,
  VOTES_FETCHED,
};
