import axios from 'axios';
import qs from 'query-string';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import getConfig from 'next/config';

import initialState from './initialState';
import rootReducer from './reducers';
import {
  Addresses,
  Announcements,
  AuthMethods,
  Awards,
  Courses,
  Events,
  Jurors,
  Maps,
  Payments,
  Profiles,
  Rooms,
  Sources,
  Votes,
} from './thunks';

const { publicRuntimeConfig } = getConfig();
const app = axios.create({
  baseURL: '',
  withCredentials: true,
  paramsSerializer: params => qs.stringify(params),
});

const thunks = {
  addresses: new Addresses(app),
  announcements: new Announcements(app),
  authMethods: new AuthMethods(app),
  awards: new Awards(app),
  courses: new Courses(app),
  events: new Events(app),
  jurors: new Jurors(app),
  maps: new Maps(publicRuntimeConfig.MAPBOX.ACCESS_TOKEN),
  payments: new Payments(app),
  profiles: new Profiles(app),
  rooms: new Rooms(app),
  sources: new Sources(app),
  votes: new Votes(app),
};

const createClientStore = (state = initialState) => {
  return createStore(
    rootReducer,
    state,
    composeWithDevTools(applyMiddleware(thunk))
  );
};

export { thunks, createClientStore };
