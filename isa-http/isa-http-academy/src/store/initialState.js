export default {
  addresses: { address: {} },
  announcements: {
    data: [],
  },
  awards: { data: [] },
  courses: {
    data: [],
    course: { referenceId: '' },
  },
  emailAddresses: {
    data: [],
    emailAddress: {
      address: '',
      verified: false,
    },
  },
  events: {
    data: [],
    event: {
      name: '',
      beginAt: new Date().toString(),
      professors: [],
    },
  },
  jurors: [],
  maps: { feature: null },
  notifications: [],
  payments: {
    payment: {
      name: '',
      quantity: {
        handle: '',
        value: 0,
      },
    },
  },
  profiles: {
    data: [],
    profile: {},
  },
  rooms: {
    data: [],
    room: {
      referenceId: '',
      name: '',
      description: '',
      backgroundImage: '',
      backgroundColor: '',
      foreign: [],
    },
  },
  session: {},
  sources: {
    data: [],
    types: [],
    source: { title: '', description: '', typeHandle: '' },
  },
  votes: {
    vote: { foreign: [] },
    data: [],
  },
};
