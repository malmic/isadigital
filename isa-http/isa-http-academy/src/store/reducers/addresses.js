import initialState from '../initialState';
import { addresses } from '../actions';

export default (state = initialState.addresses, action) => {
  switch (action.type) {
    case addresses.ADDRESS_FETCHED: {
      const address = !action.data
        ? { ...initialState.addresses.address }
        : { ...action.data };

      return { ...state, address };
    }

    default:
      return state;
  }
};
