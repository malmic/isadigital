import initialState from '../initialState';
import { announcements } from '../actions';

export default (state = initialState.announcements, action) => {
  switch (action.type) {
    case announcements.ANNOUNCEMENTS_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    default:
      return state;
  }
};
