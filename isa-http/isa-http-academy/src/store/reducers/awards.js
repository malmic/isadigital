import initialState from '../initialState';
import { awards } from '../actions';

export default (state = initialState.awards, action) => {
  switch (action.type) {
    case awards.AWARDS_FETCHED: {
      return { ...state, data: action.data };
    }

    default:
      return state;
  }
};
