import initialState from '../initialState';
import { courses } from '../actions';

export default (state = initialState.courses, action) => {
  switch (action.type) {
    case courses.COURSES_FETCHED: {
      return {
        ...state,
        data: action.data,
        course: action.data.length
          ? action.data[0]
          : initialState.courses.course,
      };
    }

    case courses.COURSE_FETCHED: {
      const courseCopy = !action.data
        ? { ...initialState.courses.course }
        : { ...action.data };

      return { ...state, course: courseCopy };
    }

    case courses.COURSE_SOURCE_CREATED: {
      return {
        ...state,
        course: {
          ...state.course,
          sources: [...state.course.sources, action.data],
        },
      };
    }

    case courses.COURSE_SOURCE_DELETED: {
      const copy = [...state.course.sources];

      const foundIndex = copy.findIndex(
        p => p.referenceId === action.data.referenceId
      );

      if (foundIndex > -1) {
        copy.splice(foundIndex, 1);
      }

      return {
        ...state,
        course: {
          ...state.course,
          sources: copy,
        },
      };
    }

    default:
      return state;
  }
};
