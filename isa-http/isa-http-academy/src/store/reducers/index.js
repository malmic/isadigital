import { combineReducers } from 'redux';

import addresses from './addresses';
import announcements from './announcements';
import awards from './awards';
import courses from './courses';
import emailAddresses from './emailAddresses';
import events from './events';
import jurors from './jurors';
import maps from './maps';
import notifications from './notifications';
import payments from './payments';
import profiles from './profiles';
import rooms from './rooms';
import session from './session';
import sources from './sources';
import votes from './votes';

export default combineReducers({
  addresses,
  announcements,
  awards,
  courses,
  emailAddresses,
  events,
  jurors,
  maps,
  notifications,
  payments,
  profiles,
  rooms,
  session,
  sources,
  votes,
});
