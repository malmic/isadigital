import initialState from '../initialState';
import { jurors } from '../actions';

export default (state = initialState.jurors, action) => {
  switch (action.type) {
    case jurors.JURORS_FETCHED: {
      return [...state, ...action.data];
    }

    default:
      return state;
  }
};
