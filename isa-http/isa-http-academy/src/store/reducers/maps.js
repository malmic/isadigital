import initialState from '../initialState';
import { maps } from '../actions';

export default (state = initialState.maps, action) => {
  switch (action.type) {
    case maps.FEATURE_FETCHED: {
      return {
        ...state,
        feature: action.data,
      };
    }

    default:
      return state;
  }
};
