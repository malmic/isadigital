import initialState from '../initialState';
import { payments } from '../actions';

export default (state = initialState.payments, action) => {
  switch (action.type) {
    case payments.PAYMENT_FETCHED: {
      const payment = !action.data
        ? { ...initialState.payments.payment }
        : { ...action.data };

      return { ...state, payment };
    }

    default:
      return state;
  }
};
