import initialState from '../initialState';
import { profiles } from '../actions';

export default (state = initialState.profiles, action) => {
  switch (action.type) {
    case profiles.PROFILES_FETCHED: {
      return { ...state, data: action.data };
    }

    case profiles.PROFILE_FETCHED: {
      const profile = !action.data
        ? { ...initialState.profiles.profile }
        : { ...action.data };

      return { ...state, profile };
    }

    default:
      return state;
  }
};
