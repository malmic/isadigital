import initialState from '../initialState';
import { rooms } from '../actions';

export default (state = initialState.rooms, action) => {
  switch (action.type) {
    case rooms.ROOMS_FETCHED: {
      return { ...state, data: action.data };
    }

    case rooms.ROOM_FETCHED: {
      return { ...state, room: action.data };
    }

    default:
      return state;
  }
};
