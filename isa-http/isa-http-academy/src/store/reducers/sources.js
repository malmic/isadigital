import initialState from '../initialState';
import { sources } from '../actions';

export default (state = initialState.sources, action) => {
  switch (action.type) {
    case sources.SOURCE_TYPES_FETCHED: {
      return { ...state, types: action.data };
    }

    case sources.SOURCES_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case sources.SOURCE_FETCHED: {
      return { ...state, source: { ...action.data } };
    }

    case sources.SOURCE_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case sources.SOURCE_DELETED: {
      const copy = [...state.data];

      const foundIndex = copy.findIndex(
        p => p.referenceId === action.data.referenceId
      );

      if (foundIndex > -1) {
        copy.splice(foundIndex, 1);
      }

      return { ...state, data: copy };
    }

    case sources.SOURCE_UPDATED: {
      const copy = [...state.data];

      const foundIndex = copy.findIndex(
        p => p.referenceId === action.data.referenceId
      );

      copy[foundIndex] = { ...copy[foundIndex], ...action.data };

      return { ...state, data: copy };
    }

    default:
      return state;
  }
};
