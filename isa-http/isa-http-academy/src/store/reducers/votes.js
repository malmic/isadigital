import initialState from '../initialState';
import { votes, sources } from '../actions';

export default (state = initialState.votes, action) => {
  switch (action.type) {
    case votes.VOTE_FETCHED: {
      const vote = !action.data
        ? { ...initialState.votes.vote }
        : { ...action.data };

      return { ...state, vote };
    }

    case sources.SOURCE_DELETED: {
      const sourceWasVoted = state.vote.foreign.find(
        f => f.id === action.data.referenceId && f.typeHandle === 'SOURCE'
      );

      const copy = [...state.data].filter(v => {
        const existsIndex = v.foreign.findIndex(
          f => f.id === action.data.referenceId && f.typeHandle === 'SOURCE'
        );

        return !(existsIndex > -1);
      });

      return {
        ...state,
        data: copy,
        vote: sourceWasVoted ? initialState.votes.vote : state.vote,
      };
    }

    case sources.SOURCE_UPDATED: {
      if (action.data.statusHandle !== 'PENDING') return state;

      const sourceWasVoted = state.vote.foreign.find(
        f => f.id === action.data.referenceId && f.typeHandle === 'SOURCE'
      );

      const copy = [...state.data].filter(v => {
        const existsIndex = v.foreign.findIndex(
          f => f.id === action.data.referenceId && f.typeHandle === 'SOURCE'
        );

        return !(existsIndex > -1);
      });

      return {
        ...state,
        data: copy,
        vote: sourceWasVoted ? initialState.votes.vote : state.vote,
      };
    }

    case votes.VOTES_FETCHED: {
      return { ...state, data: action.data };
    }

    case votes.VOTE_CREATED: {
      const copy = [...state.data];

      const userForeign = action.data.foreign.find(
        f => f.typeHandle === 'USER'
      );

      const existingVoteIndex = copy.findIndex(v =>
        v.foreign.find(f => f.id === userForeign.id)
      );

      if (existingVoteIndex > -1) {
        copy.splice(existingVoteIndex, 1);
      }

      copy.push(action.data);

      return { ...state, data: copy, vote: { ...action.data } };
    }

    default:
      return state;
  }
};
