import { addresses, notifications } from '../actions';

export default class Addresses {
  constructor(app) {
    this.app = app;
  }

  getAddress(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/addresses/getAddress', query);

        dispatch(addresses.addressFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
