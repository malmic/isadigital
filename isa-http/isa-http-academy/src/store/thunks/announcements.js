import { announcements, notifications } from '../actions';

export default class Announcements {
  constructor(app) {
    this.app = app;
  }

  getAnnouncements(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/announcements/getAnnouncements', query);

        dispatch(announcements.announcementsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
