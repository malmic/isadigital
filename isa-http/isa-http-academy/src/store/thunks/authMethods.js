import { notifications } from '../actions';

export default class AuthMethods {
  constructor(app) {
    this.app = app;
  }

  updatePassword(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/authMethods/updatePassword', query);

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
