import { awards, notifications } from '../actions';

export default class Awards {
  constructor(app) {
    this.app = app;
  }

  getAwards() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/awards/getAwards');

        dispatch(awards.awardsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
