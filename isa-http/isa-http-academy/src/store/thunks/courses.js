import { courses, notifications } from '../actions';

export default class Courses {
  constructor(app) {
    this.app = app;
  }

  getCourses() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/courses/getCourses');

        dispatch(courses.coursesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getCourse(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/courses/getCourse', query);

        dispatch(courses.courseFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
