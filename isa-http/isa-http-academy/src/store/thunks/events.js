import { events, notifications } from '../actions';

export default class Events {
  constructor(app) {
    this.app = app;
  }

  getEvents(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/events/getEvents', query);

        dispatch(events.eventsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getEvent(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/events/getEvent', query);

        dispatch(events.eventFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
