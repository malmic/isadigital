import { jurors, notifications } from '../actions';

export default class Jurors {
  constructor(app) {
    this.app = app;
  }

  getJurors() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/jurors/getJurors');

        dispatch(jurors.jurorsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
