import mapboxGeocoding from '@mapbox/mapbox-sdk/services/geocoding';

import { maps, notifications } from '../actions';

export default class Maps {
  constructor(accessToken) {
    this.mapboxClient = mapboxGeocoding({ accessToken });
  }

  queryLocation(query) {
    return async dispatch => {
      try {
        const response = await this.mapboxClient
          .forwardGeocode({
            query,
            autocomplete: false,
            limit: 1,
          })
          .send();

        if (
          response &&
          response.body &&
          response.body.features &&
          response.body.features.length
        ) {
          const feature = response.body.features[0];

          dispatch(maps.featureFetched(feature));
        }
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
