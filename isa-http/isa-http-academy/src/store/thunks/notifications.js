import { notifications } from '../actions';

export default class Notifications {
  createNotification(type, message, duration = 5000) {
    return dispatch => {
      dispatch(notifications.notificationCreated(type, message, duration));
    };
  }

  removeNotification(handle) {
    return dispatch => {
      dispatch(notifications.notificationRemoved(handle));
    };
  }
}
