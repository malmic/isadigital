import { payments, notifications } from '../actions';

export default class Payments {
  constructor(app) {
    this.app = app;
  }

  getPayment() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/payments/getPayment');

        dispatch(payments.paymentFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
