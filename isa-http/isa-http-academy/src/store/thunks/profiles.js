import { profiles, notifications } from '../actions';

export default class Profiles {
  constructor(app) {
    this.app = app;
  }

  getProfiles(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/profiles/getProfiles', query);

        dispatch(profiles.profilesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
