import { rooms, notifications } from '../actions';

export default class Rooms {
  constructor(app) {
    this.app = app;
  }

  getRooms() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/rooms/getRooms');

        dispatch(rooms.roomsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getRoom(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/rooms/getRoom', query);

        dispatch(rooms.roomFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
