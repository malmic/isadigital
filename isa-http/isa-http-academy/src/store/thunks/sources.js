import { sources, notifications } from '../actions';

export default class Sources {
  constructor(app) {
    this.app = app;
  }

  getSources(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/sources/getSources', query);

        dispatch(sources.sourcesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getSource(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/sources/getSource', query);

        dispatch(sources.sourceFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createSource(query) {
    return async dispatch => {
      try {
        const {
          data: { result: source },
        } = await this.app.post('/sources/createSource', query);

        dispatch(sources.sourceCreated(source));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteSource(query) {
    return async dispatch => {
      try {
        await this.app.post('/sources/deleteSource', query);

        dispatch(sources.sourceDeleted(query));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateSource(query) {
    return async dispatch => {
      try {
        await this.app.post('/sources/updateSource', query);

        dispatch(sources.sourceUpdated(query));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
