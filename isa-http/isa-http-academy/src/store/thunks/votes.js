import { votes, notifications } from '../actions';

export default class Votes {
  constructor(app) {
    this.app = app;
  }

  createVote(query) {
    return async dispatch => {
      try {
        const {
          data: { result: referenceId },
        } = await this.app.post('/votes/createVote', query);

        dispatch(votes.voteCreated({ ...query, referenceId }));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getVotes() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/votes/getVotes');

        dispatch(votes.votesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getVote() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/votes/getVote');

        dispatch(votes.voteFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
