import { array, func } from 'prop-types';
import { useState, useMemo, useEffect } from 'react';
import {
  Form,
  FormGroup,
  InputGroup,
  Input,
  InputGroupAddon,
  Button,
} from 'reactstrap';

const parseSource = url => {
  const isYoutubeShortened = url.match(/\/youtu.be\/(.*)$/);
  if (isYoutubeShortened)
    return { typeHandle: 'YOUTUBE', referenceId: isYoutubeShortened[1] };

  const isZoom = url.match(/\/zoom.us\/j\/([0-9]+)$/);
  if (isZoom) return { typeHandle: 'ZOOM', referenceId: isZoom[1] };

  const isYoutube = url.match(/youtube\.com\/watch\?v\=(.*)$/);
  if (isYoutube) return { typeHandle: 'YOUTUBE', referenceId: isYoutube[1] };

  const isGdrive = url.match(/drive\.google\.com\/file\/d\/([^\/]*)\//);
  if (isGdrive) return { typeHandle: 'GDRIVE', referenceId: isGdrive[1] };

  return null;
};

const CreateSource = ({ createSource, foreigns }) => {
  const [url, setUrl] = useState('');
  const [foreign, setForeign] = useState('');

  useEffect(() => {
    if (foreigns.length > 0) setForeign(foreigns[0].id);
  }, [foreigns]);

  const sourceValid = useMemo(() => {
    const s = parseSource(url);
    return !!s;
  }, [url]);

  return (
    <Form>
      <FormGroup>
        <InputGroup>
          <Input
            className="font-weight-bold"
            type="text"
            value={url}
            placeholder="URL"
            onChange={e => setUrl(e.target.value)}
            name="url"
          />
          <Input
            className="text-uppercase font-weight-bold"
            type="select"
            name="foreign"
            value={foreign}
            onChange={e => setForeign(e.target.value)}
          >
            {foreigns.map(foreign => (
              <option key={foreign.id} value={foreign.id}>
                {foreign.name}
              </option>
            ))}
          </Input>
          <InputGroupAddon addonType="append">
            <Button
              className="text-uppercase font-weight-bold"
              block
              disabled={!sourceValid}
              onClick={() => {
                const selectedForeign = foreigns.find(f => f.id === foreign);

                createSource(parseSource(url), {
                  id: selectedForeign.id,
                  typeHandle: selectedForeign.typeHandle,
                });
              }}
            >
              Upload
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </FormGroup>
    </Form>
  );
};

CreateSource.propTypes = {
  createSource: func.isRequired,
  foreigns: array.isRequired,
};

export default CreateSource;
