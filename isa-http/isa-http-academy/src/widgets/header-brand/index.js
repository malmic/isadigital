import Link from 'next/link';
import styled from 'styled-components';
import { string } from 'prop-types';

const HeaderBrand = ({ className }) => {
  return (
    <Link href={'/'}>
      <a className={['text-uppercase', 'navbar-brand', className].join(' ')}>
        Aula
      </a>
    </Link>
  );
};

HeaderBrand.propTypes = {
  className: string.isRequired,
};

export default styled(HeaderBrand)`
  font-weight: 700;
`;
