import React from 'react';
import { node, string } from 'prop-types';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { NavLink } from 'reactstrap';
import styled from 'styled-components';

const HeaderLink = ({ to, children, className }) => {
  const router = useRouter();
  const active = to === router.pathname;

  return (
    <Link href={to} passHref>
      <NavLink
        className={['text-uppercase', className].join(' ')}
        active={active}
      >
        {children}
      </NavLink>
    </Link>
  );
};

HeaderLink.propTypes = {
  children: node.isRequired,
  to: string.isRequired,
  className: string.isRequired,
};

export default styled(HeaderLink)`
  font-weight: 700;
`;
