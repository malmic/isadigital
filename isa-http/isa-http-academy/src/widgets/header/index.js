import React, { useState } from 'react';
import { string, node } from 'prop-types';
import { Collapse, Navbar, NavbarToggler } from 'reactstrap';
import styled from 'styled-components';

import HeaderBrand from '../header-brand';

const Header = ({ className, children }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Navbar expand="lg" light className={className}>
      <HeaderBrand />
      <NavbarToggler onClick={() => setIsOpen(!isOpen)} className="ml-auto" />
      <Collapse isOpen={isOpen} navbar>
        {children}
      </Collapse>
    </Navbar>
  );
};

Header.propTypes = {
  className: string.isRequired,
  children: node.isRequired,
};

export default styled(Header)`
  background-color: #ffffff;
  padding-left: 50px;
  padding-right: 50px;
`;
