import React, { useRef, useEffect } from 'react';
import { string } from 'prop-types';
import { Button, Form, FormGroup, Input } from 'reactstrap';

const LoginForm = ({ next }) => {
  const usernameRef = useRef();

  useEffect(() => {
    usernameRef.current.focus();
  }, []);

  return (
    <Form method="post" action="/session/login" className="text-center">
      <input type="hidden" name="next" value={next} />
      <FormGroup>
        <Input
          innerRef={usernameRef}
          className="font-weight-bold"
          type="username"
          placeholder="Email Address"
          name="username"
        />
      </FormGroup>
      <FormGroup>
        <Input
          className="font-weight-bold"
          type="password"
          placeholder="Password"
          name="password"
        />
      </FormGroup>
      <Button className="text-uppercase font-weight-bold" type="submit">
        Enter
      </Button>
    </Form>
  );
};

LoginForm.propTypes = {
  next: string,
};

export default LoginForm;
