import React, { useEffect } from 'react';
import { array, func } from 'prop-types';
import { Alert } from 'reactstrap';

const timeouts = {};

const typeToColor = {
  ERROR: 'danger',
  SUCCESS: 'info',
};

const Notifications = ({ notifications, removeNotification }) => {
  useEffect(() => {
    const isServer = typeof window === 'undefined';

    if (!isServer) {
      for (const notification of notifications) {
        if (
          !(notification.handle in timeouts) &&
          notification.duration !== 'Infinity'
        )
          timeouts[notification.handle] = window.setTimeout(() => {
            removeNotification(notification.handle);
            delete timeouts[notification.handle];
          }, notification.duration);
      }
    }
  }, [notifications, removeNotification]);

  return (
    <>
      {notifications.map(n => {
        return (
          <Alert color={typeToColor[n.type]} key={n.handle}>
            {n.message}
          </Alert>
        );
      })}
    </>
  );
};

Notifications.propTypes = {
  notifications: array.isRequired,
  removeNotification: func.isRequired,
};

export default Notifications;
