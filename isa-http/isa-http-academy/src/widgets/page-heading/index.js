import { node, string } from 'prop-types';
import styled from 'styled-components';

const PageHeading = ({ children, className }) => {
  return (
    <h3 className={['text-uppercase', 'text-center', className].join(' ')}>
      {children}
    </h3>
  );
};

PageHeading.propTypes = {
  children: node.isRequired,
  className: string.isRequired,
};

export default styled(PageHeading)`
  color: ${props => props.color || '#ffffff'};
  font-weight: 600;
`;
