import React, { useEffect, useMemo, useCallback } from 'react';
import { func, object, array } from 'prop-types';
import { Row, Col, Button, Media } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import moment from 'moment';

import {
  ViewRooms,
  PageHeading,
  ViewSession,
  ViewSource,
  ViewProfile,
} from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const PrivateIndex = ({
  dispatch,
  room,
  events,
  rooms,
  emailAddresses,
  sources,
  source,
  profiles,
  router,
  announcements,
}) => {
  useEffect(() => {
    const referenceId =
      router.query.id || rooms.find(r => r.default).referenceId;
    const foreign = { id: referenceId, typeHandle: 'ROOM' };

    dispatch(thunks.rooms.getRoom({ referenceId }));
    dispatch(thunks.sources.getSources({ foreign: [foreign] }));
    dispatch(thunks.events.getEvents({ foreign: [foreign] }));
    dispatch(thunks.announcements.getAnnouncements({ foreign: [foreign] }));
  }, [dispatch, room.referenceId, rooms, router, router.query.id]);

  const backgroundColor = room.backgroundColor;

  const staticRoomSource = useMemo(() => {
    const roomSource = room.foreign.find(f => f.typeHandle === 'SOURCE');

    if (roomSource) return sources.find(s => s.referenceId === roomSource.id);

    return null;
  }, [room.foreign, sources]);

  const eventSources = useMemo(() => {
    const u = [...sources].reduce((acc, cur) => {
      let eventfound = false;
      for (const k of cur.foreign) {
        if (k.typeHandle === 'EVENT') {
          const event = events.find(e => e.referenceId === k.id);
          eventfound = true;

          if (event) acc.push({ ...cur, event });
        }
      }

      if (!eventfound) {
        const roomSource = room.foreign.find(
          f => f.typeHandle === 'SOURCE' && f.id === cur.referenceId
        );

        if (!roomSource) acc.push(cur);
      }

      return acc;
    }, []);

    u.sort((a, b) => {
      if (a.event && b.event) {
        const q = moment(new Date(a.event.beginAt)).unix();
        const r = moment(new Date(b.event.beginAt)).unix();

        if (q < r) return -1;
        if (r > q) return 1;
      }

      return 0;
    });

    return u;
  }, [events, room.foreign, sources]);

  useEffect(() => {
    if (eventSources.length === 0) return;

    if (eventSources[0].event) {
      const referenceId = eventSources[0].event.referenceId;
      const foreign = { id: referenceId, typeHandle: 'EVENT' };

      dispatch(thunks.profiles.getProfiles({ foreign: [foreign] }));
      dispatch(thunks.events.getEvent({ referenceId }));
    } else {
      const referenceId = eventSources[0].referenceId;
      const foreign = { id: referenceId, typeHandle: 'SOURCE' };

      dispatch(thunks.profiles.getProfiles({ foreign: [foreign] }));
    }

    dispatch(
      thunks.sources.getSource({ referenceId: eventSources[0].referenceId })
    );
  }, [dispatch, eventSources, events, events.length]);

  const eventSourceClicked = eventSource => {
    if (eventSource.event) {
      const referenceId = eventSource.event.referenceId;
      const foreign = { id: referenceId, typeHandle: 'EVENT' };

      dispatch(thunks.profiles.getProfiles({ foreign: [foreign] }));
      dispatch(thunks.events.getEvent({ referenceId }));
    }

    dispatch(
      thunks.sources.getSource({
        referenceId: eventSource.referenceId,
      })
    );
  };

  const getURL = useCallback(source => {
    if (source.typeHandle === 'YOUTUBE')
      return `https://youtu.be/${source.referenceId}`;
    if (source.typeHandle === 'ZOOM')
      return `https://zoom.us/j/${source.referenceId}`;
    if (source.typeHandle === 'GDRIVE')
      return `https://drive.google.com/file/d/${source.referenceId}/view`;
    return '';
  }, []);

  return (
    <PrivateLayout backgroundImage={room.backgroundImage}>
      <Row>
        <Col md="4" className="align-self-center">
          <ViewSession emailAddresses={emailAddresses} />
        </Col>
        <Col md="4" className="align-self-center text-center">
          <PageHeading>{room.name}</PageHeading>
        </Col>
        <Col md="4" className="align-self-center text-center">
          {staticRoomSource && (
            <Button
              className="align-middle text-uppercase text-center font-weight-bold"
              target="_blank"
              rel="noopener noreferrer"
              href={getURL(staticRoomSource)}
            >
              Zoom
            </Button>
          )}
        </Col>
      </Row>
      <div style={{ paddingTop: '50px' }} />
      <Row>
        <Col md="6">
          <Row>
            <Col>
              <ViewSource backgroundColor={backgroundColor} source={source} />
              <h6 className="text-uppercase">Participants</h6>
              <hr />
              <div className="mt-3" />
              {profiles.map(profile => (
                <ViewProfile
                  backgroundColor={backgroundColor}
                  key={profile.referenceId}
                  profile={profile}
                />
              ))}
              <h6 className="text-uppercase">Announcements</h6>
              <hr />
              <div className="mt-3" />
              {announcements.map(announcement => (
                <Media
                  key={announcement.referenceId}
                  style={{ paddingBottom: '15px' }}
                >
                  <Media
                    left
                    style={{
                      backgroundColor,
                      width: '125px',
                      height: '100px',
                      marginRight: '15px',
                    }}
                  >
                    <Media alt="Generic placeholder image" />
                  </Media>
                  <Media body className="text-white">
                    <span className="text-uppercase">
                      {moment(new Date(announcement.createdAt))
                        .local()
                        .format('dddd')}
                    </span>
                    {',  '}
                    <span style={{ fontSize: '0.7em', fontWeight: '800' }}>
                      {moment(new Date(announcement.createdAt))
                        .local()
                        .format('DD.MM.YYYY')}{' '}
                      /{' '}
                      {moment(new Date(announcement.createdAt))
                        .local()
                        .format('HH.mm')}{' '}
                    </span>
                    <br />

                    <span style={{ fontWeight: '800' }}>
                      {announcement.title}
                    </span>

                    <br />
                    <span className="text-uppercase">{announcement.body}</span>
                  </Media>
                </Media>
              ))}
            </Col>
          </Row>
        </Col>
        <Col md="6">
          <Row>
            <Col>
              <h6 className="text-uppercase">rooms</h6>
              <hr />
              <div className="mt-3" />
              <ViewRooms
                roomSelected={id => {
                  router.push({ pathname: '/', query: { id } });
                }}
                rooms={rooms}
                selectedRoom={room.referenceId}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="mt-3" />
              <h6 className="text-uppercase">{room.description}</h6>
              <hr />
              <div className="mt-3" />
              {eventSources.map(source => (
                <Media
                  key={
                    source.event ? source.event.referenceId : source.referenceId
                  }
                  style={{ paddingBottom: '15px' }}
                >
                  <Media
                    left
                    href="#"
                    onClick={() => {
                      eventSourceClicked(source);
                    }}
                    style={{
                      backgroundColor,
                      width: '125px',
                      height: '100px',
                      marginRight: '15px',
                    }}
                  >
                    <Media alt="Generic placeholder image" />
                  </Media>
                  <Media body className="text-white">
                    {source.event && (
                      <>
                        <span className="text-uppercase">
                          {moment(new Date(source.event.beginAt))
                            .local()
                            .format('dddd')}
                        </span>
                        {',  '}
                        <span style={{ fontSize: '0.7em', fontWeight: '800' }}>
                          {moment(new Date(source.event.beginAt))
                            .local()
                            .format('DD.MM.YYYY')}{' '}
                          /{' '}
                          {moment(new Date(source.event.beginAt))
                            .local()
                            .format('HH.mm')}{' '}
                        </span>
                        <br />
                      </>
                    )}
                    <span style={{ fontWeight: '800' }}>
                      {source.event ? source.event.name : source.title}
                    </span>
                    {source.description ? (
                      <>
                        <br />
                        <span className="text-uppercase">
                          {`${source.description.substring(0, 100)}...`}
                        </span>
                      </>
                    ) : (
                      ''
                    )}
                  </Media>
                </Media>
              ))}
            </Col>
          </Row>
        </Col>
      </Row>
      <div className="mt-3" />
    </PrivateLayout>
  );
};

PrivateIndex.propTypes = {
  dispatch: func.isRequired,
  room: object.isRequired,
  events: array.isRequired,
  rooms: array.isRequired,
  emailAddresses: array.isRequired,
  sources: array.isRequired,
  source: object.isRequired,
  profiles: array.isRequired,
  router: object.isRequired,
  announcements: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  room: state.rooms.room,
  events: state.events.data,
  rooms: state.rooms.data,
  sources: state.sources.data,
  source: state.sources.source,
  emailAddresses: state.emailAddresses.data,
  profiles: state.profiles.data,
  announcements: state.announcements.data,
}))(privateCheck(PrivateIndex));
