import React from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';

const RegisterForm = () => {
  return (
    <Form method="post" action="/register">
      <FormGroup>
        <Input
          className="font-weight-bold"
          type="username"
          placeholder="Email Address"
          name="emailAddress"
        />
      </FormGroup>
      <FormGroup>
        <Input
          className="font-weight-bold"
          type="password"
          placeholder="Password"
          name="password"
        />
      </FormGroup>
      <Button
        className="text-uppercase font-weight-bold"
        block
        color="primary"
        type="submit"
      >
        Register
      </Button>
    </Form>
  );
};

export default RegisterForm;
