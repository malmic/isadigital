import { Media } from 'reactstrap';
import { object, string } from 'prop-types';

const ViewProfile = ({ backgroundColor, profile }) => {
  return (
    <Media key={profile.referenceId} style={{ paddingBottom: '15px' }}>
      <Media
        left
        style={{
          backgroundColor,
          width: '125px',
          height: '100px',
          marginRight: '15px',
        }}
      >
        <Media alt="Generic placeholder image" />
      </Media>
      <Media body className="text-white">
        <span style={{ fontWeight: '800' }} className="text-uppercase">
          {profile.fullName}
        </span>
        <br />
        <span className="text-uppercase">
          {profile.description
            ? `${profile.description.substring(0, 150)}...`
            : ''}
        </span>
      </Media>
    </Media>
  );
};

ViewProfile.propTypes = {
  profile: object.isRequired,
  backgroundColor: string.isRequired,
};

export default ViewProfile;
