import { useCallback } from 'react';
import { array, string, func } from 'prop-types';
import { CardColumns, Card, CardBody, CardTitle } from 'reactstrap';
import Link from 'next/link';

const ViewRooms = ({ rooms, selectedRoom, roomSelected }) => {
  const backgroundColor = useCallback(
    currentRoom => (currentRoom === selectedRoom ? '#d8d8d8' : '#eb3324'),
    [selectedRoom]
  );

  return (
    <CardColumns>
      {rooms.map(room => (
        <Card
          key={room.referenceId}
          style={{
            cursor: 'pointer',
            backgroundColor: backgroundColor(room.referenceId),
          }}
          onClick={() => {
            roomSelected(room.referenceId);
          }}
        >
          <CardBody>
            <CardTitle
              style={{ fontWeight: '600' }}
              className="text-uppercase text-white"
            >
              {room.name}
            </CardTitle>
            <br />
          </CardBody>
        </Card>
      ))}
    </CardColumns>
  );
};

ViewRooms.propTypes = {
  rooms: array.isRequired,
  selectedRoom: string,
  roomSelected: func.isRequired,
};

export default ViewRooms;
