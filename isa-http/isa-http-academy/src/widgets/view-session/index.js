import { Media } from 'reactstrap';
import { array } from 'prop-types';
import Link from 'next/link';

const ViewSession = ({ emailAddresses }) => {
  return (
    <Media>
      <Link href="/view-account">
        <a>
          <Media
            left
            style={{
              backgroundColor: '#d8d8d8',
              height: '75px',
              width: '75px',
              marginRight: '15px',
            }}
          />
        </a>
      </Link>
      <Media body className="text-white text-uppercase align-self-center">
        Welcome
        <br />
        <span style={{ fontWeight: '800' }} className="text-uppercase">
          {emailAddresses[0].address}
        </span>
      </Media>
    </Media>
  );
};

ViewSession.propTypes = {
  emailAddresses: array.isRequired,
};

export default ViewSession;
