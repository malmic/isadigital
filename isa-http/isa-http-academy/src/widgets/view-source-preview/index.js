import React, { useMemo } from 'react';
import { object } from 'prop-types';

const ViewSourcePreview = ({ source }) => {
  const src = useMemo(() => {
    if (source.typeHandle === 'GDRIVE') return '/images/gdrive.png';
    if (source.typeHandle === 'YOUTUBE') return '/images/yt_logo_rgb_light.png';
    if (source.typeHandle === 'ZOOM') return '/images/zoom.png';
    return '';
  }, [source.typeHandle]);

  return <img width="25%" src={src} alt="Source Preview" />;
};

ViewSourcePreview.propTypes = {
  source: object,
};

export default ViewSourcePreview;
