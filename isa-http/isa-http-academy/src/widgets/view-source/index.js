import React, { useMemo } from 'react';
import ReactPlayer from 'react-player';
import { object, string } from 'prop-types';
import { Card, CardText } from 'reactstrap';

const ViewSource = ({ source, backgroundColor }) => {
  const url = useMemo(() => {
    if (source.typeHandle === 'YOUTUBE')
      return `https://youtu.be/${source.referenceId}`;
    if (source.typeHandle === 'ZOOM')
      return `https://zoom.us/j/${source.referenceId}`;
    if (source.typeHandle === 'GDRIVE')
      return `https://drive.google.com/file/d/${source.referenceId}/view`;
    return '';
  }, [source.referenceId, source.typeHandle]);

  if (source.typeHandle === 'YOUTUBE')
    return (
      <>
        <h6 className="text-uppercase">{source.title || 'Stream'}</h6>
        <hr />
        <div className="mt-3" />
        <Card>
          <ReactPlayer width="100%" controls url={url} />
          <div className="mt-3" />
          {source.description && (
            <CardText className="text-white text-uppercase">
              {source.description.split('\n').map(function(item, idx) {
                return (
                  <span key={idx}>
                    {item}
                    <br />
                  </span>
                );
              })}
            </CardText>
          )}
        </Card>
        {source.description && <div className="mt-3" />}
      </>
    );

  return (
    <>
      <h6 className="text-uppercase">{source.title || 'Stream'}</h6>
      <hr />
      <div className="mt-3" />
      <div style={{ backgroundColor, height: '360px' }} />
      <div className="mt-3" />
      {source.description && (
        <CardText className="text-white text-uppercase">
          {source.description.split('\n').map(function(item, idx) {
            return (
              <span key={idx}>
                {item}
                <br />
              </span>
            );
          })}
        </CardText>
      )}
      {source.description && <div className="mt-3" />}
    </>
  );
};

ViewSource.propTypes = {
  source: object,
  backgroundColor: string.isRequired,
};

export default ViewSource;
