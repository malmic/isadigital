const withTM = require('next-transpile-modules');
const path = require('path');

const getAssetPrefix = env => {
  if (env === 'production') return '';

  const serverUrl = new URL(`http://${process.env.SOCKET_ADDRESS}`);
  const { port } = serverUrl;

  return `http://localhost:${port}`;
};

module.exports = withTM({
  transpileModules: ['@isa-music/libs-hocs'],
  sassOptions: {
    includePaths: [
      path.join(__dirname, '../../node_modules/bootstrap/scss'),
      path.join(__dirname, '../node_modules/bootstrap/scss'),
      path.join(__dirname, './node_modules/bootstrap/scss'),
    ],
  },
  assetPrefix: getAssetPrefix(process.env.NODE_ENV),
  distDir: 'build',
});
