import React from 'react';
import { node, string } from 'prop-types';
import { Container, Nav, NavItem } from 'reactstrap';
import styled from 'styled-components';
import { useRouter } from 'next/router';

import { Head, HeaderLink, Footer, Header } from '../widgets';
import routes from '../routes';

const PrivateLayout = ({ children, className }) => {
  const router = useRouter();

  const re = new RegExp('/([^/]+)/');
  const module = router.asPath.match(re);

  const navLinks = routes.filter(r =>
    module ? r.module === module[1] : false
  );

  navLinks.push({ pathname: '/logout', title: 'Logout' });

  return (
    <div className={className}>
      <Head />
      <Header>
        <Nav className="ml-auto text-right" navbar>
          {navLinks.map(navLink => (
            <NavItem key={navLink.title}>
              <HeaderLink to={navLink.pathname}>{navLink.title}</HeaderLink>
            </NavItem>
          ))}
        </Nav>
      </Header>
      <Container style={{ height: '100%' }}>{children}</Container>
      <Footer />
    </div>
  );
};

PrivateLayout.propTypes = {
  children: node.isRequired,
  className: string.isRequired,
};

export default styled(PrivateLayout)`
  display: grid;
  grid-template-rows: auto 1fr auto;
  min-height: 100%;
  grid-template-columns: 100%;
`;
