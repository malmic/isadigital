import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col, Container } from 'reactstrap';

import { Head, PublicPane, PublicBrand } from '../widgets';

const PublicLayout = ({ children, className }) => {
  const sm = { size: 6, offset: 3 };
  const md = { size: 4, offset: 4 };
  const lg = { size: 4, offset: 4 };
  const xl = { size: 8, offset: 2 };

  return (
    <Container fluid className={className}>
      <Head />
      <Row>
        <Col xs="12" sm={sm} md={md} lg={lg} xl={xl}>
          <Row>
            <PublicPane>
              <PublicBrand />
              <div className="mt-3" />
              {children}
              <div className="mt-3" />
              <div className="text-center">
                <a href="https://app.isa-music.local/">Homepage</a>
              </div>
            </PublicPane>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

PublicLayout.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string.isRequired,
};

export default styled(PublicLayout)`
  height: 100%;
`;
