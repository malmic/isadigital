import withRedux from 'next-redux-wrapper';
import App from 'next/app';
import { Provider } from 'react-redux';
import axios from 'axios';
import cookie from 'cookie';
import { IntlProvider } from 'react-intl';

import { assets, notifications, session } from '../store/actions';
import { createClientStore } from '../store';

import '../styles/shared.scss';

class Entry extends App {
  static async getInitialProps({ Component, ctx }) {
    const isServer = typeof window === 'undefined';

    if (isServer) {
      const { dispatch } = ctx.store;

      try {
        const { SOCKET_ADDRESS } = process.env;

        const { sid } = cookie.parse(ctx.req.headers.cookie || '');

        const app = axios.create({
          baseURL: `http://${SOCKET_ADDRESS}`,
          withCredentials: true,
        });

        if (sid) {
          try {
            const {
              data: { result },
            } = await app.get('/session/me', {
              headers: { Cookie: ctx.req.headers.cookie },
            });

            dispatch(assets.complementsFetched(result.complements));
            dispatch(assets.unitsFetched(result.units));
            dispatch(assets.assetsFetched(result.assets));
            dispatch(assets.conversionsFetched(result.conversions));

            dispatch(session.sessionCreated(result.user));

            const pageProps = Component.getInitialProps
              ? await Component.getInitialProps(ctx)
              : {};

            return {
              pageProps,
              locale: result.user.languageHandle,
            };
          } catch (err) {
            if (err.response && err.response.status === 403) {
              ctx.session = null;

              dispatch(
                notifications.notificationCreated(
                  'ERROR',
                  'Your session has expired.'
                )
              );
            } else {
              dispatch(notifications.notificationCreated('ERROR', err));
            }
          }
        }

        const pageProps = Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {};

        return { pageProps, locale: 'EN', intercomUser: {} };
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    }

    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};

    const { locale } = window.__NEXT_DATA__.props.initialProps;

    return { pageProps, locale, intercomUser: {} };
  }

  constructor(props) {
    super(props);

    this.state = {};
  }

  updateLocale = locale => {
    this.setState({ locale });
  };

  render() {
    const { Component, pageProps, store, locale } = this.props;

    const currentLocale = this.state.locale || locale;

    return (
      <IntlProvider locale={currentLocale.toLowerCase()}>
        <Provider store={store}>
          <Component {...pageProps} updateLocale={this.updateLocale} />
        </Provider>
      </IntlProvider>
    );
  }
}

export default withRedux(createClientStore)(Entry);
