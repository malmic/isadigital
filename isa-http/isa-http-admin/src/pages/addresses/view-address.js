import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  Table,
  Button,
  CardColumns,
  Card,
  CardHeader,
  CardBody,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewAddress = ({ dispatch, address, router, emailAddresses }) => {
  useEffect(() => {
    const foreign = { id: router.query.id, typeHandle: 'USER' };

    dispatch(thunks.addresses.getAddress({ foreign }));
    dispatch(thunks.emailAddresses.getEmailAddresses({ foreign }));
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Email Addresses</CardHeader>
          <CardBody>
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                {emailAddresses.map(emailAddress => (
                  <tr key={emailAddress.referenceId}>
                    <td>{emailAddress.address}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
        <Card>
          <CardHeader tag="h3">Address</CardHeader>
          <CardBody>
            Street {address.street}
            <br />
            Postal Code {address.postalCode}
            <br />
            City {address.city}
            <br />
            Country {address.country}
            <br />
            <br />
            <Button outline>Update</Button>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewAddress.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  emailAddresses: array.isRequired,
  address: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  emailAddresses: state.emailAddresses.data,
  address: state.addresses.address,
}))(privateCheck(ViewAddress));
