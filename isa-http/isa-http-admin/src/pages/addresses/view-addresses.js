import React, { useState, useEffect, useRef } from 'react';
import { object, func, array } from 'prop-types';
import {
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Table,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewAddresses = ({ dispatch, notifications, addresses, router }) => {
  const [search, setSearch] = useState(router.query.search || '');
  const formRef = useRef();

  useEffect(() => {
    dispatch(thunks.addresses.findAddresses({ search }));
  }, [dispatch, search]);

  const findAddresses = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    router.push({ pathname: '/addresses/view-addresses', query: { search } });

    setSearch(search);
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Form innerRef={formRef}>
        <InputGroup>
          <Input
            type="text"
            name="search"
            defaultValue={search}
            placeholder={'Find addresses by email address...'}
          />
          <InputGroupAddon addonType="append">
            <Button outline type="submit" onClick={findAddresses} block>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </Form>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Email Address</th>
            <th>Street</th>
            <th>Postal Code</th>
            <th>City</th>
            <th>Country</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {addresses.map(address => (
            <tr key={address.referenceId}>
              <td>{address.emailAddress.address}</td>
              <td>{address.street}</td>
              <td>{address.postalCode}</td>
              <td>{address.city}</td>
              <td>{address.country}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/addresses/view-address',
                    query: { id: address.emailAddress.foreign.id },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewAddresses.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  addresses: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  addresses: state.addresses.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewAddresses));
