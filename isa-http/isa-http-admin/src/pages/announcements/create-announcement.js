import { func, object, array } from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateAnnouncement = ({ dispatch, router, rooms }) => {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [room, setRoom] = useState('');

  useEffect(() => {
    dispatch(thunks.rooms.getRooms());
  }, [dispatch]);

  useEffect(() => {
    if (!room && rooms.length > 0) setRoom(rooms[0].referenceId);
  }, [room, rooms]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.announcements.createAnnouncement(
        {
          title,
          body,
          foreign: [
            {
              id: room,
              typeHandle: 'ROOM',
            },
          ],
        },
        id => {
          router.push({
            pathname: '/announcements/view-announcement',
            query: { id },
          });
        }
      )
    );
  };

  const submissionEnabled = useMemo(() => title && body, [body, title]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Title</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="title"
                  value={title}
                  onChange={e => setTitle(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Body</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="textarea"
                  name="body"
                  onChange={e => setBody(e.target.value)}
                  value={body}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Room</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="select"
                  name="room"
                  value={room}
                  onChange={e => setRoom(e.target.value)}
                >
                  {rooms.map(room => (
                    <option key={room.referenceId} value={room.referenceId}>
                      {room.name}
                    </option>
                  ))}
                </Input>
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create Announcement
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateAnnouncement.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  rooms: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  rooms: state.rooms.data,
}))(privateCheck(CreateAnnouncement));
