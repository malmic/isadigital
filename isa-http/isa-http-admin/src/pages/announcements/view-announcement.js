import React, { useEffect, useState, useMemo } from 'react';
import { func, object, array } from 'prop-types';
import {
  Button,
  ButtonGroup,
  CardColumns,
  Card,
  CardHeader,
  CardBody,
  CardText,
  Table,
  Form,
  InputGroup,
  InputGroupAddon,
  Input,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewAnnouncement = ({ dispatch, announcement, rooms, router }) => {
  const [room, setRoom] = useState('');

  const availableRooms = useMemo(
    () =>
      rooms.filter(
        room =>
          !announcement.foreign.find(
            f => f.typeHandle === 'ROOM' && f.id === room.referenceId
          )
      ),
    [announcement.foreign, rooms]
  );

  useEffect(() => {
    if (availableRooms.length > 0 && !room)
      setRoom(availableRooms[0].referenceId);
  }, [availableRooms, room]);

  useEffect(() => {
    dispatch(thunks.rooms.getRooms());
    dispatch(
      thunks.announcements.getAnnouncement({ referenceId: router.query.id })
    );
  }, [dispatch, router.query.id]);

  const updateAnnouncement = announcement => {
    dispatch(thunks.announcements.updateAnnouncement(announcement));
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{announcement.title}</CardHeader>
          <CardBody>
            <CardText>{announcement.body}</CardText>
            <ButtonGroup>
              <Button
                outline
                onClick={() => {
                  dispatch(
                    thunks.announcements.deleteAnnouncement(
                      { referenceId: announcement.referenceId },
                      () => {
                        router.push('/announcements/view-announcements');
                      }
                    )
                  );
                }}
              >
                Delete
              </Button>
            </ButtonGroup>
          </CardBody>
        </Card>
        <Card style={{ border: 0 }}>
          <Form>
            <InputGroup>
              <Input
                type="select"
                value={room}
                onChange={e => setRoom(e.target.value)}
              >
                {availableRooms.map(e => (
                  <option key={e.referenceId} value={e.referenceId}>
                    {e.name}
                  </option>
                ))}
              </Input>
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={() => {
                    updateAnnouncement({
                      ...announcement,
                      foreign: [
                        ...announcement.foreign,
                        { id: room, typeHandle: 'ROOM' },
                      ],
                    });
                  }}
                  block
                >
                  Add Room
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Room Name</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {rooms
                .filter(room =>
                  announcement.foreign.find(
                    f => f.typeHandle === 'ROOM' && f.id === room.referenceId
                  )
                )
                .map(room => (
                  <tr key={room.referenceId}>
                    <td>{room.name}</td>
                    <td>
                      <Button
                        outline
                        onClick={e => {
                          updateAnnouncement({
                            ...announcement,
                            foreign: announcement.foreign.filter(
                              f => f.id !== room.referenceId
                            ),
                          });
                        }}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewAnnouncement.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  announcement: object.isRequired,
  rooms: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  announcement: state.announcements.announcement,
  rooms: state.rooms.data,
}))(privateCheck(ViewAnnouncement));
