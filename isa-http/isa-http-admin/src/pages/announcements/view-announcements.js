import React, { useEffect } from 'react';
import { object, func, array } from 'prop-types';
import { Button, Table } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewAnnouncements = ({
  dispatch,
  notifications,
  announcements,
  router,
}) => {
  useEffect(() => {
    dispatch(thunks.announcements.getAnnouncements());
  }, [dispatch]);

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/announcements/create-announcement');
        }}
      >
        Create Announcement
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Title</th>
            <th>Body</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {announcements.map(announcement => (
            <tr key={announcement.referenceId}>
              <td>{announcement.title}</td>
              <td>{announcement.body}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/announcements/view-announcement',
                    query: { id: announcement.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewAnnouncements.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  announcements: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  announcements: state.announcements.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewAnnouncements));
