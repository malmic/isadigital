import React, { useEffect, useMemo, useState } from 'react';
import { func, object, array } from 'prop-types';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateAward = ({ dispatch, router, editions }) => {
  useEffect(() => {
    dispatch(thunks.editions.getEditions());
  }, [dispatch]);

  const [editionHandle, setEditionHandle] = useState('');
  const [name, setName] = useState('');

  useEffect(() => {
    if (editions.length > 0 && !editionHandle)
      setEditionHandle(editions[0].handle);
  }, [editionHandle, editions]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.awards.createAward({ name, editionHandle }, id => {
        router.push({ pathname: '/awards/view-award', query: { id } });
      })
    );
  };

  const submissionEnabled = useMemo(() => {
    return editionHandle;
  }, [editionHandle]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Edition</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="select"
                  name="editionHandle"
                  value={editionHandle}
                  onChange={e => setEditionHandle(e.target.value)}
                >
                  {editions.map(edition => (
                    <option key={edition.handle} value={edition.handle}>
                      {edition.handle}
                    </option>
                  ))}
                </Input>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="name"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>{' '}
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create Award
            </Button>
          </Form>
        </Col>
      </Row>
      <div className="mt-3" />
    </PrivateLayout>
  );
};

CreateAward.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  editions: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  editions: state.editions.data,
}))(privateCheck(CreateAward));
