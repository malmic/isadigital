import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  Button,
  CardColumns,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';
import { ShowMoreLink } from '../../widgets';

const ViewAward = ({ dispatch, award, sources, router }) => {
  useEffect(() => {
    dispatch(thunks.awards.getAward({ referenceId: router.query.id }));
    dispatch(
      thunks.sources.getSources({
        foreign: [{ id: router.query.id, typeHandle: 'AWARD' }],
      })
    );
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{award.name}</CardHeader>
          <CardBody>
            <CardTitle>Edition {award.editionHandle}</CardTitle>
            <Button
              outline
              onClick={() => {
                router.push({
                  pathname: '/awards/update-award',
                  query: {
                    id: router.query.id,
                  },
                });
              }}
            >
              Update
            </Button>
          </CardBody>
        </Card>
        <Card>
          <Table responsive>
            <thead className="thead-light">
              <tr>
                <th>Source Title</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              {sources.map(source => (
                <tr key={source.referenceId}>
                  <td>{source.title || source.referenceId}</td>
                  <td>
                    <ShowMoreLink
                      href={{
                        pathname: '/sources/view-source',
                        query: { id: source.referenceId },
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewAward.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  award: object.isRequired,
  sources: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  award: state.awards.award,
  sources: state.sources.data,
}))(privateCheck(ViewAward));
