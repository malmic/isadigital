import React, { useEffect } from 'react';
import { object, func, array } from 'prop-types';
import { Button, Table } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewAwards = ({ dispatch, notifications, awards, router }) => {
  useEffect(() => {
    dispatch(thunks.awards.getAwards());
  }, [dispatch]);

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/awards/create-award');
        }}
      >
        Create Award
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Name</th>
            <th>Edition</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {awards.map(award => (
            <tr key={award.referenceId}>
              <td>{award.name}</td>
              <td>{award.editionHandle}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/awards/view-award',
                    query: { id: award.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewAwards.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  awards: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  awards: state.awards.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewAwards));
