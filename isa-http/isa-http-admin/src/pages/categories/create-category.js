import { func, object, array } from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateCategory = ({ dispatch, router, categories }) => {
  const [name, setName] = useState('');
  const [parent, setParent] = useState('');

  useEffect(() => {
    dispatch(thunks.categories.getCategories());
  }, [dispatch]);

  useEffect(() => {
    if (categories.length > 0) setParent(categories[0].referenceId);
  }, [categories]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.categories.createCategory({ name, parent }, id => {
        router.push({ pathname: '/categories/view-category', query: { id } });
      })
    );
  };

  const submissionEnabled = useMemo(() => name, [name]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Parent</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="select"
                  name="parent"
                  value={parent}
                  onChange={e => setParent(e.target.value)}
                >
                  {categories.map(category => (
                    <option
                      key={category.referenceId}
                      value={category.referenceId}
                    >
                      {category.name}
                    </option>
                  ))}
                </Input>
              </InputGroup>
            </FormGroup>
            <div className="mt-3" />
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateCategory.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  categories: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  categories: state.categories.data,
}))(privateCheck(CreateCategory));
