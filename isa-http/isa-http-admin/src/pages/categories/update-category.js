import { func, object } from 'prop-types';
import React, { useMemo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const UpdateCategory = ({ dispatch, router, category }) => {
  useEffect(() => {
    dispatch(thunks.categories.getCategory({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  const [name, setName] = useState('');

  useEffect(() => {
    setName(category.name);
  }, [category.name]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.categories.updateCategory(
        { referenceId: router.query.id, name },
        () => {
          router.push({
            pathname: '/categories/view-category',
            query: { id: router.query.id },
          });
        }
      )
    );
  };

  const submissionEnabled = useMemo(() => name, [name]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Update
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

UpdateCategory.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  category: object.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  category: state.categories.category,
}))(privateCheck(UpdateCategory));
