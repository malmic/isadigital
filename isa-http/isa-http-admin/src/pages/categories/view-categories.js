import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewCategories = ({ dispatch, categories, router }) => {
  useEffect(() => {
    dispatch(thunks.categories.getCategories());
  }, [dispatch]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/categories/create-category');
        }}
      >
        Create Category
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Name</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {categories.map(category => (
            <tr key={category.referenceId}>
              <td>{category.name}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/categories/view-category',
                    query: { id: category.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewCategories.propTypes = {
  dispatch: func.isRequired,
  categories: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  categories: state.categories.data,
}))(privateCheck(ViewCategories));
