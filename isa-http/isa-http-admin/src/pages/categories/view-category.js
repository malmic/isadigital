import React, { useEffect } from 'react';
import { func, object } from 'prop-types';
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  ButtonGroup,
  Button,
  CardColumns,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewCategory = ({ dispatch, category, router }) => {
  useEffect(() => {
    dispatch(thunks.categories.getCategory({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  const deleteCategory = category => {
    dispatch(
      thunks.categories.deleteCategory(category, () => {
        router.push('/categories/view-categories');
      })
    );
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Category</CardHeader>
          <CardBody>
            <CardTitle>{category.name}</CardTitle>
            <ButtonGroup>
              <Button
                outline
                onClick={e => {
                  router.push({
                    pathname: '/categories/update-category',
                    query: { id: router.query.id },
                  });
                }}
              >
                Update
              </Button>
              <Button
                outline
                onClick={e => {
                  deleteCategory(category);
                }}
              >
                Delete
              </Button>
            </ButtonGroup>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewCategory.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  category: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  category: state.categories.category,
  sources: state.sources.data,
  events: state.events.data,
}))(privateCheck(ViewCategory));
