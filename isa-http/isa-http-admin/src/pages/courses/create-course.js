import React, { useEffect, useMemo, useState } from 'react';
import { array, func, object } from 'prop-types';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateCourse = ({ dispatch, router, profiles }) => {
  const [course, setCourse] = useState({ STUDENT: '', PROFESSOR: '' });

  const students = useMemo(
    () => profiles.filter(p => p.typeHandle === 'STUDENT'),
    [profiles]
  );

  const professors = useMemo(
    () => profiles.filter(p => p.typeHandle === 'PROFESSOR'),
    [profiles]
  );

  useEffect(() => {
    if (students.length > 0 && !course.STUDENT)
      setCourse({ ...course, STUDENT: students[0] });
  }, [course, students]);

  useEffect(() => {
    if (professors.length > 0 && !course.PROFESSOR)
      setCourse({ ...course, PROFESSOR: professors[0] });
  }, [course, professors]);

  useEffect(() => {
    dispatch(thunks.profiles.getProfiles());
  }, [dispatch]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.courses.createCourse(course, id => {
        router.push({ pathname: '/courses/view-course', query: { id } });
      })
    );
  };

  const submissionEnabled = useMemo(
    () => Object.values(course).every(c => !!c),
    [course]
  );

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <Form>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Student</InputGroupText>
              </InputGroupAddon>
              <Input
                type="select"
                name="student"
                onChange={e =>
                  setCourse({
                    ...course,
                    STUDENT: students.find(
                      s => s.referenceId === e.target.value
                    ),
                  })
                }
                value={course.STUDENT.referenceId}
              >
                {students.map(profile => (
                  <option key={profile.referenceId} value={profile.referenceId}>
                    {profile.fullName}
                  </option>
                ))}
              </Input>
            </InputGroup>
            <div className="mt-3" />
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Professor</InputGroupText>
              </InputGroupAddon>
              <Input
                type="select"
                name="professor"
                onChange={e =>
                  setCourse({
                    ...course,
                    PROFESSOR: professors.find(
                      s => s.referenceId === e.target.value
                    ),
                  })
                }
                value={course.PROFESSOR.referenceId}
              >
                {professors.map(profile => (
                  <option key={profile.referenceId} value={profile.referenceId}>
                    {profile.fullName}
                  </option>
                ))}
              </Input>
            </InputGroup>
            <div className="mt-3" />
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create Course
            </Button>
          </Form>
        </Col>
      </Row>
      <div className="mt-3" />
    </PrivateLayout>
  );
};

CreateCourse.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  profiles: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  profiles: state.profiles.data,
}))(privateCheck(CreateCourse));
