import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  Table,
  CardColumns,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';
import { ShowMoreLink } from '../../widgets';

const ViewCourse = ({ dispatch, course, router, sources }) => {
  useEffect(() => {
    dispatch(thunks.courses.getCourse({ referenceId: router.query.id }));
    dispatch(
      thunks.sources.getSources({
        foreign: [{ id: router.query.id, typeHandle: 'COURSE' }],
      })
    );
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Course</CardHeader>
          <CardBody>
            <CardTitle>{course.referenceId}</CardTitle>
            <CardText>
              PROFESSOR {course.PROFESSOR} STUDENT {course.STUDENT}
            </CardText>
            <Button outline>Delete</Button>
          </CardBody>
        </Card>

        <Card>
          <CardHeader tag="h3">Sources</CardHeader>
          <CardBody>
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Source</th>
                  <th>View</th>
                </tr>
              </thead>
              <tbody>
                {sources.map(source => (
                  <tr key={source.referenceId}>
                    <td>{source.referenceId}</td>
                    <td>
                      <ShowMoreLink
                        href={{
                          pathname: '/sources/view-source',
                          query: { id: source.referenceId },
                        }}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewCourse.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  course: object.isRequired,
  sources: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  course: state.courses.course,
  sources: state.sources.data,
}))(privateCheck(ViewCourse));
