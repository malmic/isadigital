import React, { useState, useEffect, useRef } from 'react';
import { func, array, object } from 'prop-types';
import { InputGroup, Input, InputGroupAddon, Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewCourses = ({ dispatch, notifications, courses, router }) => {
  const [search, setSearch] = useState(router.query.search || '');
  const formRef = useRef();

  useEffect(() => {
    dispatch(thunks.courses.findCourses({ search }));
  }, [dispatch, search]);

  const findCourses = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    router.push({ pathname: '/courses/view-courses', query: { search } });

    setSearch(search);
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/courses/create-course');
        }}
      >
        Create Course
      </Button>
      <div className="mt-3" />
      <form ref={formRef}>
        <InputGroup>
          <Input
            type="text"
            name="search"
            defaultValue={search}
            placeholder={'Find courses by name...'}
          />
          <InputGroupAddon addonType="append">
            <Button outline type="submit" onClick={findCourses} block>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </form>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Professor</th>
            <th>Student</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {courses.map(course => (
            <tr key={course.referenceId}>
              <td>{course.PROFESSOR.fullName}</td>
              <td>{course.STUDENT.fullName}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/courses/view-course',
                    query: { id: course.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewCourses.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  courses: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  courses: state.courses.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewCourses));
