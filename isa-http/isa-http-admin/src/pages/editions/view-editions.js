import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewEditions = ({ dispatch, editions, router }) => {
  useEffect(() => {
    dispatch(thunks.editions.getEditions());
  }, [dispatch]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Year</th>
          </tr>
        </thead>
        <tbody>
          {editions.map(edition => (
            <tr key={edition.handle}>
              <td>{edition.handle}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewEditions.propTypes = {
  dispatch: func.isRequired,
  editions: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  editions: state.editions.data,
}))(privateCheck(ViewEditions));
