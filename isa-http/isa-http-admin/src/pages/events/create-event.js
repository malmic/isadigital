import { array, func, object } from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';
import moment from 'moment';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateEvent = ({ dispatch, router, editions }) => {
  const [name, setName] = useState('');
  const [beginAt, setBeginAt] = useState('YYYY/MM/DD HH:mm:ss');
  const [endAt, setEndAt] = useState('YYYY/MM/DD HH:mm:ss');
  const [editionHandle, setEditionHandle] = useState(0);

  useEffect(() => {
    dispatch(thunks.editions.getEditions());
  }, [dispatch]);

  useEffect(() => {
    if (editions.length > 0) {
      setEditionHandle(parseInt(editions[0].handle));
    }
  }, [editions]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.events.createEvent(
        {
          name,
          editionHandle,
          beginAt: moment(beginAt, 'YYYY/MM/DD HH:mm:ss').toDate(),
          endAt: moment(endAt, 'YYYY/MM/DD HH:mm:ss').toDate(),
        },
        id => {
          router.push({ pathname: '/events/view-event', query: { id } });
        }
      )
    );
  };

  const submissionEnabled = useMemo(
    () =>
      name &&
      editionHandle &&
      moment(beginAt, 'YYYY/MM/DD HH:mm:ss').isValid() &&
      moment(endAt, 'YYYY/MM/DD HH:mm:ss').isValid(),
    [beginAt, editionHandle, endAt, name]
  );

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Begin</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="beginAt"
                  value={beginAt}
                  onChange={e => setBeginAt(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>End</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="endAt"
                  value={endAt}
                  onChange={e => setEndAt(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Edition</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="select"
                  name="typeHandle"
                  onChange={e => setEditionHandle(parseInt(e.target.value))}
                  value={editionHandle}
                >
                  {editions.map(edition => (
                    <option key={edition.handle} value={edition.handle}>
                      {edition.handle}
                    </option>
                  ))}
                </Input>
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create Event
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateEvent.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  editions: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  editions: state.editions.data,
}))(privateCheck(CreateEvent));
