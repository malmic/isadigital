import { func, object } from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';
import moment from 'moment';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const UpdateEvent = ({ dispatch, router, event }) => {
  const [name, setName] = useState('');
  const [beginAt, setBeginAt] = useState('YYYY/MM/DD HH:mm:ss');
  const [endAt, setEndAt] = useState('YYYY/MM/DD HH:mm:ss');

  useEffect(() => {
    dispatch(thunks.events.getEvent({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  useEffect(() => {
    if (event.name) setName(event.name);
  }, [event.name]);

  useEffect(() => {
    if (event.beginAt)
      setBeginAt(moment(new Date(event.beginAt)).format('YYYY/MM/DD HH:mm:ss'));
  }, [event.beginAt]);

  useEffect(() => {
    if (event.endAt)
      setEndAt(moment(new Date(event.endAt)).format('YYYY/MM/DD HH:mm:ss'));
  }, [event.endAt]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.events.updateEvent(
        {
          referenceId: router.query.id,
          name,
          beginAt: moment(beginAt, 'YYYY/MM/DD HH:mm:ss').toDate(),
          endAt: moment(endAt, 'YYYY/MM/DD HH:mm:ss').toDate(),
        },
        () => {
          router.push({
            pathname: '/events/view-event',
            query: {
              id: router.query.id,
            },
          });
        }
      )
    );
  };

  const submissionEnabled = useMemo(
    () =>
      name &&
      moment(beginAt, 'YYYY/MM/DD HH:mm:ss').isValid() &&
      moment(endAt, 'YYYY/MM/DD HH:mm:ss').isValid(),
    [beginAt, endAt, name]
  );

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Begin</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="beginAt"
                  value={beginAt}
                  onChange={e => setBeginAt(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>End</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="endAt"
                  value={endAt}
                  onChange={e => setEndAt(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Update Event
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

UpdateEvent.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  event: object.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  event: state.events.event,
}))(privateCheck(UpdateEvent));
