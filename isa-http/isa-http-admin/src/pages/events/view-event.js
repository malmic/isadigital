import React, { useEffect, useState, useMemo } from 'react';
import { func, object, array } from 'prop-types';
import {
  Table,
  CardColumns,
  ButtonGroup,
  Button,
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { FormattedDate, FormattedTime } from 'react-intl';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';
import { ShowMoreLink } from '../../widgets';

const ViewEvent = ({ dispatch, event, router, sources, profiles, rooms }) => {
  const [room, setRoom] = useState('');

  const availableRooms = useMemo(
    () =>
      rooms.filter(
        room =>
          !event.foreign.find(
            f => f.typeHandle === 'ROOM' && f.id === room.referenceId
          )
      ),
    [rooms, event.foreign]
  );

  useEffect(() => {
    if (availableRooms.length > 0) setRoom(availableRooms[0].referenceId);
  }, [availableRooms]);

  useEffect(() => {
    dispatch(thunks.rooms.getRooms());
    dispatch(thunks.events.getEvent({ referenceId: router.query.id }));
    dispatch(
      thunks.profiles.getProfiles({
        foreign: [{ id: router.query.id, typeHandle: 'EVENT' }],
      })
    );
    dispatch(
      thunks.sources.getSources({
        foreign: [{ id: router.query.id, typeHandle: 'EVENT' }],
      })
    );
  }, [dispatch, router.query.id]);

  const updateEvent = event => {
    dispatch(thunks.events.updateEvent(event));
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{event.name}</CardHeader>
          <CardBody>
            <CardTitle>Edition {event.editionHandle}</CardTitle>
            <CardText>
              {event.beginAt && (
                <>
                  <FormattedDate value={new Date(event.beginAt)} />{' '}
                  <FormattedTime value={new Date(event.beginAt)} />
                </>
              )}{' '}
              -{' '}
              {event.endAt && (
                <>
                  <FormattedDate value={new Date(event.endAt)} />{' '}
                  <FormattedTime value={new Date(event.endAt)} />
                </>
              )}
            </CardText>
            <ButtonGroup>
              <Button
                outline
                onClick={() => {
                  router.push({
                    pathname: '/events/update-event',
                    query: { id: router.query.id },
                  });
                }}
              >
                Update
              </Button>
              <Button outline>Delete</Button>
            </ButtonGroup>
          </CardBody>
        </Card>
        <Card style={{ border: 0 }}>
          <Form>
            <InputGroup>
              <Input
                type="select"
                value={room}
                onChange={e => setRoom(e.target.value)}
              >
                {availableRooms.map(e => (
                  <option key={e.referenceId} value={e.referenceId}>
                    {e.name}
                  </option>
                ))}
              </Input>
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={() => {
                    updateEvent({
                      ...event,
                      foreign: [
                        ...event.foreign,
                        { id: room, typeHandle: 'ROOM' },
                      ],
                    });
                  }}
                  block
                >
                  Add Room
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Room Name</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {rooms
                .filter(room =>
                  event.foreign.find(
                    f => f.typeHandle === 'ROOM' && f.id === room.referenceId
                  )
                )
                .map(room => (
                  <tr key={room.referenceId}>
                    <td>{room.name}</td>
                    <td>
                      <Button
                        outline
                        onClick={e => {
                          updateEvent({
                            ...event,
                            foreign: event.foreign.filter(
                              f => f.id !== room.referenceId
                            ),
                          });
                        }}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Card>
        <Card style={{ border: 0 }}>
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Source</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              {sources.map(source => (
                <tr key={source.referenceId}>
                  <td>{source.referenceId}</td>
                  <td>
                    <ShowMoreLink
                      href={{
                        pathname: '/sources/view-source',
                        query: { id: source.referenceId },
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
        <Card style={{ border: 0 }}>
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Profile Name</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              {profiles.map(profile => (
                <tr key={profile.referenceId}>
                  <td>
                    {profile.firstName} {profile.lastName}
                  </td>
                  <td>
                    <ShowMoreLink
                      href={{
                        pathname: '/profiles/view-profile',
                        query: {
                          id: profile.foreign.find(f => f.typeHandle === 'USER')
                            .id,
                        },
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewEvent.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  event: object.isRequired,
  profiles: array.isRequired,
  sources: array.isRequired,
  rooms: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  event: state.events.event,
  profiles: state.profiles.data,
  sources: state.sources.data,
  rooms: state.rooms.data,
}))(privateCheck(ViewEvent));
