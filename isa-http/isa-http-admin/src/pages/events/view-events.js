import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { FormattedDate, FormattedTime } from 'react-intl';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewEvents = ({ dispatch, events, router }) => {
  useEffect(() => {
    dispatch(thunks.events.getEvents());
  }, [dispatch]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/events/create-event');
        }}
      >
        Create Event
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Name</th>
            <th>Begin At</th>
            <th>End At</th>
            <th>Edition</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {events.map(event => (
            <tr key={event.referenceId}>
              <td>{event.name}</td>
              <td>
                {event.beginAt && (
                  <>
                    <FormattedDate value={new Date(event.beginAt)} />{' '}
                    <FormattedTime value={new Date(event.beginAt)} />
                  </>
                )}
              </td>
              <td>
                {event.endAt && (
                  <>
                    <FormattedDate value={new Date(event.endAt)} />{' '}
                    <FormattedTime value={new Date(event.endAt)} />
                  </>
                )}
              </td>
              <td>{event.editionHandle}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/events/view-event',
                    query: { id: event.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewEvents.propTypes = {
  dispatch: func.isRequired,
  events: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  events: state.events.data,
}))(privateCheck(ViewEvents));
