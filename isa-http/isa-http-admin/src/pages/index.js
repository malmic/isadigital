import React from 'react';
import { object } from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { PrivateIndex, PublicIndex } from '../widgets';

const Index = ({ session }) => {
  if (isEmpty(session)) {
    return <PublicIndex />;
  }

  return <PrivateIndex />;
};

Index.propTypes = {
  session: object.isRequired,
};

export default connect(state => ({
  session: state.session,
}))(Index);
