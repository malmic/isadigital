import React, { useEffect, useState, useRef } from 'react';
import { object, array, func } from 'prop-types';
import {
  InputGroupText,
  Button,
  Form,
  FormGroup,
  Input,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { usePrevious } from '@isa-music/libs-effects';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateJuror = ({ dispatch, notifications, router, emailAddresses }) => {
  const [search, setSearch] = useState('');
  const [emailAddress, setEmailAddress] = useState({
    foreign: { id: '', typeHandle: '' },
    address: '',
  });
  const formRef = useRef();
  const previousLength = usePrevious(emailAddresses.length);

  useEffect(() => {
    dispatch(thunks.emailAddresses.findEmailAddresses({ search }));
  }, [dispatch, search]);

  useEffect(() => {
    if (emailAddresses.length > 0 && emailAddresses.length !== previousLength)
      setEmailAddress(emailAddresses[0]);
    else if (emailAddresses.length === 0 && emailAddress.address)
      setEmailAddress({ foreign: { id: '', typeHandle: '' }, address: '' });
  }, [emailAddress, emailAddresses, previousLength]);

  const selectEmailAddress = e => {
    const foundAddress = emailAddresses.find(f => f.address === e.target.value);

    setEmailAddress(foundAddress);
  };

  const disabled = !emailAddress.foreign.id;

  const handleSubmit = e => {
    e.preventDefault();

    dispatch(
      thunks.jurors.createJuror({ foreign: emailAddress.foreign }, () => {
        router.push({ pathname: '/jurors/view-jurors' });
      })
    );
  };

  const findEmailAddresses = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    setSearch(search);
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <div className="mt-3" />
          <Form innerRef={formRef}>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Email Address</InputGroupText>
              </InputGroupAddon>
              <Input
                name="search"
                type="text"
                placeholder="Find an email address..."
              />
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={findEmailAddresses}
                  block
                  type="submit"
                >
                  Go
                </Button>
              </InputGroupAddon>
            </InputGroup>
            <div className="mt-3" />
            <FormGroup>
              <Input
                type="select"
                name="emailAddress"
                onChange={selectEmailAddress}
                value={emailAddress.address}
              >
                {emailAddresses.map(emailAddress => (
                  <option
                    key={emailAddress.referenceId}
                    value={emailAddress.address}
                  >
                    {emailAddress.address}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <Button
              className="float-right"
              disabled={disabled}
              onClick={handleSubmit}
              outline
            >
              Create Juror
            </Button>
          </Form>
          <div className="mt-3" />
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateJuror.propTypes = {
  router: object.isRequired,
  notifications: array.isRequired,
  dispatch: func.isRequired,
  emailAddresses: array.isRequired,
};

export default connect(state => ({
  emailAddresses: state.emailAddresses.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(CreateJuror));
