import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewProfiles = ({ dispatch, jurors, router }) => {
  useEffect(() => {
    dispatch(thunks.jurors.getJurors());
  }, [dispatch]);

  const deleteJuror = juror => {
    dispatch(thunks.jurors.deleteJuror({ foreign: juror.foreign }));
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/jurors/create-juror');
        }}
      >
        Create Juror
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Email Address</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {jurors.map(juror => (
            <tr key={juror.referenceId}>
              <td>{juror.address}</td>
              <td>
                <Button
                  outline
                  onClick={e => {
                    deleteJuror(juror);
                  }}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewProfiles.propTypes = {
  dispatch: func.isRequired,
  jurors: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  jurors: state.jurors.data,
}))(privateCheck(ViewProfiles));
