import React from 'react';
import { connect } from 'react-redux';
import { publicCheck } from '@isa-music/libs-hocs';
import { object } from 'prop-types';

import { PublicLayout } from '../layouts';
import { LoginForm } from '../widgets';

const Login = ({ router }) => {
  return (
    <PublicLayout>
      <LoginForm next={router.query.next} />
    </PublicLayout>
  );
};

Login.propTypes = {
  router: object.isRequired,
};

export default connect(state => ({ session: state.session }))(
  publicCheck(Login)
);
