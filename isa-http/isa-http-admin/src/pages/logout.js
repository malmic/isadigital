import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { Alert } from 'reactstrap';

import { PublicLayout } from '../layouts';

const Logout = () => {
  useEffect(() => {
    setTimeout(() => {
      window.location.href = '/session/delete';
    }, 1000);
  }, []);

  return (
    <PublicLayout>
      <Alert className="text-uppercase text-center" color="secondary">
        Logging out...
      </Alert>
    </PublicLayout>
  );
};

Logout.propTypes = {};

export default connect(state => ({
  session: state.session,
}))(privateCheck(Logout));
