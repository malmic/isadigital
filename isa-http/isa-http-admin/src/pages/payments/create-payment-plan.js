import React, { useState, useEffect } from 'react';
import { object, array, func } from 'prop-types';
import {
  Button,
  Form,
  Input,
  Row,
  Col,
  FormGroup,
  InputGroup,
  InputGroupButtonDropdown,
  DropdownToggle,
  InputGroupAddon,
  InputGroupText,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { connect } from 'react-redux';
import { assets, privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreatePaymentPlan = ({
  dispatch,
  notifications,
  router,
  editions,
  getAssets,
}) => {
  useEffect(() => {
    dispatch(thunks.editions.getEditions());
  }, [dispatch]);

  const assets = getAssets();

  const [editionHandle, setEditionHandle] = useState('');
  const [splitButtonOpen, setSplitButtonOpen] = useState(false);
  const [name, setName] = useState('');
  const [quantity, setQuantity] = useState({
    handle: assets[0].handle,
    value: 0,
  });

  const toggleSplit = () => setSplitButtonOpen(!splitButtonOpen);

  useEffect(() => {
    if (editions.length > 0 && !editionHandle)
      setEditionHandle(editions[0].handle);
  }, [editionHandle, editions]);

  const handleSubmit = e => {
    e.preventDefault();

    dispatch(
      thunks.paymentPlans.createPaymentPlan(
        { editionHandle, quantity, name },
        () => {
          router.push({ pathname: '/payments/view-payment-plans' });
        }
      )
    );
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Name</InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                name="name"
                placeholder="Enter a name..."
                value={name}
                onChange={e => setName(e.target.value)}
              />
            </InputGroup>
            <div className="mt-3" />
            <InputGroup>
              <InputGroupButtonDropdown
                addonType="prepend"
                isOpen={splitButtonOpen}
                toggle={toggleSplit}
              >
                <DropdownToggle outline caret>
                  {quantity.handle}
                </DropdownToggle>
                <DropdownMenu>
                  {assets.map(asset => (
                    <DropdownItem
                      onClick={e =>
                        setQuantity({ ...quantity, handle: asset.handle })
                      }
                      key={asset.handle}
                    >
                      {asset.name}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </InputGroupButtonDropdown>
              <Input
                type="number"
                placeholder="Value"
                onChange={e =>
                  setQuantity({ ...quantity, value: e.target.value })
                }
              />
            </InputGroup>
            <div className="mt-3" />
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Edition</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="select"
                  name="editionHandle"
                  value={editionHandle}
                  onChange={e => setEditionHandle(e.target.value)}
                >
                  {editions.map(edition => (
                    <option key={edition.handle} value={edition.handle}>
                      {edition.handle}
                    </option>
                  ))}
                </Input>
              </InputGroup>
            </FormGroup>
            <div className="mt-3" />
            <Button className="float-right" onClick={handleSubmit} outline>
              Create Payment Plan
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreatePaymentPlan.propTypes = {
  router: object.isRequired,
  notifications: array.isRequired,
  dispatch: func.isRequired,
  getAssets: func.isRequired,
  editions: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  editions: state.editions.data,
}))(assets(privateCheck(CreatePaymentPlan)));
