import React, { useEffect } from 'react';
import { func, object } from 'prop-types';
import {
  Button,
  CardColumns,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewPaymentPlan = ({ dispatch, paymentPlan, router }) => {
  useEffect(() => {
    dispatch(
      thunks.paymentPlans.getPaymentPlan({ referenceId: router.query.id })
    );
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{paymentPlan.name}</CardHeader>
          <CardBody>
            <CardTitle>{paymentPlan.editionHandle}</CardTitle>
            <CardText>
              {paymentPlan.quantity.value} {paymentPlan.quantity.handle}
            </CardText>
            <Button outline>Update</Button>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewPaymentPlan.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  paymentPlan: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  paymentPlan: state.paymentPlans.paymentPlan,
}))(privateCheck(ViewPaymentPlan));
