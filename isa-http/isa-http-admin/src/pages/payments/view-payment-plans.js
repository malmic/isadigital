import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewPaymentPlans = ({ dispatch, paymentPlans, router }) => {
  useEffect(() => {
    dispatch(thunks.paymentPlans.getPaymentPlans());
  }, [dispatch]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/payments/create-payment-plan');
        }}
      >
        Create Payment Plan
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Name</th>
            <th>Quantity</th>
            <th>Edition</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {paymentPlans.map(paymentPlan => (
            <tr key={paymentPlan.referenceId}>
              <td>{paymentPlan.name}</td>
              <td>
                {paymentPlan.quantity.value} {paymentPlan.quantity.handle}
              </td>
              <td>{paymentPlan.editionHandle}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/payments/view-payment-plan',
                    query: { id: paymentPlan.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewPaymentPlans.propTypes = {
  dispatch: func.isRequired,
  paymentPlans: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  paymentPlans: state.paymentPlans.data,
}))(privateCheck(ViewPaymentPlans));
