import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  CardColumns,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
  Table,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewPayment = ({ dispatch, router, payment, emailAddresses }) => {
  useEffect(() => {
    const foreign = { id: router.query.id, typeHandle: 'USER' };

    dispatch(thunks.emailAddresses.getEmailAddresses({ foreign }));
    dispatch(thunks.payments.getPayment({ foreign: [foreign] }));
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Email Addresses</CardHeader>
          <CardBody>
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                {emailAddresses.map(emailAddress => (
                  <tr key={emailAddress.referenceId}>
                    <td>{emailAddress.address}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
        <Card>
          <CardHeader tag="h3">{payment.name}</CardHeader>
          <CardBody>
            <CardTitle>{payment.editionHandle}</CardTitle>
            <CardText>
              {payment.quantity.handle} {payment.quantity.value}
            </CardText>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewPayment.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  emailAddresses: array.isRequired,
  payment: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  payment: state.payments.payment,
  emailAddresses: state.emailAddresses.data,
}))(privateCheck(ViewPayment));
