import React, { useState, useEffect, useRef } from 'react';
import { func, array, object } from 'prop-types';
import {
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Table,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewPayments = ({ dispatch, notifications, payments, router }) => {
  const [search, setSearch] = useState(router.query.search || '');
  const formRef = useRef();

  useEffect(() => {
    dispatch(thunks.payments.findPayments({ search }));
  }, [dispatch, search]);

  const findPlans = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    router.push({ pathname: '/payments/view-payments', query: { search } });

    setSearch(search);
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Form innerRef={formRef}>
        <InputGroup>
          <Input
            type="text"
            name="search"
            defaultValue={search}
            placeholder="Find plans by email address..."
          />
          <InputGroupAddon addonType="append">
            <Button outline type="submit" onClick={findPlans} block>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </Form>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Email Address</th>
            <th>Plan Name</th>
            <th>Edition</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {payments.map(payment => (
            <tr key={payment.referenceId}>
              <td>{payment.emailAddress.address}</td>
              <td>{payment.name}</td>
              <td>{payment.editionHandle}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/payments/view-payment',
                    query: { id: payment.emailAddress.foreign.id },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewPayments.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  payments: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  payments: state.payments.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewPayments));
