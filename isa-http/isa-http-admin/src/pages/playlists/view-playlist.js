import React, { useEffect, useCallback, useMemo } from 'react';
import { func, object, array } from 'prop-types';
import { Row, Col, CardColumns, Card, CardHeader, CardBody } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PlaylistVideo } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewPlaylists = ({
  dispatch,
  playlist,
  router,
  notifications,
  sources,
  categories,
  rooms,
}) => {
  useEffect(() => {
    dispatch(thunks.playlists.getPlaylist({ referenceId: router.query.id }));
    dispatch(thunks.categories.getCategories());
    dispatch(thunks.rooms.getRooms());
    dispatch(thunks.sources.getSources());
  }, [dispatch, router.query.id]);

  const getSource = useCallback(
    videoId => sources.find(s => s.referenceId === videoId),
    [sources]
  );

  const categoriesWithParents = useMemo(() => {
    const next = id => {
      const matches = categories.filter(c => c.parent === id);

      let result = [];
      for (const match of matches) {
        const children = next(match.referenceId);
        result = [
          ...result,
          { name: `${match.name}`, referenceId: match.referenceId },
          ...children.map(child => ({
            name: `${match.name} / ${child.name}`,
            referenceId: child.referenceId,
          })),
        ];
      }

      return result;
    };

    if (categories.length > 0) {
      const root = categories.find(c => c.parent === '-1').referenceId;
      return next(root);
    }

    return [];
  }, [categories]);

  const createSource = (videoId, title, description, category, room) => {
    dispatch(
      thunks.sources.createSource(
        {
          referenceId: videoId,
          typeHandle: 'YOUTUBE',
          title,
          description,
          foreign: [
            { id: room, typeHandle: 'ROOM' },
            { id: category, typeHandle: 'CATEGORY' },
          ],
        },
        () => {}
      )
    );
  };

  const deleteSource = ({ referenceId }) => {
    dispatch(thunks.sources.deleteSource({ referenceId }, () => {}));
  };

  const updateSource = (videoId, title, category, room) => {
    dispatch(
      thunks.sources.updateSource(
        {
          referenceId: videoId,
          typeHandle: 'YOUTUBE',
          title,
          foreign: [
            { id: room, typeHandle: 'ROOM' },
            { id: category, typeHandle: 'CATEGORY' },
          ],
        },
        () => {}
      )
    );
  };

  const transformedData = useMemo(() => {
    const copy = [...playlist.items];
    copy.sort((a, b) => a.referenceId - b.referenceId > 0);
    return copy;
  }, [playlist.items]);

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <Row>
        <Col>
          <div className="mt-3" />
          <CardColumns>
            {transformedData.map(item => (
              <Card key={item.referenceId}>
                <CardHeader style={{ border: '0' }} tag="h3">
                  {item.title}
                </CardHeader>
                <CardBody>
                  <PlaylistVideo
                    source={getSource(item.videoId)}
                    categories={categoriesWithParents}
                    rooms={rooms}
                    originalTitle={item.title}
                    originalDescription={item.description}
                    videoId={item.videoId}
                    createSource={createSource}
                    updateSource={updateSource}
                    deleteSource={deleteSource}
                  />
                </CardBody>
              </Card>
            ))}
          </CardColumns>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

ViewPlaylists.propTypes = {
  dispatch: func.isRequired,
  playlist: object.isRequired,
  router: object.isRequired,
  notifications: array.isRequired,
  sources: array.isRequired,
  categories: array.isRequired,
  rooms: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  playlist: state.playlists.playlist,
  sources: state.sources.data,
  categories: state.categories.data,
  notifications: state.notifications,
  rooms: state.rooms.data,
}))(privateCheck(ViewPlaylists));
