import React, { useEffect, useState, useRef } from 'react';
import { func, array, object } from 'prop-types';
import {
  Table,
  Button,
  Row,
  Col,
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewPlaylists = ({ dispatch, notifications, playlists, router }) => {
  const [channelId, setChannelId] = useState('UCtETa_DdVQwB5aAS_Fde4bw');
  const formChannelRef = useRef();
  const formPlaylistRef = useRef();

  useEffect(() => {
    dispatch(thunks.playlists.getPlaylists({ channelId }));
  }, [channelId, dispatch]);

  const findChannels = e => {
    e.preventDefault();

    const { value: channelId } = formChannelRef.current['channel'];

    setChannelId(channelId);
  };

  const loadPlaylist = e => {
    e.preventDefault();

    const { value: playlist } = formPlaylistRef.current['playlist'];

    router.push({
      pathname: '/playlists/view-playlist',
      query: { id: playlist },
    });
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Row>
        <Col md="6">
          <Form innerRef={formChannelRef}>
            <InputGroup>
              <Input
                type="text"
                name="channel"
                defaultValue={channelId}
                placeholder={'Find channels'}
              />
              <InputGroupAddon addonType="append">
                <Button outline type="submit" onClick={findChannels} block>
                  Fetch Playlists
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
        </Col>
        <Col md="6">
          <Form innerRef={formPlaylistRef}>
            <InputGroup>
              <Input
                type="text"
                name="playlist"
                placeholder={'Load playlist'}
                defaultValue={'PL4o29bINVT4EG_y-k5jGoOu3-Am8Nvi10'}
              />
              <InputGroupAddon addonType="append">
                <Button outline type="submit" onClick={loadPlaylist} block>
                  Load Playlist
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
        </Col>
      </Row>
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Title</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {playlists.map(playlist => (
            <tr key={playlist.referenceId}>
              <td>{playlist.title}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/playlists/view-playlist',
                    query: { id: playlist.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewPlaylists.propTypes = {
  dispatch: func.isRequired,
  playlists: array.isRequired,
  notifications: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  playlists: state.playlists.data,
  notifications: state.notifications,
}))(privateCheck(ViewPlaylists));
