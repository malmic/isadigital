import React, { useMemo, useState, useEffect, useRef } from 'react';
import { array, func, object } from 'prop-types';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';
import moment from 'moment';
import { usePrevious } from '@isa-music/libs-effects';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateProfile = ({ dispatch, router, emailAddresses, profileTypes }) => {
  const [search, setSearch] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [instrument, setInstrument] = useState('');
  const [university, setUniversity] = useState('');
  const [avatarUrl, setAvatarUrl] = useState('');
  const [bornAt, setBornAt] = useState('YYYY/MM/DD HH:mm:ss');
  const [typeHandle, setTypeHandle] = useState(profileTypes[0].handle);
  const [description, setDescription] = useState('');
  const [emailAddress, setEmailAddress] = useState({
    foreign: { id: '', typeHandle: '' },
    address: '',
  });
  const formRef = useRef();
  const previousLength = usePrevious(emailAddresses.length);

  const handleSubmit = event => {
    event.preventDefault();

    const profile = (() => {
      if (typeHandle === 'SPONSOR')
        return {
          firstName,
          typeHandle,
          lastName,
          foreign: [emailAddress.foreign],
        };

      if (typeHandle === 'PROFESSOR')
        return {
          firstName,
          typeHandle,
          lastName,
          foreign: [emailAddress.foreign],
          instrument,
          university,
          description,
          avatarUrl,
        };

      if (typeHandle === 'STUDENT')
        return {
          firstName,
          typeHandle,
          lastName,
          foreign: [emailAddress.foreign],
          instrument,
          university,
          bornAt: moment(bornAt, 'YYYY/MM/DD HH:mm:ss').toDate(),
        };
    })();

    dispatch(
      thunks.profiles.createProfile(profile, id => {
        router.push({ pathname: '/profiles/view-profile', query: { id } });
      })
    );
  };

  useEffect(() => {
    dispatch(thunks.emailAddresses.findEmailAddresses({ search }));
  }, [dispatch, search]);

  useEffect(() => {
    if (emailAddresses.length > 0 && emailAddresses.length !== previousLength)
      setEmailAddress(emailAddresses[0]);
    else if (emailAddresses.length === 0 && emailAddress.address)
      setEmailAddress({ foreign: { id: '', typeHandle: '' }, address: '' });
  }, [emailAddress, emailAddresses, previousLength]);

  const submissionEnabled = useMemo(() => {
    if (!(firstName && typeHandle && lastName && emailAddress.address))
      return false;

    if (typeHandle === 'PROFESSOR')
      return instrument && university && description && avatarUrl;

    if (typeHandle === 'STUDENT')
      return (
        instrument &&
        university &&
        moment(bornAt, 'YYYY/MM/DD HH:mm:ss').isValid()
      );

    return true;
  }, [
    avatarUrl,
    bornAt,
    description,
    emailAddress.address,
    firstName,
    instrument,
    lastName,
    typeHandle,
    university,
  ]);

  const findEmailAddresses = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    setSearch(search);
  };

  const selectEmailAddress = e => {
    const foundAddress = emailAddresses.find(f => f.address === e.target.value);

    setEmailAddress(foundAddress);
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <Form innerRef={formRef}>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Email Address</InputGroupText>
              </InputGroupAddon>
              <Input
                name="search"
                type="text"
                placeholder="Find an email address..."
              />
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={findEmailAddresses}
                  block
                  type="submit"
                >
                  Go
                </Button>
              </InputGroupAddon>
            </InputGroup>
            <div className="mt-3" />
            <FormGroup>
              <Input
                type="select"
                name="emailAddress"
                onChange={selectEmailAddress}
                value={emailAddress.address}
              >
                {emailAddresses.map(emailAddress => (
                  <option
                    key={emailAddress.referenceId}
                    value={emailAddress.address}
                  >
                    {emailAddress.address}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>First Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={firstName}
                  onChange={e => setFirstName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Last Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={lastName}
                  onChange={e => setLastName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <Input
                type="select"
                name="typeHandle"
                onChange={e => setTypeHandle(e.target.value)}
                value={typeHandle}
              >
                {profileTypes.map(profileType => (
                  <option key={profileType.handle} value={profileType.handle}>
                    {profileType.name}
                  </option>
                ))}
              </Input>
            </FormGroup>
            {typeHandle === 'PROFESSOR' && (
              <>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Instrument</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="instrument"
                      value={instrument}
                      onChange={e => setInstrument(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>University</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="university"
                      value={university}
                      onChange={e => setUniversity(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Description</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="textarea"
                      name="description"
                      value={description}
                      onChange={e => setDescription(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Avatar URL</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="avatarUrl"
                      value={avatarUrl}
                      onChange={e => setAvatarUrl(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
              </>
            )}
            {typeHandle === 'STUDENT' && (
              <>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Instrument</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="instrument"
                      value={instrument}
                      onChange={e => setInstrument(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>University</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="university"
                      value={university}
                      onChange={e => setUniversity(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Born At</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      name="bornAt"
                      value={bornAt}
                      onChange={e => setBornAt(e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
              </>
            )}
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create Profile
            </Button>
          </Form>
        </Col>
      </Row>
      <div className="mt-3" />
    </PrivateLayout>
  );
};

CreateProfile.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  emailAddresses: array.isRequired,
  profileTypes: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  emailAddresses: state.emailAddresses.data,
  profileTypes: state.profiles.types,
}))(privateCheck(CreateProfile));
