import React, { useEffect, useState, useMemo } from 'react';
import { func, object, array } from 'prop-types';
import {
  Button,
  CardColumns,
  Table,
  Card,
  CardHeader,
  CardBody,
  CardImg,
  CardTitle,
  CardText,
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';
import { FormattedDate, FormattedTime } from 'react-intl';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewProfile = ({ dispatch, profile, router, emailAddresses, events }) => {
  const [event, setEvent] = useState('');

  const availableEvents = useMemo(
    () =>
      events.filter(
        event =>
          !profile.foreign.find(
            f => f.typeHandle === 'EVENT' && f.id === event.referenceId
          )
      ),
    [events, profile.foreign]
  );

  useEffect(() => {
    if (availableEvents.length > 0) setEvent(availableEvents[0].referenceId);
  }, [availableEvents]);

  useEffect(() => {
    const foreign = { id: router.query.id, typeHandle: 'USER' };

    dispatch(thunks.events.getEvents({}));
    dispatch(thunks.emailAddresses.getEmailAddresses({ foreign }));
    dispatch(thunks.profiles.getProfile({ foreign: [foreign] }));
  }, [dispatch, router.query.id]);

  const updateProfile = profile => {
    dispatch(thunks.profiles.updateProfile(profile));
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Email Addresses</CardHeader>
          <CardBody>
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                {emailAddresses.map(emailAddress => (
                  <tr key={emailAddress.referenceId}>
                    <td>{emailAddress.address}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
        <Card>
          <CardHeader tag="h3">
            {profile.firstName} {profile.lastName}
          </CardHeader>
          {profile.typeHandle === 'PROFESSOR' && (
            <CardImg
              top
              width="100%"
              src={profile.avatarUrl}
              alt="Card image cap"
            />
          )}
          <CardBody>
            {profile.typeHandle === 'PROFESSOR' && (
              <>
                <CardTitle>
                  Teaches {profile.instrument} at {profile.university}
                </CardTitle>
                <Input type="textarea" value={profile.description} />
                <div className="mt-3" />
                <Button outline onClick={() => {}}>
                  Update
                </Button>
              </>
            )}
            {profile.typeHandle === 'STUDENT' && (
              <>
                <CardTitle>
                  Studies {profile.instrument} at {profile.university}
                </CardTitle>
                <CardText>
                  Born on <FormattedDate value={new Date(profile.bornAt)} />{' '}
                  <FormattedTime value={new Date(profile.bornAt)} />
                </CardText>
              </>
            )}
            {profile.typeHandle === 'SPONSOR' && (
              <>
                <CardTitle>Sponsor</CardTitle>
                <CardText>No Additional information</CardText>
              </>
            )}
          </CardBody>
        </Card>
        <Card>
          <CardHeader tag="h3">Events</CardHeader>
          <CardBody>
            <Form>
              <InputGroup>
                <Input
                  type="select"
                  value={event}
                  onChange={e => setEvent(e.target.value)}
                >
                  {availableEvents.map(e => (
                    <option key={e.referenceId} value={e.referenceId}>
                      {e.name}
                    </option>
                  ))}
                </Input>
                <InputGroupAddon addonType="append">
                  <Button
                    outline
                    onClick={() => {
                      updateProfile({
                        ...profile,
                        foreign: [
                          ...profile.foreign,
                          { id: event, typeHandle: 'EVENT' },
                        ],
                      });
                    }}
                    block
                  >
                    Add Event
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </Form>
            <div className="mt-3" />
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Event Name</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {events
                  .filter(event =>
                    profile.foreign.find(
                      f =>
                        f.typeHandle === 'EVENT' && f.id === event.referenceId
                    )
                  )
                  .map(event => (
                    <tr key={event.referenceId}>
                      <td>{event.name}</td>
                      <td>
                        <Button
                          outline
                          onClick={e => {
                            updateProfile({
                              ...profile,
                              foreign: profile.foreign.filter(
                                f => f.id !== event.referenceId
                              ),
                            });
                          }}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewProfile.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  emailAddresses: array.isRequired,
  profile: object.isRequired,
  events: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  profile: state.profiles.profile,
  emailAddresses: state.emailAddresses.data,
  events: state.events.data,
}))(privateCheck(ViewProfile));
