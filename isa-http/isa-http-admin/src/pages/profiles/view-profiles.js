import React, { useEffect, useState } from 'react';
import { func, array, object } from 'prop-types';
import {
  Table,
  Button,
  ButtonGroup,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewProfiles = ({ dispatch, profiles, router, profileTypes }) => {
  const [typeHandle, setTypeHandle] = useState(
    router.query.typeHandle || profileTypes[0].handle
  );
  const [dropdownOpen, setDropdownOpen] = useState(false);

  useEffect(() => {
    dispatch(thunks.profiles.getProfiles({ typeHandle }));
  }, [dispatch, typeHandle]);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const handleTypeChange = e => {
    setTypeHandle(e.currentTarget.innerHTML);

    router.push({
      pathname: '/profiles/view-profiles',
      query: {
        typeHandle: e.currentTarget.innerHTML,
      },
    });
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <ButtonGroup>
        <Button
          onClick={() => {
            router.push('/profiles/create-profile');
          }}
        >
          Create Profile
        </Button>
        <ButtonDropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
          <DropdownToggle outline caret>
            {typeHandle}
          </DropdownToggle>
          <DropdownMenu>
            {profileTypes.map(type => (
              <DropdownItem onClick={handleTypeChange} key={type.handle}>
                {type.handle}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </ButtonDropdown>
      </ButtonGroup>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {profiles.map(profile => (
            <tr key={profile.referenceId}>
              <td>{profile.firstName}</td>
              <td>{profile.lastName}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/profiles/view-profile',
                    query: {
                      id: profile.foreign.find(f => f.typeHandle === 'USER').id,
                    },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewProfiles.propTypes = {
  dispatch: func.isRequired,
  profiles: array.isRequired,
  router: object.isRequired,
  profileTypes: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  profiles: state.profiles.data,
  profileTypes: state.profiles.types,
}))(privateCheck(ViewProfiles));
