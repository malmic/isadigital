import { func, object } from 'prop-types';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateRoom = ({ dispatch, router }) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.rooms.createRoom({ name, description }, id => {
        router.push({ pathname: '/rooms/view-room', query: { id } });
      })
    );
  };

  const submissionEnabled = useMemo(() => {
    return name && description;
  }, [description, name]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Description</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="description"
                  value={description}
                  onChange={e => setDescription(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateRoom.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(CreateRoom));
