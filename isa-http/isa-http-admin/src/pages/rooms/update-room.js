import { func, object } from 'prop-types';
import React, { useMemo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const UpdateRoom = ({ dispatch, router, room }) => {
  useEffect(() => {
    dispatch(thunks.rooms.getRoom({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [backgroundColor, setBackgroundColor] = useState('');
  const [backgroundImage, setBackgroundImage] = useState('');

  useEffect(() => {
    if (!name) setName(room.name);
  }, [name, room.name]);

  useEffect(() => {
    if (!description) setDescription(room.description);
  }, [description, room.description]);

  useEffect(() => {
    if (!backgroundColor) setBackgroundColor(room.backgroundColor);
  }, [backgroundColor, room.backgroundColor]);

  useEffect(() => {
    if (!backgroundImage) setBackgroundImage(room.backgroundImage);
  }, [backgroundImage, room.backgroundImage]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.rooms.updateRoom(
        {
          referenceId: router.query.id,
          name,
          description,
          backgroundImage,
          backgroundColor,
        },
        () => {
          router.push({
            pathname: '/rooms/view-room',
            query: { id: router.query.id },
          });
        }
      )
    );
  };

  const submissionEnabled = useMemo(
    () => name && description && backgroundColor && backgroundImage,
    [backgroundColor, backgroundImage, description, name]
  );

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Name</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="handle"
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Description</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="textarea"
                  name="description"
                  value={description}
                  onChange={e => setDescription(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Background Color</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="backgroundColor"
                  value={backgroundColor}
                  onChange={e => setBackgroundColor(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Background Image</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="backgroundImage"
                  value={backgroundImage}
                  onChange={e => setBackgroundImage(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Update
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

UpdateRoom.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  room: object.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  room: state.rooms.room,
}))(privateCheck(UpdateRoom));
