import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import {
  Card,
  CardHeader,
  CardBody,
  CardText,
  Table,
  ButtonGroup,
  Button,
  CardColumns,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';
import { ShowMoreLink } from '../../widgets';

const ViewRoom = ({ dispatch, room, router, sources, events }) => {
  useEffect(() => {
    dispatch(thunks.rooms.getRoom({ referenceId: router.query.id }));
    dispatch(
      thunks.sources.getSources({
        foreign: [{ id: router.query.id, typeHandle: 'ROOM' }],
      })
    );
    dispatch(
      thunks.events.getEvents({
        foreign: [{ id: router.query.id, typeHandle: 'ROOM' }],
      })
    );
  }, [dispatch, router.query.id]);

  const deleteRoom = room => {
    dispatch(
      thunks.rooms.deleteRoom(room, () => {
        router.push('/rooms/view-rooms');
      })
    );
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{room.name}</CardHeader>
          <CardBody>
            <CardText>{room.description}</CardText>
            <ButtonGroup>
              <Button
                outline
                onClick={e => {
                  router.push({
                    pathname: '/rooms/update-room',
                    query: { id: router.query.id },
                  });
                }}
              >
                Update
              </Button>
              <Button
                outline
                onClick={e => {
                  deleteRoom(room);
                }}
              >
                Delete
              </Button>
            </ButtonGroup>
          </CardBody>
        </Card>
        <Card style={{ border: 0 }}>
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Event Name</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              {events.map(event => (
                <tr key={event.referenceId}>
                  <td>{event.name}</td>
                  <td>
                    <ShowMoreLink
                      href={{
                        pathname: '/events/view-event',
                        query: { id: event.referenceId },
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
        <Card style={{ border: 0 }}>
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Source Title</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              {sources.map(source => (
                <tr key={source.referenceId}>
                  <td>{source.title || source.referenceId}</td>
                  <td>
                    <ShowMoreLink
                      href={{
                        pathname: '/sources/view-source',
                        query: { id: source.referenceId },
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewRoom.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  room: object.isRequired,
  sources: array.isRequired,
  events: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  room: state.rooms.room,
  sources: state.sources.data,
  events: state.events.data,
}))(privateCheck(ViewRoom));
