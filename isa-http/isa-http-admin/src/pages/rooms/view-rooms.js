import React, { useEffect, useMemo } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewRooms = ({ dispatch, rooms, router }) => {
  useEffect(() => {
    dispatch(thunks.rooms.getRooms());
  }, [dispatch]);

  const sortedRooms = useMemo(() => {
    const copy = [...rooms];
    copy.sort((a, b) => a.position - b.position);
    return copy;
  }, [rooms]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/rooms/create-room');
        }}
      >
        Create Room
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Position</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {sortedRooms.map((room, i) => (
            <tr key={room.referenceId}>
              <td>{room.name}</td>
              <td>{room.description}</td>
              <td>
                <Input
                  type="select"
                  name="position"
                  value={room.position}
                  onChange={e => {
                    dispatch(
                      thunks.rooms.updateRoom({
                        referenceId: room.referenceId,
                        position: e.target.value,
                      })
                    );
                  }}
                >
                  {rooms.map((room, i) => (
                    <option key={room.referenceId} value={i + 1}>
                      {i + 1}
                    </option>
                  ))}
                </Input>
              </td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/rooms/view-room',
                    query: { id: room.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewRooms.propTypes = {
  dispatch: func.isRequired,
  rooms: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  rooms: state.rooms.data,
}))(privateCheck(ViewRooms));
