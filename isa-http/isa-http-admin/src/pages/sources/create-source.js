import { func, object, array } from 'prop-types';
import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const CreateRoom = ({ dispatch, router, sourceTypes }) => {
  const [referenceId, setReferenceId] = useState('');
  const [typeHandle, setTypeHandle] = useState(sourceTypes[0].handle);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.sources.createSource({ typeHandle, referenceId }, id => {
        router.push({ pathname: '/sources/view-source', query: { id } });
      })
    );
  };

  const submissionEnabled = useMemo(() => {
    return referenceId && typeHandle;
  }, [referenceId, typeHandle]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Id</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="referenceId"
                  value={referenceId}
                  onChange={e => setReferenceId(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <Input
                type="select"
                name="typeHandle"
                onChange={e => setTypeHandle(e.target.value)}
                value={typeHandle}
              >
                {sourceTypes.map(sourceType => (
                  <option key={sourceType.handle} value={sourceType.handle}>
                    {sourceType.name}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Create
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

CreateRoom.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  sourceTypes: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  sourceTypes: state.sources.types,
}))(privateCheck(CreateRoom));
