import { func, object } from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';

const UpdateSource = ({ dispatch, router, source }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    dispatch(thunks.sources.getSource({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  useEffect(() => {
    if (source.title) setTitle(source.title);
  }, [source.title]);

  useEffect(() => {
    if (source.description) setDescription(source.description);
  }, [source.description]);

  const handleSubmit = event => {
    event.preventDefault();

    dispatch(
      thunks.sources.updateSource(
        {
          referenceId: router.query.id,
          title,
          description,
        },
        () => {
          router.push({
            pathname: '/sources/view-source',
            query: { id: router.query.id },
          });
        }
      )
    );
  };

  const submissionEnabled = useMemo(() => title && description, [
    description,
    title,
  ]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <Form>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Title</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="title"
                  value={title}
                  onChange={e => setTitle(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Description</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="text"
                  name="description"
                  value={description}
                  onChange={e => setDescription(e.target.value)}
                />
              </InputGroup>
            </FormGroup>
            <Button
              disabled={!submissionEnabled}
              block
              onClick={handleSubmit}
              type="submit"
            >
              Update Source
            </Button>
          </Form>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

UpdateSource.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
  source: object.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
  session: state.session,
  source: state.sources.source,
}))(privateCheck(UpdateSource));
