import React, { useEffect, useState, useMemo } from 'react';
import { func, object, array } from 'prop-types';
import {
  Card,
  CardBody,
  CardHeader,
  CardColumns,
  CardTitle,
  CardText,
  CardImg,
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Button,
  ButtonGroup,
  Table,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewSource = ({ dispatch, router, source, rooms, events }) => {
  const [room, setRoom] = useState('');
  const [event, setEvent] = useState('');

  const availableRooms = useMemo(
    () =>
      rooms.filter(
        room =>
          !source.foreign.find(
            f => f.typeHandle === 'ROOM' && f.id === room.referenceId
          )
      ),
    [rooms, source.foreign]
  );

  const availableEvents = useMemo(
    () =>
      events.filter(
        event =>
          !source.foreign.find(
            f => f.typeHandle === 'EVENT' && f.id === event.referenceId
          )
      ),
    [events, source.foreign]
  );

  useEffect(() => {
    if (availableRooms.length > 0) setRoom(availableRooms[0].referenceId);
  }, [availableRooms]);

  useEffect(() => {
    if (availableEvents.length > 0) setEvent(availableEvents[0].referenceId);
  }, [availableEvents]);

  useEffect(() => {
    dispatch(thunks.events.getEvents());
    dispatch(thunks.rooms.getRooms());
    dispatch(thunks.sources.getSource({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  const getSourceImage = source => {
    if (source.typeHandle === 'ZOOM') return '/images/zoom.png';
    if (source.typeHandle === 'GDRIVE') return '/images/gdrive.png';
    if (source.typeHandle === 'YOUTUBE')
      return `https://img.youtube.com/vi/${source.referenceId}/0.jpg`;

    return '';
  };

  const deleteSource = referenceId => {
    dispatch(
      thunks.sources.deleteSource({ referenceId }, () => {
        router.push('/sources/view-sources');
      })
    );
  };

  const updateSource = source => {
    dispatch(thunks.sources.updateSource(source));
  };

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">{source.title || 'No Title'}</CardHeader>
          <CardImg
            top
            width="100%"
            src={getSourceImage(source)}
            alt="Card image cap"
          />
          <CardBody>
            <CardTitle>{source.referenceId}</CardTitle>
            <CardText>{source.description || 'No Description'}</CardText>
            <ButtonGroup>
              <Button
                outline
                onClick={() => {
                  router.push({
                    pathname: '/sources/update-source',
                    query: { id: router.query.id },
                  });
                }}
              >
                Update Source
              </Button>
              <Button
                outline
                onClick={() => {
                  // deleteSource(source.referenceId);
                }}
              >
                Delete Source
              </Button>
            </ButtonGroup>
          </CardBody>
        </Card>
        <Card style={{ border: 0 }}>
          <Form>
            <InputGroup>
              <Input
                type="select"
                value={room}
                onChange={e => setRoom(e.target.value)}
              >
                {availableRooms.map(e => (
                  <option key={e.referenceId} value={e.referenceId}>
                    {e.name}
                  </option>
                ))}
              </Input>
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={() => {
                    updateSource({
                      ...source,
                      foreign: [
                        ...source.foreign,
                        { id: room, typeHandle: 'ROOM' },
                      ],
                    });
                  }}
                  block
                >
                  Add Room
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Room Name</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {rooms
                .filter(room =>
                  source.foreign.find(
                    f => f.typeHandle === 'ROOM' && f.id === room.referenceId
                  )
                )
                .map(room => (
                  <tr key={room.referenceId}>
                    <td>{room.name}</td>
                    <td>
                      <Button
                        outline
                        onClick={e => {
                          updateSource({
                            ...source,
                            foreign: source.foreign.filter(
                              f => f.id !== room.referenceId
                            ),
                          });
                        }}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Card>
        <Card style={{ border: 0 }}>
          <Form>
            <InputGroup>
              <Input
                type="select"
                value={event}
                onChange={e => setEvent(e.target.value)}
              >
                {availableEvents.map(e => (
                  <option key={e.referenceId} value={e.referenceId}>
                    {e.name}
                  </option>
                ))}
              </Input>
              <InputGroupAddon addonType="append">
                <Button
                  outline
                  onClick={() => {
                    updateSource({
                      ...source,
                      foreign: [
                        ...source.foreign,
                        { id: event, typeHandle: 'EVENT' },
                      ],
                    });
                  }}
                  block
                >
                  Add Event
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
          <div className="mt-3" />
          <Table responsive bordered>
            <thead className="thead-light">
              <tr>
                <th>Event Name</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {events
                .filter(event =>
                  source.foreign.find(
                    f => f.typeHandle === 'EVENT' && f.id === event.referenceId
                  )
                )
                .map(event => (
                  <tr key={event.referenceId}>
                    <td>{event.name}</td>
                    <td>
                      <Button
                        outline
                        onClick={e => {
                          updateSource({
                            ...source,
                            foreign: source.foreign.filter(
                              f => f.id !== event.referenceId
                            ),
                          });
                        }}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewSource.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  source: object.isRequired,
  rooms: array.isRequired,
  events: array.isRequired,
};

export default connect(state => ({
  session: state.session,
  source: state.sources.source,
  rooms: state.rooms.data,
  events: state.events.data,
}))(privateCheck(ViewSource));
