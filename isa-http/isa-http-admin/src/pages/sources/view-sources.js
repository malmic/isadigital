import React, { useEffect } from 'react';
import { func, array, object } from 'prop-types';
import { Table, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { ShowMoreLink } from '../../widgets';
import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewSources = ({ dispatch, sources, router }) => {
  useEffect(() => {
    dispatch(thunks.sources.getSources());
  }, [dispatch]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <Button
        outline
        onClick={() => {
          router.push('/sources/create-source');
        }}
      >
        Create Source
      </Button>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Reference Id</th>
            <th>Type Handle</th>
            <th>Title</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {sources.map(source => (
            <tr key={source.referenceId}>
              <td>{source.referenceId}</td>
              <td>{source.typeHandle}</td>
              <td>{source.title}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/sources/view-source',
                    query: { id: source.referenceId },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewSources.propTypes = {
  dispatch: func.isRequired,
  sources: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  sources: state.sources.data,
}))(privateCheck(ViewSources));
