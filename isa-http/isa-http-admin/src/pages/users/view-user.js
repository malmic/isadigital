import React, { useEffect } from 'react';
import { func, object, array } from 'prop-types';
import { CardColumns, Card, CardBody, CardHeader, Table } from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { PrivateLayout } from '../../layouts';
import { thunks } from '../../store';

const ViewUser = ({ dispatch, router, emailAddresses, user }) => {
  useEffect(() => {
    const foreign = { id: router.query.id, typeHandle: 'USER' };

    dispatch(thunks.emailAddresses.getEmailAddresses({ foreign }));
    dispatch(thunks.users.getUser({ referenceId: router.query.id }));
  }, [dispatch, router.query.id]);

  return (
    <PrivateLayout>
      <div className="mt-3" />
      <CardColumns>
        <Card>
          <CardHeader tag="h3">Email Addresses</CardHeader>
          <CardBody>
            <Table responsive>
              <thead className="thead-light">
                <tr>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                {emailAddresses.map(emailAddress => (
                  <tr key={emailAddress.referenceId}>
                    <td>{emailAddress.address}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
        <Card>
          <CardHeader tag="h3">User</CardHeader>
          <CardBody>
            Language Handle {user.languageHandle}
            <br />
            Valid Scopes {user.validScopes}
          </CardBody>
        </Card>
      </CardColumns>
    </PrivateLayout>
  );
};

ViewUser.propTypes = {
  dispatch: func.isRequired,
  router: object.isRequired,
  emailAddresses: array.isRequired,
  user: object.isRequired,
};

export default connect(state => ({
  session: state.session,
  emailAddresses: state.emailAddresses.data,
  user: state.users.user,
}))(privateCheck(ViewUser));
