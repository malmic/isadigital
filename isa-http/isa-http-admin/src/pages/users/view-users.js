import React, { useState, useEffect, useRef } from 'react';
import { func, array, object } from 'prop-types';
import {
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Table,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import { privateCheck } from '@isa-music/libs-hocs';

import { thunks } from '../../store';
import { PrivateLayout } from '../../layouts';
import { ShowMoreLink } from '../../widgets';

const ViewUsers = ({ dispatch, notifications, users, router }) => {
  const [search, setSearch] = useState(router.query.search || '');
  const formRef = useRef();

  useEffect(() => {
    dispatch(thunks.users.findUsers({ search }));
  }, [dispatch, search]);

  const findUsers = e => {
    e.preventDefault();

    const search = formRef.current['search'].value;
    router.push({ pathname: '/users/view-users', query: { search } });

    setSearch(search);
  };

  return (
    <PrivateLayout
      notifications={notifications}
      removeNotification={handle =>
        dispatch(thunks.notifications.removeNotification(handle))
      }
    >
      <div className="mt-3" />
      <Form innerRef={formRef}>
        <InputGroup>
          <Input
            type="text"
            name="search"
            defaultValue={search}
            placeholder={'Find users by email address...'}
          />
          <InputGroupAddon addonType="append">
            <Button outline type="submit" onClick={findUsers} block>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </Form>
      <div className="mt-3" />
      <Table responsive striped bordered>
        <thead className="thead-light">
          <tr>
            <th>Email Address</th>
            <th>Scopes</th>
            <th>Language Handle</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user => (
            <tr key={user.referenceId}>
              <td>{user.emailAddress.address}</td>
              <td>{user.validScopes}</td>
              <td>{user.languageHandle}</td>
              <td>
                <ShowMoreLink
                  href={{
                    pathname: '/users/view-user',
                    query: { id: user.emailAddress.foreign.id },
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </PrivateLayout>
  );
};

ViewUsers.propTypes = {
  notifications: array.isRequired,
  dispatch: func.isRequired,
  users: array.isRequired,
  router: object.isRequired,
};

export default connect(state => ({
  users: state.users.data,
  notifications: state.notifications,
  session: state.session,
}))(privateCheck(ViewUsers));
