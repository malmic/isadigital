const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAddress', async ctx => {
    const response = await services['addresses'].getAddressAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/findAddresses', async ctx => {
    const { search } = ctx.request.body;

    if (!search) {
      ctx.body = { result: [] };
      return;
    }

    const response = await services['emailAddresses'].findEmailAddressesAsync({
      search,
    });

    const result = (await Promise.all(
      response.result.map(async emailAddress => {
        const { result: address } = await services['addresses'].getAddressAsync(
          { foreign: emailAddress.foreign }
        );

        return { ...address, emailAddress };
      })
    )).filter(address => Object.keys(address).length > 1);

    ctx.body = { result };
  });

  return router;
};
