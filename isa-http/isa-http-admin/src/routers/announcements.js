const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAnnouncements', async ctx => {
    const response = await services['announcements'].getAnnouncementsAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/createAnnouncement', async ctx => {
    const response = await services['announcements'].createAnnouncementAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getAnnouncement', async ctx => {
    const response = await services['announcements'].getAnnouncementAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/deleteAnnouncement', async ctx => {
    const response = await services['announcements'].deleteAnnouncementAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
