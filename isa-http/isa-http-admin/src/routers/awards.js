const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getAward', async ctx => {
    const response = await services['awards'].getAwardAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/getAwards', async ctx => {
    const response = await services['awards'].getAwardsAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/createAward', async ctx => {
    const response = await services['awards'].createAwardAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/updateAward', async ctx => {
    const response = await services['awards'].updateAwardAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
