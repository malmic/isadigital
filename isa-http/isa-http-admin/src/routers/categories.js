const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getCategories', async ctx => {
    const response = await services['categories'].getCategoriesAsync({});

    ctx.body = response;
  });

  router.post('/getCategory', async ctx => {
    const response = await services['categories'].getCategoryAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/createCategory', async ctx => {
    const response = await services['categories'].createCategoryAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/deleteCategory', async ctx => {
    const { result: sources } = await services['sources'].getSourcesAsync({
      foreign: [{ id: ctx.request.body.referenceId, typeHandle: 'CATEGORY' }],
    });

    if (sources.length > 0)
      throw new Error(
        'Some sources depend on this category. Delete the source first.'
      );

    const response = await services['categories'].deleteCategoryAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/updateCategory', async ctx => {
    const response = await services['categories'].updateCategoryAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
