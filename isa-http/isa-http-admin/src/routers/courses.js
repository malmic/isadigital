const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getCourse', async ctx => {
    const response = await services['courses'].getCourseAsync(ctx.request.body);

    const profiles = {};
    for (const foreign of response.result.foreign) {
      const { result: profile } = await services['profiles'].getProfileAsync({
        foreign: [foreign],
      });

      profiles[profile.typeHandle] = profile;
    }

    const result = { referenceId: response.result.referenceId, ...profiles };

    ctx.body = { result };
  });

  router.post('/findCourses', async ctx => {
    const response = await services['profiles'].findProfilesAsync(
      ctx.request.body
    );

    const result = (await Promise.all(
      response.result.map(async profile => {
        const { result: coursesWithoutProfiles } = await services[
          'courses'
        ].getCoursesAsync({
          foreign: profile.foreign,
        });

        const courses = await Promise.all(
          coursesWithoutProfiles.map(async course => {
            const profiles = {};
            for (const foreign of course.foreign) {
              const { result: profile } = await services[
                'profiles'
              ].getProfileAsync({
                foreign: [foreign],
              });

              profiles[profile.typeHandle] = profile;
            }

            return { referenceId: course.referenceId, ...profiles };
          })
        );

        return { courses };
      })
    ))
      .filter(entries => entries.courses.length >= 1)
      .reduce((acc, cur) => [...acc, ...cur.courses], []);

    ctx.body = { result };
  });

  router.post('/createCourse', async ctx => {
    const { PROFESSOR, STUDENT } = ctx.request.body;

    const response = await services['courses'].createCourseAsync({
      foreign: [PROFESSOR.foreign[0], STUDENT.foreign[0]],
    });

    ctx.body = response;
  });

  return router;
};
