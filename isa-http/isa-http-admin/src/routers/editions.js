const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getEditions', async ctx => {
    const response = await services['editions'].getEditionsAsync({});

    ctx.body = response;
  });

  router.post('/getEdition', async ctx => {
    const response = await services['editions'].getEditionAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/createEdition', async ctx => {
    const response = await services['editions'].createEditionAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/deleteEdition', async ctx => {
    const response = await services['editions'].deleteEditionAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
