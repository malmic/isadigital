const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/findEmailAddresses', async ctx => {
    const { search } = ctx.request.body;

    /* if (!search) {
      ctx.request.body = { result: [] };
    } */

    const response = await services['emailAddresses'].findEmailAddressesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getEmailAddresses', async ctx => {
    const response = await services['emailAddresses'].getEmailAddressesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
