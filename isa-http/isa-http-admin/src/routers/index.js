const addresses = require('./addresses');
const announcements = require('./announcements');
const awards = require('./awards');
const categories = require('./categories');
const courses = require('./courses');
const editions = require('./editions');
const emailAddresses = require('./emailAddresses');
const events = require('./events');
const jurors = require('./jurors');
const manifest = require('./manifest');
const oauth = require('./oauth');
const paymentPlans = require('./paymentPlans');
const payments = require('./payments');
const playlists = require('./playlists');
const profiles = require('./profiles');
const rooms = require('./rooms');
const session = require('./session');
const sources = require('./sources');
const sponsors = require('./sponsors');
const users = require('./users');

module.exports = {
  addresses,
  announcements,
  awards,
  categories,
  courses,
  editions,
  emailAddresses,
  events,
  jurors,
  manifest,
  oauth,
  paymentPlans,
  payments,
  playlists,
  profiles,
  rooms,
  session,
  sources,
  sponsors,
  users,
};
