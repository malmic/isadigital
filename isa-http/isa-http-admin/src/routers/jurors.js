const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getJurors', async ctx => {
    const response = await services['jurors'].getJurorsAsync({});

    const result = (await Promise.all(
      response.result.map(async juror => {
        const { result: emailAddresses } = await services[
          'emailAddresses'
        ].getEmailAddressesAsync({ foreign: juror.foreign });

        return emailAddresses;
      })
    )).reduce((acc, cur) => [...acc, ...cur], []);

    ctx.body = { result };
  });

  router.post('/createJuror', async ctx => {
    const response = await services['jurors'].createJurorAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/deleteJuror', async ctx => {
    const response = await services['jurors'].deleteJurorAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
