const Router = require('koa-router');

module.exports = () => {
  const router = new Router();
  router.get('/', async ctx => {
    ctx.body = { name: 'isa admin', short_name: 'isa admin' };
  });

  return router;
};
