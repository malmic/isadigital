const Router = require('koa-router');

module.exports = (env, services) => {
  const GOOGLE = JSON.parse(env.GOOGLE);

  const router = new Router();
  router.get('/google', async ctx => {
    const { result: url } = await services['google'].getUri({
      query: { access_type: 'offline' },
    });

    ctx.redirect(url);
  });

  router.get('/google/callback', async ctx => {
    const { result: oauthResponse } = await services['google'].getToken({
      url: ctx.originalUrl,
    });

    await services['accessTokens'].createAccessToken({
      ...oauthResponse,
      clientId: GOOGLE.clientId,
    });

    ctx.body = { oauthResponse };
  });

  return router;
};
