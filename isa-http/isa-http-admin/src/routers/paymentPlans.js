const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();
  router.post('/createPaymentPlan', async ctx => {
    const { result: quantity } = await services['transforms'].toQuantityAsync(
      ctx.request.body.quantity
    );

    const response = await services['paymentPlans'].createPaymentPlanAsync({
      ...ctx.request.body,
      quantity,
    });

    ctx.body = response;
  });

  router.post('/getPaymentPlan', async ctx => {
    const { result } = await services['paymentPlans'].getPaymentPlanAsync(
      ctx.request.body
    );

    const { result: quantity } = await services['transforms'].toQuantityAsync(
      result.quantity
    );

    ctx.body = { result: { ...result, quantity } };
  });

  router.post('/getPaymentPlans', async ctx => {
    const response = await services['paymentPlans'].getPaymentPlansAsync(
      ctx.request.body
    );

    const result = await Promise.all(
      response.result.map(async plan => {
        const { result: quantity } = await services[
          'transforms'
        ].toQuantityAsync(plan.quantity);

        return { ...plan, quantity };
      })
    );

    ctx.body = { ...response, result };
  });

  router.post('/deletePaymentPlan', async ctx => {
    const response = await services['paymentPlans'].deletePaymentPlanAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
