const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getPayment', async ctx => {
    const { result } = await services['payments'].getPaymentAsync(
      ctx.request.body
    );

    const { result: quantity } = await services['transforms'].toQuantityAsync(
      result.quantity
    );

    ctx.body = { result: { ...result, quantity } };
  });

  router.post('/findPayments', async ctx => {
    const { search } = ctx.request.body;

    if (!search) {
      ctx.body = { result: [] };
      return;
    }

    const response = await services['emailAddresses'].findEmailAddressesAsync({
      search,
    });

    const result = (await Promise.all(
      response.result.map(async emailAddress => {
        const { result: payment } = await services['payments'].getPaymentAsync({
          foreign: [emailAddress.foreign],
        });

        const { result: quantity } = await services[
          'transforms'
        ].toQuantityAsync(payment.quantity);

        return { ...payment, quantity, emailAddress };
      })
    )).filter(payment => Object.keys(payment).length > 1);

    ctx.body = { result };
  });

  return router;
};
