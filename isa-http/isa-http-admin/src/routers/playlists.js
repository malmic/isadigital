const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();
  router.post('/getPlaylists', async ctx => {
    const response = await services['playlists'].getPlaylistsAsync({
      ...ctx.request.body,
    });

    ctx.body = response;
  });

  router.post('/getPlaylist', async ctx => {
    const response = await services['playlists'].getPlaylistAsync({
      ...ctx.request.body,
    });

    ctx.body = response;
  });

  return router;
};
