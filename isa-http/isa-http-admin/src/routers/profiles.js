const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getProfiles', async ctx => {
    const response = await services['profiles'].getProfilesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getProfile', async ctx => {
    const response = await services['profiles'].getProfileAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/updateProfile', async ctx => {
    const response = await services['profiles'].updateProfileAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/createProfile', async ctx => {
    const response = await services['profiles'].createProfileAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
