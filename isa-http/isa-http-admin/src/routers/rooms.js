const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getRooms', async ctx => {
    const response = await services['rooms'].getRoomsAsync({});

    ctx.body = response;
  });

  router.post('/getRoom', async ctx => {
    const response = await services['rooms'].getRoomAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/createRoom', async ctx => {
    const response = await services['rooms'].createRoomAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/deleteRoom', async ctx => {
    const { referenceId } = ctx.request.body;

    const { result: events } = await services['events'].getEventsAsync({
      foreign: [{ id: referenceId, typeHandle: 'ROOM' }],
    });

    if (events.length > 0)
      throw new Error('Events still connect to this room.');

    const { result: sources } = await services['sources'].getSourcesAsync({
      foreign: [{ id: referenceId, typeHandle: 'ROOM' }],
    });

    if (sources.length > 0)
      throw new Error('Sources still connect to this room.');

    const response = await services['rooms'].deleteRoomAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/updateRoom', async ctx => {
    const response = await services['rooms'].updateRoomAsync(ctx.request.body);

    ctx.body = response;
  });

  return router;
};
