const Router = require('koa-router');

const getSanitizedRedirect = next => {
  return /^\/[a-zA-Z\-]+$/.test(next) ? next : '/';
};

module.exports = (env, services) => {
  const router = new Router();

  router.post('/login', async ctx => {
    try {
      const { username, password } = ctx.request.body;

      const { result: emailAddress } = await services[
        'emailAddresses'
      ].getEmailAddressAsync({ address: username });

      if (!emailAddress) ctx.throw(404, 'User not found!');

      const { foreign } = emailAddress;

      const { result: passwordsEqual } = await services[
        'password'
      ].passwordMatchesAsync({
        foreign,
        plaintextPassword: password,
      });

      if (!passwordsEqual) ctx.throw(403, 'Passwords are not equal!');

      const { result: user } = await services['users'].getUserAsync({
        referenceId: foreign.id,
      });

      if (!user.validScopes.includes('admin.*'))
        ctx.throw(403, 'Access denied!');

      ctx.session.foreign = foreign;
    } catch (e) {
      console.log(e);
      ctx.redirect('/login');
      return;
    }

    ctx.redirect(getSanitizedRedirect(ctx.request.body.next));
  });

  router.get('/delete', async ctx => {
    try {
      ctx.session = null;
    } catch (e) {
      console.log(e);
    } finally {
      ctx.redirect('/');
    }
  });

  router.get('/me', async ctx => {
    const { foreign } = ctx.session;

    const { result: user } = await services['users'].getUserAsync({
      referenceId: foreign.id,
    });

    const { result: complements } = await services[
      'complements'
    ].getComplementsAsync({});
    const { result: assets } = await services['assets'].getAssetsAsync({
      types: [],
    });
    const { result: units } = await services['units'].getUnitsAsync({});
    const { result: conversions } = await services[
      'conversions'
    ].getConversionsAsync({});

    ctx.body = { result: { assets, complements, conversions, units, user } };
  });

  return router;
};
