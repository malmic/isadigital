const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/createSource', async ctx => {
    const response = await services['sources'].createSourceAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/deleteSource', async ctx => {
    const response = await services['sources'].deleteSourceAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/updateSource', async ctx => {
    const response = await services['sources'].updateSourceAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getSources', async ctx => {
    const response = await services['sources'].getSourcesAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  router.post('/getSource', async ctx => {
    const response = await services['sources'].getSourceAsync(ctx.request.body);

    ctx.body = response;
  });

  return router;
};
