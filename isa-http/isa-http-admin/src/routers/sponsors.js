const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getSponsors', async ctx => {
    const response = await services['sponsors'].getSponsorsAsync(
      ctx.request.body
    );

    ctx.body = response;
  });

  return router;
};
