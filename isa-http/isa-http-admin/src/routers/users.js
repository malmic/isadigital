const Router = require('koa-router');

module.exports = (env, services) => {
  const router = new Router();

  router.post('/getUser', async ctx => {
    const response = await services['users'].getUserAsync(ctx.request.body);

    ctx.body = response;
  });

  router.post('/findUsers', async ctx => {
    const { search } = ctx.request.body;

    if (!search) {
      ctx.body = { result: [] };
      return;
    }

    const response = await services['emailAddresses'].findEmailAddressesAsync({
      search,
    });

    const result = (await Promise.all(
      response.result.map(async emailAddress => {
        const { result: user } = await services['users'].getUserAsync({
          referenceId: emailAddress.foreign.id,
        });

        return { ...user, emailAddress };
      })
    )).filter(user => Object.keys(user).length > 1);

    ctx.body = { result };
  });

  return router;
};
