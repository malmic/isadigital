export default [
  {
    module: 'announcements',
    pathname: '/announcements/view-announcements',
    title: 'Announcements',
  },
  {
    module: 'awards',
    pathname: '/awards/view-awards',
    title: 'Awards',
  },
  {
    module: 'awards',
    pathname: '/jurors/view-jurors',
    title: 'Jurors',
  },
  {
    module: 'editions',
    pathname: '/editions/view-editions',
    title: 'Editions',
  },

  {
    module: 'editions',
    pathname: '/events/view-events',
    title: 'Events',
  },
  {
    module: 'rooms',
    pathname: '/rooms/view-rooms',
    title: 'Rooms',
  },
  {
    module: 'payments',
    pathname: '/payments/view-payments',
    title: 'Payments',
  },
  {
    module: 'payments',
    pathname: '/payments/view-payment-plans',
    title: 'Payment Plans',
  },
  {
    module: 'playlists',
    pathname: '/playlists/view-playlists',
    title: 'Playlists',
  },
  {
    module: 'playlists',
    pathname: '/categories/view-categories',
    title: 'Categories',
  },
  {
    module: 'profiles',
    pathname: '/profiles/view-profiles',
    title: 'Profiles',
  },
  {
    module: 'profiles',
    pathname: '/addresses/view-addresses',
    title: 'Addresses',
  },
  {
    module: 'courses',
    pathname: '/courses/view-courses',
    title: 'Courses',
  },
  {
    module: 'users',
    pathname: '/users/view-users',
    title: 'Users',
  },
  {
    module: 'sources',
    pathname: '/sources/view-sources',
    title: 'Sources',
  },
];
