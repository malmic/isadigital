const ADDRESS_FETCHED = 'ADDRESS_FETCHED';
const ADDRESSES_FETCHED = 'ADDRESSES_FETCHED';

const addressFetched = data => ({
  type: ADDRESS_FETCHED,
  data,
});

const addressesFetched = data => ({
  type: ADDRESSES_FETCHED,
  data,
});

export { ADDRESS_FETCHED, ADDRESSES_FETCHED, addressFetched, addressesFetched };
