const ANNOUNCEMENT_FETCHED = 'ANNOUNCEMENT_FETCHED';
const ANNOUNCEMENTS_FETCHED = 'ANNOUNCEMENTS_FETCHED';
const ANNOUNCEMENT_CREATED = 'ANNOUNCEMENT_CREATED';
const ANNOUNCEMENT_UPDATED = 'ANNOUNCEMENT_UPDATED';
const ANNOUNCEMENT_DELETED = 'ANNOUNCEMENT_DELETED';

const announcementFetched = data => ({
  type: ANNOUNCEMENT_FETCHED,
  data,
});

const announcementsFetched = data => ({
  type: ANNOUNCEMENTS_FETCHED,
  data,
});

const announcementCreated = data => ({
  type: ANNOUNCEMENT_CREATED,
  data,
});

const announcementUpdated = data => ({
  type: ANNOUNCEMENT_UPDATED,
  data,
});

const announcementDeleted = data => ({
  type: ANNOUNCEMENT_DELETED,
  data,
});

export {
  ANNOUNCEMENT_FETCHED,
  ANNOUNCEMENTS_FETCHED,
  ANNOUNCEMENT_CREATED,
  ANNOUNCEMENT_UPDATED,
  ANNOUNCEMENT_DELETED,
  announcementFetched,
  announcementsFetched,
  announcementCreated,
  announcementUpdated,
  announcementDeleted,
};
