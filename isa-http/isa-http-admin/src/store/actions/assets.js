const ASSETS_FETCHED = 'ASSETS_FETCHED';
const CONVERSIONS_FETCHED = 'CONVERSIONS_FETCHED';
const UNITS_FETCHED = 'UNITS_FETCHED';
const COMPLEMENTS_FETCHED = 'COMPLEMENTS_FETCHED';

const assetsFetched = assets => ({
  type: ASSETS_FETCHED,
  assets,
});

const conversionsFetched = conversions => ({
  type: CONVERSIONS_FETCHED,
  conversions,
});

const unitsFetched = units => ({
  type: UNITS_FETCHED,
  units,
});

const complementsFetched = complements => ({
  type: COMPLEMENTS_FETCHED,
  complements,
});

export {
  assetsFetched,
  conversionsFetched,
  unitsFetched,
  complementsFetched,
  ASSETS_FETCHED,
  CONVERSIONS_FETCHED,
  UNITS_FETCHED,
  COMPLEMENTS_FETCHED,
};
