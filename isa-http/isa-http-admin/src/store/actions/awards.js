const AWARDS_FETCHED = 'AWARDS_FETCHED';
const AWARD_FETCHED = 'AWARD_FETCHED';
const AWARD_CREATED = 'AWARD_CREATED';
const AWARD_UPDATED = 'AWARD_UPDATED';

const awardsFetched = data => ({
  type: AWARDS_FETCHED,
  data,
});

const awardFetched = data => ({
  type: AWARD_FETCHED,
  data,
});

const awardCreated = data => ({
  type: AWARD_CREATED,
  data,
});

const awardUpdated = data => ({
  type: AWARD_UPDATED,
  data,
});

export {
  AWARDS_FETCHED,
  AWARD_FETCHED,
  AWARD_CREATED,
  AWARD_UPDATED,
  awardsFetched,
  awardFetched,
  awardCreated,
  awardUpdated,
};
