const CATEGORIES_FETCHED = 'CATEGORIES_FETCHED';
const CATEGORY_FETCHED = 'CATEGORY_FETCHED';
const CATEGORY_CREATED = 'CATEGORY_CREATED';
const CATEGORY_DELETED = 'CATEGORY_DELETED';
const CATEGORY_UPDATED = 'CATEGORY_UPDATED';

const categoriesFetched = data => ({
  type: CATEGORIES_FETCHED,
  data,
});

const categoryFetched = data => ({
  type: CATEGORY_FETCHED,
  data,
});

const categoryCreated = data => ({
  type: CATEGORY_CREATED,
  data,
});

const categoryDeleted = data => ({
  type: CATEGORY_DELETED,
  data,
});

const categoryUpdated = data => ({
  type: CATEGORY_UPDATED,
  data,
});

export {
  CATEGORIES_FETCHED,
  CATEGORY_FETCHED,
  CATEGORY_CREATED,
  CATEGORY_DELETED,
  CATEGORY_UPDATED,
  categoriesFetched,
  categoryFetched,
  categoryCreated,
  categoryDeleted,
  categoryUpdated,
};
