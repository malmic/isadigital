const COURSES_FETCHED = 'COURSES_FETCHED';
const COURSE_FETCHED = 'COURSE_FETCHED';
const COURSE_CREATED = 'COURSE_CREATED';

const coursesFetched = data => ({
  type: COURSES_FETCHED,
  data,
});

const courseFetched = data => ({
  type: COURSE_FETCHED,
  data,
});

const courseCreated = data => ({
  type: COURSE_CREATED,
  data,
});

export {
  COURSES_FETCHED,
  COURSE_FETCHED,
  COURSE_CREATED,
  coursesFetched,
  courseFetched,
  courseCreated,
};
