const EDITIONS_FETCHED = 'EDITIONS_FETCHED';
const EDITION_FETCHED = 'EDITION_FETCHED';
const EDITION_CREATED = 'EDITION_CREATED';
const EDITION_DELETED = 'EDITION_DELETED';

const editionsFetched = data => ({
  type: EDITIONS_FETCHED,
  data,
});

const editionFetched = data => ({
  type: EDITION_FETCHED,
  data,
});

const editionCreated = data => ({
  type: EDITION_CREATED,
  data,
});

const editionDeleted = data => ({
  type: EDITION_DELETED,
  data,
});

export {
  EDITIONS_FETCHED,
  EDITION_FETCHED,
  EDITION_CREATED,
  EDITION_DELETED,
  editionsFetched,
  editionFetched,
  editionCreated,
  editionDeleted,
};
