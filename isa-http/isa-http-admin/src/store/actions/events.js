const EVENTS_FETCHED = 'EVENTS_FETCHED';
const EVENT_FETCHED = 'EVENT_FETCHED';
const EVENT_CREATED = 'EVENT_CREATED';
const EVENT_UPDATED = 'EVENT_UPDATED';

const eventsFetched = data => ({
  type: EVENTS_FETCHED,
  data,
});

const eventFetched = data => ({
  type: EVENT_FETCHED,
  data,
});

const eventCreated = data => ({
  type: EVENT_CREATED,
  data,
});

const eventUpdated = data => ({
  type: EVENT_UPDATED,
  data,
});

export {
  EVENTS_FETCHED,
  EVENT_FETCHED,
  EVENT_CREATED,
  EVENT_UPDATED,
  eventsFetched,
  eventFetched,
  eventCreated,
  eventUpdated,
};
