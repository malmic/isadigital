const JURORS_FETCHED = 'JURORS_FETCHED';
const JUROR_CREATED = 'JUROR_CREATED';
const JUROR_DELETED = 'JUROR_DELETED';

const jurorsFetched = data => ({
  type: JURORS_FETCHED,
  data,
});

const jurorCreated = data => ({
  type: JUROR_CREATED,
  data,
});

const jurorDeleted = data => ({
  type: JUROR_DELETED,
  data,
});

export {
  JURORS_FETCHED,
  JUROR_CREATED,
  JUROR_DELETED,
  jurorsFetched,
  jurorCreated,
  jurorDeleted,
};
