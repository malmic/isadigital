const NOTIFICATION_CREATED = 'NOTIFICATION_CREATED';
const NOTIFICATION_REMOVED = 'NOTIFICATION_REMOVED';

const parseMessage = message => {
  // a rest error message
  if (message.response) {
    if (message.response.data) {
      if (message.response.data.error) {
        return message.response.data.error;
      }

      return message.response.data;
    }

    return message.response.statusText;
  }

  // all other errors
  if (message.message) {
    return message.message;
  }

  return message;
};

const notificationCreated = (type, objOrMessage, duration = 5000) => {
  if (type === 'ERROR') {
    console.log(objOrMessage);
  }

  const handle = Math.random()
    .toString(36)
    .substring(2, 15);

  return {
    type: NOTIFICATION_CREATED,
    data: {
      type,
      handle,
      message: parseMessage(objOrMessage),
      duration: duration === Infinity ? 'Infinity' : duration,
    },
  };
};

const notificationRemoved = handle => {
  return {
    type: NOTIFICATION_REMOVED,
    data: {
      handle,
    },
  };
};

export {
  NOTIFICATION_CREATED,
  NOTIFICATION_REMOVED,
  notificationCreated,
  notificationRemoved,
};
