const PAYMENT_PLANS_FETCHED = 'PAYMENT_PLANS_FETCHED';
const PAYMENT_PLAN_CREATED = 'PAYMENT_PLAN_CREATED';
const PAYMENT_PLAN_FETCHED = 'PAYMENT_PLAN_FETCHED';
const PAYMENT_PLAN_DELETED = 'PAYMENT_PLAN_DELETED';

const paymentPlansFetched = data => ({
  type: PAYMENT_PLANS_FETCHED,
  data,
});

const paymentPlanCreated = data => ({
  type: PAYMENT_PLAN_CREATED,
  data,
});

const paymentPlanFetched = data => ({
  type: PAYMENT_PLAN_FETCHED,
  data,
});

const paymentPlanDeleted = data => ({
  type: PAYMENT_PLAN_DELETED,
  data,
});

export {
  paymentPlansFetched,
  paymentPlanCreated,
  paymentPlanFetched,
  paymentPlanDeleted,
  PAYMENT_PLANS_FETCHED,
  PAYMENT_PLAN_CREATED,
  PAYMENT_PLAN_FETCHED,
  PAYMENT_PLAN_DELETED,
};
