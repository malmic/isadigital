const PAYMENT_FETCHED = 'PAYMENT_FETCHED';
const PAYMENTS_FETCHED = 'PAYMENTS_FETCHED';

const paymentFetched = data => ({
  type: PAYMENT_FETCHED,
  data,
});

const paymentsFetched = data => ({
  type: PAYMENTS_FETCHED,
  data,
});

export { PAYMENT_FETCHED, PAYMENTS_FETCHED, paymentFetched, paymentsFetched };
