const PLAYLISTS_FETCHED = 'PLAYLISTS_FETCHED';
const PLAYLIST_FETCHED = 'PLAYLIST_FETCHED';

const playlistsFetched = data => ({
  type: PLAYLISTS_FETCHED,
  data,
});

const playlistFetched = data => ({
  type: PLAYLIST_FETCHED,
  data,
});

export {
  PLAYLISTS_FETCHED,
  PLAYLIST_FETCHED,
  playlistsFetched,
  playlistFetched,
};
