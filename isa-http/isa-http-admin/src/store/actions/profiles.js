const PROFILES_FETCHED = 'PROFILES_FETCHED';
const PROFILE_FETCHED = 'PROFILE_FETCHED';
const PROFILE_TYPES_FETCHED = 'PROFILE_TYPES_FETCHED';
const PROFILE_UPDATED = 'PROFILE_UPDATED';
const PROFILE_CREATED = 'PROFILE_CREATED';

const profilesFetched = data => ({
  type: PROFILES_FETCHED,
  data,
});

const profileFetched = data => ({
  type: PROFILE_FETCHED,
  data,
});

const profileTypesFetched = data => ({
  type: PROFILE_TYPES_FETCHED,
  data,
});

const profileUpdated = data => ({
  type: PROFILE_UPDATED,
  data,
});

const profileCreated = data => ({
  type: PROFILE_CREATED,
  data,
});

export {
  PROFILES_FETCHED,
  PROFILE_FETCHED,
  PROFILE_TYPES_FETCHED,
  PROFILE_UPDATED,
  PROFILE_CREATED,
  profilesFetched,
  profileFetched,
  profileTypesFetched,
  profileUpdated,
  profileCreated,
};
