const ROOMS_FETCHED = 'ROOMS_FETCHED';
const ROOM_FETCHED = 'ROOM_FETCHED';
const ROOM_CREATED = 'ROOM_CREATED';
const ROOM_DELETED = 'ROOM_DELETED';
const ROOM_UPDATED = 'ROOM_UPDATED';

const roomsFetched = data => ({
  type: ROOMS_FETCHED,
  data,
});

const roomFetched = data => ({
  type: ROOM_FETCHED,
  data,
});

const roomCreated = data => ({
  type: ROOM_CREATED,
  data,
});

const roomDeleted = data => ({
  type: ROOM_DELETED,
  data,
});

const roomUpdated = data => ({
  type: ROOM_UPDATED,
  data,
});

export {
  ROOMS_FETCHED,
  ROOM_FETCHED,
  ROOM_CREATED,
  ROOM_DELETED,
  ROOM_UPDATED,
  roomsFetched,
  roomFetched,
  roomCreated,
  roomDeleted,
  roomUpdated,
};
