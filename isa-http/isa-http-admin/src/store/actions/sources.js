const SOURCE_CREATED = 'SOURCE_CREATED';
const SOURCE_DELETED = 'SOURCE_DELETED';
const SOURCES_FETCHED = 'SOURCES_FETCHED';
const SOURCE_FETCHED = 'SOURCE_FETCHED';
const SOURCE_TYPES_FETCHED = 'SOURCE_TYPES_FETCHED';
const SOURCE_UPDATED = 'SOURCE_UPDATED';

const sourceCreated = data => ({
  type: SOURCE_CREATED,
  data,
});

const sourceDeleted = data => ({
  type: SOURCE_DELETED,
  data,
});

const sourceUpdated = data => ({
  type: SOURCE_UPDATED,
  data,
});

const sourcesFetched = data => ({
  type: SOURCES_FETCHED,
  data,
});

const sourceFetched = data => ({
  type: SOURCE_FETCHED,
  data,
});

const sourceTypesFetched = data => ({
  type: SOURCE_TYPES_FETCHED,
  data,
});

export {
  SOURCE_CREATED,
  SOURCE_DELETED,
  SOURCE_UPDATED,
  SOURCES_FETCHED,
  SOURCE_FETCHED,
  SOURCE_TYPES_FETCHED,
  sourceCreated,
  sourceDeleted,
  sourceUpdated,
  sourcesFetched,
  sourceFetched,
  sourceTypesFetched,
};
