const USER_FETCHED = 'USER_FETCHED';
const USERS_FETCHED = 'USERS_FETCHED';

const userFetched = data => ({
  type: USER_FETCHED,
  data,
});

const usersFetched = data => ({
  type: USERS_FETCHED,
  data,
});

export { USER_FETCHED, USERS_FETCHED, userFetched, usersFetched };
