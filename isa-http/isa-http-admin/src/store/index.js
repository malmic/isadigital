import axios from 'axios';
import qs from 'query-string';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';

import initialState from './initialState';
import rootReducer from './reducers';
import {
  Addresses,
  Announcements,
  Awards,
  Categories,
  Courses,
  Editions,
  EmailAddresses,
  Events,
  Jurors,
  Notifications,
  PaymentPlans,
  Payments,
  Playlists,
  Profiles,
  Rooms,
  Sources,
  Users,
} from './thunks';

const app = axios.create({
  baseURL: '',
  withCredentials: true,
  paramsSerializer: params => qs.stringify(params),
});

const thunks = {
  addresses: new Addresses(app),
  announcements: new Announcements(app),
  awards: new Awards(app),
  categories: new Categories(app),
  courses: new Courses(app),
  editions: new Editions(app),
  emailAddresses: new EmailAddresses(app),
  events: new Events(app),
  jurors: new Jurors(app),
  notifications: new Notifications(),
  paymentPlans: new PaymentPlans(app),
  payments: new Payments(app),
  playlists: new Playlists(app),
  profiles: new Profiles(app),
  rooms: new Rooms(app),
  sources: new Sources(app),
  users: new Users(app),
};

const createClientStore = (state = initialState) => {
  return createStore(
    rootReducer,
    state,
    composeWithDevTools(applyMiddleware(thunk))
  );
};

export { thunks, createClientStore };
