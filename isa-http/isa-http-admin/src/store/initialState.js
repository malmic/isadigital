export default {
  addresses: {
    data: [],
    address: {
      street: '',
      postalCode: '',
      city: '',
      country: '',
    },
  },
  announcements: {
    announcement: {
      title: '',
      body: '',
      foreign: [],
    },
    data: [],
  },
  assets: {
    assets: {},
    conversions: {},
    units: {},
    complements: {},
  },
  awards: {
    award: { editionHandle: 0, name: '' },
    data: [],
  },
  categories: {
    data: [],
    category: {
      name: '',
    },
  },
  courses: {
    course: {
      referenceId: '',
      PROFESSOR: '',
      STUDENT: '',
    },
    data: [],
  },
  editions: {
    data: [],
    edition: {},
  },
  emailAddresses: {
    data: [],
  },
  events: {
    event: {
      name: '',
      beginAt: new Date(),
      endAt: new Date(),
      foreign: [],
      editionHandle: 1970,
    },
    data: [],
  },
  jurors: {
    data: [],
  },
  notifications: [],
  paymentPlans: {
    data: [],
    paymentPlan: {
      name: '',
      quantity: { handle: '', value: 0 },
    },
  },
  payments: {
    data: [],
    payment: {
      quantity: { handle: '', value: 0 },
    },
  },
  playlists: {
    data: [],
    playlist: {
      items: [],
    },
  },
  profiles: {
    profile: {
      foreign: [],
    },
    data: [],
    types: [],
  },
  rooms: {
    data: [],
    room: {
      name: '',
      description: '',
      backgroundColor: '',
      backgroundImage: '',
    },
  },
  session: {},
  sources: {
    types: [],
    data: [],
    source: {
      foreign: [],
      typeHandle: '',
      referenceId: '',
    },
  },
  users: {
    data: [],
    user: {
      languageHandle: '',
      validScopes: '',
    },
  },
};
