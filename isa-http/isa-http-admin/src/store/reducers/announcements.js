import initialState from '../initialState';
import { announcements } from '../actions';

export default (state = initialState.announcements, action) => {
  switch (action.type) {
    case announcements.ANNOUNCEMENT_FETCHED: {
      return { ...state, announcement: { ...action.data } };
    }

    case announcements.ANNOUNCEMENTS_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case announcements.ANNOUNCEMENT_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        room: { ...initialState.announcements.announcement },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    case announcements.ANNOUNCEMENT_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    default:
      return state;
  }
};
