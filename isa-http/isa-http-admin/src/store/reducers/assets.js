import initialState from '../initialState';
import { assets } from '../actions';

export default (state = initialState.assets, action) => {
  switch (action.type) {
    case assets.ASSETS_FETCHED: {
      return {
        ...state,
        assets: action.assets.reduce((acc, asset) => {
          return {
            ...acc,
            [asset.handle]: asset,
          };
        }, {}),
      };
    }

    case assets.COMPLEMENTS_FETCHED: {
      return {
        ...state,
        complements: action.complements,
      };
    }

    case assets.CONVERSIONS_FETCHED: {
      return {
        ...state,
        conversions: action.conversions.reduce((acc, conversion) => {
          const handle = `${conversion.baseHandle} / ${conversion.quoteHandle}`;

          return {
            ...acc,
            [handle]: conversion,
          };
        }, {}),
      };
    }

    case assets.UNITS_FETCHED: {
      return {
        ...state,
        units: action.units.reduce((acc, unit) => {
          return {
            ...acc,
            [unit.handle]: unit,
          };
        }, {}),
      };
    }

    default:
      return state;
  }
};
