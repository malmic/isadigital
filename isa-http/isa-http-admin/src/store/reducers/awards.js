import initialState from '../initialState';
import { awards } from '../actions';

export default (state = initialState.awards, action) => {
  switch (action.type) {
    case awards.AWARDS_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case awards.AWARD_FETCHED: {
      return { ...state, award: { ...action.data } };
    }

    case awards.AWARD_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case awards.AWARD_UPDATED: {
      return { ...state, award: { ...state.award, ...action.award } };
    }

    default:
      return state;
  }
};
