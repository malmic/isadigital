import initialState from '../initialState';
import { categories } from '../actions';

export default (state = initialState.categories, action) => {
  switch (action.type) {
    case categories.CATEGORIES_FETCHED: {
      return { ...state, data: action.data };
    }

    case categories.CATEGORY_FETCHED: {
      return { ...state, category: action.data };
    }

    case categories.CATEGORY_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case categories.CATEGORY_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        room: { ...initialState.categories.category },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    case categories.CATEGORY_UPDATED: {
      let i = state.data.findIndex(
        room => room.referenceId === action.data.referenceId
      );

      return {
        ...state,
        room: { ...action.data },
        data: [
          ...state.data.slice(0, i),
          { ...state.data[i], ...action.data },
          ...state.data.slice(i + 1),
        ],
      };
    }

    default:
      return state;
  }
};
