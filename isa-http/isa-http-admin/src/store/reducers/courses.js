import initialState from '../initialState';
import { courses } from '../actions';

export default (state = initialState.courses, action) => {
  switch (action.type) {
    case courses.COURSES_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case courses.COURSE_FETCHED: {
      return { ...state, course: { ...action.data } };
    }

    case courses.COURSE_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    default:
      return state;
  }
};
