import initialState from '../initialState';
import { editions } from '../actions';

export default (state = initialState.editions, action) => {
  switch (action.type) {
    case editions.EDITIONS_FETCHED: {
      return { ...state, data: action.data };
    }

    case editions.EDITION_FETCHED: {
      return { ...state, edition: action.data };
    }

    case editions.EDITION_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case editions.EDITION_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        edition: { ...initialState.editions.edition },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    default:
      return state;
  }
};
