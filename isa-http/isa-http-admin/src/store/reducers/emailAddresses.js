import initialState from '../initialState';
import { emailAddresses } from '../actions';

export default (state = initialState.emailAddresses, action) => {
  switch (action.type) {
    case emailAddresses.EMAIL_ADDRESSES_FETCHED: {
      return { ...state, data: action.data };
    }

    default:
      return state;
  }
};
