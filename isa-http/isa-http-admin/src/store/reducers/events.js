import initialState from '../initialState';
import { events } from '../actions';

export default (state = initialState.events, action) => {
  switch (action.type) {
    case events.EVENTS_FETCHED: {
      return { ...state, data: action.data };
    }

    case events.EVENT_FETCHED: {
      return { ...state, event: action.data };
    }

    case events.EVENT_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case events.EVENT_UPDATED: {
      return { ...state, event: { ...state.event, ...action.data } };
    }

    default:
      return state;
  }
};
