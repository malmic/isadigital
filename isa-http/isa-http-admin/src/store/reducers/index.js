import { combineReducers } from 'redux';

import addresses from './addresses';
import announcements from './announcements';
import assets from './assets';
import awards from './awards';
import categories from './categories';
import courses from './courses';
import editions from './editions';
import emailAddresses from './emailAddresses';
import events from './events';
import jurors from './jurors';
import notifications from './notifications';
import paymentPlans from './paymentPlans';
import payments from './payments';
import playlists from './playlists';
import profiles from './profiles';
import rooms from './rooms';
import session from './session';
import sources from './sources';
import users from './users';

export default combineReducers({
  addresses,
  announcements,
  assets,
  awards,
  categories,
  courses,
  editions,
  emailAddresses,
  events,
  jurors,
  notifications,
  paymentPlans,
  payments,
  playlists,
  profiles,
  rooms,
  session,
  sources,
  users,
});
