import initialState from '../initialState';
import { jurors } from '../actions';

export default (state = initialState.jurors, action) => {
  switch (action.type) {
    case jurors.JURORS_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case jurors.JUROR_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case jurors.JUROR_DELETED: {
      const i = state.data.findIndex(
        d => d.foreign.id === action.data.foreign.id
      );

      return {
        ...state,
        data: [...state.data.slice(0, i), ...state.data.slice(i + 1)],
      };
    }

    default:
      return state;
  }
};
