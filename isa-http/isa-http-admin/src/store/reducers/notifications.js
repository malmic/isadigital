import initialState from '../initialState';
import { notifications } from '../actions';

export default (state = initialState.notifications, action) => {
  switch (action.type) {
    case notifications.NOTIFICATION_CREATED: {
      return [...state, { ...action.data }];
    }

    case notifications.NOTIFICATION_REMOVED: {
      const found = state.findIndex(n => {
        return n.handle === action.data.handle;
      });

      if (found > -1) {
        const copy = [...state];
        copy.splice(found, 1);
        return copy;
      }

      return state;
    }

    default:
      return state;
  }
};
