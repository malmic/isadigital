import initialState from '../initialState';
import { paymentPlans } from '../actions/';

export default (state = initialState.paymentPlans, action) => {
  switch (action.type) {
    case paymentPlans.PAYMENT_PLANS_FETCHED:
      return { ...state, data: [...action.data] };

    case paymentPlans.PAYMENT_PLAN_CREATED:
      return { ...state, data: [...state.data, action.data] };

    case paymentPlans.PAYMENT_PLAN_FETCHED:
      return { ...state, paymentPlan: { ...action.data } };

    case paymentPlans.PAYMENT_PLAN_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        paymentPlan: { ...initialState.paymentPlans.paymentPlan },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    default:
      return state;
  }
};
