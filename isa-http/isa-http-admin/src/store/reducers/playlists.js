import initialState from '../initialState';
import { sources, playlists } from '../actions';

export default (state = initialState.playlists, action) => {
  switch (action.type) {
    case playlists.PLAYLISTS_FETCHED: {
      return { ...state, data: action.data };
    }

    case playlists.PLAYLIST_FETCHED: {
      return { ...state, playlist: action.data };
    }

    /* case sources.SOURCE_CREATED: {
      const copy = [...state.playlist.items];
      const idx = copy.findIndex(i => {
        return i.videoId === action.data.referenceId;
      });

      copy[idx].source = action.data;

      return {
        ...state,
        playlist: {
          ...state.playlist,
          items: copy,
        },
      };
    }

    case sources.SOURCE_DELETED: {
      const copy = [...state.playlist.items];

      const idx = copy.findIndex(i => {
        return i.source
          ? i.source.referenceId === action.data.referenceId
          : false;
      });

      delete copy[idx].source;

      return {
        ...state,
        playlist: {
          ...state.playlist,
          items: copy,
        },
      };
    } */

    default:
      return state;
  }
};
