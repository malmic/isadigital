import initialState from '../initialState';
import { profiles } from '../actions';

export default (state = initialState.profiles, action) => {
  switch (action.type) {
    case profiles.PROFILES_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    case profiles.PROFILE_FETCHED: {
      return { ...state, profile: action.data };
    }

    case profiles.PROFILE_TYPES_FETCHED: {
      return { ...state, types: action.data };
    }

    case profiles.PROFILE_UPDATED: {
      return { ...state, profile: { ...state.profile, ...action.data } };
    }

    case profiles.PROFILE_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    default:
      return state;
  }
};
