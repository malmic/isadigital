import initialState from '../initialState';
import { rooms } from '../actions';

export default (state = initialState.rooms, action) => {
  switch (action.type) {
    case rooms.ROOMS_FETCHED: {
      return { ...state, data: action.data };
    }

    case rooms.ROOM_FETCHED: {
      return { ...state, room: action.data };
    }

    case rooms.ROOM_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case rooms.ROOM_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        room: { ...initialState.rooms.room },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    case rooms.ROOM_UPDATED: {
      let i = state.data.findIndex(
        room => room.referenceId === action.data.referenceId
      );

      return {
        ...state,
        room: { ...action.data },
        data: [
          ...state.data.slice(0, i),
          {
            ...state.data[i],
            ...action.data,
          },
          ...state.data.slice(i + 1),
        ],
      };
    }

    default:
      return state;
  }
};
