import initialState from '../initialState';
import { session } from '../actions';

export default (state = initialState.session, action) => {
  switch (action.type) {
    case session.SESSION_CREATED: {
      return { ...action.session };
    }

    default:
      return state;
  }
};
