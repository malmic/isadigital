import initialState from '../initialState';
import { sources } from '../actions';

export default (state = initialState.sources, action) => {
  switch (action.type) {
    case sources.SOURCES_FETCHED: {
      return { ...state, data: action.data };
    }

    case sources.SOURCE_FETCHED: {
      return { ...state, source: { ...action.data } };
    }

    case sources.SOURCE_TYPES_FETCHED: {
      return { ...state, types: [...action.data] };
    }

    case sources.SOURCE_UPDATED: {
      return { ...state, source: { ...state.source, ...action.data } };
    }

    case sources.SOURCE_CREATED: {
      return { ...state, data: [...state.data, action.data] };
    }

    case sources.SOURCE_DELETED: {
      const { data } = action;
      const foundIndex = state.data.findIndex(
        row => row.referenceId === data.referenceId
      );

      return {
        ...state,
        room: { ...initialState.sources.source },
        data: [
          ...state.data.slice(0, foundIndex),
          ...state.data.slice(foundIndex + 1),
        ],
      };
    }

    default:
      return state;
  }
};
