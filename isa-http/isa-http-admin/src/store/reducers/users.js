import initialState from '../initialState';
import { users } from '../actions';

export default (state = initialState.users, action) => {
  switch (action.type) {
    case users.USER_FETCHED: {
      return { ...state, user: action.data };
    }

    case users.USERS_FETCHED: {
      return { ...state, data: [...action.data] };
    }

    default:
      return state;
  }
};
