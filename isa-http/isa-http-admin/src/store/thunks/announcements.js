import { announcements, notifications } from '../actions';

export default class Announcements {
  constructor(app) {
    this.app = app;
  }

  getAnnouncements(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/announcements/getAnnouncements', query);

        dispatch(announcements.announcementsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getAnnouncement(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/announcements/getAnnouncement', query);

        dispatch(announcements.announcementFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteAnnouncement(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/announcements/deleteAnnouncement', query);

        dispatch(announcements.announcementDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createAnnouncement(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/announcements/createAnnouncement', query);

        dispatch(
          announcements.announcementCreated({ referenceId: result, ...query })
        );

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
