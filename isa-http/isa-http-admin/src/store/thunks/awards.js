import { awards, notifications } from '../actions';

export default class Awards {
  constructor(app) {
    this.app = app;
  }

  createAward(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/awards/createAward', query);

        dispatch(awards.awardCreated({ referenceId: result, ...query }));

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getAward(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/awards/getAward', query);

        dispatch(awards.awardFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getAwards(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/awards/getAwards', query);

        dispatch(awards.awardsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateAward(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/awards/updateAward', query);

        dispatch(awards.awardUpdated(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
