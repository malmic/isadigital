import { categories, notifications } from '../actions';

export default class Categories {
  constructor(app) {
    this.app = app;
  }

  getCategories() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/categories/getCategories');

        dispatch(categories.categoriesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getCategory(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/categories/getCategory', query);

        dispatch(categories.categoryFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createCategory(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/categories/createCategory', query);

        dispatch(categories.categoryCreated({ referenceId: result, ...query }));

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteCategory(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/categories/deleteCategory', query);

        dispatch(categories.categoryDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateCategory(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/categories/updateCategory', query);

        dispatch(categories.categoryUpdated(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
