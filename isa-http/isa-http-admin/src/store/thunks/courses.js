import { courses, notifications } from '../actions';

export default class Courses {
  constructor(app) {
    this.app = app;
  }

  createCourse(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/courses/createCourse', query);

        dispatch(courses.courseCreated({ referenceId: result, ...query }));

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getCourse(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/courses/getCourse', query);

        dispatch(courses.courseFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  findCourses(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/courses/findCourses', query);

        dispatch(courses.coursesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
