import { editions, notifications } from '../actions';

export default class Editions {
  constructor(app) {
    this.app = app;
  }

  getEditions() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/editions/getEditions');

        dispatch(editions.editionsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getEdition(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/editions/getEdition', query);

        dispatch(editions.editionFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createEdition(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/editions/createEdition', query);

        dispatch(editions.editionCreated({ referenceId: result, ...query }));

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteEdition(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/editions/deleteEdition', query);

        dispatch(editions.editionDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
