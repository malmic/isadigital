import { emailAddresses, notifications } from '../actions';

export default class EmailAddresses {
  constructor(app) {
    this.app = app;
  }

  findEmailAddresses(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/emailAddresses/findEmailAddresses', query);

        dispatch(emailAddresses.emailAddressesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getEmailAddresses(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/emailAddresses/getEmailAddresses', query);

        dispatch(emailAddresses.emailAddressesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
