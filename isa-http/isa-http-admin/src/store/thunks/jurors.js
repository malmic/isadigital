import { jurors, notifications } from '../actions';

export default class Jurors {
  constructor(app) {
    this.app = app;
  }

  getJurors() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/jurors/getJurors');

        dispatch(jurors.jurorsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createJuror(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/jurors/createJuror', query);

        dispatch(jurors.jurorCreated({ referenceId: result, ...query }));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteJuror(query) {
    return async dispatch => {
      try {
        await this.app.post('/jurors/deleteJuror', query);

        dispatch(jurors.jurorDeleted(query));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
