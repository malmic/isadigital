import { paymentPlans, notifications } from '../actions';

export default class PaymentPlans {
  constructor(app) {
    this.app = app;
  }

  getPaymentPlans() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/paymentPlans/getPaymentPlans');

        dispatch(paymentPlans.paymentPlansFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getPaymentPlan(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/paymentPlans/getPaymentPlan', query);

        dispatch(paymentPlans.paymentPlanFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createPaymentPlan(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/paymentPlans/createPaymentPlan', query);

        dispatch(
          paymentPlans.paymentPlanCreated({ referenceId: result, ...query })
        );

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deletePaymentPlan(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/paymentPlans/deletePaymentPlan', query);

        dispatch(paymentPlans.paymentPlanDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
