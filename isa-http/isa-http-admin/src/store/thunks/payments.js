import { payments, notifications } from '../actions';

export default class Payments {
  constructor(app) {
    this.app = app;
  }

  getPayment(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/payments/getPayment', query);

        dispatch(payments.paymentFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  findPayments(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/payments/findPayments', query);

        dispatch(payments.paymentsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
