import { playlists, notifications } from '../actions';

export default class Playlists {
  constructor(app) {
    this.app = app;
  }

  getPlaylists(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/playlists/getPlaylists', query);

        dispatch(playlists.playlistsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getPlaylist(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/playlists/getPlaylist', query);

        dispatch(playlists.playlistFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
