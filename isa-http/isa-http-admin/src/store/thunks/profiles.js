import { profiles, notifications } from '../actions';

export default class Profiles {
  constructor(app) {
    this.app = app;
  }

  getProfiles(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/profiles/getProfiles', query);

        dispatch(profiles.profilesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getProfile(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/profiles/getProfile', query);

        dispatch(profiles.profileFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateProfile(query) {
    return async dispatch => {
      try {
        await this.app.post('/profiles/updateProfile', query);

        dispatch(profiles.profileUpdated(query));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createProfile(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/profiles/createProfile', query);

        dispatch(profiles.profileCreated({ referenceId: result, ...query }));

        cb(query.foreign[0].id);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
