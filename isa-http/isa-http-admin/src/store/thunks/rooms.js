import { rooms, notifications } from '../actions';

export default class Rooms {
  constructor(app) {
    this.app = app;
  }

  getRooms() {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/rooms/getRooms');

        dispatch(rooms.roomsFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getRoom(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/rooms/getRoom', query);

        dispatch(rooms.roomFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  createRoom(query, cb) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/rooms/createRoom', query);

        dispatch(rooms.roomCreated({ referenceId: result, ...query }));

        cb(result);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteRoom(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/rooms/deleteRoom', query);

        dispatch(rooms.roomDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateRoom(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/rooms/updateRoom', query);

        dispatch(rooms.roomUpdated(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
