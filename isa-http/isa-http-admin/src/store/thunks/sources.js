import { sources, notifications } from '../actions';

export default class Sources {
  constructor(app) {
    this.app = app;
  }

  createSource(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/sources/createSource', query);

        dispatch(sources.sourceCreated(query));

        cb(query.referenceId);
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  deleteSource(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/sources/deleteSource', query);

        dispatch(sources.sourceDeleted(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  updateSource(query, cb) {
    return async dispatch => {
      try {
        await this.app.post('/sources/updateSource', query);

        dispatch(sources.sourceUpdated(query));

        cb();
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getSources(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/sources/getSources', query);

        dispatch(sources.sourcesFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  getSource(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/sources/getSource', query);

        dispatch(sources.sourceFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
