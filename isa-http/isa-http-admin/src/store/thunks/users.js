import { users, notifications } from '../actions';

export default class Users {
  constructor(app) {
    this.app = app;
  }

  getUser(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/users/getUser', query);

        dispatch(users.userFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }

  findUsers(query) {
    return async dispatch => {
      try {
        const {
          data: { result },
        } = await this.app.post('/users/findUsers', query);

        dispatch(users.usersFetched(result));
      } catch (err) {
        dispatch(notifications.notificationCreated('ERROR', err));
      }
    };
  }
}
