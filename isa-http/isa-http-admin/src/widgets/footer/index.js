import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Container, Row, Col } from 'reactstrap';

const Footer = ({ className }) => (
  <div className={className}>
    <Container>
      <Row>
        <Col>
          <Row>
            <Col xs={6}>&copy; isa {new Date().getFullYear()}</Col>
            <Col xs={6} />
          </Row>
        </Col>
      </Row>
    </Container>
  </div>
);

Footer.propTypes = {
  className: PropTypes.string.isRequired,
};

export default styled(Footer)`
  line-height: 60px;
  height: 60px;
  background-color: #f8f9fa !important;
  border-top: 1px solid #f0eae6;
`;
