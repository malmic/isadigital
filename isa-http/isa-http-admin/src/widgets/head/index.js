import Head from 'next/head';

const Header = () => {
  return (
    <Head>
      <meta charSet="utf-8" />
      <title>isa summer academy</title>
      <link rel="manifest" href="/manifest" />
      <link
        href="https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css"
        rel="stylesheet"
      />
      <link
        rel="icon"
        href="https://digital.isa-music.org/wp-content/themes/isa/images/icon-32x32.png"
        sizes="32x32"
      />
      <link
        rel="icon"
        href="https://digital.isa-music.org/wp-content/themes/isa/images/icon-192x192.png"
        sizes="192x192"
      />
      <link
        rel="apple-touch-icon-precomposed"
        href="https://digital.isa-music.org/wp-content/themes/isa/images/icon-180x180.png"
      />
      <meta
        name="msapplication-TileImage"
        content="https://digital.isa-music.org/wp-content/themes/isa/images/icon-270x270.png"
      />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=yes"
      />
    </Head>
  );
};

export default Header;
