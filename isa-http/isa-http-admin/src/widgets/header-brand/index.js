import Link from 'next/link';

const HeaderBrand = () => {
  return (
    <Link href={'/'}>
      <a className="navbar-brand">ISA</a>
    </Link>
  );
};

export default HeaderBrand;
