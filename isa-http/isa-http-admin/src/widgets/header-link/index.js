import React from 'react';
import { node, string } from 'prop-types';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { NavLink } from 'reactstrap';

const HeaderLink = ({ to, children }) => {
  const router = useRouter();
  const active = to === router.pathname;

  return (
    <Link href={to} passHref>
      <NavLink className="text-uppercase" active={active}>
        {children}
      </NavLink>
    </Link>
  );
};

HeaderLink.propTypes = {
  children: node.isRequired,
  to: string.isRequired,
};

export default HeaderLink;
