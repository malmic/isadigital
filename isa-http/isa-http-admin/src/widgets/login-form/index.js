import React, { useEffect, useRef } from 'react';
import { string } from 'prop-types';
import { Button, Form, FormGroup, Input } from 'reactstrap';

const LoginForm = ({ next }) => {
  const usernameRef = useRef();

  useEffect(() => {
    usernameRef.current.focus();
  }, []);

  return (
    <Form method="post" action="/session/login">
      <input type="hidden" name="next" value={next} />
      <FormGroup>
        <Input
          innerRef={usernameRef}
          type="username"
          placeholder="Email Address"
          name="username"
        />
      </FormGroup>
      <FormGroup>
        <Input type="password" placeholder="Password" name="password" />
      </FormGroup>
      <Button block color="primary" type="submit">
        Login
      </Button>
    </Form>
  );
};

LoginForm.propTypes = {
  next: string,
};

export default LoginForm;
