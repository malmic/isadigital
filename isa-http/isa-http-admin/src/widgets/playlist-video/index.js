import React, { useEffect, useState } from 'react';
import {
  Button,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  Input,
  InputGroupText,
} from 'reactstrap';
import { array, string, object, func } from 'prop-types';

const PlaylistVideo = ({
  createSource,
  deleteSource,
  updateSource,
  source,
  categories,
  rooms,
  originalTitle,
  originalDescription,
  videoId,
}) => {
  const [category, setCategory] = useState('');
  const [room, setRoom] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    if (!title) {
      if (source) setTitle(source.title);
      else setTitle(originalTitle);
    }
  }, [originalTitle, source, title]);

  useEffect(() => {
    if (!description) {
      if (source) setDescription(source.description);
      else setDescription(originalDescription);
    }
  }, [description, originalDescription, source]);

  useEffect(() => {
    if (!category) {
      if (source)
        setCategory(source.foreign.find(f => f.typeHandle === 'CATEGORY').id);
      else if (categories.length > 0) setCategory(categories[0].referenceId);
    }
  }, [categories, category, source]);

  useEffect(() => {
    if (!room) {
      if (source && !!source.foreign.find(f => f.typeHandle === 'ROOM'))
        setRoom(source.foreign.find(f => f.typeHandle === 'ROOM').id);
      else if (rooms.length > 0) setRoom(rooms[0].referenceId);
    }
  }, [categories, room, rooms, rooms.length, source]);

  return (
    <form>
      <InputGroup>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>Title</InputGroupText>
        </InputGroupAddon>
        <Input
          type="text"
          name="title"
          onChange={e => setTitle(e.target.value)}
          value={title}
        />
      </InputGroup>
      <div className="mt-3" />
      <InputGroup>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>Description</InputGroupText>
        </InputGroupAddon>
        <Input
          type="textarea"
          name="description"
          onChange={e => setDescription(e.target.value)}
          value={description}
        />
      </InputGroup>
      <div className="mt-3" />
      <InputGroup>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>Category</InputGroupText>
        </InputGroupAddon>
        <Input
          type="select"
          name="category"
          value={category}
          onChange={e => setCategory(e.target.value)}
        >
          {categories.map(category => (
            <option key={category.referenceId} value={category.referenceId}>
              {category.name}
            </option>
          ))}
        </Input>
      </InputGroup>
      <div className="mt-3" />
      <InputGroup>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>Room</InputGroupText>
        </InputGroupAddon>
        <Input
          type="select"
          name="room"
          value={room}
          onChange={e => setRoom(e.target.value)}
        >
          {rooms.map(room => (
            <option key={room.referenceId} value={room.referenceId}>
              {room.name}
            </option>
          ))}
        </Input>
      </InputGroup>
      <div className="mt-3" />
      {!source ? (
        <Button
          outline
          block
          className="text-uppercase"
          onClick={() => {
            createSource(videoId, title, description, category, room);
          }}
        >
          Import
        </Button>
      ) : (
        <ButtonGroup>
          <Button
            outline
            className="text-uppercase"
            onClick={() => {
              updateSource(videoId, title, category, room);
            }}
          >
            Update
          </Button>
          <Button
            outline
            className="text-uppercase"
            onClick={() => {
              if (categories.length > 0) setCategory(categories[0].referenceId);
              if (rooms.length > 0) setRoom(categories[0].referenceId);
              setTitle(originalTitle);

              deleteSource(source);
            }}
          >
            Remove
          </Button>
        </ButtonGroup>
      )}
    </form>
  );
};

PlaylistVideo.propTypes = {
  categories: array.isRequired,
  rooms: array.isRequired,
  originalTitle: string.isRequired,
  originalDescription: string.isRequired,
  source: object,
  createSource: func.isRequired,
  deleteSource: func.isRequired,
  updateSource: func.isRequired,
  videoId: string.isRequired,
};

export default PlaylistVideo;
