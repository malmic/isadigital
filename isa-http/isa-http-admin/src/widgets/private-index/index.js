import React from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { CardColumns, Col, Row, Card, CardHeader, CardBody } from 'reactstrap';
import Link from 'next/link';

import { PrivateLayout } from '../../layouts';
import routes from '../../routes';

const PrivateIndex = ({ dispatch, router }) => {
  const links = routes.reduce((acc, cur) => {
    const foundIndex = acc.findIndex(a => a.module === cur.module);

    if (foundIndex === -1) {
      acc.push({
        module: cur.module,
        links: [
          { pathname: cur.pathname, active: cur.active, title: cur.title },
        ],
      });
    } else {
      acc[foundIndex].links.push({
        pathname: cur.pathname,
        active: cur.active,
        title: cur.title,
      });
    }

    return acc;
  }, []);

  links.sort((a, b) => a.module - b.module);

  return (
    <PrivateLayout>
      <Row>
        <Col>
          <div className="mt-3" />
          <CardColumns>
            {links.map(link => (
              <Card
                key={link.module}
                style={{ border: '1px solid #2b4d82' }}
                className="text-uppercase"
              >
                <CardHeader style={{ border: '0' }} tag="h3">
                  {link.module.replace('-', ' ')}
                </CardHeader>
                <CardBody>
                  {link.links.map(r => (
                    <React.Fragment key={r.pathname}>
                      <Link key={r.module} href={r.pathname}>
                        <a>{r.title}</a>
                      </Link>
                      <br />
                    </React.Fragment>
                  ))}
                </CardBody>
              </Card>
            ))}
          </CardColumns>
        </Col>
      </Row>
    </PrivateLayout>
  );
};

PrivateIndex.propTypes = {
  router: object.isRequired,
  dispatch: func.isRequired,
};

export default connect(state => ({
  session: state.session,
  notifications: state.notifications,
}))(withRouter(PrivateIndex));
