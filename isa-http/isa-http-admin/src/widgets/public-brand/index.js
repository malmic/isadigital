import React from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';
import Link from 'next/link';

const PublicBrand = ({ className }) => (
  <Link href="/">
    <a>
      <img className={className} src={'/images/logo.png'} alt="Large Brand" />
    </a>
  </Link>
);

PublicBrand.propTypes = {
  className: string.isRequired,
};

export default styled(PublicBrand)`
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 160px;
`;
