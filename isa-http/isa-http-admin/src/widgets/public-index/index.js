import React from 'react';
import { array, func } from 'prop-types';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';

import { LoginForm } from '../';
import { PublicLayout } from '../../layouts';

const PublicIndex = ({ dispatch, notifications }) => {
  const router = useRouter();

  return (
    <PublicLayout>
      <LoginForm outline next={router.query.next} />
    </PublicLayout>
  );
};

PublicIndex.propTypes = {
  dispatch: func.isRequired,
  notifications: array.isRequired,
};

export default connect(state => ({
  notifications: state.notifications,
}))(PublicIndex);
