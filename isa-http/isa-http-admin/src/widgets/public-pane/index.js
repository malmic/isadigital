import React from 'react';
import styled from 'styled-components';
import { string, node } from 'prop-types';
import { Col } from 'reactstrap';

const PublicPane = ({ className, children }) => (
  <Col className={className} xs="12" xl={{ size: 4, offset: 4 }}>
    {children}
  </Col>
);

PublicPane.propTypes = {
  className: string.isRequired,
  children: node.isRequired,
};

export default styled(PublicPane)`
  padding-top: 15px;
  padding-bottom: 15px;
  margin-top: 15px;
`;
