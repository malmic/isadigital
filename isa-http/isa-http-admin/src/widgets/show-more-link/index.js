import React from 'react';
import { object, string } from 'prop-types';
import Link from 'next/link';

const ShowMoreLink = ({ href, icon }) => {
  return (
    <Link href={href}>
      <a>{icon}</a>
    </Link>
  );
};

ShowMoreLink.defaultProps = {
  icon: '⇨ ',
};

ShowMoreLink.propTypes = {
  href: object.isRequired,
  icon: string,
};

export default ShowMoreLink;
