const { reduce } = require('lodash');

const clusterize = (ports, hostnameFn) => {
  return reduce(
    ports,
    (result, port, protoName) => {
      const i = result.findIndex(r => r.port === port);
      if (i === -1) {
        result.push({
          port,
          hostname: hostnameFn(result.length),
          protoNames: [protoName],
        });
      } else {
        result[i].protoNames.push(protoName);
      }

      return result;
    },
    []
  );
};

const clusterToSocketAddresses = cluster => {
  return reduce(
    cluster,
    (result, value) => {
      const socketAddress = `${value.hostname}:${value.port}`;
      result[socketAddress] = value.protoNames;

      return result;
    },
    {}
  );
};

module.exports = {
  clusterize,
  clusterToSocketAddresses,
};
