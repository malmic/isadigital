const test = require('ava');

const { clusterize, clusterToSocketAddresses } = require('../src');

test('clusters', t => {
  const input = { a: 50056, b: 50056 };

  const clusters = clusterize(input, i => `hostname-${i}`);
  t.is(clusters.length, 1);
  t.is(clusters[0].port, 50056);
  t.is(clusters[0].hostname, 'hostname-0');
  t.is(clusters[0].protoNames.length, 2);
  t.is(clusters[0].protoNames[0], 'a');
  t.is(clusters[0].protoNames[1], 'b');

  const socketAddresses = clusterToSocketAddresses(clusters);
  t.is(socketAddresses['hostname-0:50056'].length, 2);
  t.is(socketAddresses['hostname-0:50056'][0], 'a');
  t.is(socketAddresses['hostname-0:50056'][1], 'b');
});
