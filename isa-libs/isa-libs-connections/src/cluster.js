const net = require('net');
const Promise = require('bluebird');
const promiseRetry = require('promise-retry');

const defaultDebug = (e, socketAddress, number) =>
  console.error(`attempt number ${number} to reach ${socketAddress}`, e);

const checkSocketAddress = (socketAddress, timeout) => {
  const url = new URL(`http://${socketAddress}`);
  const { hostname, port } = url;

  return new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      reject(new Error('timeout'));
      socket.end();
    }, timeout);

    const socket = net.createConnection(port, hostname, () => {
      clearTimeout(timer);
      resolve(true);
      socket.end();
    });

    socket.on('error', err => {
      clearTimeout(timer);
      reject(err);
    });
  });
};

const checkClusters = (clusters, timeout = 10000, debugOverride) => {
  const debug = debugOverride || defaultDebug;

  return Promise.all(
    Object.keys(clusters).map(socketAddress =>
      promiseRetry(
        (retry, number) =>
          checkSocketAddress(socketAddress, timeout).catch(e => {
            debug(e, socketAddress, number);
            retry(e);
          }),
        {
          retries: 60,
          factor: 1.5,
          minTimeout: timeout,
          maxTimeout: timeout,
        }
      )
    )
  );
};

module.exports = { checkClusters };
