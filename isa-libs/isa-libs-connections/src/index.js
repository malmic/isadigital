const cluster = require('./cluster');
const Mongo = require('./mongo');
const Redis = require('./redis');

module.exports = { cluster, Mongo, Redis };
