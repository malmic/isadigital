const promiseRetry = require('promise-retry');

class Mongo {
  constructor(lib, debug = () => ({})) {
    this.lib = lib;
    this.debug = debug;

    this.retryOptions = {
      retries: 60,
      factor: 1.5,
      minTimeout: 1000,
      maxTimeout: 5000,
    };
  }

  generateClient(env, retryOptions) {
    const { url, ...options } = env;

    return promiseRetry((retry, number) => {
      this.debug(`> connecting to mongo on attempt ${number}`);

      return this.lib.connect(url, options).catch(retry);
    }, retryOptions);
  }
}

module.exports = Mongo;
