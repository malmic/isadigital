const promiseRetry = require('promise-retry');

class Redis {
  constructor(url, lib, debug = str => console.log(str)) {
    this.url = url;
    this.lib = lib;
    this.debug = debug;

    this.retryOptions = {
      retries: 60,
      factor: 1.5,
      minTimeout: 1000,
      maxTimeout: 5000,
    };

    this.instances = [];
  }

  generateClient(clientOptions) {
    return promiseRetry((retry, number) => {
      this.debug(`> connecting to ${this.url} on attempt ${number}`);

      return new Promise((resolve, reject) => {
        const r = this.lib.createClient(this.url, clientOptions);
        r.on('error', err => {
          reject(err);
        });
        r.on('connect', () => {
          this.instances.push(r);
          resolve(r);
        });
      }).catch(retry);
    }, this.retryOptions);
  }

  close() {
    return Promise.all(
      this.instances.map(
        i =>
          new Promise((resolve, reject) => {
            i.quit(err => {
              if (err) reject(err);
              resolve();
            });
          })
      )
    );
  }
}

module.exports = Redis;
