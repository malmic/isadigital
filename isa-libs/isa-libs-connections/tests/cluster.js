const test = require('ava');
const http = require('http');
const Koa = require('koa');

const { cluster } = require('../src');

const makeServer = port => {
  return new Promise((resolve, reject) => {
    const koa = new Koa();
    const server = http.createServer(koa.callback());

    server.listen(port, () => {
      resolve(server);
    });
  });
};

test('error', async t => {
  await t.throwsAsync(
    () => cluster.checkClusters({ 'localhost:1234': ['a', 'b'] }, 5, () => {}),
    { instanceOf: Error }
  );
});

test('success', async t => {
  const port = Math.floor(Math.random() * 10000) + 50000;
  await makeServer(port);

  const result = await cluster.checkClusters(
    { [`localhost:${port}`]: [] },
    5,
    () => {}
  );

  t.is(result.length, 1);
  t.is(result[0], true);
});
