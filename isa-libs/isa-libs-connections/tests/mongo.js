const test = require('ava');
const { MongoMemoryServer } = require('mongodb-memory-server');
const { MongoClient } = require('mongodb');

const mongod = new MongoMemoryServer();

const connections = require('../src');

test('normal', async t => {
  const uri = await mongod.getUri();

  const connnection = new connections.Mongo(MongoClient);

  const client = await connnection.generateClient(
    { useUnifiedTopology: true, url: uri },
    { retries: 60, factor: 1.5, minTimeout: 1000, maxTimeout: 5000 }
  );

  await client.close();
});

test('with custom debug function', async t => {
  const debugs = [];
  const uri = await mongod.getUri();

  const connnection = new connections.Mongo(MongoClient, str =>
    debugs.push(str)
  );

  await connnection.generateClient(
    { useUnifiedTopology: true, url: uri },
    { retries: 60, factor: 1.5, minTimeout: 1000, maxTimeout: 5000 }
  );

  t.is(debugs.length, 1);
});
