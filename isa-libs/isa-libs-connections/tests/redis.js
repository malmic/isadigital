const test = require('ava');
const redis = require('redis-mock');
const { promisify } = require('util');

const connections = require('../src');

test('normal', async t => {
  const connnection = new connections.Redis('', redis);

  const client = await connnection.generateClient({});

  const set = promisify(client.set).bind(client);
  const get = promisify(client.get).bind(client);

  await set('key', 'value');
  const result = await get('key');

  t.is(result, 'value');

  await connnection.close();
});

test('with custom debug function', async t => {
  const debugs = [];
  const connnection = new connections.Redis('', redis, str => debugs.push(str));

  await connnection.generateClient({});

  t.is(debugs.length, 1);

  await connnection.close();
});
