const generateDockerfile = (preInstall = [], postInstall = []) => {
  return `FROM node:15.5.1-alpine

${preInstall.join(`\n`)}

RUN apk --no-cache --virtual build-dependencies add \\
g++ gcc libgcc libstdc++ linux-headers make python \\
&& yarn install --production \\
&& apk del build-dependencies

${postInstall.join(`\n`)}
`;
};

module.exports = {
  generateDockerfile,
};
