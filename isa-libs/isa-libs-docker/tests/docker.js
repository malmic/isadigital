const test = require('ava');

const { generateDockerfile } = require('../src');

test('docker', t => {
  const result = generateDockerfile();

  const check = `FROM node:15.5.1-alpine



RUN apk --no-cache --virtual build-dependencies add \\
g++ gcc libgcc libstdc++ linux-headers make python \\
&& yarn install --production \\
&& apk del build-dependencies


`;

  t.is(result, check);
});
