const { useEffect, useRef } = require('react');

const usePrevious = value => {
  const ref = useRef(null);

  useEffect(() => {
    ref.current = value;
  }, [value]);

  return ref.current;
};

module.exports = { usePrevious };
