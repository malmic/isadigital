const EventEmitter = require('events');

class Stream extends EventEmitter {
  constructor(channelHandle) {
    super();

    this.fields = {
      channelHandle,
    };

    this.metadata = {
      get: field => [this.fields[field]],
    };
  }
}

module.exports = Stream;
