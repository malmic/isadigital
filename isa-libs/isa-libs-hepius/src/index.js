const { isPlainObject } = require('lodash');

class Hepius {
  constructor(onError = () => {}, onCall = call => call) {
    this.channels = {};
    this.onError = onError;
    this.onCall = onCall;
  }

  static makeErrorInstance(e) {
    if (e instanceof Error) return e;
    if (isPlainObject(e)) return new Error(JSON.stringify(e));

    return new Error(e);
  }

  setOnCall(onCall) {
    this.onCall = onCall;
  }

  unary(methodName, fn) {
    return {
      [methodName]: async (call, callback) => {
        try {
          const response = await fn(this.onCall(call));

          callback(null, response);
        } catch (e) {
          const errorInstance = Hepius.makeErrorInstance(e);

          this.onError(errorInstance);

          callback(errorInstance, null);
        }
      },
    };
  }

  stream(methodName, fn) {
    return {
      [methodName]: call => {
        call.on('error', e => {
          this.onError(Hepius.makeErrorInstance(e));
        });

        fn(this.onCall(call));
      },
    };
  }

  clientStream(methodName, fn) {
    return {
      [methodName]: (call, callback) => {
        call.on('error', e => {
          this.onError(Hepius.makeErrorInstance(e));
        });

        fn(this.onCall(call), callback);
      },
    };
  }

  brokeredStream(methodName, broker) {
    return {
      [methodName]: call => {
        const channelHandle = call.metadata.get('channelHandle')[0];

        if (!channelHandle) throw new Error('Must define a channel handle');

        call.on('data', async request => {
          if (!this.channels[channelHandle]) {
            this.channels[channelHandle] = await broker.create(
              request,
              this.onCall(call)
            );
          }
        });

        call.on('cancelled', async () => {
          if (this.channels[channelHandle]) {
            await broker.destroy(this.channels[channelHandle]);

            delete this.channels[channelHandle];
          }
        });

        call.on('end', async () => {
          if (this.channels[channelHandle]) {
            await broker.destroy(this.channels[channelHandle]);

            delete this.channels[channelHandle];
          }
        });

        call.on('error', e => {
          this.onError(Hepius.makeErrorInstance(e));
        });
      },
    };
  }
}

module.exports = Hepius;
