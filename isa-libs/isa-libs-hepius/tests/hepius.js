const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('../src/index');

const mock = require('../mock');

const delay = duration => {
  return new Promise(resolve => {
    return setTimeout(resolve, duration);
  });
};

test.beforeEach('setup', async t => {
  const stream = new mock.Stream('BROKER');
  const broker = new mock.Broker();

  t.context.stream = stream;
  t.context.broker = broker;
});

test.serial('unary', async t => {
  const errors = [];

  const hepius = new Hepius(e => {
    errors.push(e);
  });

  const unaryMethods = await hepius.unary('unaryMethod', ({ errorType }) => {
    if (errorType === 0) return 12;
    if (errorType === 1) throw new Error('error');

    // eslint-disable-next-line
    if (errorType === 2) throw { error: 'error' };

    // eslint-disable-next-line
    if (errorType === 3) throw 'error';
  });

  Bluebird.promisifyAll(unaryMethods);

  const result = await unaryMethods.unaryMethodAsync({ errorType: 0 });
  t.is(result, 12);

  await t.throwsAsync(
    async () => unaryMethods.unaryMethodAsync({ errorType: 2 }),
    {
      instanceOf: Error,
      message: JSON.stringify({ error: 'error' }),
    }
  );

  await t.throwsAsync(
    async () => unaryMethods.unaryMethodAsync({ errorType: 1 }),
    {
      instanceOf: Error,
      message: 'error',
    }
  );

  await t.throwsAsync(
    async () => unaryMethods.unaryMethodAsync({ errorType: 3 }),
    {
      instanceOf: Error,
      message: 'error',
    }
  );

  t.is(errors.length, 3);
});

test.serial('stream', async t => {
  const { stream } = t.context;

  const events = [];

  const hepius = new Hepius(e => {
    events.push('error');
  });

  hepius.setOnCall(t => t);

  const streamMethods = hepius.stream('streamMethod', call =>
    call.on('event', () => {
      events.push('event');
    })
  );

  streamMethods.streamMethod(stream);

  stream.emit('event');

  t.is(events.length, 1);

  stream.emit('error', new Error({ a: 'test' }));

  t.is(events.length, 2);

  stream.emit('end');
});

test.serial('brokered stream', async t => {
  const { stream, broker } = t.context;

  const events = [];

  const hepius = new Hepius(e => {
    events.push('error');
  });

  const brokeredStreamMethods = hepius.brokeredStream('method', broker);
  brokeredStreamMethods.method(stream);

  stream.emit('data', {});

  await delay(500);

  stream.emit('error', new Error({ a: 'test' }));

  await delay(500);

  stream.emit('cancelled', {});
  stream.emit('end', {});

  await delay(500);

  t.is(events.length, 1);
});
