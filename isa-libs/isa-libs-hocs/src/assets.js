import { object } from 'prop-types';
import React, { useCallback } from 'react';
import { connect } from 'react-redux';

export default WrappedComponent => {
  const Assets = ({ assets, ...props }) => {
    const getAssets = useCallback(
      (units = ['token', 'fiat']) => {
        const matchedAssets = Object.values(assets.assets).filter(asset =>
          units.includes(asset.unitHandle)
        );

        matchedAssets.sort((a, b) => a.name - b.name > 0);

        return matchedAssets;
      },
      [assets.assets]
    );

    return (
      <WrappedComponent {...props} assets={assets} getAssets={getAssets} />
    );
  };

  Assets.getInitialProps = async ctx => {
    let wrappedInitialProps = {};

    if (WrappedComponent.getInitialProps) {
      wrappedInitialProps = await WrappedComponent.getInitialProps(ctx);
    }

    return wrappedInitialProps;
  };

  Assets.propTypes = {
    assets: object.isRequired,
  };

  return connect(state => {
    return { assets: state.assets };
  })(Assets);
};
