import { isEmpty } from 'lodash';
import { withRouter } from 'next/router';
import { object } from 'prop-types';
import React from 'react';

export default WrappedComponent => {
  const PrivateCheck = props => {
    return <WrappedComponent {...props} />;
  };

  PrivateCheck.getInitialProps = async ctx => {
    let wrappedInitialProps = {};

    if (WrappedComponent.getInitialProps) {
      wrappedInitialProps = await WrappedComponent.getInitialProps(ctx);
    }

    const { store, res, pathname, asPath } = ctx;
    const { session } = store.getState();

    if (isEmpty(session)) {
      if (res && !res.finished) {
        const Location =
          pathname !== '/logout'
            ? `/login?next=${encodeURIComponent(asPath)}`
            : `/`;

        res.writeHead(302, {
          Location,
        });

        res.end();
      }
    }

    return wrappedInitialProps;
  };

  PrivateCheck.propTypes = {
    session: object.isRequired,
    router: object.isRequired,
  };

  return withRouter(PrivateCheck);
};
