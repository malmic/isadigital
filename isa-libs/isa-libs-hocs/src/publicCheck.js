import { isEmpty } from 'lodash';
import { withRouter } from 'next/router';
import { object } from 'prop-types';
import React from 'react';

export default WrappedComponent => {
  const PublicCheck = props => {
    return <WrappedComponent {...props} />;
  };

  PublicCheck.propTypes = {
    session: object.isRequired,
    router: object.isRequired,
  };

  PublicCheck.getInitialProps = async ctx => {
    let wrappedInitialProps = {};

    if (WrappedComponent.getInitialProps) {
      wrappedInitialProps = await WrappedComponent.getInitialProps(ctx);
    }

    const { store, res } = ctx;
    const { session } = store.getState();

    if (!isEmpty(session)) {
      if (res && !res.finished) {
        res.writeHead(302, {
          Location: '/',
        });

        res.end();
      }
    }

    return wrappedInitialProps;
  };

  return withRouter(PublicCheck);
};
