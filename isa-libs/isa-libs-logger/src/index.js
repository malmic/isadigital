const winston = require('winston');
const stackTrace = require('stack-trace');
const { format } = require('logform');

const { combine, timestamp, json, prettyPrint } = format;

const loggerParams = transports => {
  return {
    level: 'debug',
    format: combine(timestamp(), json(), prettyPrint()),
    transports,
    silent: false,
  };
};

const formatErrorMetadata = (e, initialMetadata = {}) => {
  const trace = stackTrace.parse(e)[0];

  const metadata = { ...initialMetadata };
  metadata.message = e.message;

  if (trace) {
    metadata.functionName = trace.getFunctionName();
    metadata.fileName = trace.getFileName();
    metadata.lineNumber = trace.getLineNumber();
    metadata.columnNumber = trace.getColumnNumber();
  }

  metadata.stack = e.stack;
  metadata.level = 'error';

  return metadata;
};

const formatInfoMetadata = initialMetadata => {
  const metadata = { ...initialMetadata };
  metadata.level = 'info';

  return metadata;
};

module.exports = moduleName => {
  const transports = [
    new winston.transports.Console({ stderrLevels: ['error'] }),
  ];

  const logger = winston.createLogger(loggerParams(transports));

  return {
    error: e => {
      logger.log(formatErrorMetadata(e, { moduleName }));
    },

    info: additional => {
      logger.log(
        formatInfoMetadata({
          moduleName,
          ...additional,
        })
      );
    },
  };
};
