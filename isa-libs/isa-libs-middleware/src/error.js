module.exports = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    if (err.response && err.response.data) {
      ctx.status = err.response.status;
      ctx.body = err.response.data;
    } else {
      ctx.status = err.status || 500;
      ctx.body = err.message || 'Internal Server Error';
    }

    ctx.app.emit('error', err, ctx);
  }
};
