const session = require('koa-session');

module.exports = (app, services, env) => {
  return session(
    {
      key: 'sid',
      maxAge: 'session',
      domain: env.DOMAIN,
      autoCommit: true,
      overwrite: true,
      httpOnly: true,
      signed: true,
      rolling: false,
      renew: false,
      secure: env.SECURE_COOKIE,
      sameSite: env.SAME_SITE || null,
      store: {
        get: async (referenceId, maxAge, { rolling }) => {
          const { result } = await services['sessions'].getSessionAsync({
            referenceId,
          });
          return result;
        },

        set: async (referenceId, data, maxAge, { rolling, changed }) => {
          const { result } = await services['sessions'].updateSessionAsync({
            referenceId,
            ...data,
            rolling,
            changed,
          });

          return result;
        },

        destroy: async referenceId => {
          const { result } = await services['sessions'].deleteSessionAsync({
            referenceId,
          });

          return result;
        },
      },
    },
    app
  );
};
