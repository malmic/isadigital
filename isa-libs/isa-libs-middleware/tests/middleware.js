const test = require('ava');
const axios = require('axios');
const services = require('@isa-music/mocks-services');
const Koa = require('koa');
const http = require('http');
const Router = require('koa-router');
const setCookie = require('set-cookie-parser');

const { errorHandler, sessionHandler } = require('../src');

test.before('instantiate server', async t => {
  const serviceLoaders = { services: { sessions: new services.Sessions() } };

  const env = { SESSION_KEY: '123' };

  const koa = new Koa();
  koa.keys = [env.SESSION_KEY];

  koa.use(sessionHandler(koa, serviceLoaders.services, env));
  koa.use(errorHandler);

  const router = new Router();
  router.post('/session', async ctx => {
    ctx.session.test = '123';
    ctx.body = ctx.session.test;
  });

  router.get('/session', async ctx => {
    ctx.body = ctx.session.test;
  });

  router.delete('/session', async ctx => {
    ctx.session = null;
    ctx.body = true;
  });

  router.get('/error_with_message', () => {
    throw new Error('an error');
  });

  router.get('/error_without_message', () => {
    throw new Error();
  });

  koa.use(router.routes());

  return new Promise((resolve, reject) => {
    const server = http.createServer(koa.callback());
    server.listen(8139, () => {
      t.context.server = server;
      t.context.serviceLoaders = serviceLoaders;

      resolve();
    });
  });
});

test.after(t => {
  t.context.server.close();
});

test('error', async t => {
  const errorWithMessage = await t.throwsAsync(
    axios({ method: 'get', url: 'http://localhost:8139/error_with_message' })
  );

  t.is(errorWithMessage.response.data, 'an error');

  const errorWithoutMessage = await t.throwsAsync(
    axios({ method: 'get', url: 'http://localhost:8139/error_without_message' })
  );

  t.is(errorWithoutMessage.response.data, 'Internal Server Error');
});

test('session', async t => {
  const postResponse = await axios({
    method: 'post',
    url: 'http://localhost:8139/session',
  });

  t.is(postResponse.data, 123);

  const cookies = setCookie.parse(postResponse.headers['set-cookie'], {
    decodeValues: true,
  });

  t.is(cookies.length, 2);
  t.true(cookies.every(c => c.httpOnly === true));

  const getResponse = await axios({
    method: 'get',
    headers: {
      Cookie: cookies
        .map(cookie => `${cookie.name}=${cookie.value};`)
        .join(' '),
    },
    url: 'http://localhost:8139/session',
  });

  t.is(getResponse.data, 123);

  const deleteResponse = await axios({
    method: 'delete',
    url: 'http://localhost:8139/session',
  });

  t.true(deleteResponse.data);
});
