const { isObjectLike } = require('lodash');

module.exports = input => {
  return Object.keys(input).reduce(
    (acc, key) => ({
      ...acc,
      [key]: isObjectLike(input[key]) ? JSON.stringify(input[key]) : input[key],
    }),
    {}
  );
};
