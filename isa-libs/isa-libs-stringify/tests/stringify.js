const test = require('ava');

const stringify = require('../src');

test('stringify object', t => {
  const result = stringify({ a: 'b', c: { a: 'b' } });

  t.is(result.c, '{"a":"b"}');
  t.is(result.a, 'b');
});
