class Sessions {
  constructor(data = []) {
    this.data = data;
  }

  getSessionAsync({ referenceId }) {
    const result = this.data.find(d => d.referenceId === referenceId);

    return { result };
  }

  updateSessionAsync({ referenceId, ...request }) {
    const i = this.data.findIndex(d => d.referenceId === referenceId);

    if (i === -1) {
      this.data.push({ referenceId, ...request });
    } else {
      this.data[i] = { referenceId, ...request };
    }

    return { result: true };
  }

  deleteSessionAsync({ referenceId }) {
    const i = this.data.findIndex(d => d.referenceId === referenceId);

    this.data.splice(i, 1);

    return { result: true };
  }
}

module.exports = Sessions;
