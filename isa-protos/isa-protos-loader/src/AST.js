const lodash = require('lodash');
const path = require('path');
const fs = require('fs-extra');

class AST {
  constructor(protoSource, parser, protoFolder = '') {
    this.protoSource = protoSource;
    this.parser = parser;
    this.protoFolder = protoFolder;
  }

  readProto(protoName) {
    const allDirectories = this.protoSource.readDirectories(this.protoFolder);
    for (const directory of allDirectories) {
      const p = path.join(directory, `${protoName}.proto`);
      if (fs.existsSync(p)) {
        return fs.readFileSync(p).toString();
      }
    }

    throw new Error(`The proto ${protoName} was not found!`);
  }

  imports(protoName) {
    const protoDefinition = this.readProto(protoName);
    const parsedProto = this.parser.parse(protoDefinition);
    const protoImports = parsedProto.content
      .filter(node => node.type === 'import')
      .map(node => node.package.substring(0, node.package.indexOf('.')));

    for (const protoImportName of protoImports) {
      const childImports = this.imports(protoImportName);

      for (const childImport of childImports) {
        if (!protoImports.includes(childImport)) {
          protoImports.push(childImport);
        }
      }
    }

    return protoImports;
  }

  services(protoName) {
    const protoDefinition = this.readProto(protoName);
    const parsedProto = this.parser.parse(protoDefinition);

    const packageNode = parsedProto.content.find(
      node => node.type === 'package'
    );

    return parsedProto.content
      .filter(entry => entry.type === 'service')
      .map(node => ({
        name: lodash.camelCase(node.name),
        className: node.name,
        packageName: packageNode.package,
      }));
  }
}

module.exports = AST;
