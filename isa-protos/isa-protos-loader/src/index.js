const peg = require('pegjs');
const fs = require('fs-extra');
const path = require('path');
const grpcProtoLoader = require('@grpc/proto-loader');

const AST = require('./AST');
const ServiceLoader = require('./serviceLoader');

const { compileJS } = require('./lib');

const generateServiceLoader = (protoSource, cluster, grpc) => {
  const protoGrammar = fs
    .readFileSync(path.join(__dirname, './proto.peg'))
    .toString();

  const parser = peg.generate(protoGrammar, {
    format: 'commonjs',
    output: 'parser',
  });

  const ast = new AST(protoSource, parser);

  const services = Object.keys(cluster).reduce((acc, socketAddress) => {
    const resolvedServices = cluster[socketAddress].reduce(
      (resolvedServicesAccumulator, protoName) => [
        ...resolvedServicesAccumulator,
        ...ast.services(protoName).map(service => ({
          ...service,
          protoName,
          socketAddress,
        })),
      ],
      []
    );

    return [...acc, ...resolvedServices];
  }, []);

  return new ServiceLoader(grpc, services, protoSource);
};

const generateServer = (
  grpc,
  protoSource,
  serviceImplementations,
  protoFolder = null
) => {
  const protoGrammar = fs
    .readFileSync(path.join(__dirname, './proto.peg'))
    .toString();

  const parser = peg.generate(protoGrammar, {
    format: 'commonjs',
    output: 'parser',
  });

  const ast = new AST(protoSource, parser, protoFolder);
  const server = new grpc.Server();

  for (const serviceImplementation of serviceImplementations) {
    const { protoName, services } = serviceImplementation;

    const loadedPackage = grpc.loadPackageDefinition(
      grpcProtoLoader.loadSync(`${protoName}.proto`, {
        arrays: true,
        objects: true,
        includeDirs: protoSource.readDirectories(protoFolder),
      })
    );

    const serviceDefinitions = ast.services(protoName);

    for (const serviceDefinition of serviceDefinitions) {
      server.addService(
        loadedPackage[serviceDefinition.packageName][
          serviceDefinition.className
        ].service,
        services[serviceDefinition.className]
      );
    }
  }

  return server;
};

const generateServiceLoaders = (protoSource, clusters, grpc) => {
  return Object.keys(clusters).reduce(
    (acc, name) => ({
      ...acc,
      [name]: generateServiceLoader(protoSource, clusters[name], grpc),
    }),
    {}
  );
};

const generateWebDefinitions = async (
  protoSource,
  protoNames,
  outputDir,
  mode = 'grpcweb'
) => {
  await fs.remove(outputDir);
  await fs.ensureDir(outputDir);

  const protoGrammar = fs
    .readFileSync(path.join(__dirname, './proto.peg'))
    .toString();

  const parser = peg.generate(protoGrammar, {
    format: 'commonjs',
    output: 'parser',
  });

  const ast = new AST(protoSource, parser);

  const astImports = protoNames.reduce((acc, cur) => {
    const imports = ast.imports(cur);
    for (const imp of imports) {
      const i = acc.findIndex(a => a === imp);
      if (i === -1) acc.push(imp);
    }

    return acc;
  }, []);

  for (const protoName of protoNames) {
    await compileWebProto(
      protoSource.readDirectories(),
      outputDir,
      protoName,
      mode
    );

    await compileProto(protoSource.readDirectories(), outputDir, protoName);
  }

  for (const astImport of astImports) {
    await compileProto(protoSource.readDirectories(), outputDir, astImport);
  }

  const fileMap = astImports.map(protoName => ({
    name: protoName,
    compiledName: `${protoName}_pb`,
  }));

  for (const protoName of protoNames) {
    fileMap.push({ name: protoName, compiledName: `${protoName}_grpc_web_pb` });
  }

  const requireStatements = fileMap.reduce((acc, cur) => {
    return acc.concat(
      `const ${cur.name} = require('./${cur.compiledName}');\n`
    );
  }, '');

  const exportStatements = fileMap.map(proto => `  ${proto.name}`).join(',\n');

  const indexSource = `${requireStatements}
module.exports = {\n${exportStatements},\n};
  `;

  await fs.outputFile(path.join(outputDir, '/index.js'), indexSource);
};

const compileWebProto = (dirs, outputDir, protoName, mode) => {
  const inputs = dirs.map(directoryPath => `-I=${directoryPath}`);

  const command = `--grpc-web_out=import_style=commonjs,mode=${mode}:${outputDir}`;

  return compileJS(inputs, `${protoName}.proto`, command);
};

const compileProto = (allDirectories, outputDir, protoName) => {
  const inputs = allDirectories.map(directoryPath => `-I=${directoryPath}`);

  const command = `--js_out=import_style=commonjs:${outputDir}`;

  return compileJS(inputs, `${protoName}.proto`, command);
};

module.exports = {
  generateServer,
  generateServiceLoader,
  generateServiceLoaders,
  generateWebDefinitions,
};
