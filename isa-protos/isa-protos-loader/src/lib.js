const { exec } = require('child_process');

const sys = cmd => {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(stdout);
    });
  });
};

const compileJS = (inputs, fileName, command) => {
  return sys(`protoc ${inputs.join(' ')} ${fileName} ${command}`);
};

module.exports = {
  compileJS,
};
