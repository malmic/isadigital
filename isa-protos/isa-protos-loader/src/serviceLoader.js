const Bluebird = require('bluebird');
const grpcProtoLoader = require('@grpc/proto-loader');

class ServiceLoader {
  constructor(grpc, services, protoSource) {
    this.grpc = grpc;
    this.services = services;
    this.protoSource = protoSource;

    this.instances = {};

    return new Proxy(this, {
      get(target, serviceName) {
        if (!target.instances[serviceName]) {
          const service = target.services.find(s => s.name === serviceName);

          const definition = grpc.loadPackageDefinition(
            grpcProtoLoader.loadSync(`${service.protoName}.proto`, {
              arrays: true,
              objects: true,
              includeDirs: target.protoSource.readDirectories(),
            })
          );

          const ServiceClass =
            definition[service.packageName][service.className];

          const instance = new ServiceClass(
            service.socketAddress,
            grpc.credentials.createInsecure()
          );

          Bluebird.promisifyAll(instance);

          target.instances[serviceName] = instance;
        }

        return target.instances[serviceName];
      },
    });
  }
}

module.exports = ServiceLoader;
