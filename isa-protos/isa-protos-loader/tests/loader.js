const test = require('ava');
const path = require('path');
const fs = require('fs-extra');
const grpc = require('@grpc/grpc-js');
const os = require('os');

const source = require('@isa-music/protos-source');

const { sep } = path;
const {
  generateServer,
  generateServiceLoader,
  generateServiceLoaders,
  generateWebDefinitions,
} = require('../src');

test('web definitions', async t => {
  const buildDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`);

  const definitionsDir = path.join(buildDir, '/definitions');

  await generateWebDefinitions(source, ['addresses'], definitionsDir);
});

test('service loaders', async t => {
  const clusters = { services: { 'localhost:1000': ['addresses'] } };

  const serviceLoader = generateServiceLoaders(source, clusters, grpc);

  t.is(typeof serviceLoader.services.addresses, 'object');

  t.throws(
    () => generateServiceLoader(source, { 'localhost:200': ['noexist'] }),
    { message: 'The proto noexist was not found!', instanceOf: Error }
  );
});

test('generate server', async t => {
  const buildDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`);

  fs.outputFileSync(
    path.join(buildDir, 'test.proto'),
    `syntax = "proto3";

package testPackage;

service Test {
  rpc check(Check) returns (CheckResponse);
}

message Check {
  string token = 1;
}

message CheckResponse {
  bool result = 1;
}    
`
  );

  const server = generateServer(
    grpc,
    source,
    [{ protoName: 'test', services: { Test: { check: () => {} } } }],
    buildDir
  );

  t.is(server.started, false);
});
