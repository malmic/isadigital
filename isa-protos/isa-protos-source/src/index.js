const path = require('path');
const fs = require('fs-extra');

const flatten = lists => {
  return lists.reduce((a, b) => a.concat(b), []);
};

const getDirectories = srcpath => {
  return fs
    .readdirSync(srcpath)
    .map(file => path.join(srcpath, file))
    .filter(path => fs.statSync(path).isDirectory());
};

const readDirectories = baseDir => {
  const dir = baseDir || path.join(__dirname, './protos');
  return [dir, ...flatten(getDirectories(dir).map(readDirectories))];
};

module.exports = {
  readDirectories,
};
