const test = require('ava');
const path = require('path');
const fs = require('fs-extra');
const os = require('os');

const { sep } = path;
const { readDirectories } = require('../src');

test('test directory', async t => {
  const buildDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`);

  await fs.ensureDir(path.join(buildDir, 'asd'));
  await fs.ensureDir(path.join(buildDir, 'asd', 'asd'));

  const allProtos = readDirectories(buildDir);

  t.is(allProtos[0], buildDir);
  t.is(allProtos[1], path.join(buildDir, 'asd'));
  t.is(allProtos[2], path.join(buildDir, 'asd', 'asd'));
});

test('actual directory', async t => {
  const allProtos = readDirectories();

  t.is(allProtos.length, 1);
  t.is(allProtos[0], path.join(__dirname, '../src/protos'));
});
