const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Addresses = await service(db, hepius);

  return { Addresses };
};
