const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('addresses', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'foreign',
            'street',
            'postalCode',
            'city',
            'country',
            'referenceId',
          ],
          properties: {
            foreign: {
              bsonType: 'object',
              properties: {
                id: { bsonType: 'string' },
                typeHandle: { bsonType: 'string' },
              },
            },
            street: { bsonType: 'string' },
            postalCode: { bsonType: 'string' },
            city: { bsonType: 'string' },
            country: { bsonType: 'string' },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createAddress(address) {
    const referenceId = uniqid();

    await this.collection.insertOne({ referenceId, ...address });

    return referenceId;
  }

  async getAddress({ foreign }) {
    const result = await this.collection.findOne({ foreign });

    return { result };
  }
}

module.exports = Model;
