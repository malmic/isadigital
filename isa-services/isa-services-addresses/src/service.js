const Model = require('./model');

module.exports = async (db, hepius) => {
  const addresses = await Model.createInstance(db);

  return {
    ...hepius.unary('createAddress', ({ request }) =>
      addresses.createAddress(request)
    ),

    ...hepius.unary('getAddress', ({ request }) =>
      addresses.getAddress(request)
    ),
  };
};
