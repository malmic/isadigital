const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Addresses: addresses } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(addresses);

  t.context.addresses = addresses;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('address', async t => {
  const { addresses } = t.context;

  await addresses.createAddressAsync({
    request: {
      foreign: { id: '1', typeHandle: 'USER' },
      street: '9 Tioga Blvd.',
      city: 'Everett',
      postalCode: '1002',
      country: 'Austria',
    },
  });

  const { result: address } = await addresses.getAddressAsync({
    request: {
      foreign: { id: '1', typeHandle: 'USER' },
    },
  });

  t.is(address.country, 'Austria');
});
