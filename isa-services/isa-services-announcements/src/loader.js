const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Announcements = await service(db, hepius);

  return { Announcements };
};
