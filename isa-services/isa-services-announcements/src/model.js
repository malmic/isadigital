const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('announcements', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'title', 'body', 'referenceId'],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            title: { bsonType: 'string' },
            body: { bsonType: 'string' },
            createdAt: { bsonType: 'date' },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createAnnouncement(announcement) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      referenceId,
      createdAt: new Date(),
      ...announcement,
    });

    return { result: referenceId };
  }

  async getAnnouncements({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.find(query).toArray();

    return { result };
  }

  async getAnnouncement({ referenceId }) {
    const result = await this.collection.findOne({ referenceId });

    return { result };
  }

  async deleteAnnouncement({ referenceId }) {
    const result = await this.collection.deleteOne({ referenceId });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
