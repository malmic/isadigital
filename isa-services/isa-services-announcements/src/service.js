const Model = require('./model');

module.exports = async (db, hepius) => {
  const announcements = await Model.createInstance(db);

  return {
    ...hepius.unary('createAnnouncement', ({ request }) =>
      announcements.createAnnouncement(request)
    ),

    ...hepius.unary('getAnnouncements', ({ request }) =>
      announcements.getAnnouncements(request)
    ),

    ...hepius.unary('getAnnouncement', ({ request }) =>
      announcements.getAnnouncement(request)
    ),

    ...hepius.unary('deleteAnnouncement', ({ request }) =>
      announcements.deleteAnnouncement(request)
    ),
  };
};
