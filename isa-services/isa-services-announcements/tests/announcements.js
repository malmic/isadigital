const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Announcements: announcements } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(announcements);

  t.context.announcements = announcements;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('announcements', async t => {
  const { announcements } = t.context;

  await announcements.createAnnouncementAsync({
    request: {
      foreign: [{ id: '1', typeHandle: 'USER' }],
      title: 'title',
      body: 'body',
    },
  });

  const {
    result: allAnnouncements,
  } = await announcements.getAnnouncementsAsync({
    request: {},
  });

  t.is(allAnnouncements.length, 1);

  const {
    result: announcementsByForeign,
  } = await announcements.getAnnouncementsAsync({
    request: { foreign: [{ id: '1', typeHandle: 'USER' }] },
  });

  t.is(announcementsByForeign.length, 1);
});

test.serial('announcement', async t => {
  const { announcements } = t.context;

  const { result: referenceId } = await announcements.createAnnouncementAsync({
    request: {
      foreign: [{ id: '1', typeHandle: 'USER' }],
      title: 'title',
      body: 'body',
    },
  });

  const { result: announcement } = await announcements.getAnnouncementAsync({
    request: { referenceId },
  });

  t.is(announcement.title, 'title');

  const { result: deleted } = await announcements.deleteAnnouncementAsync({
    request: { referenceId },
  });

  t.true(deleted);
});
