const service = require('./service');

const assetsData = require('../data/assets.json');
const conversionsData = require('../data/conversions.json');
const unitsData = require('../data/units.json');

module.exports = (db, env, hepius) => {
  const Assets = service.assets(assetsData, hepius);
  const Complements = service.complements(assetsData, hepius);
  const Conversions = service.conversions(conversionsData, hepius);
  const Transforms = service.transforms(conversionsData, assetsData, hepius);
  const Units = service.units(unitsData, hepius);

  return { Assets, Complements, Conversions, Transforms, Units };
};
