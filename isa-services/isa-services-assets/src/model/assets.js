class Assets {
  constructor(data) {
    this.data = data;
  }

  getAssets(handle, unitHandle, symbolHandle) {
    const nameSort = (a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    };

    const results = [...this.data].filter(asset => {
      if (unitHandle && asset.unitHandle !== unitHandle) return false;
      if (handle && asset.handle !== handle) return false;
      if (symbolHandle && asset.symbolHandle !== symbolHandle) return false;

      return true;
    });

    results.sort(nameSort);

    return results;
  }

  getAsset(handle, unitHandle, symbolHandle) {
    if (!handle && !unitHandle && !symbolHandle) return null;

    return this.data.find(asset => {
      if (unitHandle && asset.unitHandle !== unitHandle) return false;
      if (handle && asset.handle !== handle) return false;
      if (symbolHandle && asset.symbolHandle !== symbolHandle) return false;

      return true;
    });
  }
}

module.exports = Assets;
