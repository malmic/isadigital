class Complements {
  constructor(data) {
    this.data = data.reduce((acc, asset) => {
      const complementaryAsset = data.find(a => {
        return (
          a.symbolHandle === asset.symbolHandle && a.handle !== asset.handle
        );
      });

      return {
        ...acc,
        [asset.handle]: complementaryAsset.handle,
      };
    }, {});
  }

  getComplement(handle) {
    return this.data[handle];
  }

  getComplements() {
    return this.data;
  }
}

module.exports = Complements;
