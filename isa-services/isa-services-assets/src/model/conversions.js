const Big = require('big.js');

class Conversions {
  constructor(data) {
    this.data = data;
  }

  getConversion(baseHandle, quoteHandle) {
    return this.data.find(
      conversion =>
        conversion.baseHandle === baseHandle &&
        conversion.quoteHandle === quoteHandle
    );
  }

  getConversions() {
    return this.data;
  }

  toQuantity(handle, quantity, price) {
    if (handle === quantity.handle) {
      return {
        ...quantity,
        value: parseFloat(Big(quantity.value).toFixed(18)),
      };
    }

    const { baseHandle, quoteHandle, value } = price;

    if (handle === quoteHandle && quantity.handle === baseHandle) {
      return {
        value: parseFloat(
          Big(quantity.value)
            .times(value)
            .toFixed(18)
        ),
        handle,
      };
    }

    if (handle === baseHandle && quantity.handle === quoteHandle) {
      return {
        value: parseFloat(
          Big(quantity.value)
            .div(value)
            .toFixed(18)
        ),
        handle,
      };
    }
  }
}

module.exports = Conversions;
