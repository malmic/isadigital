const Big = require('big.js');

class Transforms {
  constructor(assets, conversions, complements) {
    this.assets = assets;
    this.conversions = conversions;
    this.complements = complements;
  }

  getComplement(handle, complements) {
    if (handle in complements) return complements[handle];

    return this.complements.getComplement(handle);
  }

  toAsset(handle, complements = {}) {
    const complementHandle = this.getComplement(handle, complements);

    const toAsset = this.assets.getAsset(complementHandle);

    return toAsset;
  }

  toPrice(price, complements = {}) {
    const { baseHandle, quoteHandle, createdAt, value } = price;

    const fromBaseAsset = this.assets.getAsset(baseHandle);
    const fromQuoteAsset = this.assets.getAsset(quoteHandle);

    const complementBase = this.getComplement(baseHandle, complements);
    const complementQuote = this.getComplement(quoteHandle, complements);

    const toBaseAsset = this.assets.getAsset(complementBase);
    const toQuoteAsset = this.assets.getAsset(complementQuote);

    const baseConversion = this.conversions.getConversion(
      toBaseAsset.unitHandle,
      fromBaseAsset.unitHandle
    );

    const quoteConversion = this.conversions.getConversion(
      fromQuoteAsset.unitHandle,
      toQuoteAsset.unitHandle
    );

    return {
      value: parseFloat(
        Big(value)
          .times(baseConversion.value)
          .times(quoteConversion.value)
          .toFixed(18)
      ),
      baseHandle: toBaseAsset.handle,
      quoteHandle: toQuoteAsset.handle,
      createdAt: createdAt || new Date(),
    };
  }

  toQuantity({ handle, value }, complements = {}) {
    const fromAsset = this.assets.getAsset(handle);
    const complementHandle = this.getComplement(handle, complements);
    const toAsset = this.assets.getAsset(complementHandle);

    const conversion = this.conversions.getConversion(
      fromAsset.unitHandle,
      toAsset.unitHandle
    );

    return {
      handle: toAsset.handle,
      value: parseFloat(
        Big(value)
          .times(conversion.value)
          .toFixed(18)
      ),
    };
  }
}

module.exports = Transforms;
