class Units {
  constructor(data) {
    this.data = data;
  }

  getUnits() {
    return this.data;
  }

  getUnit(handle) {
    const result = this.data.find(unit => unit.handle === handle);

    return result;
  }
}

module.exports = Units;
