const model = require('../model');

module.exports = (data, hepius) => {
  const assets = new model.Assets(data);

  return {
    ...hepius.unary('getAssets', async call => {
      const { request } = call;
      const { handle, unitHandle, symbolHandle } = request;

      const result = assets.getAssets(handle, unitHandle, symbolHandle);

      return { result };
    }),

    ...hepius.unary('getAsset', async call => {
      const { request } = call;
      const { handle, unitHandle, symbolHandle } = request;

      const result = assets.getAsset(handle, unitHandle, symbolHandle);

      return { result };
    }),
  };
};
