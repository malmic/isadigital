const model = require('../model');

module.exports = (assetsData, hepius) => {
  const complements = new model.Complements(assetsData);

  return {
    ...hepius.unary('getComplement', ({ request }) => {
      const { handle } = request;

      const result = complements.getComplement(handle);

      return { result };
    }),

    ...hepius.unary('getComplements', () => {
      const result = complements.getComplements();

      return { result };
    }),
  };
};
