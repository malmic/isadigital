const model = require('../model');

module.exports = (data, hepius) => {
  const conversions = new model.Conversions(data);

  return {
    ...hepius.unary('getConversions', async call => {
      const result = conversions.getConversions();

      return { result };
    }),

    ...hepius.unary('getConversion', async call => {
      const { request } = call;
      const { baseHandle, quoteHandle } = request;

      const result = conversions.getConversion(baseHandle, quoteHandle);

      return { result };
    }),

    ...hepius.unary('toCustomQuantity', async call => {
      const { request } = call;
      const { handle, quantity, price } = request;

      const result = conversions.toQuantity(handle, quantity, price);

      return { result };
    }),
  };
};
