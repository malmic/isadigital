const model = require('../model');

module.exports = (conversionsData, assetsData, hepius) => {
  const assets = new model.Assets(assetsData);
  const conversions = new model.Conversions(conversionsData);
  const complements = new model.Complements(assetsData);
  const transforms = new model.Transforms(assets, conversions, complements);

  return {
    ...hepius.unary('toAsset', ({ request }) => {
      const { handle, complements } = request;

      const result = transforms.toAsset(handle, complements || {});

      return { result };
    }),

    ...hepius.unary('toQuantity', ({ request }) => {
      const result = transforms.toQuantity(request);

      return { result };
    }),

    ...hepius.unary('toCustomQuantity', ({ request }) => {
      const { quantity, complements } = request;

      const result = transforms.toQuantity(quantity, complements);

      return { result };
    }),

    ...hepius.unary('toPrice', ({ request }) => {
      const result = transforms.toPrice(request);

      return { result };
    }),

    ...hepius.unary('toCustomPrice', ({ request }) => {
      const { price, complements } = request;

      const result = transforms.toPrice(price, complements);

      return { result };
    }),
  };
};
