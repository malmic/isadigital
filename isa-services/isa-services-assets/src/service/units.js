const model = require('../model');

module.exports = (data, hepius) => {
  const units = new model.Units(data);

  return {
    ...hepius.unary('getUnits', () => {
      const result = units.getUnits();

      return { result };
    }),

    ...hepius.unary('getUnit', call => {
      const { request } = call;
      const { handle } = request;

      const result = units.getUnit(handle);

      return { result };
    }),
  };
};
