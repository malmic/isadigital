const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', t => {
  const assets = service.assets(
    [
      {
        name: 'Euro',
        handle: 'EUR',
        unitHandle: 'fiat',
        symbolHandle: '€',
        decimalDisplay: 2,
      },
      {
        name: 'Asta',
        handle: 'ASTA',
        unitHandle: 'token',
        symbolHandle: '₳',
        decimalDisplay: 8,
      },
    ],
    new Hepius()
  );

  Bluebird.promisifyAll(assets);

  t.context.assets = assets;
});

test('assets', async t => {
  const { assets } = t.context;

  const { result: allAssets } = await assets.getAssetsAsync({ request: {} });
  t.is(allAssets.length, 2);

  const { result: currencyAssets } = await assets.getAssetsAsync({
    request: { unitHandle: 'fiat' },
  });
  t.is(currencyAssets.length, 1);

  const { result: tokenAssets } = await assets.getAssetsAsync({
    request: { unitHandle: 'token' },
  });
  t.is(tokenAssets.length, 1);
});

test('get asset', async t => {
  const { assets } = t.context;

  const { result: assetByUnit } = await assets.getAssetAsync({
    request: { unitHandle: 'token' },
  });
  t.is(assetByUnit.handle, 'ASTA');

  const { result: assetByHandle } = await assets.getAssetAsync({
    request: { handle: 'ASTA' },
  });
  t.is(assetByHandle.handle, 'ASTA');

  const { result: assetBySymbolHandle } = await assets.getAssetAsync({
    request: { symbolHandle: '€' },
  });
  t.is(assetBySymbolHandle.handle, 'EUR');

  const { result: nullAsset } = await assets.getAssetAsync({
    request: {},
  });
  t.is(nullAsset, null);
});
