const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', async t => {
  const complements = await service.complements(
    [
      {
        name: 'Asta',
        handle: 'ASTA',
        unitHandle: 'token',
        symbolHandle: '₳',
        decimalDisplay: 8,
      },
      {
        name: 'Asta',
        handle: 'KASTA',
        unitHandle: 'kova',
        symbolHandle: '₳',
        decimalDisplay: 0,
      },
    ],
    new Hepius()
  );

  Bluebird.promisifyAll(complements);

  t.context.complements = complements;
});

test.serial('complements', async t => {
  const { complements } = t.context;

  const { result: complementHandle } = await complements.getComplementAsync({
    request: { handle: 'ASTA' },
  });

  t.is(complementHandle, 'KASTA');

  const { result: originalHandle } = await complements.getComplementAsync({
    request: { handle: complementHandle },
  });

  t.is(originalHandle, 'ASTA');

  const { result: allComplements } = await complements.getComplementsAsync({});

  t.is(Object.keys(allComplements).length, 2);
});
