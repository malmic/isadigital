const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', t => {
  const conversions = service.conversions(
    [
      { quoteHandle: 'cent', baseHandle: 'fiat', value: 100.0 },
      { quoteHandle: 'fiat', baseHandle: 'cent', value: 0.01 },
      { quoteHandle: 'cent', baseHandle: 'cent', value: 1.0 },
      { quoteHandle: 'fiat', baseHandle: 'fiat', value: 1.0 },
    ],
    new Hepius()
  );

  Bluebird.promisifyAll(conversions);

  t.context.conversions = conversions;
});

test('conversions', async t => {
  const { conversions } = t.context;

  const { result: allConversions } = await conversions.getConversionsAsync({});
  t.is(allConversions.length, 4);

  const { result: conversion } = await conversions.getConversionAsync({
    request: {
      quoteHandle: 'cent',
      baseHandle: 'fiat',
    },
  });

  t.is(conversion.value, 100.0);

  const { result: aQuantity } = await conversions.toCustomQuantityAsync({
    request: {
      handle: 'A',
      quantity: { handle: 'B', value: 2.0 },
      price: {
        value: 2,
        baseHandle: 'B',
        quoteHandle: 'A',
      },
    },
  });

  t.is(aQuantity.handle, 'A');
  t.is(aQuantity.value, 4.0);

  const { result: bQuantity } = await conversions.toCustomQuantityAsync({
    request: {
      handle: 'B',
      quantity: { handle: 'A', value: 2.0 },
      price: {
        value: 2,
        baseHandle: 'B',
        quoteHandle: 'A',
      },
    },
  });

  t.is(bQuantity.handle, 'B');
  t.is(bQuantity.value, 1.0);

  const { result: identityQuantity } = await conversions.toCustomQuantityAsync({
    request: {
      handle: 'A',
      quantity: { handle: 'A', value: 2.0 },
      price: {
        value: 2,
        baseHandle: 'B',
        quoteHandle: 'A',
      },
    },
  });

  t.is(identityQuantity.handle, 'A');
  t.is(identityQuantity.value, 2.0);
});
