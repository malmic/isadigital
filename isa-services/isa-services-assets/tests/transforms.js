const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', async t => {
  const transforms = await service.transforms(
    [
      { quoteHandle: 'cent', baseHandle: 'fiat', value: 100.0 },
      { quoteHandle: 'fiat', baseHandle: 'cent', value: 0.01 },
      { quoteHandle: 'cent', baseHandle: 'cent', value: 1.0 },
      { quoteHandle: 'fiat', baseHandle: 'fiat', value: 1.0 },
      { quoteHandle: 'kova', baseHandle: 'token', value: 100000000 },
      { quoteHandle: 'token', baseHandle: 'kova', value: 1e-8 },
      { quoteHandle: 'token', baseHandle: 'token', value: 1.0 },
      { quoteHandle: 'kova', baseHandle: 'kova', value: 1.0 },
    ],
    [
      {
        name: 'Euro',
        handle: 'EUR',
        unitHandle: 'fiat',
        symbolHandle: '€',
        decimalDisplay: 2,
      },
      {
        name: 'Euro',
        handle: 'EURCENT',
        unitHandle: 'cent',
        symbolHandle: '€',
        decimalDisplay: 0,
      },
      {
        name: 'Asta',
        handle: 'ASTA',
        unitHandle: 'token',
        symbolHandle: '₳',
        decimalDisplay: 8,
      },
      {
        name: 'Asta',
        handle: 'KASTA',
        unitHandle: 'kova',
        symbolHandle: '₳',
        decimalDisplay: 0,
      },
    ],
    new Hepius()
  );

  Bluebird.promisifyAll(transforms);

  t.context.transforms = transforms;
});

test.serial('price', async t => {
  const { transforms } = t.context;

  const originalPrice = {
    baseHandle: 'ASTA',
    quoteHandle: 'EUR',
    value: 1,
  };

  const { result: tranformedPrice } = await transforms.toPriceAsync({
    request: originalPrice,
  });

  t.is(tranformedPrice.baseHandle, 'KASTA');
  t.is(tranformedPrice.quoteHandle, 'EURCENT');
  t.is(tranformedPrice.value, 0.000001);

  const { result: hopefullyOriginalPrice } = await transforms.toPriceAsync({
    request: tranformedPrice,
  });

  t.is(hopefullyOriginalPrice.baseHandle, originalPrice.baseHandle);

  t.is(hopefullyOriginalPrice.quoteHandle, originalPrice.quoteHandle);
  t.is(hopefullyOriginalPrice.value, 1);

  const { result: customPrice } = await transforms.toCustomPriceAsync({
    request: {
      price: originalPrice,
      complements: { ASTA: 'ASTA', EUR: 'EUR' },
    },
  });

  t.is(customPrice.baseHandle, originalPrice.baseHandle);
  t.is(customPrice.quoteHandle, originalPrice.quoteHandle);
  t.is(customPrice.value, originalPrice.value);
});

test.serial('quantity', async t => {
  const { transforms } = t.context;

  const originalQuantity = {
    handle: 'ASTA',
    value: 0.005,
  };

  const { result: transformedQuantity } = await transforms.toQuantityAsync({
    request: originalQuantity,
  });

  t.is(transformedQuantity.handle, 'KASTA');
  t.is(transformedQuantity.value, 500000);

  const {
    result: hopefullyOriginalQuantity,
  } = await transforms.toQuantityAsync({
    request: transformedQuantity,
  });

  t.is(hopefullyOriginalQuantity.baseHandle, originalQuantity.baseHandle);
  t.is(hopefullyOriginalQuantity.value, originalQuantity.value);

  const { result: customQuantity } = await transforms.toCustomQuantityAsync({
    request: {
      quantity: originalQuantity,
      complements: { ASTA: 'ASTA' },
    },
  });

  t.is(customQuantity.handle, originalQuantity.handle);
  t.is(customQuantity.value, originalQuantity.value);
});

test.serial('asset', async t => {
  const { transforms } = t.context;

  const { result: transformedAsset } = await transforms.toAssetAsync({
    request: { handle: 'ASTA' },
  });

  t.is(transformedAsset.handle, 'KASTA');

  const { result: noTransformedAsset } = await transforms.toAssetAsync({
    request: { handle: 'KASTA', complements: { KASTA: 'KASTA' } },
  });
  t.is(noTransformedAsset.handle, 'KASTA');
});
