const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', t => {
  const units = service.units([{ name: 'Fiat', handle: 'fiat' }], new Hepius());

  Bluebird.promisifyAll(units);

  t.context.units = units;
});

test('units', async t => {
  const { units } = t.context;

  const { result: allUnits } = await units.getUnitsAsync({});
  t.is(allUnits.length, 1);

  const { result: unit } = await units.getUnitAsync({
    request: { handle: 'fiat' },
  });

  t.is(unit.name, 'Fiat');
});
