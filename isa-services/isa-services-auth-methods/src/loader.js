const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Password = await service.password(db, hepius);

  return { Password };
};
