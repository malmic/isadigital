const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('authMethods', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'foreign',
            'createdAt',
            'retries',
            'updatedAt',
            'typeHandle',
            'statusHandle',
            'referenceId',
          ],
          properties: {
            foreign: {
              bsonType: 'object',
              required: ['id', 'typeHandle'],
              properties: {
                id: {
                  bsonType: 'string',
                },
                typeHandle: {
                  bsonType: 'string',
                },
              },
            },
            referenceId: {
              bsonType: 'string',
            },
            retries: {
              bsonType: 'number',
            },
            createdAt: {
              bsonType: 'date',
            },
            updatedAt: {
              bsonType: 'date',
            },
            typeHandle: {
              bsonType: 'string',
            },
            statusHandle: {
              bsonType: 'string',
            },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createAuthMethod({ foreign, typeHandle, statusHandle, ...params }) {
    const result = await this.collection.insertOne({
      foreign,
      typeHandle,
      statusHandle,
      createdAt: new Date(),
      updatedAt: new Date(),
      referenceId: uniqid(),
      retries: 0,
      ...params,
    });

    return result.result.ok === 1;
  }

  async updateAuthMethod({ foreign, typeHandle, referenceId, ...params }) {
    const query = { typeHandle };
    if (foreign) query.foreign = foreign;
    if (referenceId) query.referenceId = referenceId;

    const result = await this.collection.updateOne(query, {
      $set: {
        ...params,
        updatedAt: new Date(),
      },
    });

    return result.result.ok === 1;
  }

  async getAuthMethod(foreign, typeHandle, referenceId) {
    const query = { typeHandle };
    if (foreign) query.foreign = foreign;
    if (referenceId) query.referenceId = referenceId;

    const result = await this.collection.findOne(query);

    return result;
  }

  async removeAuthMethod(foreign, typeHandle) {
    const result = await this.collection.deleteOne({ foreign, typeHandle });

    return result.result.ok === 1;
  }
}

module.exports = Model;
