const otp = require('./otp');
const password = require('./password');

module.exports = {
  password,
  otp,
};
