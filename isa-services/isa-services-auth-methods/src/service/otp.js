const speakeasy = require('speakeasy');
const uuid4 = require('uuid4');

const Model = require('../model');

module.exports = async (db, hepius) => {
  const otp = await Model.createInstance(db);

  return {
    ...hepius.unary('createOtp', async call => {
      const { request } = call;
      const { foreign, passcodeIdentifier, statusHandle } = request;

      const secret = speakeasy.generateSecret({ name: passcodeIdentifier });
      const resetCode = uuid4();

      await otp.createAuthMethod({
        foreign,
        typeHandle: 'OTP',
        base32secret: secret.base32,
        resetCode,
        authUrl: secret.otpauth_url,
        statusHandle,
      });

      const result = secret.otpauth_url;

      return { result };
    }),

    ...hepius.unary('getResetCode', async call => {
      const { request } = call;
      const { foreign } = request;

      const authMethod = await otp.getAuthMethod(foreign, 'OTP');
      if (!authMethod) return { result: null };

      return { result: authMethod.resetCode };
    }),

    ...hepius.unary('getAuthUrl', async call => {
      const { request } = call;
      const { foreign } = request;

      const authMethod = await otp.getAuthMethod(foreign, 'OTP');
      if (!authMethod) return { result: null };

      return { result: authMethod.authUrl };
    }),

    ...hepius.unary('verifyToken', async call => {
      const { request } = call;
      const { foreign, token } = request;

      const authMethod = await otp.getAuthMethod(foreign, 'OTP');

      const result = speakeasy.totp.verify({
        secret: authMethod.base32secret,
        encoding: 'base32',
        token,
        window: 2,
      });

      return { result };
    }),

    ...hepius.unary('getAuthMethod', async call => {
      const { request } = call;
      const { foreign } = request;

      const result = await otp.getAuthMethod(foreign, 'OTP');

      return {
        result,
      };
    }),

    ...hepius.unary('updateAuthMethod', async call => {
      const { request } = call;
      const { foreign, statusHandle } = request;

      const result = await otp.updateAuthMethod({
        foreign,
        typeHandle: 'OTP',
        statusHandle,
      });

      return { result };
    }),

    ...hepius.unary('removeAuthMethod', async call => {
      const { request } = call;
      const { foreign } = request;

      const result = await otp.removeAuthMethod(foreign, 'OTP');

      return { result };
    }),
  };
};
