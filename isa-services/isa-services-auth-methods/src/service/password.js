const bcrypt = require('bcrypt');
const crypto = require('crypto');

const Model = require('../model');

module.exports = async (db, hepius) => {
  const authMethods = await Model.createInstance(db);
  const saltRounds = 10;

  return {
    ...hepius.unary('createPassword', async call => {
      const { request } = call;
      const { foreign, plaintextPassword, statusHandle } = request;

      // do this to be compatible with migrated passwords
      // flaw in bcrypt implementation is circumvented by
      // storing hashed UTF-8 version before storage
      const sha256Digest = crypto
        .createHash('sha256')
        .update(plaintextPassword)
        .digest('hex');

      const salt = bcrypt.genSaltSync(saltRounds, 'a');
      const bcryptHash = bcrypt.hashSync(sha256Digest, salt);

      const result = await authMethods.createAuthMethod({
        foreign,
        typeHandle: 'PASSWORD',
        bcryptHash,
        statusHandle,
      });

      return { result };
    }),

    ...hepius.unary('getAuthMethod', async call => {
      const { request } = call;
      const { foreign, referenceId } = request;

      // security concern: if nothing defined, will return the first document!
      if (!foreign && !referenceId) {
        throw new Error('Query cannot be empty.');
      }

      const result = await authMethods.getAuthMethod(
        foreign,
        'PASSWORD',
        referenceId
      );

      return { result };
    }),

    ...hepius.unary('passwordMatches', async call => {
      const { request } = call;
      const { foreign, plaintextPassword } = request;

      const authMethod = await authMethods.getAuthMethod(foreign, 'PASSWORD');

      if (!authMethod) {
        throw new Error('The foreigner does not have a password set.');
      }

      const { bcryptHash } = authMethod;
      const result = bcrypt.compareSync(
        crypto
          .createHash('sha256')
          .update(plaintextPassword)
          .digest('hex'),
        bcryptHash
      );

      return { result };
    }),

    ...hepius.unary('updatePassword', async call => {
      const { request } = call;
      const { foreign, plaintextPassword, referenceId } = request;

      // security concern: if nothing defined, will return the first document!
      if (!foreign && !referenceId) {
        throw new Error('Query cannot be empty.');
      }

      const sha256Digest = crypto
        .createHash('sha256')
        .update(plaintextPassword)
        .digest('hex');

      const salt = bcrypt.genSaltSync(saltRounds, 'a');
      const bcryptHash = bcrypt.hashSync(sha256Digest, salt);

      const result = await authMethods.updateAuthMethod({
        foreign,
        typeHandle: 'PASSWORD',
        referenceId,
        bcryptHash,
      });

      return { result };
    }),

    ...hepius.unary('updateAuthMethod', async call => {
      const { request } = call;
      const { foreign, retries, statusHandle } = request;

      const result = await authMethods.updateAuthMethod({
        foreign,
        typeHandle: 'PASSWORD',
        retries,
        statusHandle,
      });

      return { result };
    }),
  };
};
