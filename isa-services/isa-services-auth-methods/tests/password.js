const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Password: password } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(password);

  t.context.password = password;
  t.context.foreign = { id: 'id', typeHandle: 'TEST' };
});

test.after.always(async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('create password', async t => {
  const { foreign, password } = t.context;

  const { result: created } = await password.createPasswordAsync({
    request: {
      foreign,
      plaintextPassword: 'plainpassword',
      statusHandle: 'ACTIVATED',
    },
  });
  t.true(created);

  const { result: authMethod } = await password.getAuthMethodAsync({
    request: { foreign },
  });
  t.is(authMethod.statusHandle, 'ACTIVATED');
});

test.serial('password retries', async t => {
  const { foreign, password } = t.context;

  const { result: created } = await password.createPasswordAsync({
    request: {
      foreign,
      plaintextPassword: 'plainpassword',
      statusHandle: 'ACTIVATED',
    },
  });
  t.true(created);

  await password.updateAuthMethodAsync({
    request: {
      retries: 100,
      statusHandle: 'BLOCKED',
      foreign,
    },
  });

  const { result: resetted } = await password.updateAuthMethodAsync({
    request: { foreign, retries: 0, statusHandle: 'ACTIVATED' },
  });
  t.true(resetted);

  const { result: resetPasswordAuthMethod } = await password.getAuthMethodAsync(
    {
      request: {
        foreign,
      },
    }
  );
  t.is(resetPasswordAuthMethod.retries, 0);
});

test.serial('compare password', async t => {
  const { foreign, password } = t.context;

  const { result: created } = await password.createPasswordAsync({
    request: {
      foreign,
      plaintextPassword: 'plainpassword',
      statusHandle: 'ACTIVATED',
    },
  });
  t.true(created);

  const { result: unequal } = await password.passwordMatchesAsync({
    request: { foreign, plaintextPassword: 'bla' },
  });
  t.false(unequal);

  const { result: equal } = await password.passwordMatchesAsync({
    request: { foreign, plaintextPassword: 'plainpassword' },
  });
  t.true(equal);
});

test.serial('no password set', async t => {
  const { password } = t.context;

  await t.throwsAsync(
    async () => {
      return password.passwordMatchesAsync({
        request: {
          foreign: { id: 'bad', typeHandle: 'BAD' },
          plaintextPassword: 'nothing',
        },
      });
    },
    {
      instanceOf: Error,
      message: 'The foreigner does not have a password set.',
    }
  );
});

test.serial('update password', async t => {
  const { foreign, password } = t.context;

  const { result: created } = await password.createPasswordAsync({
    request: {
      foreign,
      plaintextPassword: 'plainpassword',
      statusHandle: 'ACTIVATED',
    },
  });
  t.true(created);

  const { result: updated } = await password.updatePasswordAsync({
    request: { foreign, plaintextPassword: 'updatedpassword' },
  });
  t.true(updated);

  const { result: matches } = await password.passwordMatchesAsync({
    request: { foreign, plaintextPassword: 'updatedpassword' },
  });
  t.true(matches);
});
