const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Awards = await service(db, hepius);

  return { Awards };
};
