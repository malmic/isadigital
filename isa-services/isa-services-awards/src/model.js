const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('awards', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['name', 'editionHandle', 'referenceId'],
          properties: {
            editionHandle: { bsonType: 'int' },
            name: { bsonType: 'string' },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createAward(award) {
    const referenceId = uniqid();

    await this.collection.insertOne({ ...award, referenceId });

    return { result: referenceId };
  }

  async deleteAward({ referenceId }) {
    const result = await this.collection.deleteOne({ referenceId });

    return { result: result.result.ok === 1 };
  }

  async getAwards() {
    const result = await this.collection.find().toArray();

    return { result };
  }

  async getAward({ referenceId }) {
    const result = await this.collection.findOne({ referenceId });

    return { result };
  }

  async updateAward({ referenceId, name }) {
    let $set = {};
    if (name) $set.name = name;

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
