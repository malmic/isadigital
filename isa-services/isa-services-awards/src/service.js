const Model = require('./model');

module.exports = async (db, hepius) => {
  const awards = await Model.createInstance(db);

  return {
    ...hepius.unary('createAward', ({ request }) =>
      awards.createAward(request)
    ),

    ...hepius.unary('deleteAward', ({ request }) =>
      awards.deleteAward(request)
    ),

    ...hepius.unary('getAwards', ({ request }) => awards.getAwards(request)),

    ...hepius.unary('getAward', ({ request }) => awards.getAward(request)),

    ...hepius.unary('updateAward', ({ request }) =>
      awards.updateAward(request)
    ),
  };
};
