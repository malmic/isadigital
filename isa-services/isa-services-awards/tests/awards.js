const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Awards: awards } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(awards);

  t.context.awards = awards;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('awards', async t => {
  const { awards } = t.context;

  const { result: referenceId } = await awards.createAwardAsync({
    request: { editionHandle: 2020, name: 'test' },
  });

  const { result: allAwards } = await awards.getAwardsAsync({
    request: {},
  });

  t.is(allAwards.length, 1);
  t.is(allAwards[0].name, 'test');

  const { result: award } = await awards.getAwardAsync({
    request: { referenceId },
  });

  t.is(award.editionHandle, 2020);

  await awards.updateAwardAsync({
    request: { referenceId, name: 'test2' },
  });

  const { result: updatedAwards } = await awards.getAwardsAsync({
    request: {},
  });

  t.is(updatedAwards.length, 1);
  t.is(updatedAwards[0].name, 'test2');

  const { result: deleted } = await awards.deleteAwardAsync({
    request: { referenceId },
  });

  t.true(deleted);

  const { result: deletedAwards } = await awards.getAwardsAsync({
    request: {},
  });

  t.is(deletedAwards.length, 0);
});
