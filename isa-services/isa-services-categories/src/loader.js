const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Categories = await service(db, hepius);

  return { Categories };
};
