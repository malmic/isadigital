const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('categories', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['name', 'referenceId'],
          properties: {
            name: { bsonType: 'string' },
            referenceId: { bsonType: 'string' },
            parent: { bsonType: 'string' },
          },
        },
      },
    });

    const root = await collection.findOne({ parent: '-1' });
    if (!root)
      await collection.insertOne({
        referenceId: uniqid(),
        name: 'Root',
        parent: '-1',
      });

    return new Model(collection);
  }

  async createCategory(category) {
    const referenceId = uniqid();

    await this.collection.insertOne({ referenceId, ...category });

    return { result: referenceId };
  }

  async getCategories() {
    const result = await this.collection
      .find({}, { sort: { name: 1 } })
      .toArray();

    return { result };
  }

  async getCategory({ referenceId }) {
    const result = await this.collection.findOne({ referenceId });

    return { result };
  }

  async deleteCategory({ referenceId }) {
    const result = await this.collection.deleteOne({ referenceId });

    return { result: result.result.ok === 1 };
  }

  async updateCategory({ referenceId, ...category }) {
    const $set = {};
    if (category.name) $set.name = category.name;

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
