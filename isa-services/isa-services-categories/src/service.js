const Model = require('./model');

module.exports = async (db, hepius) => {
  const categories = await Model.createInstance(db);

  return {
    ...hepius.unary('createCategory', ({ request }) =>
      categories.createCategory(request)
    ),

    ...hepius.unary('getCategory', ({ request }) =>
      categories.getCategory(request)
    ),

    ...hepius.unary('getCategories', ({ request }) =>
      categories.getCategories(request)
    ),

    ...hepius.unary('deleteCategory', ({ request }) =>
      categories.deleteCategory(request)
    ),

    ...hepius.unary('updateCategory', ({ request }) =>
      categories.updateCategory(request)
    ),
  };
};
