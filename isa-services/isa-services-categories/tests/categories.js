const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Categories: categories } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(categories);

  t.context.categories = categories;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('category', async t => {
  const { categories } = t.context;

  const { result: referenceId } = await categories.createCategoryAsync({
    request: { name: 'Something' },
  });

  const { result: category } = await categories.getCategoryAsync({
    request: { referenceId },
  });

  t.is(category.name, 'Something');

  await categories.updateCategoryAsync({
    request: { referenceId, name: 'test' },
  });

  const { result: updatedCategory } = await categories.getCategoryAsync({
    request: { referenceId },
  });

  t.is(updatedCategory.name, 'test');

  const { result: deleted } = await categories.deleteCategoryAsync({
    request: { referenceId },
  });

  t.true(deleted);

  const { result: deletedCategory } = await categories.getCategoryAsync({
    request: { referenceId },
  });

  t.is(deletedCategory, null);
});

test.serial('categories', async t => {
  const { categories } = t.context;

  const { result: referenceId } = await categories.createCategoryAsync({
    request: { name: 'test' },
  });

  const { result: category } = await categories.getCategoryAsync({
    request: { referenceId },
  });

  t.is(category.name, 'test');

  const { result: allCategories } = await categories.getCategoriesAsync({
    request: {},
  });

  t.is(allCategories.length, 2);
  t.is(allCategories[0].name, 'Root');
});
