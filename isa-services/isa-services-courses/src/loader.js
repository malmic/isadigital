const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Courses = await service(db, hepius);

  return { Courses };
};
