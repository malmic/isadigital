const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('courses', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'referenceId'],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createCourse({ foreign }) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      foreign,
      referenceId,
    });

    return { result: referenceId };
  }

  async getCourses({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.find(query).toArray();

    return { result };
  }

  async getCourse({ referenceId, foreign }) {
    const query = {};
    if (referenceId) query.referenceId = referenceId;
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.findOne(query);

    return { result };
  }
}

module.exports = Model;
