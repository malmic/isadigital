const Model = require('./model');

module.exports = async (db, hepius) => {
  const courses = await Model.createInstance(db);

  return {
    ...hepius.unary('createCourse', ({ request }) =>
      courses.createCourse(request)
    ),

    ...hepius.unary('getCourse', ({ request }) => courses.getCourse(request)),

    ...hepius.unary('getCourses', ({ request }) => courses.getCourses(request)),
  };
};
