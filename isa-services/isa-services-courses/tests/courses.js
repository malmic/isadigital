const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Courses: courses } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(courses);

  t.context.courses = courses;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('courses', async t => {
  const { courses } = t.context;

  await courses.createCourseAsync({
    request: { foreign: [{ id: '1', typeHandle: 'USER' }] },
  });

  await courses.createCourseAsync({
    request: { foreign: [{ id: '2', typeHandle: 'USER' }] },
  });

  const { result: allCourses } = await courses.getCoursesAsync({
    request: {},
  });

  t.is(allCourses.length, 2);

  const { result: coursesByForeign } = await courses.getCoursesAsync({
    request: { foreign: [{ id: '2', typeHandle: 'USER' }] },
  });

  t.is(coursesByForeign.length, 1);
});

test.serial('course', async t => {
  const { courses } = t.context;

  const { result: referenceId } = await courses.createCourseAsync({
    request: { foreign: [{ id: '1', typeHandle: 'USER' }] },
  });

  const { result: course } = await courses.getCourseAsync({
    request: {
      referenceId,
    },
  });

  t.is(course.referenceId, referenceId);

  const { result: courseByForeign } = await courses.getCourseAsync({
    request: {
      foreign: [{ id: '1', typeHandle: 'USER' }],
    },
  });

  t.is(courseByForeign.referenceId, referenceId);
});
