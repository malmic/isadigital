const service = require('./service');

const data = require('../data/editions.json');

module.exports = async (db, env, hepius) => {
  const Editions = await service(data, hepius);

  return { Editions };
};
