class Model {
  constructor(data) {
    this.data = data;
  }

  async getEditions() {
    return { result: this.data };
  }

  async getEdition({ handle }) {
    const result = this.data.find(d => d.handle === handle);

    return { result };
  }
}

module.exports = Model;
