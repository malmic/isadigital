const Model = require('./model');

module.exports = (data, hepius) => {
  const editions = new Model(data);

  return {
    ...hepius.unary('getEdition', ({ request }) =>
      editions.getEdition(request)
    ),

    ...hepius.unary('getEditions', ({ request }) =>
      editions.getEditions(request)
    ),
  };
};
