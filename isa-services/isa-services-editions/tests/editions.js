const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const loader = require('../src');

test.beforeEach('instantiate service', async t => {
  const { Editions: editions } = await loader(
    [{ handle: 2020 }, { handle: 2021 }],
    null,
    new Hepius()
  );

  Bluebird.promisifyAll(editions);

  t.context.editions = editions;
});

test('editions', async t => {
  const { editions } = t.context;

  const { result: edition } = await editions.getEditionAsync({
    request: { handle: 2020 },
  });

  t.is(edition.handle, 2020);

  const { result: allEditions } = await editions.getEditionsAsync({
    request: {},
  });

  t.is(allEditions.length, 2);
  t.is(allEditions[0].handle, 2020);
});
