const service = require('./service');

module.exports = async (db, env, hepius) => {
  const EmailAddresses = await service(db, hepius);

  return {
    EmailAddresses,
  };
};
