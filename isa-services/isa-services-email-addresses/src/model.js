const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('emailAddresses', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'createdAt',
            'updatedAt',
            'foreign',
            'referenceId',
            'address',
            'verified',
            'active',
            'blocked',
          ],
          properties: {
            updatedAt: {
              bsonType: 'date',
            },
            createdAt: {
              bsonType: 'date',
            },
            referenceId: {
              bsonType: 'string',
            },
            foreign: {
              bsonType: 'object',
              properties: {
                id: {
                  bsonType: 'string',
                },
                typeHandle: {
                  bsonType: 'string',
                },
              },
            },
            address: {
              bsonType: 'string',
            },
            verified: {
              bsonType: 'bool',
            },
            active: {
              bsonType: 'bool',
            },
            blocked: {
              bsonType: 'bool',
            },
          },
        },
      },
    });

    await collection.createIndex({ address: 1 }, { unique: true });

    return new Model(collection);
  }

  getStream(resumeToken, $match) {
    const options = {};
    if (resumeToken) {
      options.resumeAfter = { _data: resumeToken };
    }

    return this.collection.watch([{ $match }], options);
  }

  async createEmailAddress({ foreign, address, verified }) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      foreign,
      referenceId,
      address,
      verified: verified || false,
      createdAt: new Date(),
      updatedAt: new Date(),
      active: true,
      blocked: false,
    });

    return { result: referenceId };
  }

  async getActiveEmailAddress({ foreign }) {
    const result = await this.collection.findOne({
      foreign,
      active: true,
    });

    return { result };
  }

  async getEmailAddress({ address, referenceId }) {
    const query = {};
    if (address) query.address = address;
    if (referenceId) query.referenceId = referenceId;

    // security concern: if nothing defined, will return the first document!
    if (Object.keys(query).length === 0)
      throw new Error('Query cannot be empty.');

    const result = await this.collection.findOne(query);

    return { result };
  }

  async getEmailAddresses({ foreign }) {
    const result = await this.collection
      .find({ foreign }, { sort: { address: 1 } })
      .toArray();

    return { result };
  }

  async findEmailAddresses({ search }) {
    if (!search) return { result: [] };

    const result = await this.collection
      .find({ address: new RegExp(search, 'i') })
      .toArray();

    return { result };
  }

  async updateEmailAddress(emailAddress) {
    const query = {};
    if (emailAddress.address) query.address = emailAddress.address;
    if (emailAddress.referenceId) query.referenceId = emailAddress.referenceId;

    const $set = { updatedAt: new Date() };
    if ('active' in emailAddress) $set.active = emailAddress.active;
    if ('verified' in emailAddress) $set.verified = emailAddress.verified;
    if ('blocked' in emailAddress) $set.blocked = emailAddress.blocked;

    const result = await this.collection.updateOne(query, { $set });

    return { result: result.result.nModified === 1 };
  }

  async emailAddressExists({ address }) {
    const count = await this.collection.countDocuments({
      address,
    });

    return { result: count > 0 };
  }
}

module.exports = Model;
