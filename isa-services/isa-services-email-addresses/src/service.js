const Model = require('./model');

module.exports = async (db, hepius) => {
  const emailAddresses = await Model.createInstance(db);

  return {
    ...hepius.unary('getEmailAddresses', ({ request }) =>
      emailAddresses.getEmailAddresses(request)
    ),

    ...hepius.unary('updateEmailAddress', ({ request }) =>
      emailAddresses.updateEmailAddress(request)
    ),

    ...hepius.unary('getEmailAddress', ({ request }) =>
      emailAddresses.getEmailAddress(request)
    ),

    ...hepius.unary('getActiveEmailAddress', ({ request }) =>
      emailAddresses.getActiveEmailAddress(request)
    ),

    ...hepius.unary('createEmailAddress', ({ request }) =>
      emailAddresses.createEmailAddress(request)
    ),

    ...hepius.unary('emailAddressExists', ({ request }) =>
      emailAddresses.emailAddressExists(request)
    ),

    ...hepius.unary('findEmailAddresses', ({ request }) =>
      emailAddresses.findEmailAddresses(request)
    ),
  };
};
