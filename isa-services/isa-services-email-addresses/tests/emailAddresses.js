const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { EmailAddresses: emailAddresses } = await loader(
    db,
    null,
    new Hepius()
  );

  Bluebird.promisifyAll(emailAddresses);

  t.context.emailAddresses = emailAddresses;
  t.context.foreignOne = { id: '5678', typeHandle: 'TEST' };
  t.context.foreignTwo = { id: '5679', typeHandle: 'TEST' };
});

test.after(async t => {
  await t.context.client.close();
});

test.serial('email addresses exist', async t => {
  const { foreignOne, emailAddresses } = t.context;

  const { result: notExists } = await emailAddresses.emailAddressExistsAsync({
    request: { emailAddress: 'test@test.com' },
  });

  t.false(notExists);

  await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const { result: exists } = await emailAddresses.emailAddressExistsAsync({
    request: { address: 'test@test.com' },
  });

  t.true(exists);
});

test.serial('email addresses', async t => {
  const { foreignOne, emailAddresses } = t.context;

  await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const {
    result: allEmailAddresses,
  } = await emailAddresses.getEmailAddressesAsync({
    request: { foreign: foreignOne },
  });

  t.is(allEmailAddresses.length, 1);
  t.is(allEmailAddresses[0].address, 'test@test.com');
  t.is(allEmailAddresses[0].verified, false);
});

test.serial('get email address', async t => {
  const { foreignOne, emailAddresses } = t.context;

  const { result: referenceId } = await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const { result: emailAddress } = await emailAddresses.getEmailAddressAsync({
    request: {
      referenceId,
    },
  });

  t.is(emailAddress.address, 'test@test.com');

  await t.throwsAsync(
    async () => {
      return emailAddresses.getEmailAddressAsync({
        request: {},
      });
    },
    {
      instanceOf: Error,
      message: 'Query cannot be empty.',
    }
  );
});

test.serial('active email address', async t => {
  const { foreignOne, emailAddresses } = t.context;

  await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const {
    result: activeEmailAddress,
  } = await emailAddresses.getActiveEmailAddressAsync({
    request: {
      foreign: foreignOne,
    },
  });

  t.is(activeEmailAddress.address, 'test@test.com');
});

test.serial('find email address', async t => {
  const { foreignOne, foreignTwo, emailAddresses } = t.context;

  const { result: ref1 } = await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const { result: ref2 } = await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignTwo,
      address: 'teb@beb.com',
      verified: false,
    },
  });

  const {
    result: multipleEmailAddresses,
  } = await emailAddresses.findEmailAddressesAsync({
    request: {
      search: '.*t.*',
    },
  });

  t.is(multipleEmailAddresses.length, 2);
  t.is(multipleEmailAddresses[0].referenceId, ref2);
  t.is(multipleEmailAddresses[1].referenceId, ref1);

  const {
    result: singleEmailAddress,
  } = await emailAddresses.findEmailAddressesAsync({
    request: { search: '.*tes.*' },
  });

  t.is(singleEmailAddress.length, 1);
  t.is(singleEmailAddress[0].address, 'test@test.com');
});

test.serial('update verified', async t => {
  const { foreignOne, emailAddresses } = t.context;

  await emailAddresses.createEmailAddressAsync({
    request: {
      foreign: foreignOne,
      address: 'test@test.com',
      verified: false,
    },
  });

  const {
    result: updatedVerified,
  } = await emailAddresses.updateEmailAddressAsync({
    request: {
      address: 'test@test.com',
      verified: true,
    },
  });

  t.true(updatedVerified);

  const {
    result: updatedActive,
  } = await emailAddresses.updateEmailAddressAsync({
    request: {
      address: 'test@test.com',
      active: false,
    },
  });

  t.true(updatedActive);

  const { result: verifiedAddress } = await emailAddresses.getEmailAddressAsync(
    {
      request: {
        address: 'test@test.com',
      },
    }
  );

  t.is(verifiedAddress.verified, true);
  t.is(verifiedAddress.active, false);
});
