const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Events = await service(db, hepius);

  return { Events };
};
