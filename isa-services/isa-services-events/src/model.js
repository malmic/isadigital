const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('events', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'beginAt', 'endAt', 'name', 'editionHandle'],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            beginAt: { bsonType: 'date' },
            endAt: { bsonType: 'date' },
            name: { bsonType: 'string' },
            editionHandle: { bsonType: 'int' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async getEvents({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection
      .find(query, { sort: { name: 1 } })
      .toArray();

    return { result };
  }

  async getEvent({ referenceId, foreign }) {
    const query = {};
    if (referenceId) query.referenceId = referenceId;
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.findOne(query);

    return { result };
  }

  async createEvent(event) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      ...event,
      referenceId,
      beginAt: new Date(event.beginAt),
      endAt: new Date(event.endAt),
    });

    return { result: referenceId };
  }

  async updateEvent({
    referenceId,
    foreign,
    name,
    beginAt,
    endAt,
    editionHandle,
  }) {
    let $set = {};
    if (foreign && foreign.length > 0) $set.foreign = foreign;
    if (name) $set.name = name;
    if (beginAt) $set.beginAt = new Date(beginAt);
    if (endAt) $set.endAt = new Date(endAt);
    if (editionHandle) $set.editionHandle = editionHandle;

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
