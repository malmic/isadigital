const Model = require('./model');

module.exports = async (db, hepius) => {
  const events = await Model.createInstance(db);

  return {
    ...hepius.unary('createEvent', ({ request }) =>
      events.createEvent(request)
    ),

    ...hepius.unary('getEvent', ({ request }) => events.getEvent(request)),

    ...hepius.unary('getEvents', ({ request }) => events.getEvents(request)),

    ...hepius.unary('updateEvent', ({ request }) =>
      events.updateEvent(request)
    ),
  };
};
