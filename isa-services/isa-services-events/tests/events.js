const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Events: events } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(events);

  t.context.events = events;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('events', async t => {
  const { events } = t.context;

  await events.createEventAsync({
    request: {
      foreign: [{ id: '1', typeHandle: 'ROOM' }],
      beginAt: new Date(),
      endAt: new Date(),
      name: 'Some event 1',
      editionHandle: 2020,
    },
  });

  await events.createEventAsync({
    request: {
      foreign: [{ id: '2', typeHandle: 'ROOM' }],
      beginAt: new Date(),
      endAt: new Date(),
      name: 'Some event 2',
      editionHandle: 2020,
    },
  });

  const { result: event } = await events.getEventAsync({
    request: { foreign: [{ id: '2', typeHandle: 'ROOM' }] },
  });

  t.is(event.name, 'Some event 2');

  const { result: allEvents } = await events.getEventsAsync({
    request: {},
  });

  t.is(allEvents.length, 2);
  t.is(allEvents[0].name, 'Some event 1');

  const { result: allForeignEvents } = await events.getEventsAsync({
    request: { foreign: [{ id: '2', typeHandle: 'ROOM' }] },
  });

  t.is(allForeignEvents.length, 1);
  t.is(allForeignEvents[0].name, 'Some event 2');
});

test.serial('event', async t => {
  const { events } = t.context;

  const { result: referenceId } = await events.createEventAsync({
    request: {
      foreign: [{ id: '1', typeHandle: 'ROOM' }],
      beginAt: new Date(),
      endAt: new Date(),
      name: 'Some event 1',
      editionHandle: 2020,
    },
  });

  await events.createEventAsync({
    request: {
      foreign: [{ id: '2', typeHandle: 'ROOM' }],
      beginAt: new Date(),
      endAt: new Date(),
      name: 'Some event 2',
      editionHandle: 2020,
    },
  });

  const { result: event } = await events.getEventAsync({
    request: { foreign: [{ id: '2', typeHandle: 'ROOM' }] },
  });

  t.is(event.name, 'Some event 2');

  const { result: updated } = await events.updateEventAsync({
    request: { referenceId, name: 'updated', editionHandle: 2022 },
  });

  t.true(updated);

  const { result: eventByReferenceId } = await events.getEventAsync({
    request: { referenceId },
  });

  t.is(eventByReferenceId.name, 'updated');
  t.is(eventByReferenceId.editionHandle, 2022);
});
