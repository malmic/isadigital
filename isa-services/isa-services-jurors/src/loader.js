const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Jurors = await service(db, hepius);

  return { Jurors };
};
