const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('jurors', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'referenceId'],
          properties: {
            foreign: {
              bsonType: 'object',
              properties: {
                id: { bsonType: 'string' },
                typeHandle: { bsonType: 'string' },
              },
            },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createJuror(juror) {
    const referenceId = uniqid();

    await this.collection.insertOne({ ...juror, referenceId });

    return { result: referenceId };
  }

  async getJurors() {
    const result = await this.collection.find().toArray();

    return { result };
  }

  async deleteJuror({ referenceId, foreign }) {
    const query = {};
    if (referenceId) query.referenceId = referenceId;
    if (foreign) query.foreign = foreign;

    const result = await this.collection.deleteOne(query);

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
