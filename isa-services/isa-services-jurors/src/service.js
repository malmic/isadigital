const Model = require('./model');

module.exports = async (db, hepius) => {
  const jurors = await Model.createInstance(db);

  return {
    ...hepius.unary('createJuror', ({ request }) =>
      jurors.createJuror(request)
    ),

    ...hepius.unary('getJurors', ({ request }) => jurors.getJurors(request)),

    ...hepius.unary('deleteJuror', ({ request }) =>
      jurors.deleteJuror(request)
    ),
  };
};
