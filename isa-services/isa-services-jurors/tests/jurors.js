const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Jurors: jurors } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(jurors);

  t.context.jurors = jurors;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('jurors', async t => {
  const { jurors } = t.context;

  const { result: referenceId } = await jurors.createJurorAsync({
    request: {
      foreign: { id: 'id', typeHandle: 'USER' },
    },
  });

  const { result: allJurors } = await jurors.getJurorsAsync({
    request: {},
  });

  t.is(allJurors.length, 1);
  t.is(allJurors[0].referenceId, referenceId);

  const { result: deleted } = await jurors.deleteJurorAsync({
    request: {
      foreign: { id: 'id', typeHandle: 'USER' },
    },
  });

  t.true(deleted);

  const { result: emptyJurors } = await jurors.getJurorsAsync({
    request: {},
  });

  t.is(emptyJurors.length, 0);
});
