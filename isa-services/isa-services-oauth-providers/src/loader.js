const ClientOAuth2 = require('client-oauth2');

const Model = require('./model');

module.exports = async (db, { GOOGLE }) => {
  const google = new Model(new ClientOAuth2(JSON.parse(GOOGLE)));

  return { google };
};
