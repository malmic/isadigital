class Model {
  constructor(client) {
    this.client = client;
  }

  async getToken({ url }) {
    const { data } = await this.client.code.getToken(url);

    const result = {
      accessToken: data.access_token,
      expiresIn: data.expires_in,
      tokenType: data.token_type,
      state: data.state,
      refreshToken: data.refresh_token,
    };

    return { result };
  }

  getUri(params) {
    const result = this.client.code.getUri(params);

    return { result };
  }
}

module.exports = Model;
