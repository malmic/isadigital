const service = require('./service');

module.exports = async (db, env, hepius) => {
  const PaymentPlans = await service(db, hepius);

  return { PaymentPlans };
};
