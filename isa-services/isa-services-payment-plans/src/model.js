const Big = require('big.js');
const { Decimal128 } = require('mongodb');
const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;

    this.project = {
      _id: 0,
      referenceId: 1,
      name: 1,
      createdAt: 1,
      editionHandle: 1,
      quantity: {
        handle: '$quantity.handle',
        value: { $convert: { input: '$quantity.value', to: 'double' } },
      },
    };
  }

  static async createInstance(db) {
    const collection = await db.createCollection('paymentPlans', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'createdAt',
            'name',
            'editionHandle',
            'referenceId',
            'quantity',
          ],
          properties: {
            createdAt: { bsonType: 'date' },
            quantity: {
              bsonType: 'object',
              required: ['handle', 'value'],
              properties: {
                handle: { bsonType: 'string' },
                value: { bsonType: 'decimal' },
              },
            },
            name: { bsonType: 'string' },
            editionHandle: { bsonType: 'int' },
          },
        },
      },
    });

    await collection.createIndex({ createdAt: -1 });

    return new Model(collection);
  }

  async createPaymentPlan({ quantity, ...paymentPlan }) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      ...paymentPlan,
      createdAt: new Date(),
      quantity: {
        ...quantity,
        value: Decimal128.fromString(Big(quantity.value).toFixed(18)),
      },
      referenceId,
    });

    return { result: referenceId };
  }

  async getPaymentPlan({ referenceId }) {
    let $match = {};

    if (referenceId) $match.referenceId = referenceId;

    const result = await this.collection
      .aggregate([{ $match }, { $project: this.project }, { $limit: 1 }])
      .toArray();

    return { result: result.length === 1 ? result[0] : null };
  }

  async getPaymentPlans() {
    let $match = {};

    const result = await this.collection
      .aggregate([{ $match }, { $project: this.project }])
      .toArray();

    return { result };
  }

  async deletePaymentPlan({ referenceId }) {
    const result = await this.collection.deleteOne({ referenceId });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
