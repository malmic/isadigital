const Model = require('./model');

module.exports = async (db, hepius) => {
  const paymentPlans = await Model.createInstance(db);

  return {
    ...hepius.unary('createPaymentPlan', ({ request }) =>
      paymentPlans.createPaymentPlan(request)
    ),

    ...hepius.unary('getPaymentPlan', ({ request }) =>
      paymentPlans.getPaymentPlan(request)
    ),

    ...hepius.unary('getPaymentPlans', ({ request }) =>
      paymentPlans.getPaymentPlans(request)
    ),

    ...hepius.unary('deletePaymentPlan', ({ request }) =>
      paymentPlans.deletePaymentPlan(request)
    ),
  };
};
