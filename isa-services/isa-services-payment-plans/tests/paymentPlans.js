const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { PaymentPlans: paymentPlans } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(paymentPlans);

  t.context.paymentPlans = paymentPlans;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('payment plan', async t => {
  const { paymentPlans } = t.context;

  const { result: emptyPaymentPlan } = await paymentPlans.getPaymentPlanAsync({
    request: {},
  });

  t.is(emptyPaymentPlan, null);

  const { result: referenceId } = await paymentPlans.createPaymentPlanAsync({
    request: {
      quantity: { handle: 'EURCENT', value: 30.0 },
      name: 'test #1',
      editionHandle: 2020,
    },
  });

  const { result: paymentPlan } = await paymentPlans.getPaymentPlanAsync({
    request: { referenceId },
  });

  t.is(paymentPlan.referenceId, referenceId);

  const { result: deleted } = await paymentPlans.deletePaymentPlanAsync({
    request: { referenceId },
  });

  t.true(deleted);
});

test.serial('payment plans', async t => {
  const { paymentPlans } = t.context;

  await paymentPlans.createPaymentPlanAsync({
    request: {
      quantity: { handle: 'EURCENT', value: 30.0 },
      name: 'test #1',
      editionHandle: 2020,
    },
  });

  const { result: allPaymentPlans } = await paymentPlans.getPaymentPlansAsync({
    request: {},
  });

  t.is(allPaymentPlans.length, 1);
  t.is(allPaymentPlans[0].quantity.value, 30.0);
});
