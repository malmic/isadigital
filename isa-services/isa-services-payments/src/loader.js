const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Payments = await service(db, hepius);

  return { Payments };
};
