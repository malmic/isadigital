const Big = require('big.js');
const { Decimal128 } = require('mongodb');
const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;

    this.project = {
      _id: 0,
      referenceId: 1,
      name: 1,
      createdAt: 1,
      foreign: 1,
      editionHandle: 1,
      quantity: {
        handle: '$quantity.handle',
        value: { $convert: { input: '$quantity.value', to: 'double' } },
      },
    };
  }

  static async createInstance(db) {
    const collection = await db.createCollection('payments', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'foreign',
            'referenceId',
            'name',
            'editionHandle',
            'quantity',
          ],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            quantity: {
              bsonType: 'object',
              required: ['handle', 'value'],
              properties: {
                handle: { bsonType: 'string' },
                value: { bsonType: 'decimal' },
              },
            },
            name: { bsonType: 'string' },
            editionHandle: { bsonType: 'int' },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    /* const plans = await db.createCollection('plans');
    const allPlans = await plans.find().toArray();

    for (const plan of allPlans) {
      if (plan.typeHandle === 'BASIC') {
        await collection.insertOne({
          referenceId: uniqid(),
          name: 'Basic',
          editionHandle: 2020,
          foreign: [plan.foreign],
          quantity: {
            handle: 'EURCENT',
            value: Decimal128.fromString(Big(6000).toFixed(18)),
          },
        });
      } else if (plan.typeHandle === 'PLUS') {
        await collection.insertOne({
          referenceId: uniqid(),
          name: 'Plus',
          editionHandle: 2020,
          foreign: [plan.foreign],
          quantity: {
            handle: 'EURCENT',
            value: Decimal128.fromString(Big(7000).toFixed(18)),
          },
        });
      }
    } */

    return new Model(collection);
  }

  async getPayment({ referenceId, foreign }) {
    let $match = {};

    if (referenceId) $match.referenceId = referenceId;
    if (foreign && foreign.length > 0)
      $match.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection
      .aggregate([{ $match }, { $project: this.project }, { $limit: 1 }])
      .toArray();

    return { result: result.length === 1 ? result[0] : null };
  }

  async createPayment({ quantity, ...payment }) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      ...payment,
      referenceId,
      quantity: {
        ...quantity,
        value: Decimal128.fromString(Big(quantity.value).toFixed(18)),
      },
    });

    return { result: referenceId };
  }
}

module.exports = Model;
