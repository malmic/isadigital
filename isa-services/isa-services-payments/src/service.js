const Model = require('./model');

module.exports = async (db, hepius) => {
  const payments = await Model.createInstance(db);

  return {
    ...hepius.unary('createPayment', ({ request }) =>
      payments.createPayment(request)
    ),

    ...hepius.unary('getPayment', ({ request }) =>
      payments.getPayment(request)
    ),
  };
};
