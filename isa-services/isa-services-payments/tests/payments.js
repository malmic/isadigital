const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Payments: payments } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(payments);

  t.context.payments = payments;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('payment', async t => {
  const { payments } = t.context;

  await payments.createPaymentAsync({
    request: {
      name: 'Basic',
      editionHandle: 2020,
      foreign: [{ id: '2', typeHandle: 'A' }],
      quantity: { handle: 'EURCENT', value: 5.0 },
    },
  });

  const { result: payment } = await payments.getPaymentAsync({
    request: { foreign: [{ id: '2', typeHandle: 'A' }] },
  });

  t.is(payment.foreign[0].id, '2');
});
