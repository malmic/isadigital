class Axios {
  async get(url) {
    if (url === '/playlists')
      return {
        data: {
          kind: 'youtube#playlistListResponse',
          etag: 'uODtIllzZlMtT4GgmOt38sM7Tiw',
          pageInfo: {
            totalResults: 1,
            resultsPerPage: 5,
          },
          items: [
            {
              kind: 'youtube#playlist',
              etag: 'I5PXDwneD-0LYq_Z8e1ypS0nreA',
              id: 'PL4o29bINVT4EG_y-k5jGoOu3-Am8Nvi10',
              snippet: {
                publishedAt: '2016-09-05T01:32:25Z',
                channelId: 'UCLldAVZ8HsQavK0dYSpX6sg',
                title: 'POP Music Playlist 2020',
                description:
                  "Music can change the world because it can change people. Bono ♫ ♬\n==========================================================\nWe are really glad that you are here, with us! If you like my playlist, please don't forget to save and share it. Have fun listening to it!\n\nWrite us please what you would change, what disturbed you (if it's the case) and we'll make all we can to make it better more, so that the experience will be much more enjoyable. Thank you.\n\nHey folks, you wanna listen more music ? \nEnjoy another hot playlists of 2019s Music :\n===================================================================\n* TOP 50 SONGS THIS WEEK 2020 : https://goo.gl/9e9lWc\n* TOP 40 SONGS 2020 - Best Music Songs: https://goo.gl/ZgMwiJ\n* POP Popular Music Playlist 2020 - Best of POP Songs: https://goo.gl/TFV9XA\n* POP Music Playlist 2020: https://goo.gl/sP6RJC\n* Pop Best Music Videos Playlist 2020 - Popular POP HITS: https://goo.gl/oMIEP2\n* UK Top 40 Songs This Week 2020: https://goo.gl/GEUOyW\n\nBecause we received a lot of questions regarding when and for how long we will update the playlist, we attached our schedule: January 2019, February 2019, March 2019, April 2019, May 2019, June 2019, July 2019, August 2019, September 2019, October 2019, November 2019, December 2019, January 2020, 2021, 2022 etc.",
                thumbnails: {
                  default: {
                    url: 'https://i.ytimg.com/vi/oygrmJFKYZY/default.jpg',
                    width: 120,
                    height: 90,
                  },
                  medium: {
                    url: 'https://i.ytimg.com/vi/oygrmJFKYZY/mqdefault.jpg',
                    width: 320,
                    height: 180,
                  },
                  high: {
                    url: 'https://i.ytimg.com/vi/oygrmJFKYZY/hqdefault.jpg',
                    width: 480,
                    height: 360,
                  },
                  standard: {
                    url: 'https://i.ytimg.com/vi/oygrmJFKYZY/sddefault.jpg',
                    width: 640,
                    height: 480,
                  },
                  maxres: {
                    url: 'https://i.ytimg.com/vi/oygrmJFKYZY/maxresdefault.jpg',
                    width: 1280,
                    height: 720,
                  },
                },
                channelTitle:
                  'POP Music Playlists - Billboard Charts & Top Songs',
                localized: {
                  title: 'POP Music Playlist 2020',
                  description:
                    "Music can change the world because it can change people. Bono ♫ ♬\n==========================================================\nWe are really glad that you are here, with us! If you like my playlist, please don't forget to save and share it. Have fun listening to it!\n\nWrite us please what you would change, what disturbed you (if it's the case) and we'll make all we can to make it better more, so that the experience will be much more enjoyable. Thank you.\n\nHey folks, you wanna listen more music ? \nEnjoy another hot playlists of 2019s Music :\n===================================================================\n* TOP 50 SONGS THIS WEEK 2020 : https://goo.gl/9e9lWc\n* TOP 40 SONGS 2020 - Best Music Songs: https://goo.gl/ZgMwiJ\n* POP Popular Music Playlist 2020 - Best of POP Songs: https://goo.gl/TFV9XA\n* POP Music Playlist 2020: https://goo.gl/sP6RJC\n* Pop Best Music Videos Playlist 2020 - Popular POP HITS: https://goo.gl/oMIEP2\n* UK Top 40 Songs This Week 2020: https://goo.gl/GEUOyW\n\nBecause we received a lot of questions regarding when and for how long we will update the playlist, we attached our schedule: January 2019, February 2019, March 2019, April 2019, May 2019, June 2019, July 2019, August 2019, September 2019, October 2019, November 2019, December 2019, January 2020, 2021, 2022 etc.",
                },
              },
            },
          ],
        },
      };

    if (url === '/playlistItems')
      return {
        data: {
          kind: 'youtube#playlistItemListResponse',
          etag: 'FY4v8PFgmWo3vQPGylHbq-SdaZs',
          items: [
            {
              kind: 'youtube#playlistItem',
              etag: '0WEKiKqe9zPxE5Ti-pzfcu_nNfQ',
              id:
                'UEw0bzI5YklOVlQ0RUdfeS1rNWpHb091My1BbThOdmkxMC5GRjQ5Qjk1MEIwQzBBRDlF',
              snippet: {
                publishedAt: '2020-03-29T22:45:43Z',
                channelId: 'UCLldAVZ8HsQavK0dYSpX6sg',
                title: 'Maroon 5 - Memories (Official Video)',
                description:
                  '“Memories” is out now:\nhttps://smarturl.it/MemoriesMaroon5\n\nFor more, visit:\nhttps://www.facebook.com/maroon5\nhttps://twitter.com/maroon5\nhttps://www.instagram.com/maroon5\n\nSign up for updates: http://smarturl.it/Maroon5.News\n\n#Maroon5 #Memories #M5\n\nMusic video by Maroon 5 performing Memories. © 2019 Interscope Records (222 Records)\n\nhttp://vevo.ly/wdiQiA',
                thumbnails: {
                  default: {
                    url: 'https://i.ytimg.com/vi/SlPhMPnQ58k/default.jpg',
                    width: 120,
                    height: 90,
                  },
                  medium: {
                    url: 'https://i.ytimg.com/vi/SlPhMPnQ58k/mqdefault.jpg',
                    width: 320,
                    height: 180,
                  },
                  high: {
                    url: 'https://i.ytimg.com/vi/SlPhMPnQ58k/hqdefault.jpg',
                    width: 480,
                    height: 360,
                  },
                  standard: {
                    url: 'https://i.ytimg.com/vi/SlPhMPnQ58k/sddefault.jpg',
                    width: 640,
                    height: 480,
                  },
                  maxres: {
                    url: 'https://i.ytimg.com/vi/SlPhMPnQ58k/maxresdefault.jpg',
                    width: 1280,
                    height: 720,
                  },
                },
                channelTitle:
                  'POP Music Playlists - Billboard Charts & Top Songs',
                playlistId: 'PL4o29bINVT4EG_y-k5jGoOu3-Am8Nvi10',
                position: 0,
                resourceId: {
                  kind: 'youtube#video',
                  videoId: 'SlPhMPnQ58k',
                },
              },
              contentDetails: {
                videoId: 'SlPhMPnQ58k',
                videoPublishedAt: '2019-10-08T14:00:10Z',
              },
            },
          ],
        },
      };
  }
}

module.exports = Axios;
