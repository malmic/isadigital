const axios = require('axios');

const service = require('./service');

module.exports = async (db, { GOOGLE_KEY }, hepius) => {
  const axiosInstance = axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
  });

  const Playlists = service.playlists(axiosInstance, GOOGLE_KEY, hepius);
  const Videos = service.videos(axiosInstance, GOOGLE_KEY, hepius);

  return { Playlists, Videos };
};
