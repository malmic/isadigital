const Playlists = require('./playlists');
const Videos = require('./videos');

module.exports = { Playlists, Videos };
