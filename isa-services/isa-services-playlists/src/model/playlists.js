class Model {
  constructor(client, apiKey) {
    this.client = client;
    this.apiKey = apiKey;
  }

  async getPlaylists({ channelId }) {
    let result = [];

    let nextPageToken = '';

    const params = { channelId, part: 'snippet', key: this.apiKey };

    while (true) {
      if (nextPageToken) params.pageToken = nextPageToken;

      const { data } = await this.client.get('/playlists', {
        params,
      });

      result = result.concat(
        data.items.map(item => ({
          title: item.snippet.title,
          referenceId: item.id,
        }))
      );

      if (!data.nextPageToken) break;
      else nextPageToken = data.nextPageToken;
    }

    return { result };
  }

  async getPlaylist(query) {
    let result = [];

    let nextPageToken = '';

    const params = {
      part: 'snippet,contentDetails',
      playlistId: query.referenceId,
      key: this.apiKey,
    };

    let i = 0;

    while (true) {
      if (nextPageToken) params.pageToken = nextPageToken;

      const { data } = await this.client.get('/playlistItems', {
        params,
      });

      result = result.concat(
        data.items.map(item => ({
          referenceId: item.id,
          title: item.snippet.title,
          description: item.snippet.description,
          videoId: item.contentDetails.videoId,
        }))
      );

      if (!data.nextPageToken || i > 5) break;
      else nextPageToken = data.nextPageToken;

      ++i;
    }

    return { result: { items: result } };
  }
}

module.exports = Model;
