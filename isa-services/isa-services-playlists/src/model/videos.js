class Model {
  constructor(client, apiKey) {
    this.client = client;
    this.apiKey = apiKey;
  }

  async getVideo({ referenceId }) {
    const params = { id: referenceId, part: 'snippet', key: this.apiKey };

    const { data } = await this.client.get('/videos', { params });

    const result = {
      referenceId,
      title: data.items[0].snippet.title,
      description: data.items[0].snippet.description,
    };

    return { result };
  }
}

module.exports = Model;
