const model = require('../model');

module.exports = (client, apiKey, hepius) => {
  const playlists = new model.Playlists(client, apiKey);

  return {
    ...hepius.unary('getPlaylist', ({ request }) =>
      playlists.getPlaylist(request)
    ),

    ...hepius.unary('getPlaylists', ({ request }) =>
      playlists.getPlaylists(request)
    ),
  };
};
