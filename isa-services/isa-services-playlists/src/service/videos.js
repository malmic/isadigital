const model = require('../model');

module.exports = (client, apiKey, hepius) => {
  const videos = new model.Videos(client, apiKey);

  return {
    ...hepius.unary('getVideo', ({ request }) => videos.getVideo(request)),
  };
};
