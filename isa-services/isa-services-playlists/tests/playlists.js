const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');
const mock = require('../mock');

test.beforeEach('instantiate service', async t => {
  const playlists = await service.playlists(
    new mock.Axios(),
    'KEY',
    new Hepius()
  );

  Bluebird.promisifyAll(playlists);

  t.context.playlists = playlists;
});

test.serial('playlists', async t => {
  const { playlists } = t.context;

  const { result } = await playlists.getPlaylistsAsync({ request: {} });

  t.is(result.length, 1);
  t.is(result[0].title, 'POP Music Playlist 2020');
  t.is(result[0].referenceId, 'PL4o29bINVT4EG_y-k5jGoOu3-Am8Nvi10');

  const { result: playlist } = await playlists.getPlaylistAsync({
    request: {
      referenceId: 'PL4o29bINVT4EG_y-k5jGoOu3-Am8Nvi10',
    },
  });

  t.is(playlist.items.length, 1);
  t.is(playlist.items[0].videoId, 'SlPhMPnQ58k');
});
