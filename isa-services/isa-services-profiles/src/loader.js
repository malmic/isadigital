const service = require('./service');

const typesData = require('../data/types.json');

module.exports = async (db, env, hepius) => {
  const Profiles = await service.profiles(db, hepius);
  const ProfileTypes = service.types(typesData, hepius);

  return { Profiles, ProfileTypes };
};
