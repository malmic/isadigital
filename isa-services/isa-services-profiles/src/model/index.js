const Profiles = require('./profiles');
const Types = require('./types');

module.exports = { Profiles, Types };
