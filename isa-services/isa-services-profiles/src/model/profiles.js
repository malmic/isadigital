const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;

    this.project = {
      firstName: 1,
      lastName: 1,
      foreign: 1,
      typeHandle: 1,
      referenceId: 1,
      description: 1,
      instrument: 1,
      university: 1,
      fullName: { $concat: ['$firstName', ' ', '$lastName'] },
    };
  }

  static async createInstance(db) {
    const collection = await db.createCollection('profiles', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'foreign',
            'firstName',
            'lastName',
            'typeHandle',
            'referenceId',
          ],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            firstName: { bsonType: 'string' },
            lastName: { bsonType: 'string' },
            description: { bsonType: 'string' },
            instrument: { bsonType: 'string' },
            university: { bsonType: 'string' },
            avatarUrl: { bsonType: 'string' },
            typeHandle: { bsonType: 'string' },
            referenceId: { bsonType: 'string' },
            bornAt: { bsonType: 'date' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createProfile({ bornAt, ...profile }) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      ...profile,
      referenceId,
      bornAt: new Date(bornAt),
    });

    return { result: referenceId };
  }

  async getProfiles({ typeHandle, foreign }) {
    let $match = {};
    if (typeHandle) $match.typeHandle = typeHandle;
    if (foreign && foreign.length > 0)
      $match.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const pipeline = [
      { $match },
      { $sort: { firstName: 1 } },
      { $project: this.project },
    ];

    const result = await this.collection.aggregate(pipeline).toArray();

    return { result };
  }

  async findProfiles({ search }) {
    if (!search) return { result: [] };

    const pipeline = [
      { $project: this.project },
      { $match: { fullName: new RegExp(search, 'i') } },
      { $sort: { fullName: 1 } },
      { $project: this.project },
    ];

    const result = await this.collection.aggregate(pipeline).toArray();

    return { result };
  }

  async getProfile({ referenceId, foreign, firstName, lastName }) {
    let $match = {};
    if (referenceId) $match.referenceId = referenceId;
    if (firstName) $match.firstName = firstName;
    if (lastName) $match.lastName = lastName;
    if (foreign && foreign.length > 0)
      $match.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const pipeline = [{ $match }, { $limit: 1 }, { $project: this.project }];

    const result = await this.collection.aggregate(pipeline).toArray();

    return { result: result.length === 1 ? result[0] : null };
  }

  async updateProfile({ referenceId, foreign }) {
    let $set = { foreign };

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
