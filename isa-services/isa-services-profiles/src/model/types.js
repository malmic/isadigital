class Types {
  constructor(data) {
    this.data = data;
  }

  getProfileTypes() {
    return { result: this.data };
  }
}

module.exports = Types;
