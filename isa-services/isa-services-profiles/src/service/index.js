const profiles = require('./profiles');
const types = require('./types');

module.exports = { profiles, types };
