const model = require('../model');

module.exports = async (db, hepius) => {
  const profiles = await model.Profiles.createInstance(db);

  return {
    ...hepius.unary('createProfile', ({ request }) =>
      profiles.createProfile(request)
    ),

    ...hepius.unary('getProfile', ({ request }) =>
      profiles.getProfile(request)
    ),

    ...hepius.unary('getProfiles', ({ request }) =>
      profiles.getProfiles(request)
    ),

    ...hepius.unary('updateProfile', ({ request }) =>
      profiles.updateProfile(request)
    ),

    ...hepius.unary('findProfiles', ({ request }) =>
      profiles.findProfiles(request)
    ),
  };
};
