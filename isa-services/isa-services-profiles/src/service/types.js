const model = require('../model');

module.exports = (data, hepius) => {
  const types = new model.Types(data);

  return {
    ...hepius.unary('getProfileTypes', ({ request }) =>
      types.getProfileTypes(request)
    ),
  };
};
