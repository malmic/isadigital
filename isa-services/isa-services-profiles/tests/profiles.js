const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Profiles: profiles } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(profiles);

  t.context.profiles = profiles;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('profiles', async t => {
  const { profiles } = t.context;

  await profiles.createProfileAsync({
    request: {
      foreign: [{ id: 'b8m33ymkqkelfte6x', typeHandle: 'USER' }],
      firstName: 'Albena',
      lastName: 'Danailova',
      // instrument: 'VIOLIN',
      // university: 'VIENNA',
      typeHandle: 'PROFESSOR',
    },
  });

  await profiles.createProfileAsync({
    request: {
      foreign: [{ id: 'b8m34ymkqkelfte6x', typeHandle: 'USER' }],
      firstName: 'Mitch',
      lastName: 'Grinch',
      instrument: 'VIOLIN',
      university: 'VIENNA',
      typeHandle: 'STUDENT',
    },
  });

  const { result: allProfiles } = await profiles.getProfilesAsync({
    request: {},
  });

  t.is(allProfiles.length, 2);

  const { result: profilesByType } = await profiles.getProfilesAsync({
    request: {
      typeHandle: 'STUDENT',
    },
  });

  t.is(profilesByType.length, 1);
  t.is(profilesByType[0].typeHandle, 'STUDENT');

  const { result: profilesByForeign } = await profiles.getProfilesAsync({
    request: {
      foreign: [{ id: 'b8m34ymkqkelfte6x', typeHandle: 'USER' }],
    },
  });

  t.is(profilesByForeign.length, 1);
  t.is(profilesByForeign[0].typeHandle, 'STUDENT');

  const { result: foundProfiles } = await profiles.findProfilesAsync({
    request: {
      search: 'Mitch',
    },
  });

  t.is(foundProfiles.length, 1);
  t.is(foundProfiles[0].firstName, 'Mitch');

  const { result: emptyFoundProfiles } = await profiles.findProfilesAsync({
    request: {},
  });

  t.is(emptyFoundProfiles.length, 0);
});

test.serial('profile', async t => {
  const { profiles } = t.context;

  const { result: emptyProfile } = await profiles.getProfileAsync({
    request: {},
  });

  t.is(emptyProfile, null);

  const { result: referenceId } = await profiles.createProfileAsync({
    request: {
      foreign: [{ id: 'b8m33ymkqkelfte6x', typeHandle: 'USER' }],
      firstName: 'Albena',
      lastName: 'Danailova',
      instrument: 'VIOLIN',
      university: 'VIENNA',
      typeHandle: 'PROFESSOR',
    },
  });

  const { result: profile } = await profiles.getProfileAsync({
    request: { referenceId },
  });

  t.is(profile.firstName, 'Albena');

  const { result: updated } = await profiles.updateProfileAsync({
    request: {
      referenceId,
      foreign: [
        { id: 'b8m33ymkqkelfte6x', typeHandle: 'USER' },
        { id: 'new', typeHandle: 'USER' },
      ],
    },
  });

  t.true(updated);

  const { result: profileByForeign } = await profiles.getProfileAsync({
    request: {
      foreign: [{ id: 'b8m33ymkqkelfte6x', typeHandle: 'USER' }],
    },
  });

  t.is(profileByForeign.foreign.length, 2);
});
