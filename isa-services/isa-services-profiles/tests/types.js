const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', async t => {
  const profileTypes = service.types(
    [
      { name: 'Student', handle: 'STUDENT' },
      { name: 'Sponsor', handle: 'SPONSOR' },
    ],
    new Hepius()
  );

  Bluebird.promisifyAll(profileTypes);

  t.context.profileTypes = profileTypes;
});

test('profile types', async t => {
  const { profileTypes } = t.context;

  const { result: allProfileTypes } = await profileTypes.getProfileTypesAsync(
    {}
  );
  t.is(allProfileTypes.length, 2);
});
