const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Rooms = await service(db, hepius);

  return { Rooms };
};
