const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('rooms', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'description',
            'name',
            'backgroundImage',
            'backgroundColor',
            'referenceId',
            'default',
            'position',
          ],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            backgroundImage: { bsonType: 'string' },
            backgroundColor: { bsonType: 'string' },
            name: { bsonType: 'string' },
            default: { bsonType: 'bool' },
            referenceId: { bsonType: 'string' },
            description: { bsonType: 'string' },
            position: { bsonType: 'int' },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createRoom(room) {
    const referenceId = uniqid();

    await this.collection.insertOne({ referenceId, ...room });

    return { result: referenceId };
  }

  async getRooms() {
    const result = await this.collection
      .find({}, { sort: { position: 1 } })
      .toArray();

    return { result };
  }

  async getRoom({ referenceId }) {
    const result = await this.collection.findOne({ referenceId });

    return { result };
  }

  async deleteRoom({ referenceId }) {
    const result = await this.collection.deleteOne({ referenceId });

    return { result: result.result.ok === 1 };
  }

  async updateRoom({ referenceId, ...room }) {
    const $set = {};
    if (room.name) $set.name = room.name;
    if (room.description) $set.description = room.description;
    if (room.position) $set.position = room.position;
    if (room.backgroundColor) $set.backgroundColor = room.backgroundColor;
    if (room.backgroundImage) $set.backgroundImage = room.backgroundImage;

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
