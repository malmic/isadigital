const Model = require('./model');

module.exports = async (db, hepius) => {
  const rooms = await Model.createInstance(db);

  return {
    ...hepius.unary('createRoom', ({ request }) => rooms.createRoom(request)),

    ...hepius.unary('getRoom', ({ request }) => rooms.getRoom(request)),

    ...hepius.unary('getRooms', ({ request }) => rooms.getRooms(request)),

    ...hepius.unary('deleteRoom', ({ request }) => rooms.deleteRoom(request)),

    ...hepius.unary('updateRoom', ({ request }) => rooms.updateRoom(request)),
  };
};
