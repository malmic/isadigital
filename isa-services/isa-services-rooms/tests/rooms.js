const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Rooms: rooms } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(rooms);

  t.context.rooms = rooms;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('room', async t => {
  const { rooms } = t.context;

  const { result: referenceId } = await rooms.createRoomAsync({
    request: {
      name: 'Room',
      description: 'Room',
      position: 1,
      backgroundImage: 'test.png',
      backgroundColor: '#ffffff',
      default: false,
    },
  });

  const { result: room } = await rooms.getRoomAsync({
    request: { referenceId },
  });

  t.is(room.name, 'Room');
  t.is(room.description, 'Room');

  await rooms.updateRoomAsync({
    request: { referenceId, description: 'weeee', name: 'test' },
  });

  const { result: updatedRoom } = await rooms.getRoomAsync({
    request: { referenceId },
  });

  t.is(updatedRoom.name, 'test');
  t.is(updatedRoom.description, 'weeee');

  const { result: deleted } = await rooms.deleteRoomAsync({
    request: { referenceId },
  });

  t.true(deleted);

  const { result: deletedRoom } = await rooms.getRoomAsync({
    request: { referenceId },
  });

  t.is(deletedRoom, null);
});

test.serial('rooms', async t => {
  const { rooms } = t.context;

  const { result: referenceId } = await rooms.createRoomAsync({
    request: {
      name: 'Room',
      description: 'Room',
      position: 1,
      backgroundImage: 'test.png',
      backgroundColor: '#ffffff',
      default: false,
    },
  });

  const { result: room } = await rooms.getRoomAsync({
    request: { referenceId },
  });

  t.is(room.name, 'Room');

  const { result: allRooms } = await rooms.getRoomsAsync({
    request: {},
  });

  t.is(allRooms.length, 1);
  t.is(allRooms[0].name, 'Room');
});
