const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Sessions = await service(db, hepius);

  return {
    Sessions,
  };
};
