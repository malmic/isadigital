class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('sessions');

    return new Model(collection);
  }

  async get({ referenceId }) {
    const result = await this.collection.findOne({ referenceId });
    return { result };
  }

  async set({
    referenceId,
    accessToken,
    foreign,
    refreshToken,
    query,
    rolling,
    changed,
  }) {
    if (changed || rolling) {
      const record = {
        $set: {
          referenceId,
          accessToken,
          foreign,
          refreshToken,
          query,
          updatedAt: new Date(),
        },
      };

      await this.collection.findOneAndUpdate({ referenceId }, record, {
        upsert: true,
      });
    }
    return { result: { accessToken } };
  }

  async destroy({ referenceId }) {
    const result = await this.collection.removeOne({ referenceId });

    return { result };
  }
}

module.exports = Model;
