const Model = require('./model');

module.exports = async (db, hepius) => {
  const sessions = await Model.createInstance(db);

  return {
    ...hepius.unary('getSession', ({ request }) => sessions.get(request)),

    ...hepius.unary('updateSession', ({ request }) => sessions.set(request)),

    ...hepius.unary('deleteSession', ({ request }) =>
      sessions.destroy(request)
    ),
  };
};
