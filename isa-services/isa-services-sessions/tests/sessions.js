const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Sessions: sessions } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(sessions);

  t.context.sessions = sessions;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('sessions', async t => {
  /* const { referrals } = t.context;
  const referred0 = { id: 'alpha', typeHandle: 'USER' };
  const referred1 = { id: 'bravo', typeHandle: 'USER' };
  const referred2 = { id: 'covaks', typeHandle: 'USER' };
  const referred3 = { id: 'diego', typeHandle: 'USER' };
  const referred4 = { id: 'echo', typeHandle: 'USER' };

  // root
  const { result: referred0ReferenceId } = await referrals.createReferralAsync({
    request: {
      foreign: referred0,
    },
  });

  // referred0 -> referred4
  const { result: referred4ReferenceId } = await referrals.createReferralAsync({
    request: {
      parent: referred0ReferenceId,
      foreign: referred4,
    },
  });

  // referred0 -> referred1
  const { result: referred1ReferenceId } = await referrals.createReferralAsync({
    request: {
      parent: referred0ReferenceId,
      foreign: referred1,
    },
  });

  // referred1 -> referred2
  const { result: referred2ReferenceId } = await referrals.createReferralAsync({
    request: {
      parent: referred1ReferenceId,
      foreign: referred2,
    },
  });

  // referred2 -> referred3
  await referrals.createReferralAsync({
    request: {
      parent: referred2ReferenceId,
      foreign: referred3,
    },
  });

  const { result: lastReferral } = await referrals.getReferralAsync({
    request: { foreign: referred3 },
  });

  t.is(lastReferral.parent, referred2ReferenceId);
  t.is(lastReferral.foreign.id, referred3.id);

  const { result: secondReferral } = await referrals.getReferralAsync({
    request: { foreign: referred2 },
  });

  t.is(secondReferral.parent, referred1ReferenceId);

  const { result: firstReferral } = await referrals.getReferralAsync({
    request: { foreign: referred1 },
  });

  t.is(firstReferral.parent, referred0ReferenceId);

  const { result: referral0References } = await referrals.getReferredAsync({
    request: {
      foreign: referred0,
      maxDepth: 1,
    },
  });

  t.is(referral0References.length, 3);
  t.is(referral0References[0].referenceId, referred0ReferenceId);
  t.is(referral0References[0].foreign.id, referred0.id);
  t.is(referral0References[0].depth, 0);
  t.is(referral0References[1].referenceId, firstReferral.referenceId);
  t.is(referral0References[1].foreign.id, referred1.id);
  t.is(referral0References[1].depth, 1);
  t.is(referral0References[2].referenceId, referred4ReferenceId);
  t.is(referral0References[2].foreign.id, referred4.id);
  t.is(referral0References[2].depth, 1);

  const { result: referrals3 } = await referrals.getReferralsAsync({
    request: {
      foreign: referred3,
      maxDepth: 10,
    },
  });

  t.is(referrals3.length, 3);
  t.is(referrals3[0].foreign.id, referred2.id);
  t.is(referrals3[0].depth, 0);
  t.is(referrals3[1].foreign.id, referred1.id);
  t.is(referrals3[1].depth, 1);
  t.is(referrals3[2].foreign.id, referred0.id);
  t.is(referrals3[2].depth, 2);

  const { result: invalidReferred } = await referrals.getReferralsAsync({
    request: {
      foreign: { id: 'invalid', typeHandle: 'invalid' },
      maxDepth: 1000,
    },
  });

  t.is(invalidReferred.length, 0);

  t.pass(); */
});
