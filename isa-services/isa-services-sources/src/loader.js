const service = require('./service');

const typesData = require('../data/types');

module.exports = async (db, env, hepius) => {
  const SourceTypes = service.types(typesData, hepius);
  const Sources = await service.sources(db, hepius);

  return { SourceTypes, Sources };
};
