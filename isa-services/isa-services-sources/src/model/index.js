const Sources = require('./sources');
const Types = require('./types');

module.exports = { Sources, Types };
