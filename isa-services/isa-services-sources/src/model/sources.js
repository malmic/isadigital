class Sources {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('sources', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'typeHandle', 'referenceId', 'title'],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            typeHandle: { bsonType: 'string' },
            statusHandle: { bsonType: 'string' },
            referenceId: { bsonType: 'string' },
            title: { bsonType: 'string' },
            description: { bsonType: 'string' },
          },
        },
      },
    });

    await collection.createIndex({ referenceId: 1 }, { unique: true });

    return new Sources(collection);
  }

  async createSource(source) {
    const result = await this.collection.insertOne(source);

    return { result: result.result.ok === 1 };
  }

  async getSources({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection
      .find(query, { sort: { referenceId: 1 } })
      .toArray();

    return { result };
  }

  async deleteSource({ referenceId, foreign }) {
    const query = {};
    if (referenceId) query.referenceId = referenceId;
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.deleteOne(query);

    return { result: result.result.ok === 1 };
  }

  async getSource({ referenceId, foreign }) {
    const query = {};
    if (referenceId) query.referenceId = referenceId;
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.findOne(query);

    return { result };
  }

  async updateSource({
    referenceId,
    foreign,
    statusHandle,
    title,
    description,
  }) {
    let $set = {};
    if (foreign && foreign.length > 0) $set.foreign = foreign;
    if (statusHandle) $set.statusHandle = statusHandle;
    if (title) $set.title = title;
    if (description) $set.description = description;

    const result = await this.collection.updateOne({ referenceId }, { $set });

    return { result: result.result.ok === 1 };
  }
}

module.exports = Sources;
