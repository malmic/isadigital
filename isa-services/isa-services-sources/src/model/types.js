class Types {
  constructor(data) {
    this.data = data;
  }

  getSourceTypes() {
    return { result: this.data };
  }
}

module.exports = Types;
