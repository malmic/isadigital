const sources = require('./sources');
const types = require('./types');

module.exports = {
  sources,
  types,
};
