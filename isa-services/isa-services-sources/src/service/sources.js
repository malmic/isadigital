const model = require('../model');

module.exports = async (db, hepius) => {
  const sources = await model.Sources.createInstance(db);

  return {
    ...hepius.unary('createSource', ({ request }) =>
      sources.createSource(request)
    ),

    ...hepius.unary('getSource', ({ request }) => sources.getSource(request)),

    ...hepius.unary('deleteSource', ({ request }) =>
      sources.deleteSource(request)
    ),

    ...hepius.unary('updateSource', ({ request }) =>
      sources.updateSource(request)
    ),

    ...hepius.unary('getSources', ({ request }) => sources.getSources(request)),
  };
};
