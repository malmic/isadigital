const model = require('../model');

module.exports = (data, hepius) => {
  const sourceTypes = new model.Types(data);

  return {
    ...hepius.unary('getSourceTypes', () => sourceTypes.getSourceTypes()),
  };
};
