const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Sources: sources } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(sources);

  t.context.sources = sources;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('source', async t => {
  const { sources } = t.context;

  await sources.createSourceAsync({
    request: {
      typeHandle: 'TEST',
      referenceId: '12345',
      title: 'abc',
      foreign: [{ id: '2', typeHandle: 'A' }],
    },
  });

  const { result } = await sources.createSourceAsync({
    request: {
      typeHandle: 'TEST',
      referenceId: '123',
      title: 'abc',
      description: 'desc',
      foreign: [{ id: '1', typeHandle: 'A' }],
    },
  });

  t.true(result);

  const { result: source } = await sources.getSourceAsync({
    request: { referenceId: '123' },
  });

  t.is(source.typeHandle, 'TEST');

  const { result: updated } = await sources.updateSourceAsync({
    request: { referenceId: '123', title: 't', description: 'u' },
  });

  t.true(updated);

  const { result: updatedSource } = await sources.getSourceAsync({
    request: { referenceId: '123' },
  });

  t.is(updatedSource.title, 't');
  t.is(updatedSource.description, 'u');

  const { result: deleted } = await sources.deleteSourceAsync({
    request: { referenceId: '123' },
  });

  t.true(deleted);

  const { result: sourceByForeign } = await sources.getSourceAsync({
    request: { foreign: [{ id: '2', typeHandle: 'A' }] },
  });

  t.is(sourceByForeign.typeHandle, 'TEST');

  const { result: deletedByForeign } = await sources.deleteSourceAsync({
    request: { foreign: [{ id: '2', typeHandle: 'A' }] },
  });

  t.true(deletedByForeign);
});

test.serial('sources', async t => {
  const { sources } = t.context;

  await sources.createSourceAsync({
    request: {
      typeHandle: 'TEST',
      referenceId: '12345',
      title: 'abc',
      foreign: [{ id: '1', typeHandle: 'A' }],
    },
  });

  await sources.createSourceAsync({
    request: {
      typeHandle: 'TEST',
      referenceId: '123456',
      title: 'abc',
      foreign: [{ id: '2', typeHandle: 'A' }],
    },
  });

  const { result: foreignSources } = await sources.getSourcesAsync({
    request: { foreign: [{ id: '1', typeHandle: 'A' }] },
  });

  t.is(foreignSources.length, 1);

  const { result: allSources } = await sources.getSourcesAsync({
    request: {},
  });

  t.is(allSources.length, 2);
});
