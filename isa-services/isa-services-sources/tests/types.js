const test = require('ava');
const Bluebird = require('bluebird');
const Hepius = require('@isa-music/libs-hepius');

const service = require('../src/service');

test.beforeEach('instantiate service', async t => {
  const sourceTypes = service.types(
    [{ handle: 'HANDLE', name: 'name' }],
    new Hepius()
  );

  Bluebird.promisifyAll(sourceTypes);

  t.context.sourceTypes = sourceTypes;
});

test('source types', async t => {
  const { sourceTypes } = t.context;

  const { result: allSourceTypes } = await sourceTypes.getSourceTypesAsync({});
  t.is(allSourceTypes.length, 1);

  t.is(allSourceTypes[0].handle, 'HANDLE');
});
