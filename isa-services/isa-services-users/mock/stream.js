const EventEmitter = require('events');

class Stream extends EventEmitter {
  constructor(request) {
    super();

    this.data = [];
    this.request = request;
  }

  write(data) {
    this.data.push(data);
  }

  getData() {
    return this.data;
  }

  end() {}
}

module.exports = Stream;
