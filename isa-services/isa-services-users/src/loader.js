const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Users = await service(db, hepius);

  return { Users };
};
