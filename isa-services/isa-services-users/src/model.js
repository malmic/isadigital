const uniqid = require('uniqid');
const moment = require('moment');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('users', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: [
            'defaults',
            'languageHandle',
            'flags',
            'validScopes',
            'referenceId',
            'createdAt',
          ],
          properties: {
            defaults: {
              bsonType: 'array',
              items: {
                required: ['name', 'value'],
                properties: {
                  name: {
                    bsonType: 'string',
                  },
                  value: {
                    bsonType: 'string',
                  },
                },
              },
            },
            validScopes: {
              bsonType: 'array',
              items: {
                type: 'string',
              },
            },
            languageHandle: {
              bsonType: 'string',
            },
            flags: {
              bsonType: 'array',
              items: {
                required: ['name', 'value'],
                properties: {
                  name: {
                    bsonType: 'string',
                  },
                  value: {
                    bsonType: 'bool',
                  },
                },
              },
            },
            referenceId: {
              bsonType: 'string',
            },
            createdAt: {
              bsonType: 'date',
            },
          },
        },
      },
    });

    return new Model(collection);
  }

  async createUser(languageHandle, defaults, flags, createdAt, validScopes) {
    const referenceId = uniqid();

    await this.collection.insertOne({
      defaults: Object.keys(defaults).map(defaultName => {
        return { name: defaultName, value: defaults[defaultName] };
      }),
      languageHandle,
      flags: Object.keys(flags).map(flagName => {
        return { name: flagName, value: flags[flagName] };
      }),
      validScopes,
      referenceId,
      createdAt: createdAt || new Date(),
    });

    return referenceId;
  }

  async updateFlags(referenceId, flags) {
    const document = await this.collection.findOne({ referenceId });

    const result = await this.collection.bulkWrite(
      flags.map(flag => {
        const flagExists = document.flags.find(f => f.name === flag.name);
        if (flagExists) {
          return {
            updateOne: {
              filter: { referenceId, 'flags.name': flag.name },
              update: {
                $set: { 'flags.$.value': !!flag.value },
              },
            },
          };
        } else {
          return {
            updateOne: {
              filter: { referenceId },
              update: {
                $push: { flags: { name: flag.name, value: !!flag.value } },
              },
            },
          };
        }
      })
    );

    return result.modifiedCount === flags.length;
  }

  async updateDefault(referenceId, name, value) {
    const document = await this.collection.findOne({ referenceId });
    const defaultExists = document.defaults.find(d => d.name === name);

    const query = { referenceId };
    const update = {};

    if (defaultExists) {
      query['defaults.name'] = name;
      update['$set'] = { 'defaults.$.value': value };
    } else {
      update['$push'] = { defaults: { name, value } };
    }

    const result = await this.collection.updateOne(query, update);

    return result.result.ok === 1;
  }

  async updateLanguage(referenceId, languageHandle) {
    const result = await this.collection.updateOne(
      { referenceId },
      { $set: { languageHandle } }
    );

    return result.result.ok === 1;
  }

  async getUser(referenceId) {
    const result = await this.collection
      .aggregate([
        {
          $match: {
            referenceId,
          },
        },
        {
          $project: {
            defaults: {
              $arrayToObject: {
                $map: {
                  input: '$defaults',
                  as: 'default',
                  in: { k: '$$default.name', v: '$$default.value' },
                },
              },
            },
            languageHandle: 1,
            flags: {
              $arrayToObject: {
                $map: {
                  input: '$flags',
                  as: 'flag',
                  in: { k: '$$flag.name', v: '$$flag.value' },
                },
              },
            },
            createdAt: 1,
            validScopes: 1,
            referenceId: 1,
          },
        },
      ])
      .toArray();

    return result.length ? result[0] : null;
  }

  async getUsers({ referenceId, pagination, sorting }) {
    const $match = {};

    if (referenceId) $match.referenceId = referenceId;

    const field = sorting ? sorting.field : 'createdAt';
    const direction = sorting ? sorting.direction : -1;

    const pipeline = [
      { $match },
      {
        $sort: { [field]: direction },
      },
      {
        $project: {
          defaults: {
            $arrayToObject: {
              $map: {
                input: '$defaults',
                as: 'default',
                in: { k: '$$default.name', v: '$$default.value' },
              },
            },
          },
          flags: {
            $arrayToObject: {
              $map: {
                input: '$flags',
                as: 'flag',
                in: { k: '$$flag.name', v: '$$flag.value' },
              },
            },
          },
          createdAt: 1,
          validScopes: 1,
          referenceId: 1,
        },
      },
    ];

    if (pagination) {
      pipeline.push({ $limit: pagination.limit });
      pipeline.push({ $skip: (pagination.page - 1) * pagination.limit });
    }

    const count = await this.collection.countDocuments({});
    const cursor = this.collection.aggregate(pipeline);

    return {
      cursor,
      pagination: { count },
    };
  }

  async getUsersCreatedDaily(dateRange, sorting) {
    const $match = {};

    if (dateRange) {
      const { begin, end } = dateRange;

      if (begin) {
        $match.createdAt = {
          $gte: moment.utc(new Date(begin)).toDate(),
        };
      }

      if (end) {
        if (!$match.createdAt) $match.createdAt = {};
        $match.createdAt.$lte = moment.utc(new Date(end)).toDate();
      }
    }

    const dimensions = [
      {
        axis: 'x',
        typeHandle: 'date',
        value: 'Date',
      },
      {
        axis: 'y',
        typeHandle: 'int',
        value: 'Users',
      },
      {
        axis: 'a',
        typeHandle: 'double',
        value: 'Avg.',
      },
      {
        axis: 'b',
        typeHandle: 'double',
        value: 'Min.',
      },
      {
        axis: 'c',
        typeHandle: 'double',
        value: 'Max.',
      },
    ];

    const pipeline = [{ $match }];

    const vectorsFacet = [
      {
        $group: {
          _id: {
            month: { $month: '$createdAt' },
            day: { $dayOfMonth: '$createdAt' },
            year: { $year: '$createdAt' },
          },
          createdAt: { $first: '$createdAt' },
          total: { $sum: 1 },
        },
      },
      {
        $group: {
          _id: '$_id',
          createdAt: { $first: '$createdAt' },
          avg: { $avg: '$total' },
          min: { $min: '$total' },
          max: { $max: '$total' },
          total: { $first: '$total' },
        },
      },
      {
        $sort: {
          '_id.year': sorting ? sorting.direction : -1,
          '_id.month': sorting ? sorting.direction : -1,
          '_id.day': sorting ? sorting.direction : -1,
        },
      },
      {
        $project: {
          createdAt: 1,
          total: 1,
          avg: 1,
          min: 1,
          max: 1,
        },
      },
      {
        $project: {
          _id: 1,
          dimensions: [
            {
              k: 'x',
              v: '$createdAt',
            },
            {
              k: 'y',
              v: '$total',
            },
            {
              k: 'a',
              v: '$avg',
            },
            {
              k: 'b',
              v: '$min',
            },
            {
              k: 'c',
              v: '$max',
            },
          ],
        },
      },
      {
        $project: {
          _id: 0,
          vector: {
            $arrayToObject: '$dimensions',
          },
        },
      },
    ];

    const totalsFacet = [
      {
        $group: {
          _id: {
            month: { $month: '$createdAt' },
            day: { $dayOfMonth: '$createdAt' },
            year: { $year: '$createdAt' },
          },
          createdAt: { $first: '$createdAt' },
          total: { $sum: 1 },
        },
      },
      {
        $group: {
          _id: null,
          avg: { $avg: '$total' },
          min: { $min: '$total' },
          max: { $max: '$total' },
          createdAt: { $first: '$createdAt' },
          total: { $sum: '$total' },
        },
      },
      {
        $project: {
          _id: 1,
          dimensions: [
            {
              k: 'x',
              v: '$createdAt',
            },
            {
              k: 'y',
              v: '$total',
            },
            {
              k: 'a',
              v: '$avg',
            },
            {
              k: 'b',
              v: '$min',
            },
            {
              k: 'c',
              v: '$max',
            },
          ],
        },
      },
      {
        $project: {
          _id: 0,
          vector: {
            $arrayToObject: '$dimensions',
          },
        },
      },
    ];

    pipeline.push({
      $facet: {
        vectors: vectorsFacet,
        totals: totalsFacet,
      },
    });

    const result = await this.collection.aggregate(pipeline).toArray();

    return [
      {
        vectors: result[0].vectors.length ? result[0].vectors : [],
        totals: result[0].totals.length ? result[0].totals : [],
        dimensions,
      },
    ];
  }
}

module.exports = Model;
