const Model = require('./model');

module.exports = async (db, hepius) => {
  const users = await Model.createInstance(db);

  return {
    ...hepius.unary('createUser', async call => {
      const { request } = call;
      const {
        languageHandle,
        defaults,
        flags,
        createdAt,
        validScopes,
      } = request;

      const result = await users.createUser(
        languageHandle,
        defaults || {},
        flags || {},
        createdAt,
        validScopes
      );

      return { result };
    }),

    ...hepius.unary('updateLanguage', async call => {
      const { request } = call;
      const { referenceId, languageHandle } = request;

      const result = await users.updateLanguage(referenceId, languageHandle);

      return { result };
    }),

    ...hepius.unary('updateFlags', async call => {
      const { request } = call;
      const { referenceId, flags } = request;

      const result = await users.updateFlags(referenceId, flags);

      return { result };
    }),

    ...hepius.unary('getUsersCreatedDaily', async ({ request }) => {
      const { dateRange, sorting } = request;

      const result = await users.getUsersCreatedDaily(dateRange, sorting);

      return {
        result: result[0],
      };
    }),

    ...hepius.unary('getUser', async call => {
      const { request } = call;
      const { referenceId } = request;

      const result = await users.getUser(referenceId);

      return { result };
    }),

    ...hepius.unary('getUsers', async ({ request }) => {
      const { cursor, pagination } = await users.getUsers(request);

      return {
        result: await cursor.toArray(),
        pagination,
      };
    }),

    ...hepius.stream('streamUsers', async call => {
      const { request } = call;

      const { cursor } = await users.getUsers(request);

      while (await cursor.hasNext()) {
        const result = await cursor.next();
        call.write({ result });
      }

      call.end();
    }),

    ...hepius.unary('updateDefault', async call => {
      const { request } = call;
      const { referenceId, name, value } = request;

      const result = await users.updateDefault(referenceId, name, value);

      return { result };
    }),
  };
};
