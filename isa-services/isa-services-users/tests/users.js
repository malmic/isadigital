const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const moment = require('moment');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mock = require('../mock');
const loader = require('../src');

const mongod = new MongoMemoryServer();

const delay = duration => {
  return new Promise(resolve => {
    return setTimeout(resolve, duration);
  });
};

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Users: users } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(users);

  t.context.users = users;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('users', async t => {
  const { users } = t.context;

  const { result: referenceId } = await users.createUserAsync({
    request: {
      languageHandle: 'EN',
      defaults: { currencyHandle: 'EUR' },
      flags: {},
      validScopes: ['scope'],
    },
  });

  const { result: updateLanguageResult } = await users.updateLanguageAsync({
    request: {
      referenceId,
      languageHandle: 'DE',
    },
  });

  t.true(updateLanguageResult);

  const { result: updateDefaultResult } = await users.updateDefaultAsync({
    request: { referenceId, name: 'currencyHandle', value: 'EURCENT' },
  });

  t.true(updateDefaultResult);

  const { result: user } = await users.getUserAsync({
    request: { referenceId },
  });

  t.is(user.languageHandle, 'DE');
  t.is(user.defaults.currencyHandle, 'EURCENT');
  t.is(user.validScopes.length, 1);
  t.is(user.validScopes[0], 'scope');

  const { result: allUsers } = await users.getUsersAsync({ request: {} });
  t.is(allUsers.length, 1);

  const { result: paginatedUsers } = await users.getUsersAsync({
    request: {
      pagination: {
        limit: 1,
        page: 2,
      },
      sorting: {
        field: 'createdAt',
        direction: 1,
      },
    },
  });

  t.is(paginatedUsers.length, 0);

  const { result: singleUser } = await users.getUsersAsync({
    request: {
      referenceId,
    },
  });

  t.is(singleUser[0].referenceId, referenceId);

  const { result: nullUser } = await users.getUserAsync({ request: {} });
  t.is(nullUser, null);

  const stream = new mock.Stream({});
  users.streamUsers(stream);

  await delay(100);

  const streamedUsers = stream.getData();
  t.is(streamedUsers.length, 1);
});

test.serial('aggregation', async t => {
  const { users } = t.context;

  await users.createUserAsync({
    request: {
      languageHandle: 'EN',
      defaults: { currencyHandle: 'EUR' },
      flags: {},
      createdAt: moment({ year: 2018, month: 1, day: 2 }).toDate(),
      validScopes: ['scope'],
    },
  });

  await users.createUserAsync({
    request: {
      languageHandle: 'EN',
      defaults: { currencyHandle: 'EUR' },
      flags: {},
      createdAt: moment({ year: 2018, month: 1, day: 3 }).toDate(),
      validScopes: ['scope'],
    },
  });

  const { result: allUsers } = await users.getUsersCreatedDailyAsync({
    request: {},
  });

  const { dimensions, vectors, totals } = allUsers;

  t.is(dimensions.length, 5);
  t.is(dimensions[0].axis, 'x');
  t.is(dimensions[0].typeHandle, 'date');
  t.is(dimensions[1].axis, 'y');
  t.is(dimensions[1].typeHandle, 'int');
  t.is(dimensions[2].axis, 'a');
  t.is(dimensions[2].typeHandle, 'double');
  t.is(dimensions[3].axis, 'b');
  t.is(dimensions[3].typeHandle, 'double');
  t.is(dimensions[4].axis, 'c');
  t.is(dimensions[3].typeHandle, 'double');

  t.is(vectors.length, 2);
  t.is(
    moment(new Date(vectors[0].vector.x)).format('YYYY.MM.DD'),
    '2018.02.03'
  );
  t.is(vectors[0].vector.y, 1);
  t.is(
    moment(new Date(vectors[1].vector.x)).format('YYYY.MM.DD'),
    '2018.02.02'
  );
  t.is(vectors[1].vector.y, 1);

  t.is(totals.length, 1);
  t.true(totals[0].vector.x instanceof Date);
  t.is(totals[0].vector.y, 2);
  t.is(totals[0].vector.a, 1);
  t.is(totals[0].vector.b, 1);
  t.is(totals[0].vector.c, 1);

  const { result: endUsers } = await users.getUsersCreatedDailyAsync({
    request: {
      dateRange: {
        end: moment({ year: 2018, month: 1, day: 2 }).toDate(),
      },
    },
  });

  t.is(
    endUsers.vectors[0].vector.x.toString(),
    moment({ year: 2018, month: 1, day: 2 })
      .toDate()
      .toString()
  );

  const { result: beginUsers } = await users.getUsersCreatedDailyAsync({
    request: {
      dateRange: {
        begin: moment({ year: 2018, month: 1, day: 3 }).toDate(),
      },
    },
  });

  t.is(
    beginUsers.vectors[0].vector.x.toString(),
    moment({ year: 2018, month: 1, day: 3 })
      .toDate()
      .toString()
  );

  const { result: sortedUsers } = await users.getUsersCreatedDailyAsync({
    request: {
      sorting: {
        direction: 1,
      },
    },
  });

  t.is(
    sortedUsers.vectors[0].vector.x.toString(),
    moment({ year: 2018, month: 1, day: 2 })
      .toDate()
      .toString()
  );

  const { result: emptyUsers } = await users.getUsersCreatedDailyAsync({
    request: {
      dateRange: {
        begin: moment({ year: 2019, month: 1, day: 3 }).toDate(),
      },
    },
  });

  t.is(emptyUsers.vectors.length, 0);

  const { result: beginEndUsers } = await users.getUsersCreatedDailyAsync({
    request: {
      dateRange: {
        begin: moment({ year: 2018, month: 1, day: 2 }).toDate(),
        end: moment({ year: 2018, month: 1, day: 3 }).toDate(),
      },
    },
  });

  t.is(beginEndUsers.vectors.length, 2);
});

test.serial('update defaults', async t => {
  const { users } = t.context;

  const { result: referenceId } = await users.createUserAsync({
    request: { languageHandle: 'EN', validScopes: [] },
  });

  const { result: user } = await users.getUserAsync({
    request: { referenceId },
  });

  t.is(Object.keys(user.defaults).length, 0);

  const { result: updated } = await users.updateDefaultAsync({
    request: {
      referenceId,
      name: 'key',
      value: 'value',
    },
  });

  t.true(updated);

  const { result: updatedUser } = await users.getUserAsync({
    request: { referenceId },
  });

  t.is(updatedUser.defaults.key, 'value');

  const { result: updatedAgain } = await users.updateDefaultAsync({
    request: {
      referenceId,
      name: 'key',
      value: 'value2',
    },
  });

  t.true(updatedAgain);

  const { result: updatedUserAgain } = await users.getUserAsync({
    request: { referenceId },
  });

  t.is(updatedUserAgain.defaults.key, 'value2');
});

test.serial('update flags', async t => {
  const { users } = t.context;

  const { result: referenceId } = await users.createUserAsync({
    request: { languageHandle: 'EN', flags: { c: false }, validScopes: [] },
  });

  const { result: updated } = await users.updateFlagsAsync({
    request: {
      referenceId,
      flags: [
        {
          name: 'a',
        },
        {
          name: 'b',
          value: true,
        },
        {
          name: 'c',
          value: true,
        },
      ],
    },
  });

  t.true(updated);

  const { result: user } = await users.getUserAsync({
    request: { referenceId },
  });

  t.is(user.flags.a, false);
  t.is(user.flags.b, true);
  t.is(user.flags.c, true);
});
