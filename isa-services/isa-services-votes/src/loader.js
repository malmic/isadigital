const service = require('./service');

module.exports = async (db, env, hepius) => {
  const Votes = await service(db, hepius);

  return { Votes };
};
