const uniqid = require('uniqid');

class Model {
  constructor(collection) {
    this.collection = collection;
  }

  static async createInstance(db) {
    const collection = await db.createCollection('votes', {
      validationLevel: 'moderate',
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          required: ['foreign', 'referenceId'],
          properties: {
            foreign: {
              bsonType: 'array',
              items: {
                bsonType: 'object',
                properties: {
                  id: { bsonType: 'string' },
                  typeHandle: { bsonType: 'string' },
                },
              },
            },
            referenceId: { bsonType: 'string' },
          },
        },
      },
    });

    await collection.createIndex({ referenceId: 1 }, { unique: true });

    return new Model(collection);
  }

  async createVote({ ...vote }) {
    const referenceId = uniqid();

    await this.collection.insertOne({ referenceId, ...vote });

    return { result: referenceId };
  }

  async getVote({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.findOne(query);

    return { result };
  }

  async getVotes() {
    const result = await this.collection.find().toArray();

    return { result };
  }

  async deleteVotes({ foreign }) {
    const query = {};
    if (foreign && foreign.length > 0)
      query.foreign = { $all: foreign.map(f => ({ $elemMatch: f })) };

    const result = await this.collection.deleteMany(query);

    return { result: result.result.ok === 1 };
  }
}

module.exports = Model;
