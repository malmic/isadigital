const Model = require('./model');

module.exports = async (db, hepius) => {
  const votes = await Model.createInstance(db);

  return {
    ...hepius.unary('createVote', ({ request }) => votes.createVote(request)),

    ...hepius.unary('getVote', ({ request }) => votes.getVote(request)),

    ...hepius.unary('getVotes', ({ request }) => votes.getVotes(request)),

    ...hepius.unary('deleteVotes', ({ request }) => votes.deleteVotes(request)),
  };
};
