const test = require('ava');
const Bluebird = require('bluebird');
const { MongoClient } = require('mongodb');
const Hepius = require('@isa-music/libs-hepius');
const { MongoMemoryServer } = require('mongodb-memory-server');

const loader = require('../src');

const mongod = new MongoMemoryServer();

test.before(async t => {
  const uri = await mongod.getUri();
  const client = await MongoClient.connect(uri, { useUnifiedTopology: true });

  t.context.client = client;
  t.context.db = client.db();
});

test.beforeEach('instantiate service', async t => {
  const { db } = t.context;

  const collections = await db.listCollections().toArray();
  for (const collection of collections) {
    await db.collection(collection.name).deleteMany({});
  }

  const { Votes: votes } = await loader(db, null, new Hepius());

  Bluebird.promisifyAll(votes);

  t.context.votes = votes;
});

test.after.always('close connection', async t => {
  await t.context.db.dropDatabase();
  await t.context.client.close();
});

test.serial('votes', async t => {
  const { votes } = t.context;

  await votes.createVoteAsync({
    request: {
      foreign: [
        { id: '1', typeHandle: 'USER' },
        { id: '2', typeHandle: 'NOMINATION' },
      ],
    },
  });

  await votes.createVoteAsync({
    request: {
      foreign: [
        { id: '1', typeHandle: 'USER' },
        { id: '3', typeHandle: 'NOMINATION' },
      ],
    },
  });

  const { result: allVotes } = await votes.getVotesAsync({
    request: {},
  });

  t.is(allVotes.length, 2);

  const { result: deleted } = await votes.deleteVotesAsync({
    request: {
      foreign: { id: '1', typeHandle: 'USER' },
    },
  });

  t.true(deleted);

  const { result: emptyVotes } = await votes.getVotesAsync({ request: {} });

  t.is(emptyVotes.length, 0);
});

test.serial('vote', async t => {
  const { votes } = t.context;

  await votes.createVoteAsync({
    request: {
      foreign: [
        { id: '1', typeHandle: 'USER' },
        { id: '2', typeHandle: 'NOMINATION' },
      ],
    },
  });

  const { result: vote } = await votes.getVoteAsync({
    request: {
      foreign: [{ id: '2', typeHandle: 'NOMINATION' }],
    },
  });

  t.is(vote.foreign.length, 2);
});
