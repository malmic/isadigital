const grpc = require('@grpc/grpc-js');
const protoSource = require('@isa-music/protos-source');
const { generateServiceLoaders } = require('@isa-music/protos-loader');

module.exports = async (executable, env, onInit = () => {}) => {
  const serviceLoaders = generateServiceLoaders(
    protoSource,
    JSON.parse(env.SOCKET_ADDRESS_CLUSTERS),
    grpc
  );

  const restServerUrl = new URL(`http://${env.SOCKET_ADDRESS}`);
  const { hostname, port, href } = restServerUrl;

  const serverLoader = require(executable.path);
  const server = await serverLoader(serviceLoaders, env);
  server.listen(port, hostname, () => {
    console.log(`> server running on ${href}`);

    onInit();
  });
};
