const grpc = require('@grpc/grpc-js');
const protoSource = require('@isa-music/protos-source');
const {
  generateServiceLoaders,
  generateServer,
} = require('@isa-music/protos-loader');
const Hepius = require('@isa-music/libs-hepius');
const generateLogger = require('@isa-music/libs-logger');
const connections = require('@isa-music/libs-connections');
const { MongoClient } = require('mongodb');

const start = (server, socketAddress, credentials) =>
  new Promise((resolve, reject) => {
    server.bindAsync(socketAddress, credentials, (err, port) => {
      if (err) return reject(err);
      server.start();

      const serverUrl = new URL(`http://${socketAddress}`);
      const { href } = serverUrl;

      console.log(`> server running on ${href}`);

      resolve(port);
    });
  });

const onShutdown = (server, mongo) => {
  server.tryShutdown(() => {
    Promise.all([mongo.close()])
      .then(() => {
        console.log('> server successfully shutdown');
        return process.exit(0);
      })
      .catch(e => {
        console.error(e);
        process.exit(1);
      });
  });
};

module.exports = async (clusterName, env, executables, onInit = () => {}) => {
  const mongoConnection = new connections.Mongo(MongoClient);
  const logger = generateLogger(clusterName);
  const hepius = new Hepius(logger.error);

  const serviceLoaders = generateServiceLoaders(
    protoSource,
    JSON.parse(env.SOCKET_ADDRESS_CLUSTERS),
    grpc
  );

  const mongo = await mongoConnection.generateClient(
    {
      url: env.MONGO_URL,
      authSource: env.MONGO_AUTH_SOURCE,
      useUnifiedTopology: true,
    },
    { retries: 60, factor: 1.5, minTimeout: 1000, maxTimeout: 5000 }
  );

  const db = mongo.db();

  const serviceImplementations = await Promise.all(
    executables.map(async executable => {
      const serviceLoader = require(executable.path);
      const services = await serviceLoader(db, env, hepius, serviceLoaders);

      return { protoName: executable.protoName, services };
    })
  );

  const server = generateServer(grpc, protoSource, serviceImplementations);

  await start(
    server,
    env.SOCKET_ADDRESS,
    grpc.ServerCredentials.createInsecure()
  );

  onInit();

  for (const sig of ['SIGINT', 'SIGTERM', 'SIGQUIT']) {
    process.on(sig, () => {
      onShutdown(server, mongo);
    });
  }
};
