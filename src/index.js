const fs = require('fs-extra');
const path = require('path');
const yaml = require('js-yaml');

class Maker {
  constructor(outputDir, spawn) {
    this.outputDir = outputDir;
    this.spawn = spawn;
  }

  build(loaders = []) {
    return this.spawn('docker-compose', [
      '-f',
      path.join(this.outputDir, 'docker-compose.yaml'),
      'build',
    ]);
  }

  push(loaders = []) {
    return this.spawn('docker-compose', [
      '-f',
      path.join(this.outputDir, 'docker-compose.yaml'),
      'push',
    ]);
  }

  stop(loaders = []) {
    return Promise.all([
      this.spawn('pm2', ['delete', 'all']),
      this.spawn('docker-compose', [
        '-f',
        path.join(this.outputDir, 'docker-compose.yaml'),
        '-p',
        'isa',
        `down`,
      ]),
    ]);
  }

  run(loaders = []) {
    return Promise.all([
      this.spawn('pm2', [
        'start',
        path.join(this.outputDir, 'ecosystem.config.yaml'),
      ]),
      this.spawn('docker-compose', [
        '-f',
        path.join(this.outputDir, 'docker-compose.yaml'),
        '-p',
        'isa',
        'up',
        '-d',
      ]),
    ]);
  }

  async configure(loaders) {
    const runners = { docker: {}, pm2: [] };

    await fs.ensureDir(path.join(this.outputDir));

    for (const loader of loaders) {
      const processes = await loader(this.outputDir);

      runners['docker'] = { ...runners['docker'], ...processes['docker'] };

      runners['pm2'].push(...processes['pm2']);
    }

    await fs.outputFile(
      path.join(this.outputDir, 'docker-compose.yaml'),
      yaml.dump(
        {
          version: '3.7',
          services: runners['docker'],
          networks: { 'isa-net': { driver: 'bridge' } },
        },
        { lineWidth: 5000 }
      )
    );

    await fs.outputFile(
      path.join(this.outputDir, 'ecosystem.config.yaml'),
      yaml.dump({ apps: runners['pm2'] }, { lineWidth: 5000 })
    );
  }

  async clean(loaders = []) {
    await fs.remove(path.join(this.outputDir));
  }
}

module.exports = Maker;
